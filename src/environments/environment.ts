// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
    production: false,
    API_KEY: '',
    API_URL: {
        /*default1: 'http://192.168.0.102:',
        default2: 'http://196.6.104.10:9086/bvn/',
        default3: 'http://192.168.8.102:9087/',
        default5: 'http://196.6.103.30:9086/bvn/',
        // default4: 'http://172.16.16.107:',
        default4: 'http://192.168.0.104:',
        // default4: 'http://172.16.16.151:',
        // default4: 'http://172.16.16.146:',
        auth: '9090/cmms-auth',
        bank: '9091/cmms-bank',
        nibss: '9095/cmms-nibss',
        biller: '9093/cmms-biller',
        pssp: '9097/cmms-pssp',
        mandate: '9094/cmms-mandate',
        country: 'country',
        state: 'state',
        user: 'user',
        role: 'role',
        audit: 'audit',
        webaudit: 'webaudit'*/
        default1: 'http://197.255.63.146:9091/',
        default2: 'http://197.255.63.146:9091/',
        default3: 'http://197.255.63.146:9091/',
        default5: 'http://197.255.63.146:9091/',
        // default4: 'http://196.6.103.10:9091/',
        default4: 'http://197.255.63.146:9091/',
        auth: 'cmms-auth',
        bank: 'cmms-bank',
        nibss: 'cmms-nibss',
        biller: 'cmms-biller',
        pssp: 'cmms-pssp',
        mandate: 'cmms-mandate',
        country: 'country',
        state: 'state',
        user: 'user',
        role: 'role',
        audit: 'audit',
        webaudit: 'webaudit',
        cmms_emandate: 'cmms-emandate',
    }
};
