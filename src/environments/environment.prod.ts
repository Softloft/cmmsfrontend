export const environment = {
    production: true,
    API_KEY: '',
    API_URL: {
        /*default1: 'http://197.255.63.146:9091/',
        default2: 'http://197.255.63.146:9091/',
        default3: 'http://197.255.63.146:9091/',
        default5: 'http://197.255.63.146:9091/',
        default4: 'http://197.255.63.146:9091/',*/
        default1: 'http://196.6.103.10:9091/',
        default2: 'http://196.6.103.10:9091/',
        default3: 'http://197.255.63.146:9091/',
        default4: 'http://localhost:9091/',
        default5: 'http://196.6.103.10:9091/',
        auth: 'cmms-auth',
        bank: 'cmms-bank',
        nibss: 'cmms-nibss',
        pssp: 'cmms-pssp',
        biller: 'cmms-biller',
        mandate: 'cmms-mandate',
        country: 'country',
        state: 'state',
        user: 'user',
        role: 'role',
        webaudit: 'webaudit',
        cmms_emandate: 'cmms-emandate',
    }
};

