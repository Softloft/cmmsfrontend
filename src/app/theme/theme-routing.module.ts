import { NgModule } from '@angular/core';
import { ThemeComponent } from './theme.component';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from "../auth/_guards/auth.guard";
import { PasswordGuard } from "../auth/_guards/password.guard";

const routes: Routes = [
    {
        "path": "",
        "component": ThemeComponent,
        "canActivate": [AuthGuard],
        "children": [
            {
                "path": "index",
                "canActivate": [PasswordGuard],
                "loadChildren": "./pages/default/index/index.module#IndexModule"
            },
            {
                "path": "users",
                "canActivate": [PasswordGuard],
                "loadChildren": "./pages/default/user/user.module#UserModule"
            },
            {
                "path": "nibss",
                "canActivate": [PasswordGuard],
                "loadChildren": "./pages/default/nibss/nibss.module#NibssModule"
            },
            {
                "path": "bank",
                "canActivate": [PasswordGuard],
                "loadChildren": "./pages/default/bank/bank.module#BankModule"
            },
            {
                "path": "bank-user",
                "canActivate": [PasswordGuard],
                "loadChildren": "./pages/default/bank-user/bank-user.module#BankUserModule"
            },
            {
                "path": "industry",
                "canActivate": [PasswordGuard],
                "loadChildren": "./pages/default/industry/industry.module#IndustryModule"
            },
            {
                "path": "biller",
                "canActivate": [PasswordGuard],
                "loadChildren": "./pages/default/biller/biller.module#BillerModule"
            },
            {
                "path": "biller-user",
                "canActivate": [PasswordGuard],
                "loadChildren": "./pages/default/biller-user/biller-user.module#BillerUserModule"
            },
            {
                "path": "pssp",
                "canActivate": [PasswordGuard],
                "loadChildren": "./pages/default/pssp/pssp.module#PsspModule"
            },
            {
                "path": "pssp-user",
                "canActivate": [PasswordGuard],
                "loadChildren": "./pages/default/pssp-user/pssp-user.module#PsspUserModule"
            },
            {
                "path": "pssp",
                "canActivate": [PasswordGuard],
                "loadChildren": "./pages/default/pssp/pssp.module#PsspModule"
            },
            {
                "path": "mandate",
                "canActivate": [PasswordGuard],
                "loadChildren": "./pages/default/mandate/mandate.module#MandateModule"
            },
            {
                "path": "inner",
                "canActivate": [PasswordGuard],
                "loadChildren": ".\/pages\/default\/inner\/inner.module#InnerModule"
            },
            {
                "path": "profile",
                "loadChildren": ".\/pages\/default\/profile\/profile.module#ProfileModule"
            },
            {
                "path": "create-product",
                "canActivate": [PasswordGuard],
                "loadChildren": "./pages/default/create-product/create-product.module#CreateProductModule"
            },
            {
                "path": "products",
                "canActivate": [PasswordGuard],
                "loadChildren": "./pages/default/products/products.module#ProductsModule"
            },
            {
                "path": "fees",
                "canActivate": [PasswordGuard],
                "loadChildren": "./pages/default/setup-fees/setup-fees.module#SetupFeesModule"
            },
            {
                "path": "beneficiary",
                "canActivate": [PasswordGuard],
                "loadChildren": "./pages/default/beneficiary/beneficiary.module#BeneficiaryModule"
            },
            {
                "path": "audit",
                "canActivate": [PasswordGuard],
                "loadChildren": "./pages/default/audit/audit.module#AuditModule"
            },
            {
                "path": "settings",
                "canActivate": [PasswordGuard],
                "loadChildren": "./pages/default/settings/settings.module#SettingsModule"
            },
            {
                "path": "404",
                "loadChildren": ".\/pages\/default\/not-found\/not-found.module#NotFoundModule"
            },
            {
                "path": "",
                "redirectTo": "index",
                "pathMatch": "full"
            }
        ]
    },
    {
        "path": "**",
        "redirectTo": "404",
        "pathMatch": "full"
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ThemeRoutingModule { }