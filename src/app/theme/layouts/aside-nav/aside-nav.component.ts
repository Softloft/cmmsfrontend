import { AfterViewInit, Component, ElementRef, OnInit, ViewChild, ViewEncapsulation } from "@angular/core";
import Scrollbar from "smooth-scrollbar";
import { UserService } from "../../../../services/user.service";
import { Cache } from "../../../../utils/cache";
import { Router } from "@angular/router";

declare let mLayout: any;
@Component({
    selector: "app-aside-nav",
    templateUrl: "./aside-nav.component.html",
    styleUrls: ['./aside-nav.component.css'],
    encapsulation: ViewEncapsulation.None,
})
export class AsideNavComponent implements OnInit, AfterViewInit {
    public user = '';
    public roles = '';
    public details = [];
    public booleans = {
        nibssuser: false,
        billeruser: false,
        bankuserstatus: false,
        mandateinitiator: false,
        mandateauthorizer: false,
        billerstatus: false,
        configurestatus: false,
    };

    constructor(public userservice: UserService,
        private router: Router) {

    }

    ngOnInit() {
        this.user = this.userservice.getAuthUser();
        this.roles = this.user['roles'][0];
        this.details = Cache.get('userDetails');

        if (this.roles === 'NIBSS SUPER ADMIN INITIATOR'
            || this.roles === 'NIBSS INITIATOR'
            || this.roles === 'NIBSS AUTHORIZER'
            || this.roles === 'NIBSS SUPER ADMIN AUTHORIZER') {
            this.booleans.nibssuser = true;
        }

        if (this.roles === 'NIBSS AUTHORIZER'
            || this.roles === 'NIBSS INITIATOR'
            || this.roles === 'NIBSS ADMIN'
            || this.roles === 'NIBSS SUPER ADMIN INITIATOR'
            || this.roles === 'NIBSS SUPER ADMIN AUTHORIZER'
            || this.roles === 'BANK ADMIN AUTHORIZER'
            || this.roles === 'BANK ADMIN INITIATOR'
            || this.roles === 'BILLER ADMIN INITIATOR'
            || this.roles === 'BILLER ADMIN AUTHORIZER') {
            this.booleans.configurestatus = true;
        }

        if (this.roles === 'NIBSS AUTHORIZER'
            || this.roles === 'NIBSS ADMIN'
            || this.roles === 'NIBSS SUPER ADMIN INITIATOR'
            || this.roles === 'NIBSS SUPER ADMIN AUTHORIZER'
            || this.roles === 'BANK AUTHORIZER'
            || this.roles === 'BANK BILLER AUTHORIZER'
            || this.roles === 'BANK BIILER AUTHORIZER'
            || this.roles === 'BILLER ADMIN AUTHORIZER'
            || this.roles === 'BILLER AUTHORIZER') {
            this.booleans.mandateauthorizer = true;
        }

        if (this.roles === 'NIBSS INITIATOR'
            || this.roles === 'NIBSS ADMIN'
            || this.roles === 'NIBSS SUPER ADMIN INITIATOR'
            || this.roles === 'NIBSS SUPER ADMIN AUTHORIZER'
            || this.roles === 'BANK INITIATOR'
            || this.roles === 'BANK BILLER INITIATOR'
            || this.roles === 'BANK BIILER INITIATOR'
            || this.roles === 'BILLER ADMIN INITIATOR'
            || this.roles === 'BILLER INITIATOR') {
            this.booleans.mandateinitiator = true;
        }

        if (this.roles === 'NIBSS AUTHORIZER'
            || this.roles === 'NIBSS ADMIN'
            || this.roles === 'NIBSS SUPER ADMIN INITIATOR'
            || this.roles === 'NIBSS SUPER ADMIN AUTHORIZER'
            || this.roles === 'NIBSS INITIATOR'
            || this.roles === 'BANK ADMIN AUTHORIZER'
            || this.roles === 'BANK ADMIN INITIATOR') {
            this.booleans.billerstatus = true;
        }

        if (this.roles === 'BANK ADMIN AUTHORIZER'
            || this.roles === 'BANK ADMIN INITIATOR') {
            this.booleans.bankuserstatus = true;
        }


    }

    ngAfterViewInit() {
        Scrollbar.init(<HTMLScriptElement>document.querySelector('#m_ver_menu'));
        mLayout.initAside();

    }

}