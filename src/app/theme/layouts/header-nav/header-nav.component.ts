import {AfterViewInit, Component, OnInit, ViewEncapsulation} from "@angular/core";
import {UserService} from "../../../../services/user.service";
import {Cache} from "../../../../utils/cache";
import {NotificationService} from "../../../../services/notification.service";
import {EmandateService} from "../../../../services/api-service/emandate.service";
import {Router} from "@angular/router";

declare let mLayout: any;
@Component({
    selector: "app-header-nav",
    templateUrl: "./header-nav.component.html",
    encapsulation: ViewEncapsulation.None,
})
export class HeaderNavComponent implements OnInit, AfterViewInit {
    user = '';
    details = '';
    loader = false;

    constructor(private router: Router,
                public userservice: UserService,
                private notification: NotificationService,
                private emandateService: EmandateService) {

    }

    ngOnInit() {
        this.user = this.userservice.getAuthUser()['userType'];
        if (!this.user) {
            this.userservice.logout();
        }
        this.details = Cache.get('userDetails');
        console.log('user :: ', this.details)
        window.setInterval(() => {
            if (!navigator.onLine) {
                this.notification.error('Network disconnected, please connect back');
            }
        }, 5000);
    }

    ngAfterViewInit() {

        mLayout.initHeader();

    }

    logout() {
        this.userservice.logout();
    }

    onResetEmandatePassword() {
        this.loader = true;
        this.emandateService.generateCode()
            .subscribe((response) => {
                this.loader = false;
                this.emandateService.resetEmandate = true;
                this.notification.success(response.message)
                setTimeout(() => {
                    this.router.navigate(['/profile'])
                }, 1000)
            }, error => {
                this.loader = false;
                this.notification.error('Unable to process this action', error)
            })
    }

}