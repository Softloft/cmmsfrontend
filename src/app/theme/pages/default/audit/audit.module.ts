import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { RouterModule, Routes } from "@angular/router";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { NgxPaginationModule } from "ngx-pagination";
import { BootstrapSwitchModule } from "angular2-bootstrap-switch";
import { LayoutModule } from "../../../layouts/layout.module";
import { AngularMultiSelectModule } from "angular2-multiselect-dropdown";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { AuditComponent } from "./audit.component";
import { DefaultComponent } from "../default.component";


const RouterRoute: Routes = [
    {
        "path": "",
        "component": DefaultComponent,
        "children": [
            { path: '', component: AuditComponent },
        ]
    }
];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(RouterRoute),
        FormsModule,
        ReactiveFormsModule,
        NgxPaginationModule,
        BootstrapSwitchModule,
        LayoutModule,
        AngularMultiSelectModule,
        NgbModule,
    ],
    declarations: [AuditComponent]
})
export class AuditModule {
}
