import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { AuditService } from "../../../../../services/api-service/audit.service";
import { NotificationService } from "../../../../../services/notification.service";
import { Cache } from "../../../../../utils/cache";
import { UserService } from "../../../../../services/user.service";

import Swal from "sweetalert2";
import { JSON2PDFTABLE } from "../../../../../shared/export/json2pdf-table";
import { JSON2CSV } from "../../../../../shared/export/json2csv";
declare const $;
@Component({
    selector: 'app-audit',
    templateUrl: 'audit.component.html',
    styles: []
})
export class AuditComponent implements OnInit {
    public urlId = 0;
    searchOption = 'Search Type';
    public current: number = 1;
    public dateRangeSearchForm: FormGroup;
    public profile: any;
    public newObj = [];
    public updateObj = [];
    public oldObj = [];
    public p = 1;
    public totalElement = 0;
    public parentRoute = '';
    public pagesize = 10;
    public id = 0;
    public fromDate: any;
    public toDate: any;
    public moduleName = 'Audit Report';
    public booleans = {
        loader: false,
        loadingStatus: false,
        loadsearch: false,
    };

    public list = {
        allContent: [],
        roles: [],
        data: [],
        details: [],
        newObj: [],
        oldObj: [],
        updatedObj: [],
    };

    static searchDateRangeForm = () => {
        return {
            params: ['', Validators.compose([])],
            from: ['', Validators.compose([])],
            to: ['', Validators.compose([])],
            dataSize: ['', Validators.compose([])],
            className: ['', Validators.compose([])],
        }
    };
    constructor(private notification: NotificationService,
        private fb: FormBuilder,
        private router: Router,
        private auditService: AuditService,
        private route: ActivatedRoute) {
        this.dateRangeSearchForm = this.fb.group(AuditComponent.searchDateRangeForm());
    }

    ngOnInit() {
        this.parentRoute = `For ${Cache.get('audit')}`;
        this.urlId = +this.route.snapshot.paramMap.get('id');
        this.loadFunction();
        this.profile = Cache.get('credentials');
    }

    onNavBack() {
        let previousUrl = Cache.get('previousUrl');
        this.router.navigate([previousUrl]);
    }


    loadFunction() {
        this.onGetAllAudit(this.p - 1);
    }


    getPage(event) {
        this.p = event;
        this.list.allContent = [];
        this.onGetAllAudit(this.p - 1);
    }

    onGetAllAudit(pageNo) {
        this.booleans.loader = true;
        this.fromDate = null;
        this.toDate = null;
        this.auditService.getWebAudit(pageNo)
            .subscribe((response) => {
                this.booleans.loader = false;
                this.totalElement = response.totalElements;
                this.list.allContent = response.content;
            },
            error => {
                this.booleans.loader = false;
                this.notification.error('Unable to load ' + this.moduleName, error);
            })
    }


    getWebAuditById(id) {
        this.booleans.loader = true;
        this.auditService.getWebAuditById(id)
            .subscribe((response) => {
                this.updateObj = [];
                this.booleans.loader = false;
                this.list.details = response;
                this.list.newObj = JSON.parse(response.newObject);
                this.list.oldObj = JSON.parse(response.oldObject);
                if (this.list.oldObj) {
                    this.list.updatedObj = this.list.oldObj;
                } else if (this.list.newObj) {
                    this.list.updatedObj = this.list.newObj;
                }


                for (var x in this.list.updatedObj) {
                    let newvalue: string;
                    let oldvalue: string;
                    let updatevalue: string;
                    if (x === 'password') {
                        if (this.list.newObj[x] === this.list.oldObj[x]) {
                            this.updateObj.push({ index: x, newvalue: 'Password was not changed', oldvalue: '' })
                        } else {
                            this.updateObj.push({ index: x, newvalue: 'Password was changed', oldvalue: '' })
                        }
                    }
                    if (x.search(/password/i) !== -1)
                        continue;
                    if (this.list.newObj) {
                        newvalue = this.list.newObj[x];
                        updatevalue = this.list.newObj[x];
                    }
                    if (this.list.oldObj) {
                        oldvalue = this.list.oldObj[x];
                        updatevalue = this.list.oldObj[x];
                    }
                    if (updatevalue.length > 50) {
                        if (newvalue) {
                            newvalue = newvalue.substring(0, 50) + " ....";
                        }
                        if (oldvalue) {
                            oldvalue = oldvalue.substring(0, 50) + " ....";
                        }
                        this.updateObj.push({ index: x, newvalue: newvalue, oldvalue: oldvalue })
                    } else {
                        this.updateObj.push({ index: x, newvalue: newvalue, oldvalue: oldvalue })
                    }

                }
                /* for( var x in this.list.oldObj) {
                 let value : string = this.list.oldObj[x];
                 if(value.length > 50) {
                 value = value.substring(0,50) + " ....";
                 this.oldObj.push({index: x, value: value})
                 }else{
                 this.oldObj.push({index: x, value: value})
                 }
                 }*/
                $('#CreateModal').modal('show');
            },
            error => {
                this.booleans.loader = false;
                this.notification.error('Unable to load ' + this.moduleName, error);
            })
    }


    dateRangeSearchbyLog() {
        if ((this.dateRangeSearchForm.value['from'] !== '' && this.dateRangeSearchForm.value['to'] === '')
            || (this.dateRangeSearchForm.value['from'] === '' && this.dateRangeSearchForm.value['to'] !== '')) {
            this.notification.error('Please, enter a value in the parameter');
        } else {
            this.booleans.loadsearch = true;
            this.auditService.getSearchLogWeb(this.dateRangeSearchForm.value, 0)
                .subscribe((response) => {
                    this.fromDate = `${this.dateRangeSearchForm.value['from']['year']}-${this.dateRangeSearchForm.value['from']['month']}-${this.dateRangeSearchForm.value['from']['day']}`;
                    this.toDate = `${this.dateRangeSearchForm.value['to']['year']}-${this.dateRangeSearchForm.value['to']['month']}-${this.dateRangeSearchForm.value['to']['day']}`;
                    this.booleans.loadsearch = false;
                    if (response.content.length === 0) {
                        this.notification.error('No Result Found');
                    }
                    this.list.allContent = response.content;
                    this.totalElement = response.totalElements;
                    this.dateRangeSearchForm.reset();
                },
                error => {
                    this.booleans.loadsearch = false;
                    this.notification.error('Unable Search the log', error);
                })
        }
    }

    onSearchType(searchType) {
        this.searchOption = searchType;
    }

    getPdf(event?) {
        this.exportToPdf(this.list.allContent);
    }

    private exportToPdf(exportContent) {
        const rows = [];
        const columns = ["s/n  ", "User", "Class Name", "Action"];
        exportContent.forEach((value, i) => {
            rows.push([i + 1, value.user, value.className, value.action])
        });
        JSON2PDFTABLE(columns, rows, 'audit_report');
    }

    getCsv(event?) {
        const exportContent = this.list.allContent;
        this.exportToCsv(exportContent);
    }

    private exportToCsv(exportContent) {
        const csvArray = [];
        const fields = ['User', 'Class Name', 'Action'];
        exportContent.forEach((value) => {
            csvArray.push({
                'User': value.user,
                'Class Name': value.className,
                'Action': value.action
            })
        });
        JSON2CSV(fields, csvArray, 'audit_report');
    }
}
