import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductsComponent } from './products.component';
import { DefaultComponent } from "../default.component";
import { LayoutModule } from "../../../layouts/layout.module";
import { RouterModule, Routes } from "@angular/router";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { NgxPaginationModule } from "ngx-pagination";
import { BootstrapSwitchModule } from "angular2-bootstrap-switch";
import { AngularMultiSelectModule } from "angular2-multiselect-dropdown";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { PipeModule } from "../../../../../shared/modules/pipe.module";

const routes: Routes = [
    {
        "path": "",
        "component": DefaultComponent,
        "children": [
            {
                "path": "",
                "component": ProductsComponent
            },
            {
                "path": ":id",
                "component": ProductsComponent
            },
            {
                "path": ":id/:status",
                "component": ProductsComponent
            }
        ]
    }
];
@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(routes),
        LayoutModule,
        FormsModule,
        PipeModule,
        NgbModule,
        AngularMultiSelectModule,
        ReactiveFormsModule,
        NgxPaginationModule,
        BootstrapSwitchModule
    ],
    declarations: [ProductsComponent]
})
export class ProductsModule { }
