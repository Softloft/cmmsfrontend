import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ScriptLoaderService } from "../../../../_services/script-loader.service";
import { BillerService } from "../../../../../services/api-service/biller.service";
import { NotificationService } from "../../../../../services/notification.service";
import { Cache } from "../../../../../utils/cache";
import { UserService } from "../../../../../services/user.service";
import { ActivatedRoute, Router } from "@angular/router";
import { ApprovalService } from "../../../../../services/api-service/approval.service";

import Swal from "sweetalert2";
import { APPROVAL_MESSAGE_USER, CONSTANTS } from "../../../../../utils/constants";
import { JSON2CSV } from "../../../../../shared/export/json2csv";
import { JSON2PDFTABLE } from "../../../../../shared/export/json2pdf-table";

declare const $;

@Component({
    selector: 'app-products',
    templateUrl: './products.component.html',
    styles: []
})
export class ProductsComponent implements OnInit {
    createForm: FormGroup;
    public formReason: FormGroup;
    public createFormSearch: FormGroup;
    totalElement = 0;
    public p = 1;
    public id = 0;
    public user = [];
    public billerName = '';
    public paramId = '';
    public details = [];
    public billerId = 0;
    public moduleName = 'Products';
    public modal_title = 'New';
    public submit_type = '';
    public itemListBiller = [];
    public pagesize = 10;
    public pagesizeArray = CONSTANTS.ITEMS_PER_PAGE_ARRAY;
    /*******start of Approval(maker/checker) declaration*******/
    public approvalStatus = '';
    public approvalType = '';
    public statusType = '';
    public approvalName = 'products';
    public authorization = '';
    public approvalName2 = 'product';
    public approvedStatus = '';
    /*******end of Approval(maker/checker) declaration*******/
    public list = {
        allContent: [],
        details: []
    };
    public booleans = {
        loadbiller: false,
        loader: false,
        submitLoader: false,
        authorizer: false,
        initiator: false,
        loadAction: false,
        loadingSearch: false,

    };

    public settings = {};
    public selectedItems = [];
    public itemList = [];

    private searchformdata = function() {
        return {
            productName: ['', Validators.compose([])],
            activated: ['', Validators.compose([])],
            billerName: ['', Validators.compose([])],
            description: ['', Validators.compose([])]
        }
    };

    private formdata = function() {
        return {
            name: ['', Validators.compose([Validators.required])],
            amount: ['', Validators.compose([Validators.required])],
            description: ['', Validators.compose([Validators.required])]
        }
    };

    private formdataReason = function() {
        return {
            reason: ['', Validators.compose([Validators.required])],
        }
    };

    constructor(private _script: ScriptLoaderService,
        private billerService: BillerService,
        private userservice: UserService,
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private approvalservice: ApprovalService,
        private notification: NotificationService,
        private fb: FormBuilder) {
        this.createForm = this.fb.group(this.formdata());
        this.createFormSearch = this.fb.group(this.searchformdata());
        this.formReason = this.fb.group(this.formdataReason());
    }

    ngOnInit() {
        this.user = Cache.get('userDetails');
        this.paramId = this.activatedRoute.snapshot.paramMap.get('id');
        if (this.activatedRoute.snapshot.paramMap.get('id') && (this.paramId !== 'pending' && this.paramId !== 'approved' && this.paramId !== 'disapproved')) {
            this.billerId = +this.activatedRoute.snapshot.paramMap.get('id');
            this.getBillerById(this.billerId);
        } else {
            if(this.user['userBank'] && this.user['userBank']['id']){
                this.billerId = this.user['userBank']['id'];
            } else if(this.user['biller'] && this.user['biller']['id']){
                this.billerId = +this.user['biller']['id'];
            }
            this.getBiller();
        }
        const status = this.activatedRoute.snapshot.paramMap.get('status');
        if ((status && status !== '') || this.paramId === 'pending' || this.paramId === 'approved' || this.paramId === 'disapproved') {
            this.getParam();
        } else {
            this.getProduct(0, this.pagesize);
        }

        this.settings = {
            singleSelection: true,
            text: "Select Biller",
            searchPlaceholderText: 'Search Fields',
            enableSearchFilter: true,
            badgeShowLimit: 5
        };

        this.details = this.userservice.getAuthUser();
        if (this.details['roles'][0] === 'BILLER ADMIN AUTHORIZER' || this.details['roles'][0] === 'BANK ADMIN AUTHORIZER') {
            this.booleans.authorizer = true;
        }
        if (this.details['roles'][0] === 'BILLER ADMIN INITIATOR' || this.details['roles'][0] === 'BANK ADMIN INITIATOR') {
            this.booleans.initiator = true;
        }
    }

    getMainPageByBiller() {
        this.approvalStatus = '';
        this.router.navigate([this.approvalName]);
    }

    onChangePageSize(event) {
        this.pagesize = event.target.value;
        this.getProduct(this.p - 1, this.pagesize);
    }

    getMainPage() {
        this.approvalStatus = '';
        this.router.navigate([this.approvalName + '/' + this.billerId]);
    }

    getProduct(pageNo, pagesize) {
        this.booleans.loader = true;
        this.billerService.getAllProduct(this.billerId, pageNo, pagesize)
            .subscribe(response => {
                this.booleans.loader = false;
                console.log('all products :: ', response);
                this.list.allContent = response.content;
                this.totalElement = response.totalElements;
            },
            error => {
                this.booleans.loader = false;
                this.notification.error('Unable to load Product', error);
            })
    }

    create() {
        this.booleans.submitLoader = true;
        //    this.extractIds();
        this.billerService.createProduct(this.createForm.value)
            .subscribe(response => {
                this.booleans.submitLoader = false;
                this.createForm.reset();
                $('#CreateModal').modal('hide');
                this.list.allContent.unshift(response);
            }, error => {
                this.booleans.submitLoader = false;
                this.notification.error('Unable to create product', error)
            }
            )
    }

    toggle(data) {
        this.id = data.id;
        let statusMessage = '';
        statusMessage = (data.activated) ? 'Disapprove' : 'Approve';
        Swal({
            title: '',
            html: '<b style="font-weight: bold">' + APPROVAL_MESSAGE_USER(statusMessage) + '</b>',
            type: null,
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes!'
        }).then((result) => {
            if (result.value) {
                this.billerService.toggleProduct({ id: data.id })
                    .subscribe(response => {
                        console.log('response :: ', response);
                        this.list.allContent.forEach((value, i) => {
                            if (value.id === this.id) {
                                this.list.allContent[i] = value;
                            }
                        });
                        this.notification.success(CONSTANTS.TOGGLE_RESPONSE);
                    },
                    error => {
                        this.notification.error(CONSTANTS.ERROR_RESPONSE, error);
                    });
            }
        })
    }

    view(data) {
        this.list.details = data;
        $('#ViewModal').modal('show');
    }

    edit(data) {
        this.submit_type = 'Update';
        this.list.details = data;
        this.id = data.id;
        this.submit_type = 'Update';
        this.modal_title = 'Update';

        const formdata = function() {
            return {
                name: [data.name, Validators.compose([Validators.required])],
                amount: [data.amount, Validators.compose([Validators.required])],
                description: [data.description, Validators.compose([Validators.required])],
                id: [data.id, Validators.compose([])],
            }
        };
        this.createForm = this.fb.group(formdata());
        $('#CreateModal').modal('show');
    }

    update() {
        //  this.extractIds();
        this.booleans.loader = true;
        this.billerService.updateProduct(this.createForm.value)
            .subscribe(response => {
                this.list.allContent.forEach((value, i) => {
                    if (value.id === this.id) {
                        this.list.allContent[i] = response;
                    }
                });
                $('#CreateModal').modal('hide');
                this.createForm.reset();
                this.booleans.loader = false;
                if (this.approvalStatus !== '') {
                    this.list.allContent.forEach((value, i) => {
                        if (this.id === value['id']) {
                            this.list.allContent.splice(i, 1);
                        }
                    });
                }
                this.notification.success(CONSTANTS.UPDATE_RESPONSE);
            },
            error => {
                this.booleans.loader = false;
                this.notification.error(CONSTANTS.ERROR_RESPONSE, error);
            });
    }

    private extractIds() {
        if (this.createForm.value['billerId'][0]) {
            this.createForm.value['billerId'] = this.createForm.value['billerId'][0]['id'];
        }
    }

    getBillerById(id) {
        this.booleans.loadbiller = true;
        this.billerService.getBillerById(id)
            .subscribe((response) => {
                this.billerName = response.name;
            }, error => {
                this.notification.error('Unable to load biller', error);
            })
    }

    getSearchModal() {
        $('#searchModal').modal('show');
    }

    getBiller() {
        this.booleans.loadbiller = true;
        this.billerService.getAllBillersList()
            .subscribe((response) => {
                this.booleans.loadbiller = false;
                response.forEach(value => {
                    this.itemListBiller.push({
                        id: value.id,
                        itemName: value.name
                    })
                })
            }, error => {
                this.booleans.loadbiller = false;
                this.notification.error(CONSTANTS.ERROR_RESPONSE, error);
            })
    }

    getPage(event) {
        this.p = +event;
        this.list.allContent = [];
        this.getProduct(this.p - 1, this.pagesize);
    }

    openModal() {
        this.modal_title = 'New';
        this.submit_type = 'Submit';
        $('#CreateModal').modal('show');
    }

    searchMethod(pageNo) {
        if(this.createFormSearch.value['productName'] === '' && this.createFormSearch.value['billerName'] === '' && this.createFormSearch.value['activated'] === '') {
            return this.notification.error('Please, suppy item to search')
        }
        this.booleans.loadingSearch = true;
        this.billerService.productSearch(this.createFormSearch.value, pageNo)
            .subscribe((response) => {
                    this.booleans.loader = false;
                    this.booleans.loadingSearch = false;
                    if (response.content.length === 0) {
                        this.notification.error('No Result Found')
                    } else {
                        this.list.allContent = response.content;
                        $('#searchModal').modal('hide');
                    }
                },
                error => {
                    this.booleans.loader = false;
                    this.booleans.loadingSearch = false;
                    this.notification.error('Unable to Search', error);
                })
    }
    /***********************************APPROVAL(MAKER AND CHECKER)*****************************************/
    public getParam(param?) {
        this.list.allContent = [];
        if (!param && ((this.user['biller'] && this.user['biller']['id']) || (this.user['userBank'] && this.user['userBank']['id']))) {
            const status = this.activatedRoute.snapshot.paramMap.get('id');
            let paramUrl = '';
            switch (status) {
                case 'pending':
                    paramUrl = this.approvalName + '/pending';
                    this.statusType = 'UNAUTHORIZED';
                    this.approvalStatus = 'Pending';
                    break;
                case 'approved':
                    paramUrl = this.approvalName + '/approved';
                    this.statusType = 'AUTHORIZED';
                    this.approvalStatus = 'Approved';
                    break;
                case 'disapproved':
                    paramUrl = this.approvalName + '/disapproved';
                    this.statusType = 'REJECTED';
                    this.approvalStatus = 'Disapproved';
                    break;
            }
            this.router.navigate(['/' + paramUrl]);
        } else if (param) {
            switch (param) {
                case this.approvalName + '/pending':
                    this.statusType = 'UNAUTHORIZED';
                    this.approvalStatus = 'Pending';
                    break;
                case this.approvalName + '/approved':
                    this.statusType = 'AUTHORIZED';
                    this.approvalStatus = 'Approved';
                    break;
                case this.approvalName + '/disapproved':
                    this.statusType = 'REJECTED';
                    this.approvalStatus = 'Disapproved';
                    break;
            }
            this.router.navigate(['/' + param]);
        } /*else {
            const status = this.activatedRoute.snapshot.paramMap.get('status');
            switch (status) {
                case 'pending':
                    this.statusType = 'UNAUTHORIZED';
                    this.approvalStatus = 'Pending';
                    break;
                case 'approved':
                    this.statusType = 'AUTHORIZED';
                    this.approvalStatus = 'Approved';
                    break;
                case 'disapproved':
                    this.statusType = 'REJECTED';
                    this.approvalStatus = 'Disapproved';
                    break;
            }
        }*/
        this.getAuthorization(this.statusType, 0);
    }

    getApprovalPage(event) {
        this.p = +event;
        this.list.allContent = [];
        this.getAuthorization(this.statusType, this.p - 1);
    }


    getAuthorization(approval_type, pageNo) {
        this.booleans.loader = true;
        this.approvalservice.getAuthorization(approval_type, pageNo, this.approvalName2, 'not_user')
            .subscribe(response => {
                this.booleans.loader = false;
                this.list.allContent = response.content;
                this.totalElement = response.totalElements;
            },
            error => {
                this.booleans.loader = false;
                this.notification.error('Unable to load content', error);
            })
    }

    getReason(data, authorizationStatus) {
        this.id = data.id;
        this.authorization = authorizationStatus;
        this.list.details = data;
        this.approvalType = 'disapprove';
        $('#reasonModal').modal('show');
    }

    disapprove() {
        let action = '';
        this.approvedStatus = 'reject';
        this.booleans.loadAction = true;
        switch (this.authorization) {
            case 'UNAUTHORIZED_CREATE':
                action = 'REJECT_CREATE';
                break;
            case 'UNAUTHORIZED_UPDATE':
                action = 'REJECT_UPDATE'
                break;
            case 'UNAUTHORIZED_TOGGLE':
                action = 'REJECT_TOGGLE'
                break;
        }

        this.Approve(this.id, action, this.formReason.value['reason']);
    }

    approve(id, authorizationStatus) {
        this.id = id;
        this.approvedStatus = '';
        let action = '';
        switch (authorizationStatus) {
            case 'UNAUTHORIZED_CREATE':
                action = 'APPROVE_CREATE';
                break;
            case 'UNAUTHORIZED_UPDATE':
                action = 'APPROVE_UPDATE'
                break;
            case 'UNAUTHORIZED_TOGGLE':
                action = 'APPROVE_TOGGLE'
                break;
        }
        this.approvalType = 'approve';
        this.Approve(this.id, action);
    }

    Approve(id, authorizationStatus, reason?) {
        this.booleans.loadAction = true;
        this.approvalservice.approve({
            id: id,
            reason: (reason && reason !== '') ? reason : ''
        }, authorizationStatus, this.approvalName2)
            .subscribe(response => {
                this.booleans.loadAction = false;
                this.list.allContent.forEach((value, i) => {
                    if (id === value['id']) {
                        this.list.allContent.splice(i, 1);
                    }
                });
                this.formReason.reset();
                const status = (this.approvedStatus === 'reject') ? 'Disapproval' : 'Approval';
                this.notification.success(status + ' was successful');
            },
            error => {
                this.booleans.loadAction = false;
                this.notification.error(CONSTANTS.ERROR_RESPONSE, error);
            })
    }

    getPdf(event?) {
        this.exportToPdf(this.list.allContent);
    }

    private exportToPdf(exportContent) {
        const rows = [];
        const columns = ["s/n  ", "Name", "Amount", "Biller", "Date Created"];
        exportContent.forEach((value, i) => {
            rows.push([i + 1, value.name, value.amount, value.biller['name'], new Date(value.createdAt).toString().split('GMT')[0]])
        });
        JSON2PDFTABLE(columns, rows, 'audit_report');
    }

    getCsv(event?) {
        const exportContent = this.list.allContent;
        this.exportToCsv(exportContent);
    }

    private exportToCsv(exportContent) {
        const csvArray = [];
        const fields = ['Name', 'Amount', 'Biller'];
        exportContent.forEach((value) => {
            csvArray.push({
                'Name': value.name,
                'Amount': value.amount,
                'Biller': value.biller['name'],
                'Date Created': new Date(value.createdAt).toString().split('GMT')[0]
            })
        });
        JSON2CSV(fields, csvArray, 'audit_report');
    }
}