import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BankComponent } from './bank.component';
import { BootstrapSwitchModule } from "angular2-bootstrap-switch";
import { NgxPaginationModule } from "ngx-pagination";
import { AngularMultiSelectModule } from "angular2-multiselect-dropdown";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule, Routes } from "@angular/router";
import { LayoutModule } from "../../../layouts/layout.module";
import { DefaultComponent } from "../default.component";
import { PipeModule } from "../../../../../shared/modules/pipe.module";
import { SharedModule } from "../../../../../shared/modules/shared.module";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";

const routes: Routes = [
    {
        "path": "",
        "component": DefaultComponent,
        "children": [
            {
                "path": "",
                "component": BankComponent
            },
            {
                "path": ":status",
                "component": BankComponent
            }
        ]
    }
];

@NgModule({
    imports: [
        CommonModule,
        LayoutModule,
        FormsModule,
        SharedModule,
        PipeModule,
        NgbModule,
        RouterModule.forChild(routes),
        ReactiveFormsModule,
        AngularMultiSelectModule,
        NgxPaginationModule,
        BootstrapSwitchModule
    ],
    declarations: [BankComponent]
})
export class BankModule { }
