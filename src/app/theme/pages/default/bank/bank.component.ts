import { Component, OnInit, ViewChild } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ScriptLoaderService } from "../../../../_services/script-loader.service";
import { FormService } from "../../../../../services/api-service/formService";
import { NotificationService } from "../../../../../services/notification.service";
import { APPROVAL_MESSAGE_USER, CONSTANTS } from "../../../../../utils/constants";

import { DomSanitizer } from "@angular/platform-browser";
import { JSON2PDFTABLE } from "../../../../../shared/export/json2pdf-table";
import { JSON2CSV } from "../../../../../shared/export/json2csv";
import { UserService } from "../../../../../services/user.service";
import { BankService } from "../../../../../services/api-service/bank.service";
import { ApprovalService } from "../../../../../services/api-service/approval.service";
import { ActivatedRoute, Router } from "@angular/router";
import Swal from "sweetalert2";
import { SetupEmandateComponent } from "../../../../../shared/components/setup-emandate/setup-emandate.component";

declare const $;

@Component({
    selector: 'app-bank',
    templateUrl: 'bank.component.html',
    styles: []
})
export class BankComponent implements OnInit {
    @ViewChild(SetupEmandateComponent) emandate: SetupEmandateComponent;
    public itemsPerPage = CONSTANTS.ITEMS_PER_PAGE;
    public moduleName = 'Bank';
    public approvedStatus = '';
    public userType = 'not_user';
    public searchtext = '';
    public user = [];
    public details = [];
    public billerdata = [];
    public totalElement = '';
    public createForm: FormGroup;
    public createFormSearch: FormGroup;
    public formReason: FormGroup;
    public id = 0;
    public p = 1;
    public lga = null;
    public submit_type = 'Submit';
    public modal_title = 'Add';
    public pagesize = 10;
    public pagesizeArray = CONSTANTS.ITEMS_PER_PAGE_ARRAY;
    /*******start of Approval(maker/checker) declaration*******/
    public approvalStatus = '';
    public approvalType = '';
    public statusType = '';
    public approvalName = 'bank';
    public authorization = '';
    /*******end of Approval(maker/checker) declaration*******/
    public list = {
        allContent: [],
        details: [],
        country: [],
        searchContent: []
    };
    searchParams = {
        firstName: '',
        lastName: '',
        middleName: '',
        email: '',
        staffNumber: '',
        phoneNumber: '',
        city: '',
        role: '',
        state: '',
        lga: ''
    };

    public booleans = {
        loader: false,
        searchStatus: false,
        csvsearchloader: false,
        pdfsearchloader: false,
        getAllloader: false,
        loadingSearch: false,
        searchloader: false,
        loadRole: false,
        loadsubmit: false,
        initiator: false,
        authorizer: false,
        loadAction: false,
    };

    private formdataSearch = function() {
        return {
            activated: ['', Validators.compose([])],
            code: ['', Validators.compose([])],
            name: ['', Validators.compose([])],
            nipBankCode: ['', Validators.compose([])],
        }
    };

    private formdata = function() {
        return {
            code: ['', Validators.compose([Validators.required, Validators.pattern(/^\d+$/)])],
            name: ['', Validators.compose([Validators.required])],
            nipBankCode: ['', Validators.compose([Validators.required, Validators.pattern(/^\d+$/)])],
            notificationUrl: ['', Validators.compose([])],
            domainName: ['', Validators.compose([])],
            username: ['', Validators.compose([])],
            id: ['', Validators.compose([])],
        }
    };

    private formdataReason = function() {
        return {
            reason: ['', Validators.compose([Validators.required])],
        }
    };

    constructor(private _script: ScriptLoaderService,
        private fb: FormBuilder,
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private approvalservice: ApprovalService,
        private domsanitizer: DomSanitizer,
        private formservice: FormService,
        private userservice: UserService,
        private bankservice: BankService,
        private notification: NotificationService) {
        this.createForm = this.fb.group(this.formdata());
        this.createFormSearch = this.fb.group(this.formdataSearch());
        this.formReason = this.fb.group(this.formdataReason());
    }

    ngOnInit() {
        // this.user['userType'] === 'NIBSS'
        const status = this.activatedRoute.snapshot.paramMap.get('status');
        if (status && status !== '') {
            this.getParam();
        } else {
            this.getBank(0, this.pagesize);
        }
        this.user = this.userservice.getAuthUser();
        this.list.allContent = [];

        this.details = this.userservice.getAuthUser();
        if (this.details['roles'][0] === 'NIBSS AUTHORIZER'
            || this.details['roles'][0] === 'BANK ADMIN AUTHORIZER'
            || this.details['roles'][0] === 'PSSP ADMIN AUTHORIZER') {
            this.booleans.authorizer = true;
        }
        if (this.details['roles'][0] === 'NIBSS INITIATOR'
            || this.details['roles'][0] === 'BANK ADMIN INITIATOR'
            || this.details['roles'][0] === 'PSSP ADMIN INITIATOR') {
            this.booleans.initiator = true;
        }
    }


    openModal() {
        this.modal_title = 'Add';
        this.submit_type = 'Submit';
        $('#CreateModal').modal('show');
    }

    getMainPage() {
        this.approvalStatus = '';
        this.router.navigate([this.approvalName]);
    }

    onChangePageSize(event) {
        this.pagesize = event.target.value;
        this.getBank(this.p - 1, this.pagesize);
    }

    setupemandate(data) {
        this.billerdata = data;
        this.emandate.setupemandate(data);
    }

    toggle(data) {
        this.id = data.id;
        let statusMessage = '';
        statusMessage = (data.activated) ? 'Disapprove' : 'Approve';
        Swal({
            title: '',
            html: '<b style="font-weight: bold">' + APPROVAL_MESSAGE_USER(statusMessage) + '</b>',
            type: null,
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes!'
        }).then((result) => {
            if (result.value) {
                this.bankservice.toggle({ id: data.id })
                    .subscribe(response => {
                        this.list.allContent.forEach((value, i) => {
                            if (value.id === this.id) {
                                this.list.allContent[i] = response;
                            }
                        });
                        this.notification.success(CONSTANTS.TOGGLE_RESPONSE);
                    },
                    error => {
                        this.notification.error('Unable to perform this action', error);
                    });
            }
        })
    }

    exportSearchMethod(pageNo) {
        let role = '';
        if (this.searchParams.role !== '') {
            role = this.searchParams.role.split(' ').join('_');
        }
        const params = `firstName=${this.searchParams.firstName}&lastName=${this.searchParams.lastName}&middleName=${this.searchParams.middleName}&email=${this.searchParams.email}&phoneNumber=${this.searchParams.phoneNumber}&city=${this.searchParams.city}&role=${role}&state=${this.searchParams.state}&lga=${this.searchParams.lga}`;
        this.booleans.loadingSearch = true;
        this.bankservice.search(params, pageNo)
            .subscribe((response) => {
                this.list.allContent = response.content;
                this.totalElement = response.totalElements;
                if (this.booleans.pdfsearchloader) {
                    this.getPdf()
                } else if (this.booleans.csvsearchloader) {
                    this.getCsv();
                }
                this.booleans.csvsearchloader = false;
                this.booleans.pdfsearchloader = false;
                this.booleans.loadingSearch = false;
            },
            error => {
                this.booleans.csvsearchloader = false;
                this.booleans.pdfsearchloader = false;
                this.booleans.loadingSearch = false;
                this.notification.error('Unable to Search', error);
            })
    }

    searchMethod(pageNo) {
        this.booleans.searchStatus = true;
        this.booleans.loadingSearch = true;
        this.bankservice.banksearch(this.createFormSearch.value, pageNo)
            .subscribe((response) => {
                this.booleans.loader = false;
                this.booleans.loadingSearch = false;
                if (response.content.length === 0) {
                    this.notification.error('No Result Found')
                } else {
                    this.list.allContent = response.content;
                    if (this.booleans.pdfsearchloader || this.booleans.csvsearchloader) {
                        this.exportAs();
                    }
                    $('#searchModal').modal('hide');
                }
                this.booleans.csvsearchloader = false;
                this.booleans.pdfsearchloader = false;
            },
            error => {
                this.booleans.loader = false;
                this.booleans.csvsearchloader = false;
                this.booleans.pdfsearchloader = false;
                this.booleans.loadingSearch = false;
                this.notification.error('Unable to Search', error);
            })
    }

    private exportAs() {
        if (this.booleans.pdfsearchloader) {
            this.getPdf()
        } else if (this.booleans.csvsearchloader) {
            this.getCsv();
        }
    }

    exportCsvSearch() {
        this.booleans.csvsearchloader = true;
        this.searchMethod(0);
    }

    exportPdfSearch() {
        this.booleans.pdfsearchloader = true;
        this.searchMethod(0);
    }

    getCsv(event?) {
        if (event === 'all') {
            this.getAllBank(0, this.totalElement, 'csv');
        } else {
            const exportContent = this.list.allContent;
            this.exportToCsv(exportContent);
        }
    }

    private exportToCsv(exportContent) {
        const csvArray = [];
        const fields = [
            'Name',
            'CBN Code',
            'NIBSS Bank Code',
            'Date Created',
            'Status',
        ];
        exportContent.forEach((value) => {
            csvArray.push({
                'Name': value.name,
                'CBN Code': value.code,
                'NIBSS Bank Code': value.nipBankCode,
                'Date Created': new Date(value.createdAt).toString().split('GMT')[0],
                'Status': (value.activated) ? 'Active' : 'Inactive'
            })
        });
        JSON2CSV(fields, csvArray, 'bank');
    }

    getPdf(event?) {
        if (event === 'all') {
            this.getAllBank(0, this.totalElement, 'pdf');
        } else {
            const exportContent = this.list.allContent;
            this.exportToPdf(exportContent);
        }
    }

    private exportToPdf(exportContent) {
        const rows = [];
        const columns = ["s/n  ", "Name", "CBN Code", "NIBSS Bank Code", "Date Created", "Status"];
        exportContent.forEach((value, i) => {
            rows.push([i + 1, value.name, value.code, value.nipBankCode, new Date(value.createdAt).toString().split('GMT')[0], (value.activated) ? 'Active' : 'Inactive'])
        });
        JSON2PDFTABLE(columns, rows, 'bank');
    }

    submit() {
        this.booleans.loadsubmit = true;
        this.createForm.value['code'] = this.createForm.value['code'].toString();
        this.createForm.value['nipBankCode'] = this.createForm.value['nipBankCode'].toString();
        this.bankservice.postBank(this.createForm.value)
            .subscribe(response => {
                this.list.allContent.unshift(response);
                this.booleans.loadsubmit = false;
                $('#CreateModal').modal('hide');
                this.createForm.reset();
                this.notification.success(CONSTANTS.SUBMIT_RESPONSE);
            },
            error => {
                this.booleans.loadsubmit = false;
                this.notification.error('Unable to perform this action', error);
            });
    }

    view(data) {
        this.list.details = data;
        $('#ViewModal').modal('show');
    }

    edit(data) {
        this.id = data.id;
        this.submit_type = 'Update';
        const formdata = function() {
            return {
                bankPassKey: [data.bankPassKey, Validators.compose([])],
                code: [data.code, Validators.compose([Validators.required, Validators.pattern(/^\d+$/)])],
                name: [data.name, Validators.compose([Validators.required])],
                nipBankCode: [data.nipBankCode, Validators.compose([Validators.required, Validators.pattern(/^\d+$/)])],
                notificationUrl: [data.notificationUrl, Validators.compose([])],
                id: [data.id, Validators.compose([])],
            }
        };
        this.createForm = this.fb.group(formdata());
        $('#CreateModal').modal('show');
    }

    update() {
        this.booleans.loadsubmit = true;
        this.bankservice.updateBank(this.createForm.value)
            .subscribe(response => {
                this.list.allContent.forEach((value, i) => {
                    if (value.id === this.id) {
                        this.list.allContent[i] = response;
                    }
                });
                $('#CreateModal').modal('hide');
                this.createForm.reset();
                this.booleans.loadsubmit = false;
                if (this.approvalStatus !== '') {
                    this.list.allContent.forEach((value, i) => {
                        if (this.id === value['id']) {
                            this.list.allContent.splice(i, 1);
                        }
                    });
                }
                this.notification.success(CONSTANTS.UPDATE_RESPONSE);
            },
            error => {
                this.booleans.loadsubmit = false;
                this.notification.error('Unable to perform this action', error);
            });
    }


    getBank(pageNo, pagesize) {
        this.booleans.searchStatus = false;
        this.booleans.loader = true;
        this.bankservice.getBank(pageNo, pagesize)
            .subscribe(response => {
                this.booleans.loader = false;
                this.list.allContent = response.content;
                this.totalElement = response.totalElements;
            },
            error => {
                this.booleans.loader = false;
                this.notification.error('Unable to load Bank(s)', error);
            })
    }

    getAllBank(pageNo, totalNumber, type) {
        this.booleans.getAllloader = true;
        this.bankservice.getAllBank(pageNo, totalNumber)
            .subscribe(response => {
                this.booleans.getAllloader = false;
                (type === 'csv') ? this.exportToCsv(response.content) : this.exportToPdf((response.content));
            },
            error => {
                this.booleans.getAllloader = false;
                this.notification.error('Unable to load NIBSS Users', error);
            })
    }

    /*search() {
     this.booleans.searchloader = true;
     this.bankservice.getBank('search')
     .subscribe(response => {
     this.booleans.searchloader = false;
     this.list.searchContent = response.content;
     },
     error => {
     this.booleans.searchloader = false;
     this.notification.error('Unable to search NIBSS Users', error);
     })
     }*/

    getPage(event) {
        this.p = event;
        this.list.allContent = [];
        (!this.booleans.searchStatus) ? this.getBank(this.p - 1, this.pagesize) : this.searchMethod(this.p - 1);
    }

    getSearchModal() {
        for (let params in this.searchParams) {
            this.searchParams[params] = '';
        }
        $('#searchModal').modal('show');

    }

    /***********************************APPROVAL(MAKER AND CHECKER)*****************************************/
    public getParam(param?) {
        this.list.allContent = [];
        if (param) {
            switch (param) {
                case this.approvalName + '/pending':
                    this.statusType = 'UNAUTHORIZED';
                    this.approvalStatus = 'Pending';
                    break;
                case this.approvalName + '/approved':
                    this.statusType = 'AUTHORIZED';
                    this.approvalStatus = 'Approved';
                    break;
                case this.approvalName + '/disapproved':
                    this.statusType = 'REJECTED';
                    this.approvalStatus = 'Disapproved';
                    break;
            }
            this.router.navigate(['/' + param]);
        } else {
            const status = this.activatedRoute.snapshot.paramMap.get('status');
            switch (status) {
                case 'pending':
                    this.statusType = 'UNAUTHORIZED';
                    this.approvalStatus = 'Pending';
                    break;
                case 'approved':
                    this.statusType = 'AUTHORIZED';
                    this.approvalStatus = 'Approved';
                    break;
                case 'disapproved':
                    this.statusType = 'REJECTED';
                    this.approvalStatus = 'Disapproved';
                    break;
            }
        }
        this.getAuthorization(this.statusType, 0);
    }

    getApprovalPage(event) {
        this.p = +event;
        this.list.allContent = [];
        this.getAuthorization(this.statusType, this.p - 1);
    }


    getAuthorization(approval_type, pageNo) {
        this.booleans.loader = true;
        this.approvalservice.getAuthorization(approval_type, pageNo, this.approvalName, this.userType)
            .subscribe(response => {
                this.booleans.loader = false;
                this.list.allContent = response.content;
                this.totalElement = response.totalElements;
            },
            error => {
                this.booleans.loader = false;
                this.notification.error('Unable to load content', error);
            })
    }

    getReason(data, authorizationStatus) {
        this.id = data.id;
        this.authorization = authorizationStatus;
        this.list.details = data;
        $('#reasonModal').modal('show');
    }

    disapprove() {
        let action = '';
        this.approvedStatus = 'reject';
        this.booleans.loadAction = true;
        switch (this.authorization) {
            case 'UNAUTHORIZED_CREATE':
                action = 'REJECT_CREATE';
                break;
            case 'UNAUTHORIZED_UPDATE':
                action = 'REJECT_UPDATE'
                break;
            case 'UNAUTHORIZED_TOGGLE':
                action = 'REJECT_TOGGLE'
                break;
        }

        this.Approve(this.id, action, this.formReason.value['reason']);
    }

    approve(id, authorizationStatus) {
        this.id = id;
        let action = '';
        this.approvedStatus = '';
        switch (authorizationStatus) {
            case 'UNAUTHORIZED_CREATE':
                action = 'APPROVE_CREATE';
                break;
            case 'UNAUTHORIZED_UPDATE':
                action = 'APPROVE_UPDATE'
                break;
            case 'UNAUTHORIZED_TOGGLE':
                action = 'APPROVE_TOGGLE'
                break;
        }
        this.approvalType = 'approve';
        this.Approve(id, action);
    }

    Approve(id, authorizationStatus, reason?) {
        this.booleans.loadAction = true;
        this.approvalservice.approve({
            id: id,
            reason: (reason && reason !== '') ? reason : ''
        }, authorizationStatus, this.approvalName, this.userType)
            .subscribe(response => {
                this.booleans.loadAction = false;
                this.list.allContent.forEach((value, i) => {
                    if (id === value['id']) {
                        this.list.allContent.splice(i, 1);
                    }
                });
                this.formReason.reset();
                const status = (this.approvedStatus === 'reject') ? 'Disapproval' : 'Approval';
                this.notification.success(status + ' was successful');
            },
            error => {
                this.booleans.loadAction = false;
                this.notification.error('Unable to perform this action', error);
            })
    }
}