import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PsspComponent } from './pssp.component';
import { DefaultComponent } from "../default.component";
import { RouterModule, Routes } from "@angular/router";
import { LayoutModule } from "../../../layouts/layout.module";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { AngularMultiSelectModule } from "angular2-multiselect-dropdown";
import { NgxPaginationModule } from "ngx-pagination";
import { BootstrapSwitchModule } from "angular2-bootstrap-switch";
import { SharedModule } from "../../../../../shared/modules/shared.module";
import { PipeModule } from "../../../../../shared/modules/pipe.module";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";

const routes: Routes = [
    {
        "path": "",
        "component": DefaultComponent,
        "children": [
            {
                "path": "",
                "component": PsspComponent
            },
            {
                "path": ":status",
                "component": PsspComponent
            }
        ]
    }
];

@NgModule({
    imports: [
        CommonModule,
        LayoutModule,
        FormsModule,
        NgbModule,
        PipeModule,
        SharedModule,
        RouterModule.forChild(routes),
        ReactiveFormsModule,
        AngularMultiSelectModule,
        NgxPaginationModule,
        BootstrapSwitchModule
    ],
    declarations: [PsspComponent]
})
export class PsspModule { }
