import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ScriptLoaderService } from "../../../../_services/script-loader.service";
import { FormService } from "../../../../../services/api-service/formService";
import { NotificationService } from "../../../../../services/notification.service";
import { BankService } from "../../../../../services/api-service/bank.service";
import { JSON2CSV } from "../../../../../shared/export/json2csv";
import { JSON2PDFTABLE } from "../../../../../shared/export/json2pdf-table";
import { PsspService } from "../../../../../services/api-service/pssp.service";
import { IndustryService } from "../../../../../services/api-service/industry.service";
import { Cache } from "../../../../../utils/cache";
import { UserService } from "../../../../../services/user.service";
import { ApprovalService } from "../../../../../services/api-service/approval.service";
import { ActivatedRoute, Router } from "@angular/router";
import { AuthService } from "../../../../../services/api-service/authService";

import Swal from "sweetalert2";
import { APPROVAL_MESSAGE_USER, CONSTANTS } from "../../../../../utils/constants";
declare const $;

@Component({
    selector: 'app-pssp',
    templateUrl: 'pssp.component.html',
    styles: []
})
export class PsspComponent implements OnInit {
    public moduleName = 'PSSP';
    public isInteger = false;
    public errorMessage = '';
    public feeType = '';
    public industry = null;
    public createForm: FormGroup;
    public createFormSearch: FormGroup;
    public createFormBearer: FormGroup;
    public formReason: FormGroup;
    public id = 0;
    public details = [];
    public p = 1;
    public user = [];
    public totalElement = '';
    public lgaId = null;
    public submit_type = 'Submit';
    public modal_title = 'Add';
    public pagesize = 10;
    public pagesizeArray = CONSTANTS.ITEMS_PER_PAGE_ARRAY;
    /*******start of Approval(maker/checker) declaration*******/
    public approvalStatus = '';
    public approvalType = '';
    public statusType = '';
    public approvalName = 'pssp';
    public authorization = '';
    /*******end of Approval(maker/checker) declaration*******/
    public list = {
        allContent: [],
        accountResult: [],
        details: [],
        country: [],
        beneficiaryList: [],
        beneficiaryArray: [],
        searchContent: []
    };

    public booleans = {
        loader: false,
        loadFee: false,
        acountloader: false,
        searchStatus: false,
        formulaError: false,
        loadBeneficiary: false,
        csvsearchloader: false,
        pdfsearchloader: false,
        getAllloader: false,
        loadingSearch: false,
        searchloader: false,
        loadIndustry: false,
        loadsubmit: false,
        loadProduct: false,
        initiator: false,
        authorizer: false,
        loadAction: false,
    };

    public error = {
        percentageError: '',
        accountnoerror: false
    };

    public settingsIndustry = {};
    public selectedIndustry = [];
    public itemListIndustry = [];

    public settingsBank = {};
    public selectedBank = [];
    public itemListBank = [];

    public settings = {};
    public selectedItems = [];
    public itemList = [];

    private formdataSearch = function() {
        return {
            id: ['', Validators.compose([])],
            accountNumber: ['', Validators.compose([Validators.minLength(10), Validators.maxLength(10)])],
            accountName: ['', Validators.compose([Validators.required])],
            activated: ['', Validators.compose([])],
            bankCode: ['', Validators.compose([])],
            bank: ['', Validators.compose([])],
            bvn: ['', Validators.compose([Validators.minLength(11), Validators.maxLength(11)])],
            description: ['', Validators.compose([])],
            // industry: ['', Validators.compose([])],
            psspName: ['', Validators.compose([])],
            // domainName: ['', Validators.compose([])],
            rcNumber: ['', Validators.compose([])],
            // selfPSSP: ['', Validators.compose([])]
        }
    };

    private formdataReason = function() {
        return {
            reason: ['', Validators.compose([Validators.required])],
        }
    };

    private formdata = function() {
        return {
            id: ['', Validators.compose([])],
            accountNumber: ['', Validators.compose([Validators.required, Validators.minLength(10), Validators.maxLength(10), , Validators.pattern(/^\d+$/)])],
            accountName: ['', Validators.compose([Validators.required])],
            bank: ['', Validators.compose([])],
            bankCode: ['', Validators.compose([])],
            bvn: ['', Validators.compose([Validators.required, Validators.minLength(11), Validators.maxLength(11), , Validators.pattern(/^\d+$/)])],
            description: ['', Validators.compose([Validators.required])],
            industry: ['', Validators.compose([Validators.required])],
            psspName: ['', Validators.compose([Validators.required])],
            rcNumber: ['', Validators.compose([Validators.required, Validators.pattern(/^\d+$/)])],
        }
    };

    constructor(private _script: ScriptLoaderService,
        private fb: FormBuilder,
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private authservice: AuthService,
        private approvalservice: ApprovalService,
        private psspService: PsspService,
        private industryService: IndustryService,
        private userservice: UserService,
        private bankservice: BankService,
        private formservice: FormService,
        private notification: NotificationService) {
        this.psspService.shareFeeData(null);
        this.psspService.shareFormularData(null);
        this.createForm = this.fb.group(this.formdata());
        this.createFormSearch = this.fb.group(this.formdataSearch());
        this.formReason = this.fb.group(this.formdataReason());
    }

    ngOnInit() {
        this.details = this.userservice.getAuthUser();
        if (this.details['roles'][0] === 'NIBSS AUTHORIZER'
            || this.details['roles'][0] === 'BANK ADMIN AUTHORIZER') {
            this.booleans.authorizer = true;
        }
        if (this.details['roles'][0] === 'NIBSS INITIATOR'
            || this.details['roles'][0] === 'BANK ADMIN INITIATOR') {
            this.booleans.initiator = true;
        }
        this.user = Cache.get('userDetails');
        if (this.details['userType'] !== 'BANK') {
            this.getActiveBank();
        } else {
            this.createForm.controls['bankCode'].patchValue(this.user['userBank']['code']);
        }
        this.getIndustry();
        const status = this.activatedRoute.snapshot.paramMap.get('status');
        if (status && status !== '') {
            this.getParam();
        } else {
            this.getPSSP(0, this.pagesize);
        }
        this.settingsBank = {
            singleSelection: true,
            text: "Select Bank",
            enableSearchFilter: true,
            badgeShowLimit: 5
        };
        this.settingsIndustry = {
            singleSelection: true,
            text: "Select Category",
            enableSearchFilter: true,
            badgeShowLimit: 5
        };
    }

    getMainPage() {
        this.approvalStatus = '';
        this.router.navigate([this.approvalName]);
    }

    onChangePageSize(event) {
        this.pagesize = event.target.value;
        this.getPSSP(this.p - 1, this.pagesize);
    }

    getActiveBank() {
        this.bankservice.getActiveBank()
            .subscribe(response => {
                response.forEach(value => {
                    this.itemListBank.push({
                        id: value.id,
                        itemName: value.name,
                        code: value.code
                    })
                });
            },
            error => {
                this.notification.error('Unable to load active bank', error);
            })
    }

    getPSSP(pageNo, pagesize) {
        this.approvalStatus = '';
        this.booleans.loader = true;
        this.psspService.getPSSP(pageNo, this.user, pagesize)
            .subscribe(response => {
                this.booleans.loader = false;
                this.list.allContent = response.content;
                this.totalElement = response.totalElements;
            },
            error => {
                this.booleans.loader = false;
                this.notification.error('Unable to load PSSP', error);
            })
    }

    toggle(data) {
        this.id = data.id;
        let statusMessage = '';
        statusMessage = (data.activated) ? 'Disapprove' : 'Approve';
        Swal({
            title: '',
            html: '<b style="font-weight: bold">' + APPROVAL_MESSAGE_USER(statusMessage) + '</b>',
            type: null,
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes!'
        }).then((result) => {
            if (result.value) {
                this.psspService.toggle({ id: data.id })
                    .subscribe(response => {
                        this.list.allContent.forEach((value, i) => {
                            if (value.id === this.id) {
                                this.list.allContent[i] = value;
                            }
                        });
                        this.notification.success(CONSTANTS.TOGGLE_RESPONSE);
                    },
                    error => {
                        this.notification.error(CONSTANTS.ERROR_RESPONSE, error);
                    });
            }
        })
    }

    openModal() {
        this.modal_title = 'Add';
        this.submit_type = 'Submit';
        $('#CreateModal').modal('show');
    }

    getCsv(event?) {
        if (event === 'all') {
            this.getAllPSSP(0, this.totalElement, 'csv');
        } else {
            const exportContent = this.list.allContent;
            this.exportToCsv(exportContent);
        }
    }

    private exportToCsv(exportContent) {
        const csvArray = [];
        const fields = [
            'PSSP Name',
            'RC Number',
            'Account No.',
            'Bank',
            'Category',
            'Date Created',
            'Status'
        ];
        exportContent.forEach((value) => {
            csvArray.push({
                'PSSP Name': value.psspName,
                'RC Number': value.rcNumber,
                'Account No.': value.accountNumber,
                'Bank': value.bank.name,
                'Category': value.industry.name,
                'Date Created': new Date(value.createdAt).toString().split('GMT')[0],
                'Status': (value.activated) ? 'Active' : 'Inactive'
            })
        });
        JSON2CSV(fields, csvArray, 'industry');
    }

    getPdf(event?) {
        if (event === 'all') {
            this.getAllPSSP(0, this.totalElement, 'pdf');
        } else {
            const exportContent = this.list.allContent;
            this.exportToPdf(exportContent);
        }
    }

    private exportToPdf(exportContent) {
        const rows = [];
        const columns = ["s/n  ", "PSSP Name", "RC Number", "Account No.", "Bank", "Category", "Date Created", "Status"];
        exportContent.forEach((value, i) => {
            rows.push([i + 1, value.psspName, value.rcNumber, value.accountNumber, value.bank.name, value.industry.name, new Date(value.createdAt).toString().split('GMT')[0], (value.activated) ? 'Active' : 'Inactive'])
        });
        JSON2PDFTABLE(columns, rows, 'PSSP');
    }

    submit() {
        this.extractIds();
        if (this.errorMessage !== '') {
            this.notification.error(this.errorMessage);
            return;
        }
        this.booleans.loadsubmit = true;
        this.psspService.createpssp(this.createForm.value)
            .subscribe(response => {
                this.list.allContent.unshift(response);
                this.booleans.loadsubmit = false;
                $('#CreateModal').modal('hide');
                // this.createForm.reset();
            },
            error => {
                this.booleans.loadsubmit = false;
                this.notification.error(CONSTANTS.ERROR_RESPONSE, error);
            });
    }

    private extractIds() {
        this.errorMessage = '';
        if (isNaN(this.createForm.value['accountNumber'])) {
            this.errorMessage = 'Account number must be a number';
        }
        if ((this.createForm.value['bvn']) !== '' && isNaN(this.createForm.value['bvn'])) {
            this.errorMessage = 'BVN must be a number';
        }

        if (this.createForm.value['bank'] && this.createForm.value['bank'][0]) {
            this.createForm.value['bankCode'] = this.createForm.value['bank'][0]['code'];
        } else {
            this.createForm.value['bankCode'] = this.user['userBank']['code'];
        }
        if (this.createForm.value['industry'] && this.createForm.value['industry'][0]) {
            this.createForm.value['industryId'] = this.createForm.value['industry'][0]['id'];
        }
    }

    view(data) {
        this.modal_title = 'View';
        this.list.details = data;
        $('#ViewModal').modal('show');
    }

    edit(data) {
        this.list.details = data;
        this.id = data.id;
        this.submit_type = 'Update';
        this.modal_title = 'Update';

        const formdata = function() {
            return {
                accountNumber: [data.accountNumber, Validators.compose([Validators.required, Validators.minLength(10), Validators.maxLength(10)])],
                accountName: [data.accountNumber, Validators.compose([Validators.required])],
                rcNumber: [data.rcNumber, Validators.compose([Validators.required])],
                bank: [[{
                    'id': data.bank.id,
                    'itemName': data.bank.name,
                    'code': data.bank.code
                }], Validators.compose([])],
                industry: [[{
                    'id': data.industry.id,
                    'itemName': data.industry.name
                }], Validators.compose([Validators.required])],
                bvn: [data.bvn, Validators.compose([Validators.required, Validators.minLength(11), Validators.maxLength(11)])],
                psspName: [data.psspName, Validators.compose([Validators.required])],
                description: [data.description, Validators.compose([Validators.required])],
                id: [data.id, Validators.compose([])],
            }
        };
        this.createForm = this.fb.group(formdata());
        $('#CreateModal').modal('show');
    }

    update() {
        this.extractIds();
        this.booleans.loadsubmit = true;
        this.psspService.updatePSSP(this.createForm.value)
            .subscribe(response => {
                this.list.allContent.forEach((value, i) => {
                    if (value.id === this.id) {
                        this.list.allContent[i] = response;
                    }
                });
                $('#CreateModal').modal('hide');
                this.createForm.reset();
                this.booleans.loadsubmit = false;
                if ((this.approvalStatus !== '')) {
                    this.list.allContent.forEach((value, i) => {
                        if (this.id === value['id']) {
                            this.list.allContent.splice(i, 1);
                        }
                    });
                }
                this.notification.success(CONSTANTS.UPDATE_RESPONSE);
            },
            error => {
                this.booleans.loadsubmit = false;
                this.notification.error(CONSTANTS.ERROR_RESPONSE, error);
            });
    }


    getIndustry() {
        this.booleans.loadIndustry = true;
        this.industryService.getIndustryList()
            .subscribe(response => {
                this.booleans.loadIndustry = false;
                response.forEach(value => {
                    this.itemListIndustry.push({
                        id: value.id,
                        itemName: value.name
                    })
                });
            },
            error => {
                this.booleans.loadIndustry = false;
                this.notification.error('Unable to load category', error);
            })
    }

    getAllPSSP(pageNo, totalNumber, type) {
        this.booleans.getAllloader = true;
        this.psspService.getAllPSSP(pageNo, totalNumber)
            .subscribe(response => {
                this.booleans.getAllloader = false;
                (type === 'csv') ? this.exportToCsv(response.content) : this.exportToPdf((response.content));
            },
            error => {
                this.booleans.getAllloader = false;
                this.notification.error('Unable to load PSSP', error);
            })
    }


    search() {
    }

    getPage(event) {
        this.p = event;
        this.list.allContent = [];
        (!this.booleans.searchStatus) ? this.getPSSP(this.p - 1, this.pagesize) : this.searchMethod(this.p - 1);
    }

    getSearchModal() {
        $('#searchModal').modal('show');

    }

    searchMethod(pageNo) {
        this.booleans.searchStatus = true;
        this.booleans.loadingSearch = true;
        this.createFormSearch.value['bankCode'] = (this.createFormSearch.value['bank'][0]) ? this.createFormSearch.value['bank'][0]['code'] : this.createFormSearch.value['bank'];
        this.psspService.search(this.createFormSearch.value, pageNo)
            .subscribe((response) => {
                this.booleans.loader = false;
                this.booleans.loadingSearch = false;
                if (response.content.length === 0) {
                    this.notification.error('No Result Found')
                } else {
                    this.list.allContent = response.content;
                    if (this.booleans.pdfsearchloader || this.booleans.csvsearchloader) {
                        console.log('export csv22')
                        this.exportAs();
                    }
                    $('#searchModal').modal('hide');
                }
                this.booleans.csvsearchloader = false;
                this.booleans.pdfsearchloader = false;

            },
            error => {
                this.booleans.loader = false;
                this.booleans.csvsearchloader = false;
                this.booleans.pdfsearchloader = false;
                this.booleans.loadingSearch = false;
                this.notification.error('Unable to Search', error);
            })
    }

    private exportAs() {
        if (this.booleans.pdfsearchloader) {
            this.getPdf()
        } else if (this.booleans.csvsearchloader) {
            this.getCsv();
        }
    }

    exportCsvSearch() {
        this.booleans.csvsearchloader = true;
        this.booleans.pdfsearchloader = false;
        this.searchMethod(0);
    }

    exportPdfSearch() {
        this.booleans.csvsearchloader = false;
        this.booleans.pdfsearchloader = true;
        this.searchMethod(0);
    }

    keyupAccountNumber() {
        this.isInteger = !isNaN(this.createForm.value['accountNumber']);
        if (this.createForm.value['accountNumber'].length === 10 && !isNaN(this.createForm.value['accountNumber'])) {
            this.booleans.acountloader = true;
            this.accountLookup();
        } else {
            this.list.accountResult = null;
        }
    }

    accountLookup() {
        let bankCode = 0;
        if (this.user['userBank'] && this.user['userBank']['code']) {
            bankCode = this.user['userBank']['code'];
        } else if (this.createForm.value['bank'] && this.createForm.value['bank'][0]) {
            bankCode = this.createForm.value['bank'][0]['code'];
        }
        const data = {
            accountNumber: this.createForm.value['accountNumber'],
            bankCode: bankCode
        };
        this.authservice.accountLookup(data)
            .subscribe(response => {
                this.list.accountResult = response;
                if (this.list.accountResult['accountName']) {
                    this.createForm['controls']['accountName'].patchValue(this.list.accountResult['accountName']);
                }
                this.booleans.acountloader = false;
            },
            error => {
                this.booleans.acountloader = false;
                this.notification.error('Unable to process the action', error)
            })
    }

    /***********************************APPROVAL(MAKER AND CHECKER)*****************************************/
    public getParam(param?) {
        this.list.allContent = [];
        if (param) {
            switch (param) {
                case this.approvalName + '/pending':
                    this.statusType = 'UNAUTHORIZED';
                    this.approvalStatus = 'Pending';
                    break;
                case this.approvalName + '/approved':
                    this.statusType = 'AUTHORIZED';
                    this.approvalStatus = 'Approved';
                    break;
                case this.approvalName + '/disapproved':
                    this.statusType = 'REJECTED';
                    this.approvalStatus = 'Disapproved';
                    break;
            }
            this.router.navigate(['/' + param]);
        } else {
            const status = this.activatedRoute.snapshot.paramMap.get('status');
            switch (status) {
                case 'pending':
                    this.statusType = 'UNAUTHORIZED';
                    this.approvalStatus = 'Pending';
                    break;
                case 'approved':
                    this.statusType = 'AUTHORIZED';
                    this.approvalStatus = 'Approved';
                    break;
                case 'disapproved':
                    this.statusType = 'REJECTED';
                    this.approvalStatus = 'Disapproved';
                    break;
            }
        }
        this.getAuthorization(this.statusType, 0);
    }

    getApprovalPage(event) {
        this.p = +event;
        this.list.allContent = [];
        this.getAuthorization(this.statusType, this.p - 1);
    }


    getAuthorization(approval_type, pageNo) {
        this.booleans.loader = true;
        this.approvalservice.getAuthorization(approval_type, pageNo, this.approvalName, 'not_user')
            .subscribe(response => {
                this.booleans.loader = false;
                this.list.allContent = response.content;
                this.totalElement = response.totalElements;
            },
            error => {
                this.booleans.loader = false;
                this.notification.error('Unable to load content', error);
            })
    }

    getReason(data, authorizationStatus) {
        this.id = data.id;
        this.authorization = authorizationStatus;
        this.list.details = data;
        this.approvalType = 'disapprove';
        $('#reasonModal').modal('show');
    }

    disapprove() {
        let action = '';
        this.booleans.loadAction = true;
        switch (this.authorization) {
            case 'UNAUTHORIZED_CREATE':
                action = 'REJECT_CREATE';
                break;
            case 'UNAUTHORIZED_UPDATE':
                action = 'REJECT_UPDATE'
                break;
            case 'UNAUTHORIZED_TOGGLE':
                action = 'REJECT_TOGGLE'
                break;
        }

        this.Approve(this.id, action, this.formReason.value['reason']);
    }

    approve(id, authorizationStatus) {
        this.id = id;
        let action = '';
        switch (authorizationStatus) {
            case 'UNAUTHORIZED_CREATE':
                action = 'APPROVE_CREATE';
                break;
            case 'UNAUTHORIZED_UPDATE':
                action = 'APPROVE_UPDATE'
                break;
            case 'UNAUTHORIZED_TOGGLE':
                action = 'APPROVE_TOGGLE'
                break;
        }
        this.approvalType = 'approve';
        this.Approve(id, action);
    }

    Approve(id, authorizationStatus, reason?) {
        this.booleans.loadAction = true;
        this.approvalservice.approve({
            id: id,
            reason: (reason && reason !== '') ? reason : ''
        }, authorizationStatus, this.approvalName, 'not_user')
            .subscribe(response => {
                this.booleans.loadAction = false;
                this.list.allContent.forEach((value, i) => {
                    if (id === value['id']) {
                        this.list.allContent.splice(i, 1);
                    }
                });
                this.notification.success('Approval was successful');
            },
            error => {
                this.booleans.loadAction = false;
                this.notification.error(CONSTANTS.ERROR_RESPONSE, error);
            })
    }
}