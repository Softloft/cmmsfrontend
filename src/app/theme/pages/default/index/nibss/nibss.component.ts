import { AfterViewInit, Component, OnDestroy, OnInit } from "@angular/core";
import { AmChart, AmChartsService } from "@amcharts/amcharts3-angular";
import { ScriptLoaderService } from "../../../../../_services/script-loader.service";
import { NibssService } from "../../../../../../services/api-service/nibss.service";
import { NotificationService } from "../../../../../../services/notification.service";
import { Router } from "@angular/router";
import { AuthService } from "../../../../../../services/api-service/authService";

declare const $;

@Component({
    selector: 'app-nibss',
    templateUrl: './nibss.component.html',
    styles: []
})
export class NibssComponent implements OnInit, AfterViewInit, OnDestroy {
    public duration = 1;
    public durationType = 'year';
    public duration2 = 1;
    public durationType2 = 'year';
    public type = '';
    public itemListBiller = [];
    public selectedItemsBiller = [];
    public settingsBiller = {};
    public itemListBank = [];
    public selectedItemsBank = [];
    public settingsBank = {};
    public name: any;

    private userDetailList = [];
    private userList = [];
    private billerList = [];
    private mandateList = [];

    private chart: AmChart;
    private userChart: AmChart;
    private userChart2: AmChart;
    private billerChart: AmChart;
    private mandateChart: AmChart;
    private transactionDetailChart1: AmChart;
    private transactionDetailChart2: AmChart;
    private transactionDetailChart3: AmChart;
    private transactionDetailChart4: AmChart;
    private mandateTransactionDetailChart: AmChart;
    private userDetailChart: AmChart;

    public volumeList = [];
    public valueList = [];

    public volumeList2 = [];
    public valueList2 = [];

    public booleans = {
        loader: false
    };

    constructor(private _script: ScriptLoaderService,
        private router: Router,
        private authservice: AuthService,
        private nibssservice: NibssService,
        private notification: NotificationService,
        private AmCharts: AmChartsService, ) {

    }

    ngOnInit() {
        this.settingsBiller = {
            singleSelection: true,
            text: "Select Fields",
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            searchPlaceholderText: 'Search Fields',
            enableSearchFilter: true,
            badgeShowLimit: 5,
            groupBy: "category"
        };

        this.settingsBank = {
            singleSelection: true,
            text: "Select Fields",
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            searchPlaceholderText: 'Search Fields',
            enableSearchFilter: true,
            badgeShowLimit: 5,
            groupBy: "category"
        };

        this.getDashboard();
        this.getDashboard2();
    }

    getDashboard() {
        let value = 0;
        let volume = 0;
        let nibss = 0;
        let biller = 0;
        let bank = 0;
        let pssp = 0;
        this.booleans.loader = true;
        this.nibssservice.getDashboard(`successful/${this.durationType}/${this.duration}`)
            .subscribe((response) => {
                this.valueList = [];
                this.volumeList = [];
                response.forEach(res => {
                    value = +res.value;
                    volume = +res.volume;
                });

                this.valueList.push({
                    "year": 'value',
                    "value": value,
                    "expenses": 0
                });
                this.volumeList.push({
                    "year": 'Volume',
                    "value": volume,
                    "expenses": 0
                });

                this.AmCharts.updateChart(this.transactionDetailChart1, () => {
                    this.transactionDetailChart1.dataProvider = this.volumeList;
                });

                this.AmCharts.updateChart(this.transactionDetailChart2, () => {
                    this.transactionDetailChart2.dataProvider = this.valueList;
                });
            },
            error => {
                this.booleans.loader = false;
                this.notification.error('Unable to load Dashboard', error);
            })

        this.authservice.getUserDashboard()
            .subscribe((response) => {
                this.userList = [
                    { biller: 'NIBSS Users', count: response.totalNibssUsers },
                    { biller: 'Biller Users', count: response.totalBillerUsers },
                    { biller: 'Bank Users', count: response.totalBankUsers },
                    { biller: 'PSSP Users', count: response.totalPsspUsers },
                ];
                this.AmCharts.updateChart(this.userChart, () => {
                    this.userChart.dataProvider = this.userList;
                });
            },
            error => {
                this.booleans.loader = false;
                this.notification.error('Unable to load Dashboard', error);
            })

        this.authservice.getActiveStatusUserDashboard()
            .subscribe((response) => {
                this.userDetailList = [{
                    "year": 'NIBSS',
                    "Active": response['nibssUserStatus']['active'],
                    "Inactive": response['nibssUserStatus']['inactive'],
                }, {
                    "year": 'BANK',
                    "Active": response['bankUserStatus']['active'],
                    "Inactive": response['bankUserStatus']['inactive'],
                }, {
                    "year": 'BILLER',
                    "Active": response['billerUserStatus']['active'],
                    "Inactive": response['billerUserStatus']['inactive'],
                }, {
                    "year": 'PSSP',
                    "Active": response['psspUserStatus']['active'],
                    "Inactive": response['psspUserStatus']['inactive'],
                }];
                this.AmCharts.updateChart(this.userDetailChart, () => {
                    this.userDetailChart.dataProvider = this.userDetailList;
                });
            },
            error => {
                this.booleans.loader = false;
                this.notification.error('Unable to load Dashboard', error);
            })

        this.authservice.getBillerDashboard()
            .subscribe((response) => {
                this.billerList = [{
                    "country": "Active",
                    "value": response.activeBillers,
                    "pulled": true
                }, {
                    "country": "Inactive",
                    "value": response.inactiveBillers,
                    "pulled": true
                }];
                this.AmCharts.updateChart(this.billerChart, () => {
                    this.billerChart.dataProvider = this.billerList;
                });
            },
            error => {
                this.booleans.loader = false;
                this.notification.error('Unable to load Dashboard', error);
            })

        this.authservice.getMandateDashboard('nibss')
            .subscribe((response) => {
                this.mandateList = [
                    { "branch": 'Rejected', "count": response.rejectedMandates },
                    { "branch": 'Pending', "count": response.pendingMandates },
                    { "branch": 'Approved', "count": response.approvedMandates }
                ];
                this.AmCharts.updateChart(this.mandateChart, () => {
                    this.mandateChart.dataProvider = this.mandateList;
                });
            },
            error => {
                this.booleans.loader = false;
                this.notification.error('Unable to load Dashboard', error);
            })


    }

    getDashboard2() {
        let value = 0;
        let volume = 0;
        this.booleans.loader = true;
        this.nibssservice.getDashboard(`unsuccessful/${this.durationType}/${this.duration}`)
            .subscribe((response) => {
                this.valueList2 = [];
                this.volumeList2 = [];
                response.forEach(res => {
                    value = +res.value;
                    volume = +res.volume;
                });

                this.valueList2.push({
                    "year": 'value',
                    "value": 0,
                    "expenses": value
                });
                this.volumeList2.push({
                    "year": 'Volume',
                    "value": 0,
                    "expenses": volume
                });

                this.AmCharts.updateChart(this.transactionDetailChart3, () => {
                    this.transactionDetailChart3.dataProvider = this.volumeList2;
                });

                this.AmCharts.updateChart(this.transactionDetailChart4, () => {
                    this.transactionDetailChart4.dataProvider = this.valueList2;
                });
            },
            error => {
                this.booleans.loader = false;
                this.notification.error('Unable to load Dashboard', error);
            })
    }

    onViewTransactionDetail(type) {
        //  this.type = type;
        //  $('#viewBillerTransactionGraph').modal('show');
        this.router.navigate(['index/reports']);
    }

    onViewUserDetail() {
        $('#viewUserTransactionGraph').modal('show');
    }

    ngAfterViewInit() {
        this.userChart = this.AmCharts.makeChart("useChartDiv", {
            "type": "pie",
            "theme": "light",
            "labelsEnabled": false,
            "legend": {
                "markerType": "circle",
                "position": "right",
                "marginRight": 80,
                "autoMargins": false,
                "truncateLabels": 25 // custom parameter
            },
            "dataProvider": this.userList,
            "valueField": "count",
            "titleField": "biller",
            "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>"
        });
        this.userChart2 = this.AmCharts.makeChart("useChartDiv2", {
            "type": "serial",
            "theme": "light",
            "orderByField": "visits",
            "dataProvider": [{
                "country": "NIBSS",
                "visits": 1523
            }, {
                "country": "BILLER",
                "visits": 1882
            }, {
                "country": "BANK",
                "visits": 1809
            }, {
                "country": "PSSP",
                "visits": 1996
            }],
            "valueAxes": [{
                "gridColor": "#FFFFFF",
                "gridAlpha": 0.2,
                "dashLength": 0
            }],
            "gridAboveGraphs": true,
            "startDuration": 1,
            "graphs": [{
                "balloonText": "[[category]]: <b>[[value]]</b>",
                "fillAlphas": 0.8,
                "lineAlpha": 0.2,
                "type": "column",
                "valueField": "visits"
            }],
            "chartCursor": {
                "categoryBalloonEnabled": false,
                "cursorAlpha": 0,
                "zoomable": false
            },
            "categoryField": "country",
            "categoryAxis": {
                "gridPosition": "start",
                "gridAlpha": 0,
                "tickPosition": "start",
                "tickLength": 20
            },
            "export": {
                "enabled": true
            }

        });


        this.billerChart = this.AmCharts.makeChart("billerChartdiv", {
            "type": "pie",
            "theme": "light",
            "dataProvider": this.billerList,
            "valueField": "value",
            "titleField": "country",
            "pulledField": "pulled",
            "outlineAlpha": 0.4,
            "depth3D": 15,
            "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
            "angle": 30,
            "export": {
                "enabled": true
            }
        });


        this.mandateTransactionDetailChart = this.AmCharts.makeChart("transactionDetailDiv", {
            "type": "serial",
            "theme": "light",
            "dataDateFormat": "YYYY-MM-DD",
            "precision": 2,
            "valueAxes": [{
                "id": "v1",
                "title": "Total Amount",
                "position": "left",
                "autoGridCount": false,
                "labelFunction": function(value) {
                    return "N" + Math.round(value) + "M";
                }
            }, {
                "id": "v2",
                "title": "No. of Mandates",
                "gridAlpha": 0,
                "position": "right",
                "autoGridCount": false
            }],
            "graphs": [{
                "id": "g4",
                "valueAxis": "v1",
                "lineColor": "#62cf73",
                "fillColors": "#62cf73",
                "fillAlphas": 1,
                "type": "column",
                "title": "Total Amount",
                "valueField": "amount",
                "clustered": false,
                "columnWidth": 0.3,
                "legendValueText": "N[[value]]M",
                "balloonText": "[[title]]<br/><b style='font-size: 130%'>N[[value]]M</b>"
            }, {
                "id": "g1",
                "valueAxis": "v2",
                "bullet": "round",
                "bulletBorderAlpha": 1,
                "bulletColor": "#FFFFFF",
                "bulletSize": 5,
                "hideBulletsCount": 50,
                "lineThickness": 2,
                "lineColor": "#20acd4",
                "type": "smoothedLine",
                "title": "No. of Mandates",
                "useLineColorForBulletBorder": true,
                "valueField": "mandate",
                "balloonText": "[[title]]<br/><b style='font-size: 130%'>[[value]]</b>"
            }],
            "chartScrollbar": {
                "graph": "g1",
                "oppositeAxis": false,
                "offset": 30,
                "scrollbarHeight": 50,
                "backgroundAlpha": 0,
                "selectedBackgroundAlpha": 0.1,
                "selectedBackgroundColor": "#888888",
                "graphFillAlpha": 0,
                "graphLineAlpha": 0.5,
                "selectedGraphFillAlpha": 0,
                "selectedGraphLineAlpha": 1,
                "autoGridCount": true,
                "color": "#AAAAAA"
            },
            "chartCursor": {
                "pan": true,
                "valueLineEnabled": true,
                "valueLineBalloonEnabled": true,
                "cursorAlpha": 0,
                "valueLineAlpha": 0.2
            },
            "categoryField": "date",
            "categoryAxis": {
                "parseDates": true,
                "dashLength": 1,
                "minorGridEnabled": true
            },
            "legend": {
                "useGraphSettings": true,
                "position": "top"
            },
            "balloon": {
                "borderThickness": 1,
                "shadowAlpha": 0
            },
            "export": {
                enabled: true
            },
            "dataProvider": [{
                "date": "nibss",
                "mandate": 87,
                "amount": 5
            }, {
                "date": "2013-01-17",
                "mandate": 74,
                "amount": 4
            }, {
                "date": "2013-01-18",
                "mandate": 78,
                "amount": 5
            }, {
                "date": "2013-01-19",
                "mandate": 85,
                "amount": 8
            }, {
                "date": "2013-01-20",
                "mandate": 82,
                "amount": 9
            }, {
                "date": "2013-01-21",
                "mandate": 83,
                "amount": 3
            }, {
                "date": "2013-01-22",
                "mandate": 88,
                "amount": 5
            }, {
                "date": "2013-01-23",
                "mandate": 85,
                "amount": 7
            }, {
                "date": "2013-01-24",
                "mandate": 85,
                "amount": 9
            }, {
                "date": "2013-01-25",
                "mandate": 80,
                "amount": 5
            }, {
                "date": "2013-01-26",
                "mandate": 87,
                "amount": 4
            }, {
                "date": "2013-01-27",
                "mandate": 84,
                "amount": 3
            }, {
                "date": "2013-01-28",
                "mandate": 83,
                "amount": 5
            }, {
                "date": "2013-01-29",
                "mandate": 84,
                "amount": 5
            }, {
                "date": "2013-01-30",
                "mandate": 81,
                "amount": 4
            }]
        });

        this.userDetailChart = this.AmCharts.makeChart("userDetails", {
            "type": "serial",
            "dataProvider": this.userDetailList,
            "dataTableId": "chartdata",
            "categoryField": "year",
            "categoryAxis": {
                "gridAlpha": 0.07,
                "axisColor": "#DADADA",
                "startOnAxis": false,
                "gridPosition": "start",
                "tickPosition": "start",
                "tickLength": 25,
                "boldLabels": true
            },
            "valueAxes": [{
                //"stackType": "regular",
                "gridAlpha": 0.07,
                "title": "User Details"
            }],
            "graphs": [{
                "type": "column",
                "title": "Active",
                "valueField": "Active",
                "lineAlpha": 0,
                "fillAlphas": 0.6
            }, {
                "type": "column",
                "title": "Inactive",
                "valueField": "Inactive",
                "lineAlpha": 0,
                "fillAlphas": 0.6
            }],
            "chartCursor": {
                "cursorAlpha": 0,
                "categoryBalloonEnabled": false
            },
            "autoMargins": false,
            "marginLeft": 150,
            "marginRight": 0,
            "marginBottom": 25
        });

        this.mandateChart = this.AmCharts.makeChart("mandateChartDiv", {
            "type": "pie",
            "theme": "light",
            "titles": [{
                "text": `Mandate Details`,
                "size": 14
            }],
            "dataProvider": this.mandateList,
            "valueField": "count",
            "titleField": "branch",
            "startEffect": "elastic",
            "startDuration": 2,
            "labelRadius": 15,
            "innerRadius": "50%",
            "depth3D": 10,
            "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
            "angle": 15,
            "export": {
                "enabled": true
            }
        });


        this.transactionDetailChart1 = this.AmCharts.makeChart("trantractionChartDiv1", {
            "type": "serial",
            "theme": "light",
            "categoryField": "year",
            "rotate": true,
            "startDuration": 1,
            "categoryAxis": {
                "gridPosition": "start",
                "position": "left"
            },
            "columnSpacing": 0,
            "columnWidth": 0.6,
            "graphs": [{
                /**
                 * These two graphs are just here for labels
                 * They're basically invisible, save for the label itself,
                 * but underneath they're just stacked column graphs
                 */
                "labelText": "[[title]]",
                "labelPosition": "inside",
                "fontSize": 14,
                "fillAlphas": 0,
                "lineAlpha": 0,
                "title": "",
                "type": "column",
                "valueField": "value",
                "showBalloon": false,
                "visibleInLegend": false
            }, {
                "labelText": "[[title]]",
                "labelPosition": "inside",
                "fontSize": 14,
                "fillAlphas": 0,
                "lineAlpha": 0,
                "title": "Expenses",
                "type": "column",
                "valueField": "expenses",
                "showBalloon": false,
                "visibleInLegend": false
            }, {
                /**
                 * We start a new stack with this graph
                 */
                "newStack": true,
                "balloonText": "Volume:[[value]]",
                "labelText": "[[value]]",
                "labelPosition": "inside",
                "color": "#fff",
                "fillAlphas": 0.8,
                "lineAlpha": 0.2,
                "title": "Volume",
                "type": "column",
                "valueField": "value"
            }, {
                "balloonText": "Expenses:[[value]]",
                "labelText": "[[value]]",
                "labelPosition": "inside",
                "color": "#fff",
                "fillAlphas": 0.8,
                "lineAlpha": 0.2,
                "title": "Expenses",
                "type": "column",
                "valueField": "expenses"
            }],
            "valueAxes": [{
                "stackType": "regular",
                "position": "top",
                "axisAlpha": 0
            }],
            "dataProvider": this.volumeList

        });

        this.transactionDetailChart2 = this.AmCharts.makeChart("trantractionChartDiv2", {
            "type": "serial",
            "theme": "light",
            "categoryField": "year",
            "rotate": true,
            "startDuration": 1,
            "categoryAxis": {
                "gridPosition": "start",
                "position": "left"
            },
            "columnSpacing": 0,
            "columnWidth": 0.6,
            "graphs": [{
                /**
                 * These two graphs are just here for labels
                 * They're basically invisible, save for the label itself,
                 * but underneath they're just stacked column graphs
                 */
                "labelText": "[[title]]",
                "labelPosition": "inside",
                "fontSize": 14,
                "fillAlphas": 0,
                "lineAlpha": 0,
                "title": "",
                "type": "column",
                "valueField": "value",
                "showBalloon": false,
                "visibleInLegend": false
            }, {
                "labelText": "[[title]]",
                "labelPosition": "inside",
                "fontSize": 14,
                "fillAlphas": 0,
                "lineAlpha": 0,
                "title": "Expenses",
                "type": "column",
                "valueField": "expenses",
                "showBalloon": false,
                "visibleInLegend": false
            }, {
                /**
                 * We start a new stack with this graph
                 */
                "newStack": true,
                "balloonText": "Value:[[value]]",
                "labelText": "[[value]]",
                "labelPosition": "inside",
                "color": "#fff",
                "fillAlphas": 0.8,
                "lineAlpha": 0.2,
                "title": "Value",
                "type": "column",
                "valueField": "value"
            }, {
                "balloonText": "Expenses:[[value]]",
                "labelText": "[[value]]",
                "labelPosition": "inside",
                "color": "#fff",
                "fillAlphas": 0.8,
                "lineAlpha": 0.2,
                "title": "Expenses",
                "type": "column",
                "valueField": "expenses"
            }],
            "valueAxes": [{
                "stackType": "regular",
                "position": "top",
                "axisAlpha": 0
            }],
            "dataProvider": this.valueList

        });

        this.transactionDetailChart3 = this.AmCharts.makeChart("trantractionChartDiv3", {
            "type": "serial",
            "theme": "light",
            "categoryField": "year",
            "rotate": true,
            "startDuration": 1,
            "categoryAxis": {
                "gridPosition": "start",
                "position": "left"
            },
            "columnSpacing": 0,
            "columnWidth": 0.6,
            "graphs": [{
                /**
                 * These two graphs are just here for labels
                 * They're basically invisible, save for the label itself,
                 * but underneath they're just stacked column graphs
                 */
                "labelText": "[[title]]",
                "labelPosition": "inside",
                "fontSize": 14,
                "fillAlphas": 0,
                "lineAlpha": 0,
                "title": "",
                "type": "column",
                "valueField": "value",
                "showBalloon": false,
                "visibleInLegend": false
            }, {
                "labelText": "[[title]]",
                "labelPosition": "inside",
                "fontSize": 14,
                "fillAlphas": 0,
                "lineAlpha": 0,
                "title": "",
                "type": "column",
                "valueField": "expenses",
                "showBalloon": false,
                "visibleInLegend": false
            }, {
                /**
                 * We start a new stack with this graph
                 */
                "newStack": true,
                "balloonText": "Volume:[[value]]",
                "labelText": "[[value]]",
                "labelPosition": "inside",
                "color": "#fff",
                "fillAlphas": 0.8,
                "lineAlpha": 0.2,
                "title": "Value",
                "type": "column",
                "valueField": "value"
            }, {
                "balloonText": "Volume:[[value]]",
                "labelText": "[[value]]",
                "labelPosition": "inside",
                "color": "#fff",
                "fillAlphas": 0.8,
                "lineAlpha": 0.2,
                "title": "Expenses",
                "type": "column",
                "valueField": "expenses"
            }],
            "valueAxes": [{
                "stackType": "regular",
                "position": "top",
                "axisAlpha": 0
            }],
            "dataProvider": this.valueList2

        });

        this.transactionDetailChart4 = this.AmCharts.makeChart("trantractionChartDiv4", {
            "type": "serial",
            "theme": "light",
            "categoryField": "year",
            "rotate": true,
            "startDuration": 1,
            "categoryAxis": {
                "gridPosition": "start",
                "position": "left"
            },
            "columnSpacing": 0,
            "columnWidth": 0.6,
            "graphs": [{
                /**
                 * These two graphs are just here for labels
                 * They're basically invisible, save for the label itself,
                 * but underneath they're just stacked column graphs
                 */
                "labelText": "[[title]]",
                "labelPosition": "inside",
                "fontSize": 14,
                "fillAlphas": 0,
                "lineAlpha": 0,
                "title": "",
                "type": "column",
                "valueField": "value",
                "showBalloon": false,
                "visibleInLegend": false
            }, {
                "labelText": "[[title]]",
                "labelPosition": "inside",
                "fontSize": 14,
                "fillAlphas": 0,
                "lineAlpha": 0,
                "title": "",
                "type": "column",
                "valueField": "expenses",
                "showBalloon": false,
                "visibleInLegend": false
            }, {
                /**
                 * We start a new stack with this graph
                 */
                "newStack": true,
                "balloonText": "Value:[[value]]",
                "labelText": "[[value]]",
                "labelPosition": "inside",
                "color": "#fff",
                "fillAlphas": 0.8,
                "lineAlpha": 0.2,
                "title": "Value",
                "type": "column",
                "valueField": "value"
            }, {
                "balloonText": "Value:[[value]]",
                "labelText": "[[value]]",
                "labelPosition": "inside",
                "color": "#fff",
                "fillAlphas": 0.8,
                "lineAlpha": 0.2,
                "title": "Expenses",
                "type": "column",
                "valueField": "expenses"
            }],
            "valueAxes": [{
                "stackType": "regular",
                "position": "top",
                "axisAlpha": 0
            }],
            "dataProvider": this.valueList2

        });


        this._script.loadScripts('app-index',
            ['assets/app/js/dashboard.js']);

    }


    onItemSelectBiller(item: any) {
        this.selectedItemsBiller = [{ 'id': item.id, 'itemName': item.itemName }];
    }

    OnItemDeSelectBiller(item: any) {
    }

    onItemSelectBank(item: any) {
        this.selectedItemsBank = [{ 'id': item.id, 'itemName': item.itemName }];
    }

    OnItemDeSelectBank(item: any) {
    }

    ngOnDestroy() {
        if (this.transactionDetailChart1) {
            this.AmCharts.destroyChart(this.transactionDetailChart1);
        }
        if (this.transactionDetailChart2) {
            this.AmCharts.destroyChart(this.transactionDetailChart2);
        }
        if (this.transactionDetailChart3) {
            this.AmCharts.destroyChart(this.transactionDetailChart3);
        }
        if (this.transactionDetailChart4) {
            this.AmCharts.destroyChart(this.transactionDetailChart4);
        }
    }
}