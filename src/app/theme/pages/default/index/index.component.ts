import { AfterViewInit, Component, OnInit, ViewEncapsulation } from "@angular/core";
import { ScriptLoaderService } from "../../../../_services/script-loader.service";
import { AmChart, AmChartsService } from "@amcharts/amcharts3-angular";
import { UserService } from "../../../../../services/user.service";

import Swal from "sweetalert2"; declare const $;

@Component({
    selector: "app-index",
    templateUrl: "./index.component.html",
    styleUrls: ['index.component.css'],
    encapsulation: ViewEncapsulation.None,
})
export class IndexComponent implements OnInit {

    user: any;
    constructor(private _script: ScriptLoaderService,
        private userservice: UserService,
        private AmCharts: AmChartsService) {

    }

    ngOnInit() {
        this.user = this.userservice.getAuthUser()['userType'];
    }

}