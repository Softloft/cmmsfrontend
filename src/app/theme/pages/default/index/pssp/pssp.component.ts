import { AfterViewInit, Component, OnDestroy, OnInit } from "@angular/core";
import { AmChart, AmChartsService } from "@amcharts/amcharts3-angular";
import { ScriptLoaderService } from "../../../../../_services/script-loader.service";
import { NibssService } from "../../../../../../services/api-service/nibss.service";
import { NotificationService } from "../../../../../../services/notification.service";
import { Router } from "@angular/router";
import { AuthService } from "../../../../../../services/api-service/authService";

import Swal from "sweetalert2"; declare const $;

@Component({
    selector: 'app-pssp',
    templateUrl: './pssp.component.html',
    styles: []
})
export class PsspComponent implements OnInit, AfterViewInit {
    public duration = 1;
    public durationType = 'year';
    public type = '';
    public moduleName = 'pssp';
    public itemListBiller = [];
    public selectedItemsBiller = [];
    public settingsBiller = {};
    public itemListBank = [];
    public selectedItemsBank = [];
    public settingsBank = {};
    public name: any;

    private billerList = [];
    private mandateList = [];

    private billerChart: AmChart;
    private mandateChart: AmChart;
    private transactionDetailChart1: AmChart;
    private transactionDetailChart2: AmChart;
    private transactionDetailChart3: AmChart;
    private transactionDetailChart4: AmChart;

    public volumeList = [];
    public valueList = [];

    public volumeList2 = [];
    public valueList2 = [];

    public booleans = {
        loader: false
    };

    constructor(private _script: ScriptLoaderService,
        private router: Router,
        private authservice: AuthService,
        private nibssservice: NibssService,
        private notification: NotificationService,
        private AmCharts: AmChartsService, ) {

    }

    ngOnInit() {
        this.settingsBiller = {
            singleSelection: true,
            text: "Select Fields",
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            searchPlaceholderText: 'Search Fields',
            enableSearchFilter: true,
            badgeShowLimit: 5,
            groupBy: "category"
        };

        this.settingsBank = {
            singleSelection: true,
            text: "Select Fields",
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            searchPlaceholderText: 'Search Fields',
            enableSearchFilter: true,
            badgeShowLimit: 5,
            groupBy: "category"
        };

        this.getDashboard();
    }

    getDashboard() {
        let value = 0;
        let volume = 0;
        let biller = 0;
        this.booleans.loader = true;
        this.nibssservice.getDashboard(`successful/${this.durationType}/${this.duration}`)
            .subscribe((response) => {
                this.valueList = [];
                this.volumeList = [];
                response.forEach(res => {
                    value = +res.value;
                    volume = +res.volume;
                });

                this.valueList.push({
                    "year": 'value',
                    "value": value,
                    "expenses": 0
                });
                this.volumeList.push({
                    "year": 'Volume',
                    "value": volume,
                    "expenses": 0
                });

                this.AmCharts.updateChart(this.transactionDetailChart1, () => {
                    this.transactionDetailChart1.dataProvider = this.volumeList;
                });

                this.AmCharts.updateChart(this.transactionDetailChart2, () => {
                    this.transactionDetailChart2.dataProvider = this.valueList;
                });
            },
            error => {
                this.booleans.loader = false;
                this.notification.error('Unable to load Dashboard', error);
            });

        /*this.authservice.getMandateDashboard(this.moduleName)
            .subscribe((response) => {
                    this.mandateList = [
                        {"branch": 'Rejected', "count": response.rejectedMandates},
                        {"branch": 'Pending', "count": response.pendingMandates},
                        {"branch": 'Approved', "count": response.approvedMandates}
                    ];
                    this.AmCharts.updateChart(this.mandateChart, () => {
                        this.mandateChart.dataProvider = this.mandateList;
                    });
                },
                error => {
                    this.booleans.loader = false;
                    this.notification.error('Unable to load Dashboard', error);
                })*/


        this.authservice.getBillerActiveDashboard(this.moduleName)
            .subscribe((response) => {
                console.log('response :: ', response);
                this.billerList = [{
                    "country": "Active",
                    "value": response.activeBillers,
                    "pulled": true
                }, {
                    "country": "Inactive",
                    "value": response.inactiveBillers,
                    "pulled": true
                }];
                this.AmCharts.updateChart(this.billerChart, () => {
                    this.billerChart.dataProvider = this.billerList;
                });
            },
            error => {
                this.booleans.loader = false;
                this.notification.error('Unable to load Dashboard', error);
            })
    }

    onViewTransactionDetail(type) {
        //  this.type = type;
        //  $('#viewBillerTransactionGraph').modal('show');
        this.router.navigate(['index/reports']);
    }

    onViewUserDetail() {
        $('#viewUserTransactionGraph').modal('show');
    }

    ngAfterViewInit() {

        this.billerChart = this.AmCharts.makeChart("billerChartdiv", {
            "type": "pie",
            "theme": "light",
            "dataProvider": this.billerList,
            "valueField": "value",
            "titleField": "country",
            "pulledField": "pulled",
            "outlineAlpha": 0.4,
            "depth3D": 15,
            "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
            "angle": 30,
            "export": {
                "enabled": true
            }
        });

        this.mandateChart = this.AmCharts.makeChart("mandateChartDiv", {
            "type": "pie",
            "theme": "light",
            "titles": [{
                "text": `Mandate Details`,
                "size": 14
            }],
            "dataProvider": this.mandateList,
            "valueField": "count",
            "titleField": "branch",
            "startEffect": "elastic",
            "startDuration": 2,
            "labelRadius": 15,
            "innerRadius": "50%",
            "depth3D": 10,
            "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
            "angle": 15,
            "export": {
                "enabled": true
            }
        });


        this.transactionDetailChart1 = this.AmCharts.makeChart("trantractionChartDiv1", {
            "type": "serial",
            "theme": "light",
            "categoryField": "year",
            "rotate": true,
            "startDuration": 1,
            "categoryAxis": {
                "gridPosition": "start",
                "position": "left"
            },
            "columnSpacing": 0,
            "columnWidth": 0.6,
            "graphs": [{
                /**
                 * These two graphs are just here for labels
                 * They're basically invisible, save for the label itself,
                 * but underneath they're just stacked column graphs
                 */
                "labelText": "[[title]]",
                "labelPosition": "inside",
                "fontSize": 14,
                "fillAlphas": 0,
                "lineAlpha": 0,
                "title": "",
                "type": "column",
                "valueField": "value",
                "showBalloon": false,
                "visibleInLegend": false
            }, {
                "labelText": "[[title]]",
                "labelPosition": "inside",
                "fontSize": 14,
                "fillAlphas": 0,
                "lineAlpha": 0,
                "title": "Expenses",
                "type": "column",
                "valueField": "expenses",
                "showBalloon": false,
                "visibleInLegend": false
            }, {
                /**
                 * We start a new stack with this graph
                 */
                "newStack": true,
                "balloonText": "Volume:[[value]]",
                "labelText": "[[value]]",
                "labelPosition": "inside",
                "color": "#fff",
                "fillAlphas": 0.8,
                "lineAlpha": 0.2,
                "title": "Volume",
                "type": "column",
                "valueField": "value"
            }, {
                "balloonText": "Expenses:[[value]]",
                "labelText": "[[value]]",
                "labelPosition": "inside",
                "color": "#fff",
                "fillAlphas": 0.8,
                "lineAlpha": 0.2,
                "title": "Expenses",
                "type": "column",
                "valueField": "expenses"
            }],
            "valueAxes": [{
                "stackType": "regular",
                "position": "top",
                "axisAlpha": 0
            }],
            "dataProvider": this.volumeList

        });

        this.transactionDetailChart2 = this.AmCharts.makeChart("trantractionChartDiv2", {
            "type": "serial",
            "theme": "light",
            "categoryField": "year",
            "rotate": true,
            "startDuration": 1,
            "categoryAxis": {
                "gridPosition": "start",
                "position": "left"
            },
            "columnSpacing": 0,
            "columnWidth": 0.6,
            "graphs": [{
                /**
                 * These two graphs are just here for labels
                 * They're basically invisible, save for the label itself,
                 * but underneath they're just stacked column graphs
                 */
                "labelText": "[[title]]",
                "labelPosition": "inside",
                "fontSize": 14,
                "fillAlphas": 0,
                "lineAlpha": 0,
                "title": "",
                "type": "column",
                "valueField": "value",
                "showBalloon": false,
                "visibleInLegend": false
            }, {
                "labelText": "[[title]]",
                "labelPosition": "inside",
                "fontSize": 14,
                "fillAlphas": 0,
                "lineAlpha": 0,
                "title": "Expenses",
                "type": "column",
                "valueField": "expenses",
                "showBalloon": false,
                "visibleInLegend": false
            }, {
                /**
                 * We start a new stack with this graph
                 */
                "newStack": true,
                "balloonText": "Value:[[value]]",
                "labelText": "[[value]]",
                "labelPosition": "inside",
                "color": "#fff",
                "fillAlphas": 0.8,
                "lineAlpha": 0.2,
                "title": "Value",
                "type": "column",
                "valueField": "value"
            }, {
                "balloonText": "Expenses:[[value]]",
                "labelText": "[[value]]",
                "labelPosition": "inside",
                "color": "#fff",
                "fillAlphas": 0.8,
                "lineAlpha": 0.2,
                "title": "Expenses",
                "type": "column",
                "valueField": "expenses"
            }],
            "valueAxes": [{
                "stackType": "regular",
                "position": "top",
                "axisAlpha": 0
            }],
            "dataProvider": this.valueList

        });

        this.transactionDetailChart3 = this.AmCharts.makeChart("trantractionChartDiv3", {
            "type": "serial",
            "theme": "light",
            "categoryField": "year",
            "rotate": true,
            "startDuration": 1,
            "categoryAxis": {
                "gridPosition": "start",
                "position": "left"
            },
            "columnSpacing": 0,
            "columnWidth": 0.6,
            "graphs": [{
                /**
                 * These two graphs are just here for labels
                 * They're basically invisible, save for the label itself,
                 * but underneath they're just stacked column graphs
                 */
                "labelText": "[[title]]",
                "labelPosition": "inside",
                "fontSize": 14,
                "fillAlphas": 0,
                "lineAlpha": 0,
                "title": "",
                "type": "column",
                "valueField": "value",
                "showBalloon": false,
                "visibleInLegend": false
            }, {
                "labelText": "[[title]]",
                "labelPosition": "inside",
                "fontSize": 14,
                "fillAlphas": 0,
                "lineAlpha": 0,
                "title": "",
                "type": "column",
                "valueField": "expenses",
                "showBalloon": false,
                "visibleInLegend": false
            }, {
                /**
                 * We start a new stack with this graph
                 */
                "newStack": true,
                "balloonText": "Volume:[[value]]",
                "labelText": "[[value]]",
                "labelPosition": "inside",
                "color": "#fff",
                "fillAlphas": 0.8,
                "lineAlpha": 0.2,
                "title": "Value",
                "type": "column",
                "valueField": "value"
            }, {
                "balloonText": "Volume:[[value]]",
                "labelText": "[[value]]",
                "labelPosition": "inside",
                "color": "#fff",
                "fillAlphas": 0.8,
                "lineAlpha": 0.2,
                "title": "Expenses",
                "type": "column",
                "valueField": "expenses"
            }],
            "valueAxes": [{
                "stackType": "regular",
                "position": "top",
                "axisAlpha": 0
            }],
            "dataProvider": this.valueList2

        });

        this.transactionDetailChart4 = this.AmCharts.makeChart("trantractionChartDiv4", {
            "type": "serial",
            "theme": "light",
            "categoryField": "year",
            "rotate": true,
            "startDuration": 1,
            "categoryAxis": {
                "gridPosition": "start",
                "position": "left"
            },
            "columnSpacing": 0,
            "columnWidth": 0.6,
            "graphs": [{
                /**
                 * These two graphs are just here for labels
                 * They're basically invisible, save for the label itself,
                 * but underneath they're just stacked column graphs
                 */
                "labelText": "[[title]]",
                "labelPosition": "inside",
                "fontSize": 14,
                "fillAlphas": 0,
                "lineAlpha": 0,
                "title": "",
                "type": "column",
                "valueField": "value",
                "showBalloon": false,
                "visibleInLegend": false
            }, {
                "labelText": "[[title]]",
                "labelPosition": "inside",
                "fontSize": 14,
                "fillAlphas": 0,
                "lineAlpha": 0,
                "title": "",
                "type": "column",
                "valueField": "expenses",
                "showBalloon": false,
                "visibleInLegend": false
            }, {
                /**
                 * We start a new stack with this graph
                 */
                "newStack": true,
                "balloonText": "Value:[[value]]",
                "labelText": "[[value]]",
                "labelPosition": "inside",
                "color": "#fff",
                "fillAlphas": 0.8,
                "lineAlpha": 0.2,
                "title": "Value",
                "type": "column",
                "valueField": "value"
            }, {
                "balloonText": "Value:[[value]]",
                "labelText": "[[value]]",
                "labelPosition": "inside",
                "color": "#fff",
                "fillAlphas": 0.8,
                "lineAlpha": 0.2,
                "title": "Expenses",
                "type": "column",
                "valueField": "expenses"
            }],
            "valueAxes": [{
                "stackType": "regular",
                "position": "top",
                "axisAlpha": 0
            }],
            "dataProvider": this.valueList2

        });


        this._script.loadScripts('app-index',
            ['assets/app/js/dashboard.js']);

    }


    onItemSelectBiller(item: any) {
        this.selectedItemsBiller = [{ 'id': item.id, 'itemName': item.itemName }];
    }

    OnItemDeSelectBiller(item: any) {
    }

    onItemSelectBank(item: any) {
        this.selectedItemsBank = [{ 'id': item.id, 'itemName': item.itemName }];
    }

    OnItemDeSelectBank(item: any) {
    }

    ngOnDestroy() {
        if (this.transactionDetailChart1) {
            this.AmCharts.destroyChart(this.transactionDetailChart1);
        }
        if (this.transactionDetailChart2) {
            this.AmCharts.destroyChart(this.transactionDetailChart2);
        }
        if (this.transactionDetailChart3) {
            this.AmCharts.destroyChart(this.transactionDetailChart3);
        }
        if (this.transactionDetailChart4) {
            this.AmCharts.destroyChart(this.transactionDetailChart4);
        }
    }
}