import { Component, OnInit } from '@angular/core';
import { NibssService } from "../../../../../../services/api-service/nibss.service";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { NotificationService } from "../../../../../../services/notification.service";
import { ActivatedRoute, Router } from "@angular/router";
import { AuditComponent } from "../../audit/audit.component";
import { AuthService } from "../../../../../../services/api-service/authService";
import { UserService } from "../../../../../../services/user.service";

import Swal from "sweetalert2"; declare const $;
@Component({
    selector: 'app-mandate-report',
    templateUrl: 'mandate-report.component.html',
    styles: []
})
export class MandateReportComponent implements OnInit {
    public current: number = 1;
    public profile: any;
    public userType = '';
    public p = 1;
    public totalElement = 0;
    public id = 0;
    public moduleName = 'Mandate Report';
    public booleans = {
        loader: false,
        loadingStatus: false,
        loadsearch: false,
    };

    public list = {
        allContent: [],
        roles: [],
        data: [],
        details: [],
    };

    constructor(private notification: NotificationService,
        private fb: FormBuilder,
        private router: Router,
        private authservice: AuthService,
        private userservice: UserService,
        private route: ActivatedRoute) {
    }

    ngOnInit() {
        // this.urlId = +this.route.snapshot.paramMap.get('id');
        this.loadFunction();
    }


    loadFunction() {
        this.userType = this.userservice.getAuthUser()['userType'];
        if (this.userType === 'NIBSS') {
            this.getMandateReport(this.p - 1);
        } else if (this.userType === 'BILLER') {
            this.getBillerMandateReport(this.p - 1)
        } else if (this.userType === 'BANK') {
            this.getBankMandateReport(this.p - 1)
        }
    }


    getPage(event) {
        this.p = event;
        this.list.allContent = [];
        this.getMandateReport(this.p - 1);
    }

    getMandateReport(pageNo) {
        this.booleans.loader = true;
        this.authservice.getMandateReport(pageNo)
            .subscribe((response) => {
                this.booleans.loader = false;
                // this.totalElement = response.totalElements;
                this.list.allContent = response.mandateReport;
            },
            error => {
                this.booleans.loader = false;
                this.notification.error('Unable to load ' + this.moduleName, error);
            })
    }

    getBillerMandateReport(pageNo) {
        this.booleans.loader = true;
        this.authservice.getBillerMandateReport(pageNo)
            .subscribe((response) => {
                this.booleans.loader = false;
                // this.totalElement = response.totalElements;
                this.list.allContent = response.mandateReport;
            },
            error => {
                this.booleans.loader = false;
                this.notification.error('Unable to load ' + this.moduleName, error);
            })
    }

    getBankMandateReport(pageNo) {
        this.booleans.loader = true;
        this.authservice.getBankMandateReport(pageNo)
            .subscribe((response) => {
                console.log('biller mandate report :: ', response);
                this.booleans.loader = false;
                // this.totalElement = response.totalElements;
                this.list.allContent = response.mandateReport;
            },
            error => {
                this.booleans.loader = false;
                this.notification.error('Unable to load ' + this.moduleName, error);
            })
    }
}
