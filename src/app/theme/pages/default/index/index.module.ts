import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { IndexComponent } from './index.component';
import { LayoutModule } from '../../../layouts/layout.module';
import { DefaultComponent } from "../default.component";
import { AmChartsModule } from "@amcharts/amcharts3-angular";
import { NibssComponent } from './nibss/nibss.component';
import { BillerComponent } from './biller/biller.component';
import { BankComponent } from './bank/bank.component';
import { PsspComponent } from './pssp/pssp.component';
import { AngularMultiSelectModule } from "angular2-multiselect-dropdown";
import { FormsModule } from "@angular/forms";
import { TransactionReportComponent } from './transaction-report/transaction-report.component';
import { NgxPaginationModule } from "ngx-pagination";
import { DataTablesModule } from "angular-datatables";
import { PipeModule } from "../../../../../shared/modules/pipe.module";
import { MandateReportComponent } from "./mandate-report/mandate-report.component";

const routes: Routes = [
    {
        "path": "",
        "component": DefaultComponent,
        "children": [
            {
                "path": "",
                "component": IndexComponent
            },
            {
                "path": "mandate-report",
                "component": MandateReportComponent
            },
            {
                "path": "transactions/:type",
                "component": TransactionReportComponent
            },
            {
                "path": "transactions/:status/:type",
                "component": TransactionReportComponent
            }
        ]
    }
];
@NgModule({
    imports: [
        CommonModule,
        AmChartsModule,
        FormsModule,
        PipeModule,
        NgxPaginationModule,
        DataTablesModule,
        AngularMultiSelectModule,
        RouterModule.forChild(routes), LayoutModule
    ], exports: [
        RouterModule
    ], declarations: [
        IndexComponent,
        NibssComponent,
        BillerComponent,
        BankComponent,
        PsspComponent,
        TransactionReportComponent,
        MandateReportComponent
    ]
})
export class IndexModule {



}