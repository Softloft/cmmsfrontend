import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { NibssService } from "../../../../../../services/api-service/nibss.service";
import { NotificationService } from "../../../../../../services/notification.service";
import { Subject } from "rxjs/Subject";
import { BillerService } from "../../../../../../services/api-service/biller.service";

import Swal from "sweetalert2"; declare const $;
@Component({
    selector: 'app-transaction-report',
    templateUrl: 'transaction-report.component.html',
    styles: []
})
export class TransactionReportComponent implements OnInit, OnDestroy {
    public urlId = 0;
    public status = '';
    public type = '';
    public duration = 1;
    public durationtype = 'year';
    public durationLabel = 1;
    public durationtypeLabel = 'year';
    dtOptions: DataTables.Settings = {};
    dtTrigger: Subject<TransactionReportComponent> = new Subject();

    searchOption = 'Search Type';
    public transactionType = '';
    public current: number = 1;
    public dateRangeSearchForm: FormGroup;
    public profile: any;
    public updateObj = [];
    public p = 1;
    public totalElement = 0;
    public id = 0;
    public moduleName = 'Transactions';
    public booleans = {
        loader: false,
        loadingStatus: false,
        loadsearch: false,
    };

    public list = {
        allContent: [],
        roles: [],
        data: [],
        details: [],
        newObj: [],
        oldObj: [],
        updatedObj: [],
    };

    static searchDateRangeForm = () => {
        return {
            from: ['', Validators.compose([])],
            to: ['', Validators.compose([])],
        }
    };
    constructor(private notification: NotificationService,
        private fb: FormBuilder,
        private router: Router,
        private billerService: BillerService,
        private nibssservice: NibssService,
        private route: ActivatedRoute) {
        this.dateRangeSearchForm = this.fb.group(TransactionReportComponent.searchDateRangeForm());
    }

    ngOnInit() {
        this.dtOptions = {
            pagingType: 'full_numbers',
            pageLength: 2
        };
        this.type = this.route.snapshot.paramMap.get('type');
        this.status = this.route.snapshot.paramMap.get('status');
        console.log('status :: ', this.status);

        //  this.dtTrigger.next();
        this.switch(this.type);
    }

    switch(type) {
        this.type = type;
        if (!this.status) {
            this.getTransactions(type);
        } else {
            this.durationtype = 'month';
            this.duration = 6;
            this.getTransactions(type)
        }
    }

    getTransactions(type) {
        this.booleans.loader = true;
        this.list.allContent = [];
        this.nibssservice.getDashboard(`${type}/${this.durationtype}/${this.duration}`)
            .subscribe((response) => {
                this.durationLabel = this.duration;
                this.durationtypeLabel = this.durationtype;
                this.booleans.loader = false;
                this.totalElement = response.length;
                this.list.allContent = response;
                this.transactionType = this.type;
                //    this.dtTrigger.next();
            },
            error => {
                this.booleans.loader = false;
                this.notification.error('Unable to load Transaction', error);
            })
    }

    onSearchTransaction() {
        this.getTransactions(this.type);
    }

    ngOnDestroy(): void {
        // Do not forget to unsubscribe the event
        this.dtTrigger.unsubscribe();
    }
}
