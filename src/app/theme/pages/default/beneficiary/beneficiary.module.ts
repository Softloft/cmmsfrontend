import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BootstrapSwitchModule } from "angular2-bootstrap-switch";
import { NgxPaginationModule } from "ngx-pagination";
import { AngularMultiSelectModule } from "angular2-multiselect-dropdown";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule, Routes } from "@angular/router";
import { LayoutModule } from "../../../layouts/layout.module";
import { DefaultComponent } from "../default.component";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { BeneficiaryComponent } from "./beneficiary.component";
import { PipeModule } from "../../../../../shared/modules/pipe.module";

const routes: Routes = [
    {
        "path": "",
        "component": DefaultComponent,
        "children": [
            {
                "path": "",
                "component": BeneficiaryComponent
            }
        ]
    }
];

@NgModule({
    imports: [
        CommonModule,
        LayoutModule,
        FormsModule,
        NgbModule,
        PipeModule,
        RouterModule.forChild(routes),
        ReactiveFormsModule,
        AngularMultiSelectModule,
        NgxPaginationModule,
        BootstrapSwitchModule
    ],
    declarations: [BeneficiaryComponent]
})
export class BeneficiaryModule {
}