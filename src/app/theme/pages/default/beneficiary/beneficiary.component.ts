import { Component, OnInit } from "@angular/core";
import { BillerService } from "../../../../../services/api-service/biller.service";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { NotificationService } from "../../../../../services/notification.service";
import { BankService } from "../../../../../services/api-service/bank.service";
import { UserService } from "../../../../../services/user.service";
import { JSON2PDFTABLE } from "../../../../../shared/export/json2pdf-table";
import { JSON2CSV } from "../../../../../shared/export/json2csv";
import { ActivatedRoute, Router } from "@angular/router";
import { ApprovalService } from "../../../../../services/api-service/approval.service";

import Swal from "sweetalert2";
import { APPROVAL_MESSAGE_USER, CONSTANTS } from "../../../../../utils/constants";
declare const $;
@Component({
    selector: 'app-beneficiary',
    templateUrl: 'beneficiary.component.html',
    styles: []
})
export class BeneficiaryComponent implements OnInit {
    public moduleName = 'Beneficiary';
    public modal_title = '';

    public createForm: FormGroup;
    public createFormSearch: FormGroup;
    public formReason: FormGroup;
    public id = 0;
    public details = [];
    public p = 1;
    public totalElement = '';
    public submit_type = 'Submit';
    /*******start of Approval(maker/checker) declaration*******/
    public approvalStatus = '';
    public approvalType = '';
    public statusType = '';
    public approvalName = 'beneficiary';
    public authorization = '';
    /*******end of Approval(maker/checker) declaration*******/
    public list = {
        details: [],
        allContent: [],
        searchContent: []
    };

    public booleans = {
        loader: false,
        searchStatus: false,
        getAllloader: false,
        loadingSearch: false,
        searchloader: false,
        loadsubmit: false,
        csvsearchloader: false,
        pdfsearchloader: false,
        initiator: false,
        authorizer: false,
        loadAction: false,
    };

    public settingsBank = {};
    public selectedBank = [];
    public itemListBank = [];

    private formdataSearch = function() {
        return {
            accountName: ['', Validators.compose([Validators.required])],
            accountNumber: ['', Validators.compose([Validators.minLength(10), Validators.maxLength(10)])],
            bankId: ['', Validators.compose([Validators.required])],
            activated: ['', Validators.compose([Validators.required])],
            beneficiaryName: ['', Validators.compose([Validators.required])],
        };
    };

    private formdata = function() {
        return {
            accountName: ['', Validators.compose([Validators.required])],
            accountNumber: ['', Validators.compose([Validators.minLength(10), Validators.maxLength(10)])],
            bank: ['', Validators.compose([Validators.required])],
            beneficiaryName: ['', Validators.compose([Validators.required])],
        };
    };

    private formdataReason = function() {
        return {
            reason: ['', Validators.compose([Validators.required])],
        }
    };

    constructor(private billerService: BillerService,
        private bankservice: BankService,
        private userservice: UserService,
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private approvalservice: ApprovalService,
        private notification: NotificationService,
        private fb: FormBuilder) {
        this.createForm = this.fb.group(this.formdata());
        this.createFormSearch = this.fb.group(this.formdataSearch());
        this.formReason = this.fb.group(this.formdataReason());
    }

    ngOnInit() {
        this.settingsBank = {
            singleSelection: true,
            text: "Select Bank",
            enableSearchFilter: true,
            badgeShowLimit: 5
        };
        this.getActiveBank();
        const status = this.activatedRoute.snapshot.paramMap.get('status');
        if (status && status !== '') {
            this.getParam();
        } else {
            this.getAllBeneficiary(0);
        }

        this.details = this.userservice.getAuthUser();
        if (this.details['roles'][0] === 'NIBSS ADMIN AUTHORIZER'
            || this.details['roles'][0] === 'BANK ADMIN AUTHORIZER'
            || this.details['roles'][0] === 'BILLER ADMIN AUTHORIZER') {
            this.booleans.authorizer = true;
        }
        if (this.details['roles'][0] === 'NIBSS ADMIN INITIATOR'
            || this.details['roles'][0] === 'BANK ADMIN INITIATOR'
            || this.details['roles'][0] === 'BILLER ADMIN INITIATOR') {
            this.booleans.initiator = true;
        }
    }

    getMainPage() {
        this.approvalStatus = '';
        this.router.navigate([this.approvalName]);
    }

    getPage(event) {
        this.p = event;
        this.list.allContent = [];
        if (!this.booleans.searchStatus) {
            this.getAllBeneficiary(this.p - 1);
        } else {
            this.searchMethod(this.p - 1)
        }
    }

    getAllBeneficiary(pageNo) {
        this.booleans.loader = true;
        this.billerService.getAllBeneficiary(pageNo)
            .subscribe(response => {
                this.booleans.loader = false;
                this.list.allContent = response.content;
                this.totalElement = response.totalElements;
            },
            error => {
                this.booleans.loader = false;
                this.notification.error('Unable to load Beneficiary', error);
            })
    }

    getActiveBank() {
        this.bankservice.getActiveBank()
            .subscribe(response => {
                response.forEach(value => {
                    this.itemListBank.push({
                        id: value.code,
                        itemName: value.name,
                        code: value.code
                    });
                });
            },
            error => {
                this.notification.error('Unable to load active bank', error);
            })
    }

    edit(data) {
        this.id = data.id;
        this.submit_type = 'Update';
        const editForm = function() {
            return {
                beneficiaryId: [data.id, Validators.compose([])],
                accountName: [data.accountName, Validators.compose([Validators.required])],
                accountNumber: [data.accountNumber, Validators.compose([Validators.minLength(10), Validators.maxLength(10)])],
                bank: [(data.bank && data.bank.code) ? [{
                    'id': data.bank.code,
                    'itemName': data.bank.name
                }] : '', Validators.compose([Validators.required])],
                beneficiaryName: [data.beneficiaryName, Validators.compose([Validators.required])],
            };
        };
        this.createForm = this.fb.group(editForm());
        $('#CreateModal').modal('show');
    }

    submit() {
        this.extractIds();
        this.booleans.loadsubmit = true;
        this.billerService.createBeneficiary(this.createForm.value)
            .subscribe(response => {
                this.booleans.loadsubmit = false;
                this.list.allContent.unshift(response);
                this.createForm.reset();
                $('#CreateModal').modal('hide');
                this.notification.success(CONSTANTS.SUBMIT_RESPONSE)
            },
            error => {
                this.booleans.loadsubmit = false;
                this.notification.error('Unable to load Beneficiary', error);
            })
    }

    toggle(data) {
        this.id = data.id;
        let statusMessage = '';
        statusMessage = (data.activated) ? 'Disapprove' : 'Approve';
        Swal({
            title: '',
            html: '<b style="font-weight: bold">' + APPROVAL_MESSAGE_USER(statusMessage) + '</b>',
            type: null,
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes!'
        }).then((result) => {
            if (result.value) {
                this.billerService.toggleBeneficiary({ id: data.id })
                    .subscribe(response => {
                        this.list.allContent.forEach((value, i) => {
                            if (value.id === this.id) {
                                this.list.allContent[i] = response;
                            }
                        });
                        this.notification.success(CONSTANTS.TOGGLE_RESPONSE);
                    },
                    error => {
                        this.notification.error(CONSTANTS.ERROR_RESPONSE, error);
                    });
            }
        })
    }

    private extractIds() {
        if (this.createForm.value['bank'] && this.createForm.value['bank'][0]) {
            this.createForm.value['bankCode'] = this.createForm.value['bank'][0]['id'];
        } else {
            this.createForm.value['bankId'] = this.details['bank']['id'];
        }
    }


    updateBeneficiary() {
        this.extractIds();
        this.booleans.loadsubmit = true;
        this.billerService.updateBeneficiary(this.createForm.value)
            .subscribe(response => {
                this.list.allContent.forEach((value, i) => {
                    if (value.id === this.id) {
                        this.list.allContent[i] = response;
                    }
                });
                /* if (this.approvalStatus !== '') {
                    this.list.allContent.forEach((value, i) => {
                        if (this.id === value['id']) {
                            this.list.allContent.splice(i, 1);
                        }
                    });
                }*/
                this.notification.success(CONSTANTS.UPDATE_RESPONSE);

                this.booleans.loadsubmit = false;
                $('#CreateModal').modal('hide');
            },
            error => {
                this.booleans.loadsubmit = false;
                this.notification.error(CONSTANTS.ERROR_RESPONSE, error);
            });
    }

    view(data) {
        this.list.details = data;
        $('#viewModal').modal('show');
    }

    open() {
        this.submit_type = 'Submit';
        this.createForm.reset();
        $('#CreateModal').modal('show');
    }

    searchMethod(pageNo) {
        this.booleans.searchStatus = true;
        this.booleans.loadingSearch = true;
        this.billerService.beneficiarySearch(this.createFormSearch.value, pageNo)
            .subscribe((response) => {
                this.booleans.loader = false;
                this.booleans.loadingSearch = false;
                if (response.content.length === 0) {
                    this.notification.error('No Result Found')
                } else {
                    this.list.allContent = response.content;
                    if (this.booleans.pdfsearchloader || this.booleans.csvsearchloader) {
                        this.exportAs();
                    }
                    $('#searchModal').modal('hide');
                }
                this.booleans.csvsearchloader = false;
                this.booleans.pdfsearchloader = false;
            },
            error => {
                this.booleans.loader = false;
                this.booleans.csvsearchloader = false;
                this.booleans.pdfsearchloader = false;
                this.booleans.loadingSearch = false;
                this.notification.error('Unable to Search', error);
            })
    }

    private exportAs() {
        if (this.booleans.pdfsearchloader) {
            this.getPdf()
        } else if (this.booleans.csvsearchloader) {
            this.getCsv();
        }
    }

    exportCsvSearch() {
        this.booleans.csvsearchloader = true;
        this.searchMethod(0);
    }

    exportPdfSearch() {
        this.booleans.pdfsearchloader = true;
        this.searchMethod(0);
    }

    getCsv(event?) {
        if (event === 'all') {
            this.retrieveAllBeneficiary(0, this.totalElement, 'csv');
        } else {
            const exportContent = this.list.allContent;
            this.exportToCsv(exportContent);
        }
    }

    private exportToCsv(exportContent) {
        const csvArray = [];
        const fields = [
            'Beneficiary Name',
            'Account No.',
            'Account Name',
            'Bank',
        ];
        exportContent.forEach((value) => {
            csvArray.push({
                'Beneficiary Name': value.beneficiaryName,
                'Account No.': value.accountNumber,
                'Account Name': value.accountName,
                'Bank': value.bank.name
            })
        });
        JSON2CSV(fields, csvArray, 'Beneficiary');
    }

    getPdf(event?) {
        if (event === 'all') {
            this.retrieveAllBeneficiary(0, this.totalElement, 'pdf');
        } else {
            const exportContent = this.list.allContent;
            this.exportToPdf(exportContent);
        }
    }

    private exportToPdf(exportContent) {
        const rows = [];
        const columns = ["s/n  ", "Beneficiary Name", "Account No.", "Account Name", "Bank"];
        exportContent.forEach((value, i) => {
            rows.push([i + 1, value.beneficiaryName, value.accountNumber, value.accountName, value.bank.name])
        });
        JSON2PDFTABLE(columns, rows, 'Beneficiary');
    }

    retrieveAllBeneficiary(pageNo, totalNumber, type) {
        this.booleans.getAllloader = true;
        this.billerService.retrieveAllBeneficiary(pageNo, totalNumber)
            .subscribe(response => {
                this.booleans.getAllloader = false;
                (type === 'csv') ? this.exportToCsv(response.content) : this.exportToPdf((response.content));
            },
            error => {
                this.booleans.getAllloader = false;
                this.notification.error('Unable to load NIBSS Users', error);
            })
    }

    getSearchModal() {
        $('#searchModal').modal('show');
    }

    /***********************************APPROVAL(MAKER AND CHECKER)*****************************************/
    public getParam(param?) {
        this.list.allContent = [];
        if (param) {
            switch (param) {
                case this.approvalName + '/pending':
                    this.statusType = 'UNAUTHORIZED';
                    this.approvalStatus = 'Pending';
                    break;
                case this.approvalName + '/approved':
                    this.statusType = 'AUTHORIZED';
                    this.approvalStatus = 'Approved';
                    break;
                case this.approvalName + '/disapproved':
                    this.statusType = 'REJECTED';
                    this.approvalStatus = 'Disapproved';
                    break;
            }
            this.router.navigate(['/' + param]);
        } else {
            const status = this.activatedRoute.snapshot.paramMap.get('status');
            switch (status) {
                case 'pending':
                    this.statusType = 'UNAUTHORIZED';
                    this.approvalStatus = 'Pending';
                    break;
                case 'approved':
                    this.statusType = 'AUTHORIZED';
                    this.approvalStatus = 'Approved';
                    break;
                case 'disapproved':
                    this.statusType = 'REJECTED';
                    this.approvalStatus = 'Disapproved';
                    break;
            }
        }
        this.getAuthorization(this.statusType, 0);
    }

    getApprovalPage(event) {
        this.p = +event;
        this.list.allContent = [];
        this.getAuthorization(this.statusType, this.p - 1);
    }


    getAuthorization(approval_type, pageNo) {
        this.booleans.loader = true;
        this.approvalservice.getAuthorization(approval_type, pageNo, this.userservice.getAuthUser())
            .subscribe(response => {
                this.booleans.loader = false;
                this.list.allContent = response.content;
                this.totalElement = response.totalElements;
            },
            error => {
                this.booleans.loader = false;
                this.notification.error('Unable to load content', error);
            })
    }

    getReason(data, authorizationStatus) {
        this.id = data.id;
        this.authorization = authorizationStatus;
        this.list.details = data;
        this.approvalType = 'disapprove';
        $('#reasonModal').modal('show');
    }

    disapprove() {
        let action = '';
        this.booleans.loadAction = true;
        switch (this.authorization) {
            case 'UNAUTHORIZED_CREATE':
                action = 'REJECT_CREATE';
                break;
            case 'UNAUTHORIZED_UPDATE':
                action = 'REJECT_UPDATE'
                break;
            case 'UNAUTHORIZED_TOGGLE':
                action = 'REJECT_TOGGLE'
                break;
        }

        this.Approve(this.id, action, this.formReason.value['reason']);
    }

    approve(id, authorizationStatus) {
        this.id = id;
        let action = '';
        switch (authorizationStatus) {
            case 'UNAUTHORIZED_CREATE':
                action = 'APPROVE_CREATE';
                break;
            case 'UNAUTHORIZED_UPDATE':
                action = 'APPROVE_UPDATE'
                break;
            case 'UNAUTHORIZED_TOGGLE':
                action = 'APPROVE_TOGGLE'
                break;
        }
        this.approvalType = 'approve';
        this.Approve(id, action);
    }

    Approve(id, authorizationStatus, reason?) {
        this.booleans.loadAction = true;
        this.approvalservice.approve({
            id: id,
            reason: (reason && reason !== '') ? reason : ''
        }, authorizationStatus, this.userservice.getAuthUser())
            .subscribe(response => {
                this.booleans.loadAction = false;
                this.list.allContent.forEach((value, i) => {
                    if (id === value['id']) {
                        this.list.allContent.splice(i, 1);
                    }
                });
                this.formReason.reset();
                this.notification.success('Approval was successful');
            },
            error => {
                this.booleans.loadAction = false;
                this.notification.error(CONSTANTS.ERROR_RESPONSE, error);
            })
    }
}