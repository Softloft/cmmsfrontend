import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormControl, FormGroup, Validators } from "@angular/forms";
import { ScriptLoaderService } from "../../../../_services/script-loader.service";
import { FormService } from "../../../../../services/api-service/formService";
import { NotificationService } from "../../../../../services/notification.service";
import { NibssService } from "../../../../../services/api-service/nibss.service";
import { APPROVAL_MESSAGE_USER, CONSTANTS } from "../../../../../utils/constants";

import { DomSanitizer } from "@angular/platform-browser";
import { JSON2PDFTABLE } from "../../../../../shared/export/json2pdf-table";
import { JSON2CSV } from "../../../../../shared/export/json2csv";
import { UserService } from "../../../../../services/user.service";
import { ActivatedRoute, Router } from "@angular/router";
import { ApprovalService } from "../../../../../services/api-service/approval.service";

import Swal from "sweetalert2";
declare const $;

@Component({
    selector: 'app-nibss',
    templateUrl: 'nibss.component.html',
    styles: []
})
export class NibssComponent implements OnInit {
    public itemsPerPage = CONSTANTS.ITEMS_PER_PAGE;
    public moduleName = 'NIBSS';
    public userType = 'not_user';
    public approvedStatus = '';
    public searchtext = '';
    public user = [];
    public searchItemLength = 0;
    public details = [];
    public totalElement = 0;
    public createForm: FormGroup;
    public createFormSearch: FormGroup;
    public formReason: FormGroup;
    public id = 0;
    public p = 1;
    public lga = null;
    public submit_type = 'Submit';
    public modal_title = 'Add';
    /*******start of Approval(maker/checker) declaration*******/
    public approvalStatus = '';
    public approvalType = '';
    public statusType = '';
    public approvalName = 'nibss';
    public authorization = '';
    public itemRoleList = [];
    public pagesize = 10;
    public pagesizeArray = CONSTANTS.ITEMS_PER_PAGE_ARRAY;
    /*******end of Approval(maker/checker) declaration*******/
    public list = {
        allContent: [],
        searchRoles: [],
        details: [],
        country: [],
        searchContent: [],
    };
    searchParams = {
        firstName: '',
        lastName: '',
        middleName: '',
        email: '',
        phoneNumber: '',
        city: '',
        role: '',
        state: '',
        lga: ''
    };

    public booleans = {
        loader: false,
        searchStatus: false,
        csvsearchloader: false,
        pdfsearchloader: false,
        getAllloader: false,
        loadingSearch: false,
        searchloader: false,
        loadCountry: false,
        loadState: false,
        loadLga: false,
        loadRole: false,
        loadsubmit: false,
        initiator: false,
        authorizer: false,
        loadAction: false,
    };

    itemListCountry = [];
    selectedItemsCountry = [];
    settingsCountry = {};

    itemListState = [];
    selectedItemsState = [];
    settingsState = {};

    itemListLga = [];
    selectedItemsLga = [];
    settingsLga = {};

    public settings = {};
    public selectedItems = [];
    public itemList = [];

    private formdataSearch = function() {
        return {
            firstName: ['', Validators.compose([])],
            lastName: ['', Validators.compose([])],
            middleName: ['', Validators.compose([])],
            city: ['', Validators.compose([])],
            staffNumber: ['', Validators.compose([])],
            userAuthorisationType: ['', Validators.compose([])],
            email: ['', Validators.compose([])],
            phoneNumber: ['', Validators.compose([])],
            role: ['', Validators.compose([])],
            activated: ['', Validators.compose([])],
            createdAt: ['', Validators.compose([])],
        }
    };

    private formdata = function() {
        return {
            id: ['', Validators.compose([])],
            city: ['', Validators.compose([])],
            lga: ['', Validators.compose([])],
            country: ['', Validators.compose([])],
            state: ['', Validators.compose([])],
            countryId: ['', Validators.compose([])],
            stateId: ['', Validators.compose([])],
            staffNumber: ['', Validators.compose([])],
            houseNumber: ['', Validators.compose([])],
            streetName: ['', Validators.compose([])],
            userAuthorisationType: ['', Validators.compose([])],
            emailAddress: ['', Validators.compose([Validators.required, Validators.email])],
            phoneNumber: ['', Validators.compose([Validators.required, Validators.pattern(CONSTANTS.PHONE_REGEX)])],
            roleIds: ['', Validators.compose([Validators.required])],
            userId: ['', Validators.compose([])],
            name: new FormGroup({
                firstName: new FormControl('', Validators.required),
                lastName: new FormControl('', Validators.required),
                middleName: new FormControl(''),
                title: new FormControl(''),
            }),
        }
    };

    private formdataReason = function() {
        return {
            reason: ['', Validators.compose([Validators.required])],
        }
    };

    constructor(private _script: ScriptLoaderService,
        private fb: FormBuilder,
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private approvalservice: ApprovalService,
        private domsanitizer: DomSanitizer,
        private formservice: FormService,
        private userservice: UserService,
        private nibssservice: NibssService,
        private notification: NotificationService) {
        this.createForm = this.fb.group(this.formdata());
        this.createFormSearch = this.fb.group(this.formdataSearch());
        this.formReason = this.fb.group(this.formdataReason());
    }

    ngOnInit() {
        this.user = this.userservice.getAuthUser();
        this.getSearchRole('NIBSS')

        const status = this.activatedRoute.snapshot.paramMap.get('status');
        if (status && status !== '') {
            this.getParam();
            this.loadFormFunctions();
        } else {
            this.loadFunctions();
        }

        this.list.allContent = [];

        this.details = this.userservice.getAuthUser();
        if (this.details['roles'][0].toUpperCase() === 'NIBSS SUPER ADMIN AUTHORIZER' || this.details['roles'][0].toUpperCase() === 'NIBSS AUTHORIZER') {
            this.booleans.authorizer = true;
        }
        if (this.details['roles'][0] === 'NIBSS SUPER ADMIN INITIATOR' || this.details['roles'][0] === 'NIBSS INITIATOR') {
            this.booleans.initiator = true;
        }
    }

    loadFunctions() {
        this.getNibss(0, this.pagesize);
        this.loadFormFunctions()
    }

    loadFormFunctions() {
        this.getRole('NIBSS');
        this.getCountry();
        this.settings = {
            singleSelection: true,
            text: "Select Fields",
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            searchPlaceholderText: 'Search Fields',
            enableSearchFilter: true,
            badgeShowLimit: 5,
            groupBy: "category"
        };

        this.settingsLga = {
            singleSelection: true,
            text: "Select Lga",
            enableSearchFilter: true,
            badgeShowLimit: 5
        };

        this.settingsState = {
            singleSelection: true,
            text: "Select State",
            enableSearchFilter: true,
            badgeShowLimit: 5
        };

        this.settingsCountry = {
            singleSelection: true,
            text: "Select Country",
            enableSearchFilter: true,
            badgeShowLimit: 5
        };
    }

    openModal() {
        this.modal_title = 'Add';
        this.submit_type = 'Submit';
        $('#CreateModal').modal('show');
    }

    getCsv(event?) {
        if (event === 'all') {
            this.getAllNibss(0, this.totalElement, 'csv');
        } else {
            const exportContent = this.list.allContent;
            this.exportToCsv(exportContent);
        }
    }

    private exportToCsv(exportContent) {
        const csvArray = [];
        const fields = ['First Name', 'Last Name', 'Email', 'Role', 'Created Date', 'Status'];
        exportContent.forEach((value) => {
            if (value.roles[0]['name'] === 'NIBSS_INITIATOR') {
                value.roles[0]['name'] = 'NIBSS_ADMIN_INITIATOR';
            } else if (value.roles[0]['name'] === 'NIBSS_AUTHORIZER') {
                value.roles[0]['name'] = 'NIBSS_ADMIN_AUTHORIZER';
            }
            csvArray.push({
                'First Name': value['name'] ? value['name']['firstName'] : '',
                'Last Name': value['name'] ? value['name']['lastName'] : '',
                'Email': value.emailAddress,
                'Role': value.roles[0]['name'].split('_').join(''),
                'Created Date': new Date(value.createdAt).toString().split('GMT')[0],
                'Status': (value.activated) ? 'Active' : 'Inactive',
            })
        });
        JSON2CSV(fields, csvArray, 'nibss');
    }

    private exportToPdf(exportContent) {
        const rows = [];
        const columns = ["s/n  ", "First Name", "Last Name", "Email", "Role", "Date Created", "Status"];
        exportContent.forEach((value, i) => {
            if (value.roles[0]['name'] === 'NIBSS_INITIATOR') {
                value.roles[0]['name'] = 'NIBSS_ADMIN_INITIATOR';
            } else if (value.roles[0]['name'] === 'NIBSS_AUTHORIZER') {
                value.roles[0]['name'] = 'NIBSS_ADMIN_AUTHORIZER';
            }
            rows.push([i + 1, value['name'] ? value['name']['firstName'] : '', value['name'] ? value['name']['lastName'] : '', value.emailAddress, value.roles[0]['name'].split('_').join(' '), new Date(value.createdAt).toString().split('GMT')[0], (value.activated) ? 'Active' : 'Inactive'])
        });
        JSON2PDFTABLE(columns, rows, this.moduleName);
    }

    getPdf(event?) {
        if (event === 'all') {
            this.getAllNibss(0, this.totalElement, 'pdf');
        } else {
            this.exportToPdf(this.list.allContent);
        }
    }


    toggle(data) {
        this.id = data.id;
        let statusMessage = '';
        statusMessage = (data.activated) ? 'Disapprove' : 'Approve';
        Swal({
            title: '',
            html: '<b style="font-weight: bold">' + APPROVAL_MESSAGE_USER(statusMessage) + '</b>',
            type: null,
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes!'
        }).then((result) => {
            if (result.value) {
                this.nibssservice.toggle({ id: data.id })
                    .subscribe(response => {
                        response['activated'] = !response['activated'];
                        this.list.allContent.forEach((value, i) => {
                            if (value.id === this.id) {
                                console.log('respoonse toggle :: ', response);
                                this.list.allContent[i] = response;
                            }
                        });
                        this.notification.success(CONSTANTS.TOGGLE_RESPONSE);
                    },
                    error => {
                        this.notification.error(CONSTANTS.ERROR_RESPONSE, error);
                    });
            }
        })
    }

    exportSearchMethod(pageNo) {
        this.booleans.loadingSearch = true;
        this.nibssservice.search(this.createFormSearch.value, pageNo)
            .subscribe((response) => {
                this.list.allContent = response.content;
                this.exportAs();
                this.booleans.csvsearchloader = false;
                this.booleans.pdfsearchloader = false;
                this.booleans.loadingSearch = false;
            },
            error => {
                this.booleans.csvsearchloader = false;
                this.booleans.pdfsearchloader = false;
                this.booleans.loadingSearch = false;
                this.notification.error('Unable to Search', error);
            })
    }

    searchMethod(pageNo) {
        this.booleans.searchStatus = true;
        this.booleans.loadingSearch = true;
        this.nibssservice.search(this.createFormSearch.value, pageNo)
            .subscribe((response) => {
                this.booleans.loader = false;
                this.booleans.loadingSearch = false;
                if (response.content.length === 0) {
                    this.notification.error('No Result Found')
                } else {
                    this.list.allContent = response.content;
                    if (this.booleans.pdfsearchloader || this.booleans.csvsearchloader) {
                        this.exportAs();
                    }
                    $('#searchModal').modal('hide');
                }
                this.booleans.csvsearchloader = false;
                this.booleans.pdfsearchloader = false;

            },
            error => {
                this.booleans.loader = false;
                this.booleans.csvsearchloader = false;
                this.booleans.pdfsearchloader = false;
                this.booleans.loadingSearch = false;
                this.notification.error('Unable to Search', error);
            })
    }

    private exportAs() {
        if (this.booleans.pdfsearchloader) {
            this.getPdf()
        } else if (this.booleans.csvsearchloader) {
            this.getCsv();
        }
    }

    exportCsvSearch() {
        this.booleans.csvsearchloader = true;
        this.searchMethod(0);
    }

    exportPdfSearch() {
        this.booleans.pdfsearchloader = true;
        this.searchMethod(0);
    }

    onItemSelect(item: any) {
    }

    OnItemDeSelect(item: any) {
    }

    onSelectAll(items: any) {
    }

    onDeSelectAll(items: any) {
    }

    onItemSelectLga(item: any) {
        this.selectedItemsLga = [{ 'id': item.id, 'itemName': item.itemName }];
    }

    OnItemDeSelectLga(item: any) {
    }

    onSelectAllLga(items: any) {
    }

    onDeSelectAllLga(items: any) {
    }

    onItemSelectState(item: any) {
        this.itemListLga = [];
        this.getLgaByState(item.id);
    }

    getCountry() {
        this.booleans.loadCountry = true;
        this.formservice.getCountry()
            .subscribe((response) => {
                response.forEach((value) => {
                    this.itemListCountry.push({
                        "id": value.id,
                        "itemName": value.name
                    });
                });
                this.booleans.loadCountry = false;
            },
            error => {
                this.booleans.loadCountry = false;
                this.notification.error('Unable to load country', error);
            })
    }

    getStateByCountry(id) {
        this.itemListState = [];
        this.booleans.loadState = true;
        this.formservice.getStateByCountry(id)
            .subscribe((response) => {
                response.forEach((value) => {
                    this.itemListState.push({
                        "id": value.id,
                        "itemName": value.name
                    });
                });
                this.booleans.loadState = false;
            },
            (error) => {
                this.booleans.loadState = false;
                this.notification.error('Unable to load state', error);
            })
    }

    getLgaByState(id) {
        this.booleans.loadLga = true;
        this.formservice.getLgaByState(id)
            .subscribe((response) => {
                response.forEach((value) => {
                    this.itemListLga.push({
                        "id": value.id,
                        "itemName": value.name
                    });
                });
                this.booleans.loadLga = false;
            },
            (error) => {
                this.booleans.loadLga = false;
                this.notification.error('Unable to load lga', error);
            })
    }

    getRole(event) {
        this.booleans.loadRole = true;
        this.formservice.getRoleByUser(event)
            .subscribe((response) => {
                this.itemList = [];
                response.forEach(value => {
                    if (this.details['roles'][0].toUpperCase() === 'NIBSS SUPER ADMIN INITIATOR') {
                        if (value.name === 'NIBSS_INITIATOR' || value.name === 'NIBSS_AUTHORIZER') {
                            this.filterByUserRole(value);
                            this.filterRoleList(value);
                        }
                    } else if (this.details['roles'][0].toUpperCase() === 'NIBSS INITIATOR') {
                        if (value.name === 'NIBSS_INITIATOR' || value.name === 'NIBSS_AUTHORIZER' || value.name === 'NIBSS_USER') {
                            this.filterRoleList(value);
                        }
                    }
                });
                this.booleans.loadRole = false;
            },
            (error) => {
                this.booleans.loadRole = false;
                this.notification.error('Unable to load role', error);
            })
    }

    getSearchRole(event) {
        this.booleans.loadRole = true;
        this.formservice.getSearchRole(event)
            .subscribe((response) => {
                this.list.searchRoles = response;
                this.booleans.loadRole = false;
            },
            (error) => {
                this.booleans.loadRole = false;
                this.notification.error('Unable to load role', error);
            })
    }

    /* getRoleList(event) {
     this.booleans.loadRole = true;
     this.formservice.getRoleByUser(event)
     .subscribe((response) => {
     this.itemList = [];
     response.forEach(value => {
     if (value.name === 'NIBSS_INITIATOR' || value.name === 'NIBSS_AUTHORIZER') {
     if(value.name === 'NIBSS_INITIATOR') {
     value.name = 'NIBSS_ADMIN_INITIATOR'
     }else if(value.name === 'NIBSS_AUTHORIZER') {
     value.name = 'NIBSS_ADMIN_AUTHORIZER'
     }
     this.filterRoleList(value);
     }else{
     this.itemList.push({
     id: value.id,
     itemName: value.name.split('_').join(' '),
     category: value.userType
     })
     }
     });
     this.booleans.loadRole = false;
     },
     (error) => {
     this.booleans.loadRole = false;
     this.notification.error('Unable to load role', error);
     })
     }
     */

    private filterRoleList(value) {
        console.log('user role :: ', value.name)
        if (value.name === 'NIBSS_INITIATOR') {
            value['id'] = 'NIBSS_INITIATOR';
            value.name = 'NIBSS_ADMIN_INITIATOR';
        } else if (value.name === 'NIBSS_AUTHORIZER') {
            value['id'] = 'NIBSS_AUTHORIZER';
            value.name = 'NIBSS_ADMIN_AUTHORIZER';
        } else if (value.name === 'NIBSS_USER') {
            value.name = 'NIBSS_USER';
        }
        this.itemRoleList.push({
            id: value['id'],
            itemName: value.name.split('_').join(' '),
            category: value.userType,
            description: value.description
        })
        this.itemList.push({
            id: value.id,
            itemName: value.name.split('_').join(' '),
            category: value.userType,
            description: value.description
        })
    }

    private filterByUserRole(value) {
        if (value.name === 'NIBSS_INITIATOR') {
            value.name = 'NIBSS_ADMIN_INITIATOR'
        } else if (value.name === 'NIBSS_AUTHORIZER') {
            value.name = 'NIBSS_ADMIN_AUTHORIZER'
        }
        this.itemList.push({
            id: value.id,
            itemName: value.name.split('_').join(' '),
            category: value.userType,
            description: value.description
        })
    }

    submit() {
        this.extractIds();
        this.booleans.loadsubmit = true;
        this.nibssservice.postNibss(this.createForm.value)
            .subscribe(response => {
                this.list.allContent.unshift(response);
                this.booleans.loadsubmit = false;
                $('#CreateModal').modal('hide');
                this.createForm.reset();
            },
            error => {
                this.booleans.loadsubmit = false;
                this.notification.error(CONSTANTS.ERROR_RESPONSE, error);
            });
    }

    edit(data) {
        console.log('data :: ', data);
        this.selectedItems = [];
        this.submit_type = 'Update';
        this.list.details = data;
        if (data.contactDetails && data.contactDetails.state) {
            this.itemListState.push({ id: data.contactDetails.state.id, itemName: data.contactDetails.state.name })
            this.itemListCountry.push({ id: data.contactDetails.country.id, itemName: data.contactDetails.country.name })
        }
        if (data.contactDetails && data.contactDetails.lga) {
            this.itemListLga.push({ id: data.contactDetails.lga.id, itemName: data.contactDetails.lga.name })
        }
        data.roles.forEach((value) => {
            if (value.name === 'NIBSS_INITIATOR') {
                value.name = 'NIBSS_ADMIN_INITIATOR';
                this.selectedItems.push({
                    id: value.id,
                    itemName: value.name.split('_').join(' '),
                    category: value.userType
                })
            } else if (value.name === 'NIBSS_AUTHORIZER') {
                value.name = 'NIBSS_ADMIN_AUTHORIZER';
                this.selectedItems.push({
                    id: value.id,
                    itemName: value.name.split('_').join(' '),
                    category: value.userType
                })
            }
        });

        //        this.getLga(data.user.contactDetails.lga.state.id);
        // this.selectedItemsRole = [{'id': data.user.contactDetails.lga.state.id, 'itemName': data.user.contactDetails.lga.state.name}]
        if (data.contactDetails && data.contactDetails.lga) {
            this.lga = [{ 'id': data.contactDetails.lga.id, 'itemName': data.contactDetails.lga.name }];
        }
        const formdata = function() {
            return {
                city: [(data.contactDetails && data.contactDetails.city) ? data.contactDetails.city : '', Validators.compose([])],
                lga: [(data.contactDetails && data.contactDetails.lga) ? [{
                    'id': data.contactDetails.lga.id,
                    'itemName': data.contactDetails.lga.name
                }] : '', Validators.compose([])],
                roleIds: ['', Validators.compose([Validators.required])],


                state: [(data.contactDetails && data.contactDetails.state) ? [{
                    'id': data.contactDetails.state.id,
                    'itemName': data.contactDetails.state.name
                }] : '', Validators.compose([])],
                country: [(data.contactDetails && data.contactDetails.country) ? [{
                    'id': data.contactDetails.country.id,
                    'itemName': data.contactDetails.country.name
                }] : '', Validators.compose([])],
                /*roleId: [[{
                 'id': data.role.id,
                 'itemName': data.role.name
                 }], Validators.compose([Validators.required])],*/
                emailAddress: [data.emailAddress, Validators.compose([Validators.required, Validators.email])],
                userAuthorisationType: [(data.makerCheckerType) ? data.makerCheckerType : '', Validators.compose([])],
                streetName: [(data.contactDetails && data.contactDetails.streetName) ? data.contactDetails.streetName : '', Validators.compose([])],
                userId: [data.id, Validators.compose([])],
                id: [data.id, Validators.compose([])],
                name: new FormGroup({
                    firstName: new FormControl((data.name && data.name.firstName) ? data.name.firstName : '', Validators.required),
                    lastName: new FormControl((data.name && data.name.lastName) ? data.name.lastName : '', Validators.required),
                    middleName: new FormControl(((data.name && data.name.middleName) ? data.name.middleName : '')),
                }),
                phoneNumber: [data.phoneNumber, Validators.compose([Validators.required, Validators.pattern(CONSTANTS.PHONE_REGEX)])],
            }
        };
        this.createForm = this.fb.group(formdata());
        $('#CreateModal').modal('show');
    }

    update() {
        this.extractIds();
        this.booleans.loadsubmit = true;
        this.nibssservice.updateNibss(this.createForm.value)
            .subscribe(response => {
                this.list.allContent.forEach((value, i) => {
                    if (value.id === this.id) {
                        this.list.allContent[i] = response;
                    }
                });
                $('#CreateModal').modal('hide');
                this.createForm.reset();
                if (this.approvalStatus !== '') {
                    this.list.allContent.forEach((value, i) => {
                        if (response.id === value['id']) {
                            this.list.allContent.splice(i, 1);
                        }
                    });
                }
                this.notification.success(CONSTANTS.UPDATE_RESPONSE);
                this.booleans.loadsubmit = false;
            },
            error => {
                this.booleans.loadsubmit = false;
                this.notification.error(CONSTANTS.ERROR_RESPONSE, error);
            });
    }

    private extractIds() {
        const role = [];
        this.createForm.value['countryId'] = (this.createForm.value['country'] && this.createForm.value['country'][0] ? this.createForm.value['country'][0]['id'] : '');
        this.createForm.value['stateId'] = (this.createForm.value['state'] && this.createForm.value['state'][0] ? this.createForm.value['state'][0]['id'] : '');
        this.createForm.value['lgaId'] = (this.createForm.value['lga'] && this.createForm.value['lga'][0] ? this.createForm.value['lga'][0]['id'] : '');
        this.createForm.value['roleIds'].forEach((value) => {
            role.push(value.id);
        });
        this.createForm.value['roleId'] = role[0];
    }

    getMainPage() {
        this.approvalStatus = '';
        this.router.navigate([this.approvalName]);
    }

    onChangePageSize(event) {
        this.pagesize = event.target.value;
        this.getNibss(this.p - 1, this.pagesize);
    }

    getNibss(pageNo, pagesize) {
        this.booleans.searchStatus = false;
        this.booleans.loader = true;
        this.nibssservice.getNibss(pageNo, pagesize)
            .subscribe(response => {
                this.booleans.loader = false;
                this.list.allContent = response.content;
                this.totalElement = +response.totalElements;
            },
            error => {
                this.booleans.loader = false;
                this.notification.error('Unable to load NIBSS Users', error);
            })
    }

    getAllNibss(pageNo, totalNumber, type) {
        this.booleans.getAllloader = true;
        this.nibssservice.getAllNibss(pageNo, totalNumber)
            .subscribe(response => {
                this.booleans.getAllloader = false;
                (type === 'csv') ? this.exportToCsv(response.content) : this.exportToPdf((response.content));
            },
            error => {
                this.booleans.getAllloader = false;
                this.notification.error('Unable to load NIBSS Users', error);
            })
    }

    /*search() {
     this.booleans.searchloader = true;
     this.nibssservice.getNibss('search')
     .subscribe(response => {
     this.booleans.searchloader = false;
     this.list.searchContent = response.content;
     },
     error => {
     this.booleans.searchloader = false;
     this.notification.error('Unable to search NIBSS Users', error);
     })
     }*/

    getPage(event) {
        this.p = event;
        this.list.allContent = [];
        (!this.booleans.searchStatus) ? this.getNibss(this.p - 1, this.pagesize) : this.searchMethod(this.p - 1);
    }

    getSearchModal() {
        for (let params in this.searchParams) {
            this.searchParams[params] = '';
        }
        $('#searchModal').modal('show');

    }

    OnItemDeSelectState(item: any) {
        this.itemListLga = [];
    }

    onSelectAllState(items: any) {
    }

    onDeSelectAllState(items: any) {
    }

    onItemSelectCountry(item: any) {
        this.getStateByCountry(item.id);
    }

    OnItemDeSelectCountry(item: any) {
        this.itemListState = [];
        this.itemListLga = [];
    }

    onSelectAllCountry(items: any) {
    }

    onDeSelectAllCountry(items: any) {
    }


    /***********************************APPROVAL(MAKER AND CHECKER)*****************************************/
    public getParam(param?) {
        this.list.allContent = [];
        if (param) {
            switch (param) {
                case this.approvalName + '/pending':
                    this.statusType = 'UNAUTHORIZED';
                    this.approvalStatus = 'Pending';
                    break;
                case this.approvalName + '/approved':
                    this.statusType = 'AUTHORIZED';
                    this.approvalStatus = 'Approved';
                    break;
                case this.approvalName + '/disapproved':
                    this.statusType = 'REJECTED';
                    this.approvalStatus = 'Disapproved';
                    break;
            }
            this.router.navigate(['/' + param]);
        } else {
            const status = this.activatedRoute.snapshot.paramMap.get('status');
            switch (status) {
                case 'pending':
                    this.statusType = 'UNAUTHORIZED';
                    this.approvalStatus = 'Pending';
                    break;
                case 'approved':
                    this.statusType = 'AUTHORIZED';
                    this.approvalStatus = 'Approved';
                    break;
                case 'disapproved':
                    this.statusType = 'REJECTED';
                    this.approvalStatus = 'Disapproved';
                    break;
            }
        }
        this.getAuthorization(this.statusType, 0);
    }

    getApprovalPage(event) {
        this.p = +event;
        this.list.allContent = [];
        this.getAuthorization(this.statusType, this.p - 1);
    }


    getAuthorization(approval_type, pageNo) {
        this.booleans.loader = true;
        console.log('get authorization');
        this.approvalservice.getAuthorization(approval_type, pageNo, this.approvalName)
            .subscribe(response => {
                this.booleans.loader = false;
                this.list.allContent = response.content;
                this.totalElement = response.totalElements;
            },
            error => {
                this.booleans.loader = false;
                this.notification.error('Unable to load content', error);
            })
    }

    getReason(data, authorizationStatus) {
        this.id = data.id;
        this.authorization = authorizationStatus;
        this.list.details = data;
        this.approvalType = 'disapprove';
        $('#reasonModal').modal('show');
    }

    view(data) {
        this.list.details = data;
        $('#ViewModal').modal('show');
    }

    disapprove() {
        let action = '';
        this.approvedStatus = 'reject';
        this.booleans.loadAction = true;
        switch (this.authorization) {
            case 'UNAUTHORIZED_CREATE':
                action = 'REJECT_CREATE';
                break;
            case 'UNAUTHORIZED_UPDATE':
                action = 'REJECT_UPDATE'
                break;
            case 'UNAUTHORIZED_TOGGLE':
                action = 'REJECT_TOGGLE'
                break;
        }

        this.Approve(this.id, action, this.formReason.value['reason']);
    }

    approve(id, authorizationStatus) {
        this.id = id;
        let action = '';
        this.approvedStatus = '';
        switch (authorizationStatus) {
            case 'UNAUTHORIZED_CREATE':
                action = 'APPROVE_CREATE';
                break;
            case 'UNAUTHORIZED_UPDATE':
                action = 'APPROVE_UPDATE'
                break;
            case 'UNAUTHORIZED_TOGGLE':
                action = 'APPROVE_TOGGLE'
                break;
        }
        this.approvalType = 'approve';
        this.Approve(id, action);
    }

    Approve(id, authorizationStatus, reason?) {
        this.booleans.loadAction = true;
        this.approvalservice.approve({
            id: id,
            reason: (reason && reason !== '') ? reason : ''
        }, authorizationStatus, this.approvalName)
            .subscribe(response => {
                this.booleans.loadAction = false;
                this.list.allContent.forEach((value, i) => {
                    if (id === value['id']) {
                        this.list.allContent.splice(i, 1);
                    }
                });
                this.formReason.reset();
                const status = (this.approvedStatus === 'reject') ? 'Disapproval' : 'Approval';
                this.notification.success(status + ' was successful');
            },
            error => {
                this.booleans.loadAction = false;
                this.notification.error(CONSTANTS.ERROR_RESPONSE, error);
            })
    }
}
