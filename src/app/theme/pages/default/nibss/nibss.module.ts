import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NibssComponent } from './nibss.component';
import { NgxPaginationModule } from "ngx-pagination";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule, Routes } from "@angular/router";
import { LayoutModule } from "../../../layouts/layout.module";
import { BootstrapSwitchModule } from "angular2-bootstrap-switch";
import { DefaultComponent } from "../default.component";
import { AngularMultiSelectModule } from "angular2-multiselect-dropdown";
import { SharedModule } from "../../../../../shared/modules/shared.module";
import { PipeModule } from "../../../../../shared/modules/pipe.module";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { TransactionBillingComponent } from './transaction-billing/transaction-billing.component';

const routes: Routes = [
    {
        "path": "",
        "component": DefaultComponent,
        "children": [
            {
                "path": "",
                "component": NibssComponent
            },
            {
                "path": "transaction-billing",
                "component": TransactionBillingComponent
            },
            {
                "path": ":status",
                "component": NibssComponent
            }
        ]
    }
];

@NgModule({
    imports: [
        CommonModule,
        LayoutModule,
        FormsModule,
        SharedModule,
        PipeModule,
        NgbModule,
        RouterModule.forChild(routes),
        ReactiveFormsModule,
        AngularMultiSelectModule,
        NgxPaginationModule,
        BootstrapSwitchModule
    ],
    declarations: [NibssComponent, TransactionBillingComponent]
})
export class NibssModule {
}
