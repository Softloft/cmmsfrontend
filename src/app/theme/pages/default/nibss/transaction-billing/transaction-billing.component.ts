import { Component, OnInit } from "@angular/core";
import { NibssService } from "../../../../../../services/api-service/nibss.service";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { NotificationService } from "../../../../../../services/notification.service";
import { ActivatedRoute, Router } from "@angular/router";
import { saveAs } from "file-saver";

import Swal from "sweetalert2"; declare const $;
@Component({
    selector: 'app-transaction-billing',
    templateUrl: 'transaction-billing.component.html',
    styles: []
})
export class TransactionBillingComponent implements OnInit {
    public urlId = 0;
    searchOption = 'Search Type';
    public current: number = 1;
    public dateRangeSearchForm: FormGroup;
    public profile: any;
    public updateObj = [];
    public p = 1;
    public totalElement = 0;
    public id = 0;
    public moduleName = 'Transactions';
    public booleans = {
        loader: false,
        loadingStatus: false,
        loadsearch: false,
    };

    public list = {
        allContent: [],
        roles: [],
        data: [],
        details: [],
        newObj: [],
        oldObj: [],
        updatedObj: [],
    };

    static searchDateRangeForm = () => {
        return {
            from: ['', Validators.compose([])],
            to: ['', Validators.compose([])],
        }
    };

    constructor(private notification: NotificationService,
        private fb: FormBuilder,
        private router: Router,
        private nibssservice: NibssService,
        private route: ActivatedRoute) {
        this.dateRangeSearchForm = this.fb.group(TransactionBillingComponent.searchDateRangeForm());
    }

    ngOnInit() {
        this.urlId = +this.route.snapshot.paramMap.get('id');
        this.loadFunction();
    }


    loadFunction() {
        //  this.onGetTransactions(this.p - 1);
    }


    getPage(event) {
        this.p = event;
        this.list.allContent = [];
        this.onGetTransactions(this.p - 1);
    }

    onGetTransactions(pageNo) {
        this.booleans.loader = true;
        this.nibssservice.getTransaction(pageNo)
            .subscribe((response) => {
                console.log('transaction billings :: ', response);
                this.booleans.loader = false;
                this.totalElement = response.totalElements;
                this.list.allContent = response.content;
            },
            error => {
                this.booleans.loader = false;
                this.notification.error('Unable to load ' + this.moduleName, error);
            })
    }

    dateRangeSearchbyLog(download?) {
        if ((this.dateRangeSearchForm.value['from'] !== '' && this.dateRangeSearchForm.value['to'] === '')
            || (this.dateRangeSearchForm.value['from'] === '' && this.dateRangeSearchForm.value['to'] !== '')) {
            this.notification.error('Please, enter a value in the parameter');
        } else {
            this.booleans.loadsearch = true;
            if (download === 'download') {
                this.getDownload(download)
            } else {
                this.nibssservice.getTransaction(this.dateRangeSearchForm.value, 0)
                    .subscribe((response) => {
                        this.booleans.loadsearch = false;
                        console.log('response from list :: ', response)
                        this.list.allContent = response;
                        /* if (response.content.length === 0) {
                         this.notification.error('No Result Found');
                         }
                         this.list.allContent = response.content;
                         this.totalElement = response.totalElements;
                         this.dateRangeSearchForm.reset();*/
                    },
                    error => {
                        this.booleans.loadsearch = false;
                        this.notification.error('Unable Search the log', error);
                    })
            }
        }
    }

    onSearchType(searchType) {
        this.searchOption = searchType;
    }

    getDownload(download) {
        this.nibssservice.getTransaction(this.dateRangeSearchForm.value, 0, download)
            .subscribe((response) => {
                this.booleans.loadsearch = false;
                console.log('file response :: ', response)
                saveAs(response, 'transaction_billing_report.zip');
                this.dateRangeSearchForm.reset();
            },
            error => {
                this.booleans.loadsearch = false;
                this.notification.error('Unable download file', error);
            })
    }
}
