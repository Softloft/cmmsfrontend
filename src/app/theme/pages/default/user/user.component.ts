import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ScriptLoaderService } from "../../../../_services/script-loader.service";

import Swal from "sweetalert2";

declare const $;
@Component({
    selector: 'app-user',
    templateUrl: 'user.component.html',
    styles: []
})
export class UserComponent implements OnInit {
    createForm: FormGroup;
    public p = 1;
    public moduleName = 'Users';
    public modal_title = 'New';
    public list = {
        allContent: []
    };
    private formdata = function() {
        return {
            name: ['', Validators.compose([Validators.required])]
        }
    }

    constructor(private _script: ScriptLoaderService,
        private fb: FormBuilder) {
        this.createForm = this.fb.group(this.formdata());
    }

    ngOnInit() {
        this.list.allContent = [
            { name: 'elvis' },
        ];
    }

    getPage($event) {
    }

    openModal() {
        this.modal_title = 'New';
        $('#CreateModal').modal('show');
    }
}
