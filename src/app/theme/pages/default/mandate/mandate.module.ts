import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MandateComponent } from './mandate.component';
import { DefaultComponent } from "../default.component";
import { RouterModule, Routes } from "@angular/router";
import { LayoutModule } from "../../../layouts/layout.module";
import { BootstrapSwitchModule } from "angular2-bootstrap-switch";
import { NgxPaginationModule } from "ngx-pagination";
import { AngularMultiSelectModule } from "angular2-multiselect-dropdown";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { ApprovalComponent } from './approval/approval.component';
import { BankMandateComponent } from './bank-mandate/bank-mandate.component';
import { Daterangepicker } from "ng2-daterangepicker";
import { PipeModule } from "../../../../../shared/modules/pipe.module";


const routes: Routes = [
    {
        "path": "",
        "component": DefaultComponent,
        "children": [
            {
                "path": "",
                "component": MandateComponent
            }/*,
            {
                "path": "bank/:status",
                "component": BankMandateComponent
            },
            {
                "path": "bank",
                "component": BankMandateComponent
            }*/,
            {
                "path": ":status",
                "component": ApprovalComponent
            }
        ]
    }
];

@NgModule({
    imports: [
        CommonModule,
        LayoutModule,
        FormsModule,
        NgbModule,
        PipeModule,
        Daterangepicker,
        RouterModule.forChild(routes),
        ReactiveFormsModule,
        AngularMultiSelectModule,
        NgxPaginationModule,
        BootstrapSwitchModule
    ],
    exports: [
        RouterModule
    ],
    declarations: [MandateComponent, ApprovalComponent, BankMandateComponent]
})
export class MandateModule { }
