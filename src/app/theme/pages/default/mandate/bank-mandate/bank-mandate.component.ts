import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { NotificationService } from "../../../../../../services/notification.service";
import { Cache } from "../../../../../../utils/cache";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { MandateService } from "../../../../../../services/api-service/mandate.service";
import { UserService } from "../../../../../../services/user.service";

import Swal from "sweetalert2";
import { CONSTANTS } from "../../../../../../utils/constants";
declare const $;

@Component({
    selector: 'app-bank-mandate',
    templateUrl: 'bank-mandate.component.html',
    styles: []
})
export class BankMandateComponent implements OnInit {
    public id = 0;
    public p = 1;
    public form: FormGroup;
    public statusType = '';
    public approvalStatus = '';
    public approvalType = '';
    public reason = '';
    public user = [];
    public details = [];
    public userType: any;
    public totalElement = 0;
    public list = {
        allContent: [],
        details: [],
        rejectionReason: []
    };
    public booleans = {
        loader: false,
        initiator: false,
        authorizer: false,
        loadAction: false,
        loadProduct: false,
        loadrejectionreason: false
    };

    private formdata = function() {
        return {
            comment: ['', Validators.compose([Validators.required])],
            rejectionId: ['', Validators.compose([Validators.required])]
        }
    };

    constructor(private activatedRoute: ActivatedRoute,
        private router: Router,
        private fb: FormBuilder,
        private notification: NotificationService,
        private userservice: UserService,
        private mandateservice: MandateService) {
        this.form = this.fb.group(this.formdata());
    }

    ngOnInit() {
        this.user = Cache.get('userDetails');
        this.userType = this.userservice.getAuthUser()['roles'][0];
        this.getParam();
        this.details = this.userservice.getAuthUser();
        if (this.details['roles'][0] === 'NIBSS AUTHORIZER'
            || this.details['roles'][0] === 'BANK BILLER AUTHORIZER'
            || this.details['roles'][0] === 'BANK BIILER AUTHORIZER'
            || this.details['roles'][0] === 'BILLER AUTHORIZER') {
            this.booleans.authorizer = true;
        }
        if (this.details['roles'][0] === 'NIBSS INITIATOR'
            || this.details['roles'][0] === 'BANK BILLER INITIATOR'
            || this.details['roles'][0] === 'BANK BIILER INITIATOR'
            || this.details['roles'][0] === 'BILLER INITIATOR') {
            this.booleans.initiator = true;
        }
    }

    getPage(event) {
        this.p = +event;
        this.list.allContent = [];
        this.getMandate(this.statusType, this.p - 1);
    }

    public getParam(param?) {
        this.list.allContent = [];
        if (param) {
            switch (param) {
                case 'mandate/bank/pending':
                    this.statusType = 'approved';
                    this.approvalStatus = 'Pending';
                    break;
                case 'mandate/bank/approved':
                    this.statusType = 'authorized';
                    this.approvalStatus = 'Approved';
                    break;
                case 'mandate/bank/disapproved':
                    this.statusType = 'rejected';
                    this.approvalStatus = 'Disapproved';
                    break;
            }
            this.router.navigate(['/' + param]);
        } else {
            const status = this.activatedRoute.snapshot.paramMap.get('status');
            if (!status) {
                this.statusType = 'approved';
                this.approvalStatus = 'Pending';
            } else {
                switch (status) {
                    case 'pending':
                        this.statusType = 'approved';
                        this.approvalStatus = 'Pending';
                        break;
                    case 'approved':
                        this.statusType = 'authorized';
                        this.approvalStatus = 'Approved';
                        break;
                    case 'disapproved':
                        this.statusType = 'rejected';
                        this.approvalStatus = 'Disapproved';
                        break;
                }
            }
        }
        this.getMandate(this.statusType, 0);
    }

    getMandate(statusType, pageNo) {
        this.booleans.loader = true;
        this.mandateservice.getBankMandateByStatus(statusType, pageNo)
            .subscribe(response => {
                this.booleans.loader = false;
                this.list.allContent = response.content;
                this.totalElement = response.totalElements;
            },
            error => {
                this.booleans.loader = false;
                this.notification.error('Unable to load Mandate', error);
            })
    }

    getReason(data) {
        this.id = data.id;
        this.getRejectionReason();
        this.list.details = data;
        this.approvalType = 'disapprove';
        $('#reasonModal').modal('show');
    }

    getRejectionReason() {
        this.booleans.loadrejectionreason = true;
        this.mandateservice.getRejectionReason()
            .subscribe(response => {
                this.booleans.loadrejectionreason = false;
                this.list.rejectionReason = response;
            },
            error => {
                this.booleans.loadrejectionreason = false;
                this.notification.error('Unable to load rejection reason', error);
            })
    }

    view(data) {
        this.list.details = data;
        $('#ViewModal').modal('show');
    }

    disapprove() {
        this.booleans.loadAction = true;
        this.mandateservice.rejectMandate(this.id, this.form.value, this.userservice.getAuthUser())
            .subscribe(response => {
                this.booleans.loadAction = false;
                this.list.allContent.forEach((value, i) => {
                    if (this.id === value['id']) {
                        this.list.allContent.splice(i, 1);
                    }
                });
                this.notification.success('Mandate was successfully disapproved');
            },
            error => {
                this.booleans.loadAction = false;
                this.notification.error(CONSTANTS.ERROR_RESPONSE, error);
            })
    }

    approve(id, approvalType) {
        this.id = id;
        this.approvalType = approvalType;
        this.performAction(id, approvalType);
    }

    performAction(mandateId, action) {
        this.booleans.loadAction = true;
        this.mandateservice.approvedBankMandate({ mandateId: mandateId, action: action })
            .subscribe(response => {
                this.booleans.loadAction = false;
                this.list.allContent.forEach((value, i) => {
                    if (mandateId === value['id']) {
                        this.list.allContent.splice(i, 1);
                    }
                });
                this.notification.success('Mandate was successfully approved');
            },
            error => {
                this.booleans.loadAction = false;
                this.notification.error(CONSTANTS.ERROR_RESPONSE, error);
            })
    }

}
