import {Component, ElementRef, OnInit, ViewChild} from "@angular/core";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ScriptLoaderService} from "../../../../_services/script-loader.service";
import {BillerService} from "../../../../../services/api-service/biller.service";
import {FormService} from "../../../../../services/api-service/formService";
import {NotificationService} from "../../../../../services/notification.service";
import {Cache} from "../../../../../utils/cache";
import {BankService} from "../../../../../services/api-service/bank.service";
import {JSON2PDFTABLE} from "../../../../../shared/export/json2pdf-table";
import {JSON2CSV} from "../../../../../shared/export/json2csv";
import {NibssService} from "../../../../../services/api-service/nibss.service";
import {UserService} from "../../../../../services/user.service";
import {saveAs} from "file-saver";
import {MandateService} from "../../../../../services/api-service/mandate.service";
import {Router} from "@angular/router";

import Swal from "sweetalert2";
import {APPROVAL_MESSAGE_USER, CONSTANTS} from "../../../../../utils/constants";
import {AuthService} from "../../../../../services/api-service/authService";
declare const $;

@Component({
    selector: 'app-mandate',
    templateUrl: './mandate.component.html',
    styleUrls: ['mandate.component.css']
})
export class MandateComponent implements OnInit {
    @ViewChild('myInput')
    myInputVariable: ElementRef;
    public mandatefile: any;
    public isInteger = false;
    public fileName = '';
    public submitFile = false;
    public moduleName = 'Mandate';
    public mandateTypePlaceholder = '';
    public settingsFrequency = {};
    public settingsBank = {};
    public settingsBiller = {};
    public settingsProduct = {};
    public itemListBiller = [];
    public itemListProduct = [];
    public itemListBank = [];
    public itemListFrequency = [];
    public frequency = [];
    public user = [];
    public details = [];
    public userType = '';
    public errorMessage = '';
    public file: any;
    public createForm: FormGroup;
    public createFormSearch: FormGroup;
    public p = 1;
    public id = 0;
    public totalElement = 0;
    public lgaId = null;
    public submit_type = 'Submit';
    public modal_title = 'Add';
    public imageResponse: any;
    public daterange: any = {};

    // see original project for full list of options
    // can also be setup using the config service to apply to multiple pickers
    public options: any = {
        locale: {format: 'YYYY-MM-DD'},
        alwaysShowCalendars: false,
    };

    public list = {
        allContent: [],
        details: [],
        country: [],
        accountResult: [],
        mandateTypeData: [],
        mandateChannelData: [],
        mandateCategoryData: [],
    };
    public booleans = {
        loader: false,
        loadType: false,
        mandateType: null,
        loadChannel: false,
        loadCategory: false,
        initiator: false,
        authorizer: false,
        loadbank: false,
        acountloader: false,
        updateStatus: false,
        searchStatus: false,
        loadbiller: false,
        loadAction: false,
        getAllloader: false,
        loadFrequency: false,
        loadProduct: false,
        loadImage: false,
        loadbillerProduct: false,
        newMandateStatus: false,
        pdfsearchloader: false,
        csvsearchloader: false,
        loadingSearch: false,
        showProduct: false,

    };

    private formdataSearch = function () {
        return {
            accName: ['', Validators.compose([])],
            accNumber: ['', Validators.compose([])],
            address: ['', Validators.compose([])],
            bankCode: ['', Validators.compose([])],
            bvn: ['', Validators.compose([])],
            channel: ['', Validators.compose([])],
            email: ['', Validators.compose([])],
            biller: ['', Validators.compose([])],
            frequency: ['', Validators.compose([])],
            mandateCategory: ['', Validators.compose([])],
            mandateCode: ['', Validators.compose([])],
            mandateEndDate: ['', Validators.compose([])],
            mandateStartDate: ['', Validators.compose([])],
            mandateStatus: ['', Validators.compose([])],
            mandateType: ['', Validators.compose([])],
            productName: ['', Validators.compose([])],
            subscriberCode: ['', Validators.compose([])],
            payerName: ['', Validators.compose([])],
        }
    };

    private formdata = function () {
        return {
            id: ['', Validators.compose([])],
            email: ['', Validators.compose([Validators.required])],
            billerSubscriberRef: ['', Validators.compose([])],
            payerName: ['', Validators.compose([Validators.required])],
            amount: ['', Validators.compose([Validators.required, Validators.pattern(/^\d+$/)])],
            phoneNumber: ['', Validators.compose([Validators.required, Validators.pattern(/^\d+$/)])],
            biller: ['', Validators.compose([])],
            bvn: ['', Validators.compose([Validators.minLength(11), Validators.maxLength(11), Validators.pattern(/^\d+$/)])],
            product: ['', Validators.compose([Validators.required])],
            mandateStartDate: ['', Validators.compose([])],
            mandateEndDate: ['', Validators.compose([])],
            payerAddress: ['', Validators.compose([Validators.required])],
            frequency: ['', Validators.compose([])],
            bank: ['', Validators.compose([Validators.required])],
            accountNumber: ['', Validators.compose([Validators.required, Validators.minLength(10), Validators.maxLength(10), Validators.pattern(/^\d+$/)])],
            accountName: ['', Validators.compose([Validators.required])],
            narration: ['', Validators.compose([Validators.required])],
            fixedAmountMandate: ['', Validators.compose([Validators.required])],
        }
    };

    constructor(private _script: ScriptLoaderService,
                private fb: FormBuilder,
                private mandateservice: MandateService,
                private billerService: BillerService,
                private bankservice: BankService,
                private nibssservice: NibssService,
                private userservice: UserService,
                private authservice: AuthService,
                private formservice: FormService,
                private elem: ElementRef,
                private router: Router,
                private notification: NotificationService) {
        this.createForm = this.fb.group(this.formdata());
        this.createFormSearch = this.fb.group(this.formdataSearch());
    }

    ngOnInit() {
        this.user = Cache.get('userDetails');
        this.userType = this.userservice.getAuthUser()['roles'][0];
        this.getBanks();
        this.getMandate(0);
        this.getMandateType();
        this.getMandateCategory();
        this.getMandateChannel();
        this.getFrequency();
        this.settingsFrequency = {
            singleSelection: true,
            text: 'Select Frequency',
            searchPlaceholderText: 'Search Fields',
            enableSearchFilter: true,
            badgeShowLimit: 5,
        };

        this.settingsBank = {
            singleSelection: true,
            text: 'Select Bank',
            searchPlaceholderText: 'Search Fields',
            enableSearchFilter: true,
            badgeShowLimit: 5
        };

        this.settingsBiller = {
            singleSelection: true,
            text: 'Select Biller',
            searchPlaceholderText: 'Search Fields',
            enableSearchFilter: true,
            badgeShowLimit: 5
        };

        this.settingsProduct = {
            singleSelection: true,
            text: 'Select Product',
            searchPlaceholderText: 'Search Fields',
            enableSearchFilter: true,
            badgeShowLimit: 5
        };

        this.list.allContent = [];

        if (this.user['biller'] && this.user['biller']['id']) {
            this.booleans.showProduct = true;
            this.getAllProduct(this.user['biller']['id']);
            this.createForm.controls['biller'].patchValue(this.user['biller']['id']);
        } else {
            this.getBiller();
        }

        this.details = this.userservice.getAuthUser();
        /*if (this.details['roles'][0] === 'BANK AUTHORIZER' || this.details['roles'][0] === 'BANK INITIATOR') {
         this.router.navigate(['/mandate/pending']);
         }*/
        if (this.details['roles'][0] === 'NIBSS AUTHORIZER'
            || this.details['roles'][0] === 'BANK BILLER AUTHORIZER'
            || this.details['roles'][0] === 'BANK BIILER AUTHORIZER'
            || this.details['roles'][0] === 'BANK AUTHORIZER'
            || this.details['roles'][0] === 'BANK INITIATOR'
            || this.details['roles'][0] === 'BILLER AUTHORIZER') {
            this.booleans.authorizer = true;
        }
        if (this.details['roles'][0] === 'NIBSS INITIATOR'
            || this.details['roles'][0] === 'BANK BILLER INITIATOR'
            || this.details['roles'][0] === 'BANK BIILER INITIATOR'
            || this.details['roles'][0] === 'BILLER INITIATOR') {
            this.booleans.initiator = true;
        }
    }

    getPage(event) {
        this.p = +event;
        this.list.allContent = [];
        this.getMandate(this.p - 1);
    }

    openModal() {
        this.modal_title = 'Add';
        this.submit_type = 'Submit';
        this.booleans.updateStatus = false;
        this.booleans.newMandateStatus = true;
        //  $('#CreateModal').modal('show');
    }

    close() {
        this.booleans.newMandateStatus = false;
    }

    getBanks() {
        this.booleans.loadbiller = true;
        this.bankservice.getActiveBank()
            .subscribe((response) => {
                this.booleans.loadbiller = false;
                response.forEach(value => {
                    this.itemListBank.push({
                        id: value.id,
                        code: value.code,
                        itemName: value.name
                    })
                })
            }, error => {
                this.booleans.loadbiller = false;
                this.notification.error(CONSTANTS.ERROR_RESPONSE, error);
            })
    }

    getBiller() {
        this.booleans.loadbiller = true;
        this.billerService.getAllBillersList()
            .subscribe((response) => {
                this.booleans.loadbiller = false;
                response.forEach(value => {
                    this.itemListBiller.push({
                        id: value.id,
                        itemName: value.name
                    })
                })
            }, error => {
                this.booleans.loadbiller = false;
                this.notification.error(CONSTANTS.ERROR_RESPONSE, error);
            })
    }

    getMandate(pageNo) {
        this.booleans.searchStatus = false;
        this.booleans.loader = true;
        this.mandateservice.getMandate(pageNo, this.userservice.getAuthUser())
            .subscribe(response => {
                    this.booleans.loader = false;
                    this.list.allContent = response.content;
                    this.totalElement = response.totalElements;
                },
                error => {
                    this.booleans.loader = false;
                    this.notification.error('Unable to load Mandate', error);
                })
    }

    getAllMandate(pageNo, totalNumber, type) {
        this.booleans.getAllloader = true;
        this.mandateservice.getAllMandate(pageNo, totalNumber, this.userservice.getAuthUser())
            .subscribe(response => {
                    this.booleans.getAllloader = false;
                    (type === 'csv') ? this.exportToCsv(response.content) : this.exportToPdf((response.content));
                },
                error => {
                    this.booleans.getAllloader = false;
                    this.notification.error('Unable to load NIBSS Users', error);
                })
    }

    view(data) {
        this.list.details = data;
        $('#ViewModal').modal('show');
    }

    edit(data) {
        console.log('mandate data :: ', data);
        this.createForm.reset();
        this.booleans.updateStatus = true;
        // data['billerSubscriberRef'] = '';
        // data['mandateStartDate'] = '';
        // data['mandateEndDate'] = '';
        // data['biller'] = '';
        // data['bank'] = '';
        // data['frequency'] = '';
        this.itemListProduct = [];
        this.submit_type = 'Update';
        this.getAllProduct(data.product.biller.id);
        const getDateFormate = (date) => {
            const dateObj = new Date(date);
            const month = dateObj.getUTCMonth() + 1; //months from 1-12
            const day = dateObj.getUTCDate();
            const year = dateObj.getUTCFullYear();

            return day + '/' + month + '/' + year;
        };

        const formdata = function () {
            return {
                id: [data.id, Validators.compose([])],
                bvn: [data.bvn, Validators.compose([Validators.minLength(11), Validators.maxLength(11), Validators.pattern(/^\d+$/)])],
                email: [data.email, Validators.compose([Validators.required])],
                billerSubscriberRef: [data.billerSubscriberRef, Validators.compose([])],
                payerName: [data.payerName, Validators.compose([Validators.required])],
                amount: [data.amount, Validators.compose([Validators.required, Validators.pattern(/^\d+$/)])],
                phoneNumber: [data.phoneNumber, Validators.compose([Validators.required, Validators.pattern(/^\d+$/)])],
                // biller: [data.biller, Validators.compose([Validators.required])],
                // product: [data.product, Validators.compose([Validators.required])],
                mandateStartDate: [getDateFormate(data.startDate), Validators.compose([Validators.required])],
                mandateEndDate: [getDateFormate(data.endDate), Validators.compose([Validators.required])],
                payerAddress: [data.payerAddress, Validators.compose([Validators.required])],
                frequency: ['', Validators.compose([Validators.required])],
                bank: [data.bank ? [{
                    'id': data.bank.code,
                    'itemName': data.bank.name
                }] : '', Validators.compose([Validators.required])],
                product: [data.product ? [{
                    'id': data.product.id,
                    'itemName': data.product.name
                }] : '', Validators.compose([Validators.required])],
                biller: [data.biller ? [{
                    'id': data.biller.id,
                    'itemName': data.biller.name
                }] : '', Validators.compose([])],
                // bankCode: [data.bankCode, Validators.compose([Validators.required])],
                accountNumber: [data.accountNumber, Validators.compose([Validators.required, Validators.pattern(/^\d+$/)])],
                accountName: [data.accountName, Validators.compose([Validators.required])],
                narration: [data.narration, Validators.compose([Validators.required])],
                fixedAmountMandate: [data.fixedAmountMandate, Validators.compose([Validators.required])],
            }
        };
        this.onSelectMandateType(data.fixedAmountMandate);
        this.createForm = this.fb.group(formdata());
        this.booleans.newMandateStatus = true;
        // $('#CreateModal').modal('show');
    }

    toggle(data) {
        this.id = data.id;
        let statusMessage = '';
        statusMessage = (data.activated) ? 'Disapprove' : 'Approve';
        Swal({
            title: '',
            html: '<b style="font-weight: bold">' + APPROVAL_MESSAGE_USER(statusMessage) + '</b>',
            type: null,
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes!'
        }).then((result) => {
            if (result.value) {
                this.mandateservice.toggle({id: data.id})
                    .subscribe(response => {
                            console.log('response :: ', response);
                            this.list.allContent.forEach((value, i) => {
                                if (value.id === this.id) {
                                    this.list.allContent[i] = value;
                                }
                            });
                            this.notification.success(CONSTANTS.TOGGLE_RESPONSE);
                        },
                        error => {
                            this.notification.error(CONSTANTS.ERROR_RESPONSE, error);
                        });
            }
        })
    }

    getMandateById(id) {
        this.id = id;
        this.booleans.loadImage = true;
        this.mandateservice.getMandateById(id)
            .subscribe(response => {
                    console.log('image response 2 :: ', response)
                    // saveAs(response['body'], 'mandateImage');
                    this.list.details = response['mandate'];
                    this.imageResponse = response['mandateImageInBase64'];
                    $('#ViewModal').modal('show');
                    this.booleans.loadImage = false;
                },
                error => {
                    this.booleans.loadImage = false;
                    this.notification.error(CONSTANTS.ERROR_RESPONSE, error);
                })
    }

    getFrequency() {
        this.booleans.loadFrequency = true;
        this.mandateservice.getFrequency()
            .subscribe((response) => {
                console.log('frequency response :: ', response)
                this.frequency = response;
                this.booleans.loadFrequency = false;
            }, error => {
                this.booleans.loadFrequency = false;
                this.notification.error(CONSTANTS.ERROR_RESPONSE, error);
            })
    }

    onSelectMandateType(mandateType) {
        this.itemListFrequency = [];
        console.log('frequency :: ', this.frequency);
        this.frequency.forEach(value => {
            // if (value.description !== 'Variable') {
                this.itemListFrequency.push({
                    id: value.id,
                    itemName: value.description
                })
           // }
        });
        const event = (mandateType.target) ? mandateType.target.value : mandateType;
        const array = ['frequency', 'mandateStartDate', 'mandateEndDate'];
        if (event === 'true' || event === true) {
            this.booleans.mandateType = true;
            this.mandateTypePlaceholder = 'Enter Amount';
            array.forEach(value => {
                if (this.createForm['controls'] && this.createForm['controls'][value]) {
                    this.createForm.controls[value].patchValue('');
                    this.createForm['controls'][value].setValidators([Validators.required]);
                }
            });
        } else if (event === 'false' || event === false) {
            this.booleans.mandateType = false;
            this.mandateTypePlaceholder = 'Enter Cap Value';
            array.forEach(value => {
                if (this.createForm['controls'] && this.createForm['controls'][value]) {
                    this.createForm.controls[value].patchValue('');
                    this.createForm['controls'][value].clearValidators();
                    this.createForm['controls'][value].setErrors(null);
                    this.createForm['controls'][value].reset();
                }
            });
        }
    }

    onItemSelectBiller(item) {
        this.booleans.showProduct = true;
        this.getAllProduct(item.id);
    }

    OnItemDeSelectBiller(item) {

    }

    create() {
        this.extractIds();
        if (!this.file) {
            this.notification.error('Please, add your mandate image');
            return;
        }
        if (this.errorMessage !== '') {
            this.notification.error(this.errorMessage);
            return;
        }
        this.booleans.loader = true;
        this.mandateservice.create(this.createForm.value, this.userservice.getAuthUser())
            .subscribe(response => {
                    this.booleans.loader = false;
                    this.notification.success('Mandate was created successfully');
                    this.myInputVariable.nativeElement.value = '';
                    this.createForm.reset();
                    this.itemListProduct = [];
                    this.list.allContent.unshift(response.mandate);
                }, error => {
                    this.booleans.loader = false;
                    this.notification.error('Unable to create mandate', error)
                }
            )
    }

    update() {
        this.extractIds();
        if (!this.file) {
            this.notification.error('Please, add your mandate image');
            return;
        }
        if (this.errorMessage !== '') {
            this.notification.error(this.errorMessage);
            return;
        }
        this.booleans.loader = true;
        this.mandateservice.updatemandate(this.createForm.value)
            .subscribe(response => {
                    this.booleans.loader = false;
                    this.notification.success('Mandate was updated successfully');
                    this.myInputVariable.nativeElement.value = '';
                    this.createForm.reset();
                    this.itemListProduct = [];
                    this.list.allContent.unshift(response.mandate);
                }, error => {
                    this.booleans.loader = false;
                    this.notification.error('Unable to create mandate', error)
                }
            )
    }

    private extractIds() {
        this.errorMessage = '';
        if (this.file) {
            this.createForm.value['uploadImage'] = this.file;
        }
        if (this.createForm.value['bvn'] !== ''
            && isNaN(this.createForm.value['bvn'])) {
            this.errorMessage = 'BVN must be a number';
        }
        if (isNaN(this.createForm.value['accountNumber'])) {
            this.errorMessage = 'Account Number must be a number';
        }
        if (this.createForm.value['bank'] && this.createForm.value['bank'][0]) {
            this.createForm.value['bankCode'] = this.createForm.value['bank'][0]['code'];
        }
        if (this.createForm.value['product'] && this.createForm.value['product'][0]) {
            this.createForm.value['product'] = this.createForm.value['product'][0]['id'];
        }
        if (this.user['biller'] && this.user['biller']['id']) {
            this.getAllProduct(this.user['biller']['id']);
            this.createForm.value['biller'] = this.user['biller']['id'];
        } else if (this.createForm.value['biller'] && this.createForm.value['biller'][0]) {
            this.createForm.value['biller'] = this.createForm.value['biller'][0]['id'];
        }
        if (this.createForm.value['frequency'] && this.createForm.value['frequency'][0]) {
            this.createForm.value['frequency'] = this.createForm.value['frequency'][0]['id'];
        }
    }

    getImage(event) {
        let reader = new FileReader();
        if (event.target.files && event.target.files.length > 0) {
            let file = event.target.files[0];
            reader.readAsDataURL(file);
            reader.onload = () => {
                console.log('image result :: ', reader.result);
                this.file = reader.result;
            };
        }
        /*   this.file = event.target.files[0];
         console.log('image :: ', this.file)*/

    }

    getAllProduct(id) {
        this.booleans.loadProduct = true;
        this.itemListProduct = [];
        this.billerService.getProduct(id)
            .subscribe(response => {
                    console.log('product response :: ', response);
                    this.booleans.loadProduct = false;
                    this.booleans.loadbillerProduct = true;
                    response.forEach(value => {
                        this.itemListProduct.push({
                            id: value.id,
                            itemName: value.name
                        })
                    });
                    console.log('itemListProduct :: ', this.itemListProduct);
                },
                error => {
                    this.booleans.loadProduct = false;
                    this.booleans.loadbillerProduct = true;
                    this.notification.error('Unable to load product(s)', error);
                })
    }

    showAlert(mandateId, actionId) {
        Swal({
            title: 'Are you sure?',
            text: 'You are about to remove a mandate!',
            showCancelButton: true,
            confirmButtonText: 'Yes, remove it!',
            cancelButtonText: 'No, keep it'
        }).then((result) => {
            if (result.value) {
                this.performAction(mandateId, actionId);
            } else if (result.dismiss === Swal.DismissReason.cancel) {
                Swal(
                    'Cancelled',
                    'Your mandate is safe :)',
                    'error'
                );
            }
        });
    }

    performAction(mandateId, actionId) {
        this.booleans.loadAction = true;
        this.mandateservice.getMandateAction({mandateId: mandateId, action: actionId}, this.userservice.getAuthUser())
            .subscribe(response => {
                    this.booleans.loadAction = false;
                    this.list.allContent.forEach((value, i) => {
                        if (mandateId === value['id']) {
                            (actionId === 3) ? this.list.allContent.splice(i, 1) : this.list.allContent[i] = response.mandate;
                        }
                    });
                    switch (actionId) {
                        case 1:
                            this.notification.success('Mandate was successfully activated');
                            break;
                        case 2:
                            this.notification.success('Mandate was successfully suspended');
                            break;
                        case 3:
                            this.notification.success('Mandate was successfully removed');
                            break;
                    }
                },
                error => {
                    this.booleans.loadProduct = false;
                    this.notification.error(CONSTANTS.ERROR_RESPONSE, error);
                })
    }


    public selectedDate(value: any, datepicker?: any) {
        const date1 = new Date(value['start']['_d']);
        const date2 = new Date(value['end']['_d']);
        const stateDate = `${date1.getFullYear()}-${+date1.getMonth() + 1}-${date1.getDate()}`;
        const endDate = `${date2.getFullYear()}-${+date2.getMonth() + 1}-${date2.getDate()}`;

        // this is the date the iser selected
        this.createForm.controls['mandateStartDate'].patchValue(stateDate)
        this.createForm.controls['mandateEndDate'].patchValue(endDate)

    }

    getMandateType() {
        this.booleans.loadType = true;
        this.nibssservice.getMandateType()
            .subscribe(response => {
                    this.booleans.loadType = false;
                    this.list.mandateTypeData = response;
                },
                error => {
                    this.booleans.loadType = false;
                    this.notification.error('Unable to list mandate type', error);
                })
    }

    getMandateChannel() {
        this.booleans.loadChannel = true;
        this.nibssservice.getMandateChannel()
            .subscribe(response => {
                    this.booleans.loadChannel = false;
                    this.list.mandateChannelData = response;
                },
                error => {
                    this.booleans.loadChannel = false;
                    this.notification.error('Unable to list mandate channel', error);
                })
    }

    getMandateCategory() {
        this.booleans.loadCategory = true;
        this.nibssservice.getMandateCategory()
            .subscribe(response => {
                    this.booleans.loadCategory = false;
                    this.list.mandateCategoryData = response;
                },
                error => {
                    this.booleans.loadCategory = false;
                    this.notification.error('Unable to list mandate category', error);
                })
    }

    searchMethod(pageNo) {
        this.extractSearchIds();
        this.booleans.searchStatus = true;
        this.booleans.loadingSearch = true;
        this.mandateservice.mandatesearch(this.createFormSearch.value, pageNo)
            .subscribe((response) => {
                    this.booleans.loader = false;
                    this.booleans.loadingSearch = false;
                    if (response.content.length === 0) {
                        this.notification.error('No Result Found')
                    } else {
                        this.list.allContent = response.content;
                        if (this.booleans.pdfsearchloader || this.booleans.csvsearchloader) {
                            this.exportAs();
                        }
                        $('#searchModal').modal('hide');
                    }
                    this.booleans.csvsearchloader = false;
                    this.booleans.pdfsearchloader = false;
                },
                error => {
                    this.booleans.loader = false;
                    this.booleans.csvsearchloader = false;
                    this.booleans.pdfsearchloader = false;
                    this.booleans.loadingSearch = false;
                    this.notification.error('Unable to Search', error);
                })
    }

    private extractSearchIds() {
        this.errorMessage = '';
        if (this.createFormSearch.value['bvn'] !== ''
            && isNaN(this.createFormSearch.value['bvn'])) {
            this.errorMessage = 'BVN must be a number';
        }
        if (this.createFormSearch.value['accountNumber'] !== '' && isNaN(this.createFormSearch.value['accountNumber'])) {
            this.errorMessage = 'Account Number must be a number';
        }
        if (this.createFormSearch.value['bankCode'] && this.createFormSearch.value['bankCode'][0]) {
            this.createFormSearch.value['bankCode'] = this.createFormSearch.value['bankCode'][0]['code'];
        }
        if (this.createFormSearch.value['productName'] && this.createFormSearch.value['productName'][0]) {
            this.createFormSearch.value['productName'] = this.createFormSearch.value['productName'][0]['id'];
        }
        if (this.user['biller'] && this.user['biller']['id']) {
            this.getAllProduct(this.user['biller']['id']);
            this.createFormSearch.value['biller'] = this.user['biller']['id'];
        } else if (this.createFormSearch.value['biller'] && this.createFormSearch.value['biller'][0]) {
            this.createFormSearch.value['biller'] = this.createFormSearch.value['biller'][0]['id'];
        }
        if (this.createFormSearch.value['frequency'] && this.createFormSearch.value['frequency'][0]) {
            this.createFormSearch.value['frequency'] = this.createFormSearch.value['frequency'][0]['id'];
        }
    }

    private exportAs() {
        if (this.booleans.pdfsearchloader) {
            this.getPdf()
        } else if (this.booleans.csvsearchloader) {
            this.getCsv();
        }
    }

    exportCsvSearch() {
        this.booleans.csvsearchloader = true;
        this.searchMethod(0);
    }

    exportPdfSearch() {
        this.booleans.pdfsearchloader = true;
        this.searchMethod(0);
    }

    getCsv(event?) {
        if (event === 'all') {
            this.getAllMandate(0, this.totalElement, 'csv');
        } else {
            const exportContent = this.list.allContent;
            this.exportToCsv(exportContent);
        }
    }

    private exportToCsv(exportContent) {
        const csvArray = [];
        const fields = [
            'Account Name',
            'Account Number',
            'Amount'
        ];
        exportContent.forEach((value) => {
            csvArray.push({
                'Account Name': value.accountName,
                'Account Number': value.accountNumber,
                'Amount': value.amount
            })
        });
        JSON2CSV(fields, csvArray, 'mandate');
    }

    getPdf(event?) {
        if (event === 'all') {
            this.getAllMandate(0, this.totalElement, 'pdf');
        } else {
            const exportContent = this.list.allContent;
            this.exportToPdf(exportContent);
        }
    }

    private exportToPdf(exportContent) {
        const rows = [];
        const columns = ["s/n  ", "Account Name", "Account Number", "Amount"];
        exportContent.forEach((value, i) => {
            rows.push([i + 1, value.accountName, value.accountNumber, value.amount])
        });
        JSON2PDFTABLE(columns, rows, 'mandate');
    }

    getSearchModal() {
        this.createFormSearch.reset();
        $('#searchModal').modal('show');

    }

    downloadTemplate() {
        this.mandateservice.getTemplate()
            .subscribe(response => {
                    saveAs(response, 'template.xls');
                },
                error => {
                    this.notification.error('Unable to process this action', error);
                })
    }

    onSelectFile(files) {
        const $linkId = $('#customFile');
        this.fileName = files[0]['name'];
        console.log('file ', this.fileName);
        this.mandatefile = files[0];
    }

    public submitBulkFile() {
        const file_type = this.fileName.split('.')[1].toLowerCase();
        console.log('file type :: ', file_type)
        if (file_type !== 'xls') {
            return this.notification.error('You can only upload xls files');
        }
        this.submitFile = true;
        console.log('mandate file');
        this.mandateservice.postTemplate(this.mandatefile).subscribe(
            (response) => {
                this.fileName = '';
                this.submitFile = false;
                saveAs(response, 'template');
                this.notification.success('Bulk upload was Successful');
            },
            (err) => {
                this.submitFile = false;
                console.log('error bulk :: ', err);
                this.notification.error('there was an error while uploading, please retry', err);
            }
        );
    }

    keyupAccountNumber() {
        this.isInteger = !isNaN(this.createForm.value['accountNumber']);
        if (this.createForm.value['accountNumber'].length === 10 && !isNaN(this.createForm.value['accountNumber'])) {
            this.booleans.acountloader = true;
            this.accountLookup();
        } else {
            this.list.accountResult = null;
        }
    }

    accountLookup() {
        const data = {
            accountNumber: this.createForm.value['accountNumber'],
            bankCode: ((this.createForm.value['bank'][0]) ? this.createForm.value['bank'][0]['code'] : '')
        };
        this.authservice.accountLookup(data)
            .subscribe(response => {
                    this.list.accountResult = response;
                    if (this.list.accountResult['accountName']) {
                        this.createForm['controls']['accountName'].patchValue(this.list.accountResult['accountName']);
                    }
                    this.booleans.acountloader = false;
                },
                error => {
                    this.booleans.acountloader = false;
                    this.notification.error('Unable to process the action', error)
                })
    }
}
