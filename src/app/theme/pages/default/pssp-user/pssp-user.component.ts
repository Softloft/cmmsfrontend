import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from "@angular/forms";
import { ScriptLoaderService } from "../../../../_services/script-loader.service";
import { FormService } from "../../../../../services/api-service/formService";
import { NotificationService } from "../../../../../services/notification.service";
import { NibssService } from "../../../../../services/api-service/nibss.service";
import { Cache } from "../../../../../utils/cache";
import { APPROVAL_MESSAGE_USER, CONSTANTS } from "../../../../../utils/constants";

import { DomSanitizer, SafeUrl } from "@angular/platform-browser";
import { JSON2PDFTABLE } from "../../../../../shared/export/json2pdf-table";
import { JSON2CSV } from "../../../../../shared/export/json2csv";
import { UserService } from "../../../../../services/user.service";
import { ApprovalService } from "../../../../../services/api-service/approval.service";
import { ActivatedRoute, Router } from "@angular/router";
import { BankService } from "../../../../../services/api-service/bank.service";
import { PsspService } from "../../../../../services/api-service/pssp.service";
import Swal from "sweetalert2";

declare const $;

@Component({
    selector: 'app-pssp-user',
    templateUrl: './pssp-user.component.html',
    styles: []
})
export class PsspUserComponent implements OnInit {
    public itemsPerPage = CONSTANTS.ITEMS_PER_PAGE;
    public moduleName = 'PSSP Users';
    public searchtext = '';
    private userType = '';
    public paramId: any;
    public user = [];
    public totalElement = '';
    public createForm: FormGroup;
    public createFormSearch: FormGroup;
    public formReason: FormGroup;
    public id = 0;
    public psspId = 0;
    public p = 1;
    public lga = null;
    public name = '';
    public details = [];
    public submit_type = 'Submit';
    public modal_title = 'Add';
    public role = '';
    public pagesize = 10;
    public pagesizeArray = CONSTANTS.ITEMS_PER_PAGE_ARRAY;
    /*******start of Approval(maker/checker) declaration*******/
    public approvalStatus = '';
    public approvalType = '';
    public statusType = '';
    public approvalName = 'pssp-user';
    public authorization = '';
    public approvedStatus = '';
    public approvalName2 = 'pssp';
    /*******end of Approval(maker/checker) declaration*******/
    public list = {
        allContent: [],
        searchRoles: [],
        details: [],
        country: [],
        searchContent: [],
        itemRoleList: [],
    };
    searchParams = {
        firstName: '',
        lastName: '',
        middleName: '',
        email: '',
        staffNumber: '',
        phoneNumber: '',
        city: '',
        role: '',
        state: '',
        lga: ''
    };

    public booleans = {
        loader: false,
        searchStatus: false,
        csvsearchloader: false,
        pdfsearchloader: false,
        getAllloader: false,
        loadingSearch: false,
        searchloader: false,
        loadCountry: false,
        loadState: false,
        loadLga: false,
        loadRole: false,
        loadBank: false,
        loadsubmit: false,
        initiator: false,
        authorizer: false,
        loadAction: false,
    };

    itemListCountry = [];
    selectedItemsCountry = [];
    settingsCountry = {};

    itemListState = [];
    selectedItemsState = [];
    settingsState = {};

    itemListLga = [];
    selectedItemsLga = [];
    settingsLga = {};

    itemListBank = [];
    selectedItemsBank = [];
    settingsBank = {};

    public settings = {};
    public selectedItems = [];
    public itemList = [];

    private formdata = function() {
        return {
            id: ['', Validators.compose([])],
            city: ['', Validators.compose([])],
            lga: ['', Validators.compose([])],
            country: ['', Validators.compose([])],
            state: ['', Validators.compose([])],
            streetName: ['', Validators.compose([])],

            userAuthorisationType: ['', Validators.compose([])],
            emailAddress: ['', Validators.compose([Validators.required, Validators.email])],
            isBankAsPSSP: ['', Validators.compose([])],
            phoneNumber: ['', Validators.compose([Validators.required, Validators.pattern(CONSTANTS.PHONE_REGEX)])],
            roleId: ['', Validators.compose([Validators.required])],
            userBankCode: ['', Validators.compose([])],
            userId: ['', Validators.compose([])],
            name: new FormGroup({
                firstName: new FormControl('', Validators.required),
                lastName: new FormControl('', Validators.required),
                middleName: new FormControl(''),
            }),
        }
    };

    private formdataSearch = function() {
        return {
            firstName: ['', Validators.compose([])],
            lastName: ['', Validators.compose([])],
            middleName: ['', Validators.compose([])],
            city: ['', Validators.compose([])],
            staffNumber: ['', Validators.compose([])],
            userAuthorisationType: ['', Validators.compose([])],
            email: ['', Validators.compose([])],
            phoneNumber: ['', Validators.compose([])],
            role: ['', Validators.compose([])],
            activated: ['', Validators.compose([])],
            createdAt: ['', Validators.compose([])],
        }
    };

    private formdataReason = function() {
        return {
            reason: ['', Validators.compose([Validators.required])],
        }
    };

    constructor(private _script: ScriptLoaderService,
        private fb: FormBuilder,
        private domsanitizer: DomSanitizer,
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private formservice: FormService,
        private bankservice: BankService,
        private psspService: PsspService,
        private userservice: UserService,
        private approvalservice: ApprovalService,
        private nibssservice: NibssService,
        private notification: NotificationService) {
        this.createForm = this.fb.group(this.formdata());
        this.createFormSearch = this.fb.group(this.formdataSearch());
        this.formReason = this.fb.group(this.formdataReason());
    }

    ngOnInit() {
        this.user = Cache.get('userDetails');
        // console.log('user :: ', this.user['roles'][0]['name']);
        this.getUserParam();
        // this.getActivatedBank();
        this.getRole('PSSP');
        this.getSearchRole('PSSP');
        this.getCountry();
        this.details = this.userservice.getAuthUser();
        console.log('role :: ', this.details['roles'][0]);
        this.role = this.userservice.getAuthUser()['roles'][0].split(' ').join('_');
        if (this.details['roles'][0] === 'NIBSS AUTHORIZER' || this.details['roles'][0] === 'BANK ADMIN AUTHORIZER' || this.details['roles'][0] === 'PSSP ADMIN AUTHORIZER') {
            this.booleans.authorizer = true;
        }
        if (this.details['roles'][0] === 'NIBSS INITIATOR' || this.details['roles'][0] === 'BANK ADMIN INITIATOR' || this.details['roles'][0] === 'PSSP ADMIN INITIATOR') {
            this.booleans.initiator = true;
        }
        // this.user = this.userservice.getAuthUser();
        this.settings = {
            singleSelection: true,
            text: "Select Fields",
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            searchPlaceholderText: 'Search Fields',
            enableSearchFilter: true,
            badgeShowLimit: 5,
            groupBy: "category"
        };

        this.settingsBank = {
            singleSelection: true,
            text: "Select Fields",
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            searchPlaceholderText: 'Search Fields',
            enableSearchFilter: true,
            badgeShowLimit: 5,
            groupBy: "category"
        };

        this.settingsLga = {
            singleSelection: true,
            text: "Select Lga",
            enableSearchFilter: true,
            badgeShowLimit: 5
        };

        this.settingsState = {
            singleSelection: true,
            text: "Select State",
            enableSearchFilter: true,
            badgeShowLimit: 5
        };

        this.settingsCountry = {
            singleSelection: true,
            text: "Select Country",
            enableSearchFilter: true,
            badgeShowLimit: 5
        };

        this.list.allContent = [];
    }


    openModal() {
        this.modal_title = 'Add';
        this.submit_type = 'Submit';
        this.createForm.reset();
        $('#CreateModal').modal('show');
    }

    onChangePageSize(event) {
        this.pagesize = event.target.value;
        this.getPSSPUserById(this.p - 1, this.pagesize);
    }

    getMainPage() {
        this.approvalStatus = '';
        this.router.navigate([this.approvalName]);
    }

    getUserParam() {
        this.paramId = this.activatedRoute.snapshot.paramMap.get('id');
        if (this.activatedRoute.snapshot.paramMap.get('id')) {
            this.userType = 'admin';
            this.id = +this.activatedRoute.snapshot.paramMap.get('id');
            this.psspId = +this.activatedRoute.snapshot.paramMap.get('id');
        } else {
            this.psspId = this.user['pssp']['id'];
            this.id = this.user['pssp']['id'];
        }
        this.getPSSPById();
        const status = this.activatedRoute.snapshot.paramMap.get('status');
        if (status && status !== '') {
            this.getParam();
        } else {
            this.getPSSPUserById(0, this.pagesize);
        }
    }

    getPSSPById() {
        this.psspService.getPSSPById(this.psspId)
            .subscribe((response) => {
                this.name = response.psspName;
            },
            error => {
                this.notification.error('Unable to load pssp', error);
            })
    }

    getPSSPUserById(pageNumber, pagesize) {
        this.booleans.loader = true;
        this.approvalStatus = '';
        this.list.allContent = [];
        this.psspService.getPSSPUserById(this.psspId, pageNumber, pagesize)
            .subscribe((response) => {
                this.booleans.loader = false;
                this.list.allContent = response.content;
                this.totalElement = (response.totalElement) ? response.totalElement : response.totalElements;
            },
            error => {
                this.booleans.loader = false;
                this.notification.error('Unable to load PSSP user', error);
            })
    }


    getPSSPUser(pageNumber) {
        this.booleans.loader = true;
        this.psspService.getPSSPUserById(this.psspId, pageNumber, this.pagesize)
            .subscribe((response) => {
                this.booleans.loader = false;
                this.list.allContent = response.content;
                this.totalElement = (response.totalElement) ? response.totalElement : response.totalElements;
            },
            error => {
                this.booleans.loader = false;
                this.notification.error('Unable to load pssp user', error);
            })
    }

    getCsv(event?) {
        if (event === 'all') {
            this.getAlLPSSPUser(0, this.totalElement, 'csv');
        } else {
            const exportContent = this.list.allContent;
            this.exportToCsv(exportContent);
        }
    }


    getPdf(event?) {
        if (event === 'all') {
            this.getAlLPSSPUser(0, this.totalElement, 'pdf');
        } else {
            const exportContent = this.list.allContent;
            this.exportToPdf(exportContent);
        }
    }

    private exportToCsv(exportContent) {
        const csvArray = [];
        const fields = ['First Name', 'Last Name', 'Email', 'Role', 'Created Date', 'Status'];
        exportContent.forEach((value) => {
            csvArray.push({
                'First Name': value['name'] ? value['name']['firstName'] : '',
                'Last Name': value['name'] ? value['name']['lastName'] : '',
                'Email': value.emailAddress,
                'Role': value.roles[0]['name'].split('_').join(''),
                'Created Date': new Date(value.createdAt).toString().split('GMT')[0],
                'Status': (value.activated) ? 'Active' : 'Inactive',
            })
        });
        JSON2CSV(fields, csvArray, this.moduleName);
    }

    private exportToPdf(exportContent) {
        const rows = [];
        const columns = ["s/n  ", "First Name", "Last Name", "Email", "Role", "Date Created", "Status"];
        exportContent.forEach((value, i) => {
            rows.push([i + 1, value['name'] ? value['name']['firstName'] : '', value['name'] ? value['name']['lastName'] : '', value.emailAddress, value.roles[0]['name'].split('_').join(' '), new Date(value.createdAt).toString().split('GMT')[0], (value.activated) ? 'Active' : 'Inactive'])
        });
        JSON2PDFTABLE(columns, rows, this.moduleName);
    }

    toggle(data) {
        this.id = data.id;
        let statusMessage = '';
        statusMessage = (data.activated) ? 'Disapprove' : 'Approve';
        Swal({
            title: '',
            html: '<b style="font-weight: bold">' + APPROVAL_MESSAGE_USER(statusMessage) + '</b>',
            type: null,
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes!'
        }).then((result) => {
            if (result.value) {
                this.psspService.toggleUser({ id: data.id })
                    .subscribe(response => {
                        console.log('response :: ', response);
                        this.list.allContent.forEach((value, i) => {
                            if (value.id === this.id) {
                                this.list.allContent[i] = value;
                            }
                        });
                        this.notification.success(CONSTANTS.TOGGLE_RESPONSE);
                    },
                    error => {
                        this.notification.error(CONSTANTS.ERROR_RESPONSE, error);
                    });
            }
        })
    }

    exportSearchMethod(pageNo) {
        let role = '';
        if (this.searchParams.role !== '') {
            role = this.searchParams.role.split(' ').join('_');
        }
        const params = `firstName=${this.searchParams.firstName}&lastName=${this.searchParams.lastName}&middleName=${this.searchParams.middleName}&email=${this.searchParams.email}&phoneNumber=${this.searchParams.phoneNumber}&city=${this.searchParams.city}&role=${role}&state=${this.searchParams.state}&lga=${this.searchParams.lga}`;
        this.booleans.loadingSearch = true;
        this.nibssservice.search(params, pageNo)
            .subscribe((response) => {
                this.list.allContent = response.content;
                this.totalElement = response.totalElements;
                if (this.booleans.pdfsearchloader) {
                    this.getPdf()
                } else if (this.booleans.csvsearchloader) {
                    this.getCsv();
                }
                this.booleans.csvsearchloader = false;
                this.booleans.pdfsearchloader = false;
                this.booleans.loadingSearch = false;
            },
            error => {
                this.booleans.csvsearchloader = false;
                this.booleans.pdfsearchloader = false;
                this.booleans.loadingSearch = false;
                this.notification.error('Unable to Search', error);
            })
    }

    searchMethod(pageNo) {
        this.booleans.searchStatus = true;
        this.booleans.loadingSearch = true;
        this.psspService.searchUser(this.createFormSearch.value, pageNo)
            .subscribe((response) => {
                this.booleans.loader = false;
                this.booleans.loadingSearch = false;
                if (response.content.length === 0) {
                    this.notification.error('No Result Found')
                } else {
                    this.list.allContent = response.content;
                    if (this.booleans.pdfsearchloader || this.booleans.csvsearchloader) {
                        this.exportAs();
                    }
                    $('#searchModal').modal('hide');
                }
                this.booleans.csvsearchloader = false;
                this.booleans.pdfsearchloader = false;
            },
            error => {
                this.booleans.loader = false;
                this.booleans.csvsearchloader = false;
                this.booleans.pdfsearchloader = false;
                this.booleans.loadingSearch = false;
                this.notification.error('Unable to Search', error);
            })
    }

    private exportAs() {
        if (this.booleans.pdfsearchloader) {
            this.getPdf()
        } else if (this.booleans.csvsearchloader) {
            this.getCsv();
        }
    }

    exportCsvSearch() {
        this.booleans.csvsearchloader = true;
        this.searchMethod(0);
    }

    exportPdfSearch() {
        this.booleans.pdfsearchloader = true;
        this.searchMethod(0);
    }

    onItemSelect(item: any) {
    }

    OnItemDeSelect(item: any) {
    }

    onSelectAll(items: any) {
    }

    onDeSelectAll(items: any) {
    }

    onItemSelectBank(item: any) {
        this.selectedItemsBank = [{ 'id': item.id, 'itemName': item.itemName }];
    }

    OnItemDeSelectBank(item: any) {
    }

    onSelectAllBank(items: any) {
    }

    onDeSelectAllBank(items: any) {
    }

    onItemSelectLga(item: any) {
        this.selectedItemsLga = [{ 'id': item.id, 'itemName': item.itemName }];
    }

    OnItemDeSelectLga(item: any) {
    }

    onSelectAllLga(items: any) {
    }

    onDeSelectAllLga(items: any) {
    }

    onItemSelectState(item: any) {
        this.itemListLga = [];
        this.getLgaByState(item.id);
    }

    getCountry() {
        this.booleans.loadCountry = true;
        this.formservice.getCountry()
            .subscribe((response) => {
                response.forEach((value) => {
                    this.itemListCountry.push({
                        "id": value.id,
                        "itemName": value.name
                    });
                });
                this.booleans.loadCountry = false;
            },
            error => {
                this.booleans.loadCountry = false;
                this.notification.error('Unable to load country', error);
            })
    }

    getStateByCountry(id) {
        this.itemListState = [];
        this.booleans.loadState = true;
        this.formservice.getStateByCountry(id)
            .subscribe((response) => {
                response.forEach((value) => {
                    this.itemListState.push({
                        "id": value.id,
                        "itemName": value.name
                    });
                });
                this.booleans.loadState = false;
            },
            (error) => {
                this.booleans.loadState = false;
                this.notification.error('Unable to load state', error);
            })
    }

    getLgaByState(id) {
        this.booleans.loadLga = true;
        this.formservice.getLgaByState(id)
            .subscribe((response) => {
                response.forEach((value) => {
                    this.itemListLga.push({
                        "id": value.id,
                        "itemName": value.name
                    });
                });
                this.booleans.loadLga = false;
            },
            (error) => {
                this.booleans.loadLga = false;
                this.notification.error('Unable to load lga', error);
            })
    }

    getRole(event) {
        this.booleans.loadRole = true;
        let userType = '';
        if (this.user['roles'][0]['name'] === 'NIBSS_ADMIN') {
            userType = 'ADMIN';
        } else {
            userType = 'USER';
        }
        this.formservice.getRoleByUser(event, userType)
            .subscribe((response) => {
                this.itemList = [];
                response.forEach(value => {
                    this.itemList.push({
                        id: value.id,
                        itemName: value.name.split('_').join(' '),
                        category: value.userType,
                        description: value.description,
                    })
                })
                this.booleans.loadRole = false;
            },
            (error) => {
                this.booleans.loadRole = false;
                this.notification.error('Unable to load role', error);
            })
    }

    getSearchRole(event) {
        this.formservice.getSearchRole(event)
            .subscribe((response) => {
                    this.list.searchRoles = response;
                    this.booleans.loadRole = false;
                },
                (error) => {
                    this.booleans.loadRole = false;
                    this.notification.error('Unable to load role', error);
                })
    }

    getActivatedBank() {
        this.booleans.loadBank = true;
        this.approvalservice.getBankApproval('APPROVAL')
            .subscribe((response) => {
                console.log('approved bank :: ', response);
                this.itemListBank = [];
                response.forEach(value => {
                    this.itemListBank.push({
                        id: value.id,
                        itemName: value.name.split('_').join(' '),
                        category: value.userType
                    })
                })
                this.booleans.loadBank = false;
            },
            (error) => {
                this.booleans.loadBank = false;
                this.notification.error('Unable to load Bank', error);
            })
    }

    submit() {
        this.extractIds();
        this.booleans.loadsubmit = true;
        this.psspService.createPSSPUser(this.createForm.value, this.userType)
            .subscribe(response => {
                this.list.allContent.unshift(response);
                this.booleans.loadsubmit = false;
                $('#CreateModal').modal('hide');
                this.createForm.reset();
                this.notification.success(CONSTANTS.SUBMIT_RESPONSE);
            },
            error => {
                this.booleans.loadsubmit = false;
                this.notification.error(CONSTANTS.ERROR_RESPONSE, error);
            });
    }

    view(data) {
        this.list.details = data;
        $('#ViewModal').modal('show');
    }

    edit(data) {
        this.id = data.id;
        this.createForm.reset();
        this.selectedItems = [];
        this.submit_type = 'Update';
        this.list.details = data;
        if (data.contactDetails && data.contactDetails.lga) {
            this.itemListLga.push({ id: data.contactDetails.lga.id, itemName: data.contactDetails.lga.name });
        }
        if (data.roles) {
            this.selectedItems.push({
                id: data.roles[0]['id'],
                itemName: data.roles[0]['name'].split('_').join(' '),
                category: data.roles[0]['userType']
            })
        }
        if (data.contactDetails && data.contactDetails.lga) {
            this.lga = [{ 'id': data.contactDetails.lga.id, 'itemName': data.contactDetails.lga.name }];
        }
        const formdata = function() {
            return {

                city: [(data.contactDetails && data.contactDetails.city) ? data.contactDetails.city : '', Validators.compose([])],
                lga: [(data.contactDetails && data.contactDetails.lga) ? [{
                    'id': data.contactDetails.lga.id,
                    'itemName': data.contactDetails.lga.name
                }] : '', Validators.compose([])],
                // roleId: ['', Validators.compose([Validators.required])],
                // userBankCode: ['', Validators.compose([Validators.required])],


                state: [(data.contactDetails && data.contactDetails.state) ? [{
                    'id': data.contactDetails.state.id,
                    'itemName': data.contactDetails.state.name
                }] : '', Validators.compose([])],
                country: [(data.contactDetails && data.contactDetails.country) ? [{
                    'id': data.contactDetails.country.id,
                    'itemName': data.contactDetails.country.name
                }] : '', Validators.compose([])],
                roleId: [[{
                    'id': data.roles[0]['id'],
                    'itemName': data.roles[0]['name']
                }], Validators.compose([Validators.required])],
                emailAddress: [data.emailAddress, Validators.compose([Validators.required, Validators.email])],
                isBankAsPSSP: [(data.isBankAsPSSP) ? data.isBankAsPSSP : '', Validators.compose([])],
                userAuthorisationType: [(data.makerCheckerType) ? data.makerCheckerType : '', Validators.compose([])],
                streetName: [(data.contactDetails && data.contactDetails.streetName) ? data.contactDetails.streetName : '', Validators.compose([])],
                userId: [data.id, Validators.compose([])],
                id: [data.id, Validators.compose([])],
                name: new FormGroup({
                    firstName: new FormControl((data.name && data.name.firstName) ? data.name.firstName : '', Validators.required),
                    lastName: new FormControl((data.name && data.name.lastName) ? data.name.lastName : '', Validators.required),
                    middleName: new FormControl(((data.name && data.name.middleName) ? data.name.middleName : '')),
                }),
                phoneNumber: [data.phoneNumber, Validators.compose([Validators.required, Validators.pattern(CONSTANTS.PHONE_REGEX)])],
            }
        };
        this.createForm = this.fb.group(formdata());
        $('#CreateModal').modal('show');
    }

    update() {
        this.extractIds();
        this.booleans.loadsubmit = true;
        this.psspService.updatePSSPUser(this.createForm.value)
            .subscribe(response => {
                this.list.allContent.forEach((value, i) => {
                    if (value.id === this.id) {
                        this.list.allContent[i] = response;
                    }
                });
                $('#CreateModal').modal('hide');
                this.createForm.reset();
                this.notification.success('Update was successful. Awaiting Authorization')
                this.booleans.loadsubmit = false;
            },
            error => {
                this.booleans.loadsubmit = false;
                this.notification.error(CONSTANTS.ERROR_RESPONSE, error);
            });
    }

    private extractIds() {
        if (this.user['pssp'] && this.user['pssp']['id']) {
            this.createForm.value['psspId'] = this.user['pssp']['id'];
        } else {
            this.createForm.value['psspId'] = this.psspId;
        }
        if (this.createForm.value['country'] && this.createForm.value['country'][0]) {
            this.createForm.value['countryId'] = this.createForm.value['country'][0]['id'];
        }
        if (this.createForm.value['roleId'] && this.createForm.value['roleId'][0]) {
            this.createForm.value['roleId'] = this.createForm.value['roleId'][0]['id'];
        }
        if (this.createForm.value['state'] && this.createForm.value['state'][0]) {
            this.createForm.value['stateId'] = this.createForm.value['state'][0]['id'];
        }
        if (this.createForm.value['lga'] && this.createForm.value['lga'][0]) {
            this.createForm.value['lgaId'] = this.createForm.value['lga'][0]['id'];
        }
    }

    /*getBankUser(pageNo) {
        this.booleans.searchStatus = false;
        this.booleans.loader = true;
        this.bankservice.getBankUser(pageNo, this.pagesize)
            .subscribe(response => {
                this.booleans.loader = false;
                this.list.allContent = response.content;
                this.totalElement = response.totalElements;
            },
            error => {
                this.booleans.loader = false;
                this.notification.error('Unable to load Users', error);
            })
    }*/

    getAlLPSSPUser(pageNo, totalNumber, type) {
        this.booleans.getAllloader = true;
        this.psspService.getAllPSSPsUser(this.id, pageNo, totalNumber)
            .subscribe(response => {
                this.booleans.getAllloader = false;
                (type === 'csv') ? this.exportToCsv(response.content) : this.exportToPdf((response.content));
            },
            error => {
                this.booleans.getAllloader = false;
                this.notification.error('Unable to load Bank Users', error);
            })
    }

    /*search() {
        this.booleans.searchloader = true;
        this.nibssservice.getNibss('search')
            .subscribe(response => {
                this.booleans.searchloader = false;
                this.list.searchContent = response.content;
            },
            error => {
                this.booleans.searchloader = false;
                this.notification.error('Unable to search Bank Users', error);
            })
    }*/

    getPage(event) {
        this.p = event;
        this.list.allContent = [];
        if (!this.booleans.searchStatus) {
            this.getPSSPUser(this.p - 1);
        } else {
            this.searchMethod(this.p - 1)
        }
    }

    getSearchModal() {
        for (let params in this.searchParams) {
            this.searchParams[params] = '';
        }
        $('#searchModal').modal('show');

    }

    OnItemDeSelectState(item: any) {
        this.itemListLga = [];
    }

    onSelectAllState(items: any) {
    }

    onDeSelectAllState(items: any) {
    }

    onItemSelectCountry(item: any) {
        this.getStateByCountry(item.id);
    }

    OnItemDeSelectCountry(item: any) {
        this.itemListState = [];
        this.itemListLga = [];
    }

    onSelectAllCountry(items: any) {
    }

    onDeSelectAllCountry(items: any) {
    }
    /***********************************APPROVAL(MAKER AND CHECKER)*****************************************/
    public getParam(param?) {
        this.list.allContent = [];
        if (param) {
            switch (param) {
                case this.approvalName + '/' + this.psspId + '/pending':
                    this.statusType = 'UNAUTHORIZED';
                    this.approvalStatus = 'Pending';
                    break;
                case this.approvalName + '/' + this.psspId + '/approved':
                    this.statusType = 'AUTHORIZED';
                    this.approvalStatus = 'Approved';
                    break;
                case this.approvalName + '/' + this.psspId + '/disapproved':
                    this.statusType = 'REJECTED';
                    this.approvalStatus = 'Disapproved';
                    break;
            }
            this.router.navigate(['/' + param]);
        } else {
            const status = this.activatedRoute.snapshot.paramMap.get('status');
            switch (status) {
                case 'pending':
                    this.statusType = 'UNAUTHORIZED';
                    this.approvalStatus = 'Pending';
                    break;
                case 'approved':
                    this.statusType = 'AUTHORIZED';
                    this.approvalStatus = 'Approved';
                    break;
                case 'disapproved':
                    this.statusType = 'REJECTED';
                    this.approvalStatus = 'Disapproved';
                    break;
            }
        }
        this.getAuthorization(this.statusType, 0);
    }

    getApprovalPage(event) {
        this.p = +event;
        this.list.allContent = [];
        this.getAuthorization(this.statusType, this.p - 1);
    }


    getAuthorization(approval_type, pageNo) {
        this.booleans.loader = true;
        this.approvalservice.getAuthorization(approval_type, pageNo, this.approvalName2, this.psspId)
            .subscribe(response => {
                this.booleans.loader = false;
                this.list.allContent = response.content;
                this.totalElement = response.totalElements;
            },
            error => {
                this.booleans.loader = false;
                this.notification.error('Unable to load content', error);
            })
    }

    getReason(data, authorizationStatus) {
        this.id = data.id;
        this.authorization = authorizationStatus;
        this.list.details = data;
        this.approvalType = 'disapprove';
        $('#reasonModal').modal('show');
    }

    disapprove() {
        let action = '';
        this.approvedStatus = 'reject';
        this.booleans.loadAction = true;
        switch (this.authorization) {
            case 'UNAUTHORIZED_CREATE':
                action = 'REJECT_CREATE';
                break;
            case 'UNAUTHORIZED_UPDATE':
                action = 'REJECT_UPDATE'
                break;
            case 'UNAUTHORIZED_TOGGLE':
                action = 'REJECT_TOGGLE'
                break;
        }

        this.Approve(this.id, action, this.formReason.value['reason']);
    }

    approve(id, authorizationStatus) {
        this.id = id;
        this.approvedStatus = '';
        let action = '';
        switch (authorizationStatus) {
            case 'UNAUTHORIZED_CREATE':
                action = 'APPROVE_CREATE';
                break;
            case 'UNAUTHORIZED_UPDATE':
                action = 'APPROVE_UPDATE'
                break;
            case 'UNAUTHORIZED_TOGGLE':
                action = 'APPROVE_TOGGLE'
                break;
        }
        this.approvalType = 'approve';
        this.Approve(this.id, action);
    }

    Approve(id, authorizationStatus, reason?) {
        this.booleans.loadAction = true;
        this.approvalservice.approve({
            id: id,
            reason: (reason && reason !== '') ? reason : ''
        }, authorizationStatus, this.approvalName2)
            .subscribe(response => {
                this.booleans.loadAction = false;
                this.list.allContent.forEach((value, i) => {
                    if (id === value['id']) {
                        this.list.allContent.splice(i, 1);
                    }
                });
                this.formReason.reset();
                const status = (this.approvedStatus === 'reject') ? 'Disapproval' : 'Approval';
                this.notification.success(status + ' was successful');
            },
            error => {
                this.booleans.loadAction = false;
                this.notification.error(CONSTANTS.ERROR_RESPONSE, error);
            })
    }

    getAllPSSPUser() {
        this.router.navigate(['/pssp-user/' + this.psspId]);
    }
}