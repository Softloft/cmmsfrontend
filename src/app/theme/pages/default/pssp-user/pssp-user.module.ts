import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PsspUserComponent } from './pssp-user.component';
import { BootstrapSwitchModule } from "angular2-bootstrap-switch";
import { NgxPaginationModule } from "ngx-pagination";
import { AngularMultiSelectModule } from "angular2-multiselect-dropdown";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule, Routes } from "@angular/router";
import { LayoutModule } from "../../../layouts/layout.module";
import { DefaultComponent } from "../default.component";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { PipeModule } from "../../../../../shared/modules/pipe.module";


const routes: Routes = [
    {
        "path": "",
        "component": DefaultComponent,
        "children": [
            {
                "path": "",
                "component": PsspUserComponent
            },
            {
                "path": ":id",
                "component": PsspUserComponent
            },
            {
                "path": ":id/:status",
                "component": PsspUserComponent
            }
        ]
    }
];
@NgModule({
    imports: [
        CommonModule,
        LayoutModule,
        FormsModule,
        NgbModule,
        PipeModule,
        RouterModule.forChild(routes),
        ReactiveFormsModule,
        AngularMultiSelectModule,
        NgxPaginationModule,
        BootstrapSwitchModule
    ],
    declarations: [PsspUserComponent]
})
export class PsspUserModule {
}
