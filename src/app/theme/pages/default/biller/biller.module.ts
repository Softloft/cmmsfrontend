import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BillerComponent } from './biller.component';
import { BootstrapSwitchModule } from "angular2-bootstrap-switch";
import { NgxPaginationModule } from "ngx-pagination";
import { AngularMultiSelectModule } from "angular2-multiselect-dropdown";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule, Routes } from "@angular/router";
import { LayoutModule } from "../../../layouts/layout.module";
import { DefaultComponent } from "../default.component";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { SharedModule } from "../../../../../shared/modules/shared.module";
import { PipeModule } from "../../../../../shared/modules/pipe.module";
import { FeesComponent } from "./fees/fees.component";
import { FeeConfigurationComponent } from "../../../../../shared/components/fee-configuration/fee-configuration.component";

const routes: Routes = [
    {
        "path": "",
        "component": DefaultComponent,
        "children": [
            {
                "path": "",
                "component": BillerComponent
            },
            {
                "path": ":status",
                "component": BillerComponent
            },
            {
                "path": "fees/:id",
                "component": FeeConfigurationComponent
            }
        ]
    }
];

@NgModule({
    imports: [
        CommonModule,
        LayoutModule,
        FormsModule,
        NgbModule,
        PipeModule,
        SharedModule,
        RouterModule.forChild(routes),
        ReactiveFormsModule,
        AngularMultiSelectModule,
        NgxPaginationModule,
        BootstrapSwitchModule
    ],
    declarations: [BillerComponent, FeesComponent]
})
export class BillerModule {
}
