import { Component, OnInit, ViewChild } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ScriptLoaderService } from "../../../../_services/script-loader.service";
import { FormService } from "../../../../../services/api-service/formService";
import { NotificationService } from "../../../../../services/notification.service";
import { BankService } from "../../../../../services/api-service/bank.service";
import { JSON2CSV } from "../../../../../shared/export/json2csv";
import { JSON2PDFTABLE } from "../../../../../shared/export/json2pdf-table";
import { BillerService } from "../../../../../services/api-service/biller.service";
import { IndustryService } from "../../../../../services/api-service/industry.service";
import { Cache } from "../../../../../utils/cache";
import { UserService } from "../../../../../services/user.service";
import { ApprovalService } from "../../../../../services/api-service/approval.service";
import { ActivatedRoute, Router } from "@angular/router";
import { AuthService } from "../../../../../services/api-service/authService";

import Swal from "sweetalert2";
import { APPROVAL_MESSAGE_USER, CONSTANTS } from "../../../../../utils/constants";
import { EmandateService } from "../../../../../services/api-service/emandate.service";
import { SetupEmandateComponent } from "../../../../../shared/components/setup-emandate/setup-emandate.component";
declare const $;

@Component({
    selector: 'app-biller',
    templateUrl: 'biller.component.html',
    styles: []
})
export class BillerComponent implements OnInit {
    @ViewChild(SetupEmandateComponent) emandate: SetupEmandateComponent;
    public moduleName = 'Biller';
    public isInteger = false;
    public errorMessage = '';
    public totalFeeSubmitted = 0;
    public totalFee = 0;
    public feeType = '';
    public bearerType = '';
    public showAmount = false;

    public feeSubmitType = '';
    public formulaErrorMessage = '';
    public feeErrorMessage = '';

    public billerdata = [];

    public createForm: FormGroup;
    public createFormSearch: FormGroup;
    public createFormEmandate: FormGroup;
    public formReason: FormGroup;
    public id = 0;
    public details = [];
    public p = 1;
    public user = [];
    public itemListBankList = [];
    public totalElement = '';
    public lgaId = null;
    public submit_type = 'Submit';
    public modal_title = 'Add';
    public pagesize = 10;
    public pagesizeArray = CONSTANTS.ITEMS_PER_PAGE_ARRAY;

    /***********Start Fees setyp*************************/
    public editFees = false;
    public feeData = [];

    public settingsBank = {};
    public selectedBank = [];
    public itemListBank = [];

    public createFormBearer: FormGroup;

    public settingsBearer = {};
    public selectedBearer = [];
    public itemListBearer = [{ id: 'Bank', itemName: 'Bank' }, { id: 'Subscriber', itemName: 'Subscriber' }, {
        id: 'Biller', itemName: 'Biller'
    }];

    private formdataBearer = function() {
        return {
            id: ['', Validators.compose([])],
            bankCode: ['', Validators.compose([])],
            feeBearer: ['', Validators.compose([])],
            feeBearerItem: ['', Validators.compose([])],
            billerDebitAccountNumber: ['', Validators.compose([Validators.minLength(10), Validators.maxLength(10)])],
            splitType: ['Fixed', Validators.compose([])],
            fixedAmount: ['', Validators.compose([])],
            percentageAmount: ['', Validators.compose([])],
            debitAtTransactionTime: [false, Validators.compose([])],
            billerDebitaccountName: ['', Validators.compose([])],
            accountName: ['', Validators.compose([])],
            accountNumber: ['', Validators.compose([])],
            billerDebitbank: ['', Validators.compose([])],
            bank: ['', Validators.compose([])],
            billerId: ['', Validators.compose([])],
            markUpFee: ['', Validators.compose([])],
            markUpFeeSelected: ['', Validators.compose([Validators.required])],
        }
    };

    /***********End Fees setyp*************************/

    /*******start of Approval(maker/checker) declaration*******/
    public approvalStatus = '';
    public approvalType = '';
    public statusType = '';
    public approvalName = 'biller';
    public authorization = '';
    public approvedStatus = '';
    /*******end of Approval(maker/checker) declaration*******/
    public list = {
        allContent: [],
        productList: [],
        details: [],
        fees: [],
        searchContent: [],
        accountResult: {},
        billerDebitaccountResult: '',
        accountResultBiller: '',
        country: [],
        beneficiaryList: [],
        beneficiaryArray: [],
    };

    public booleans = {
        loader: false,
        loadFee: false,
        loadsubmitFees: false,
        showBillSetup: true,
        acountloaderBiller: false,
        billerDebitacountloader: false,
        loadproductdetails: false,
        acountloader: false,
        loadsetupFees: false,
        searchStatus: false,
        formulaError: false,
        loadBeneficiary: false,
        csvsearchloader: false,
        pdfsearchloader: false,
        getAllloader: false,
        loadingSearch: false,
        searchloader: false,
        loadIndustry: false,
        loadsubmit: false,
        loadProduct: false,
        initiator: false,
        authorizer: false,
        loadAction: false,
        loadBank: false,
        markUpFeeSelected: false,
    };

    public error = {
        percentageError: '',
        accountnoerror: false
    };

    public settingsIndustry = {};
    public selectedIndustry = [];
    public itemListIndustry = [];

    public settings = {};
    public selectedItems = [];
    public itemList = [];

    private formdataSearch = function() {
        return {
            id: ['', Validators.compose([])],
            accountNumber: ['', Validators.compose([Validators.minLength(10), Validators.maxLength(10), Validators.pattern(/^\d+$/)])],
            accountName: ['', Validators.compose([])],
            activated: ['', Validators.compose([])],
            code: ['', Validators.compose([])],
            bank: ['', Validators.compose([])],
            name: ['', Validators.compose([])],
            bvn: ['', Validators.compose([])],
            description: ['', Validators.compose([])],
            industry: ['', Validators.compose([])],
            companyName: ['', Validators.compose([])],
            // domainName: ['', Validators.compose([])],
            rcNumber: ['', Validators.compose([Validators.pattern(/^\d+$/)])],
            // selfBiller: ['', Validators.compose([])]
        }
    };

    private formdataReason = function() {
        return {
            reason: ['', Validators.compose([Validators.required])],
        }
    };

    private formdata = function() {
        return {
            id: ['', Validators.compose([])],
            accountNumber: ['', Validators.compose([Validators.required, Validators.minLength(10), Validators.maxLength(10), Validators.pattern(/^\d+$/)])],
            accountName: ['', Validators.compose([Validators.required])],
            bank: ['', Validators.compose([Validators.required])],
            bvn: ['', Validators.compose([Validators.required, Validators.minLength(11), Validators.maxLength(11), Validators.pattern(/^\d+$/)])],
            description: ['', Validators.compose([Validators.required])],
            industry: ['', Validators.compose([Validators.required])],
            industryId: ['', Validators.compose([])],
            name: ['', Validators.compose([Validators.required])],
            domainName: ['', Validators.compose([])],
            rcNumber: ['', Validators.compose([Validators.required, Validators.pattern(/^\d+$/)])],
            selfBiller: ['', Validators.compose([Validators.required])]
        }
    };

    private formdataEmandate = function() {
        return {
            id: ['', Validators.compose([])],
            domainName: ['', Validators.compose([Validators.required])],
            entityType: ['', Validators.compose([Validators.required])],
            notificationUlr: ['', Validators.compose([])],
            ownerId: ['', Validators.compose([Validators.required])],
            username: ['', Validators.compose([Validators.required])]
        }
    };

    constructor(private _script: ScriptLoaderService,
        private fb: FormBuilder,
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private authservice: AuthService,
        private approvalservice: ApprovalService,
        private billerService: BillerService,
        private industryService: IndustryService,
        private userservice: UserService,
        private bankservice: BankService,
        private emandateservice: EmandateService,
        private notification: NotificationService) {
        this.billerService.shareFeeData(null);
        this.billerService.shareFormularData(null);
        this.createForm = this.fb.group(this.formdata());
        this.createFormSearch = this.fb.group(this.formdataSearch());
        this.formReason = this.fb.group(this.formdataReason());
        this.createFormBearer = this.fb.group(this.formdataBearer());
        this.createFormEmandate = this.fb.group(this.formdataEmandate());
    }

    ngOnInit() {
        this.billerService.getResponse.subscribe(response => this.booleans.loadsetupFees = response);
        this.billerService.getformularResponse.subscribe(response => this.booleans.loadFee = response);
        this.details = this.userservice.getAuthUser();
        if (this.details['roles'][0] === 'NIBSS AUTHORIZER'
            || this.details['roles'][0] === 'BANK ADMIN AUTHORIZER'
            || this.details['roles'][0] === 'PSSP ADMIN AUTHORIZER') {
            this.booleans.authorizer = true;
        }
        if (this.details['roles'][0] === 'NIBSS INITIATOR'
            || this.details['roles'][0] === 'BANK ADMIN INITIATOR'
            || this.details['roles'][0] === 'PSSP ADMIN INITIATOR') {
            this.booleans.initiator = true;
        }
        this.user = Cache.get('userDetails');
        if (this.details['userType'] !== 'BANK') {
            this.getActiveBank();
            this.getAllBankList();
        }
        this.getIndustry();
        const status = this.activatedRoute.snapshot.paramMap.get('status');
        if (status && status !== '') {
            this.getParam();
        } else {
            this.getBiller(0, this.pagesize);
        }
        this.settingsBearer = {
            singleSelection: true,
            text: "Select Bearer",
            enableSearchFilter: true,
            badgeShowLimit: 5
        };
        this.settingsBank = {
            singleSelection: true,
            text: "Select Bank",
            enableSearchFilter: true,
            badgeShowLimit: 5
        };
        this.settingsIndustry = {
            singleSelection: true,
            text: "Select Category",
            enableSearchFilter: true,
            badgeShowLimit: 5
        };
    }

    getMainPage() {
        this.approvalStatus = '';
        this.router.navigate([this.approvalName]);
    }

    onChangePageSize(event) {
        this.pagesize = event.target.value;
        this.getBiller(this.p - 1, this.pagesize);
    }

    getActiveBank() {
        this.booleans.loadBank = true;
        this.bankservice.getActiveBank()
            .subscribe(response => {
                this.booleans.loadBank = false;
                response.forEach(value => {
                    this.itemListBank.push({
                        id: value.id,
                        itemName: value.name,
                        code: value.code
                    })
                });
            },
            error => {
                this.booleans.loadBank = false;
                this.notification.error('Unable to load active bank', error);
            })
    }

    getAllBankList() {
        this.booleans.loadBank = true;
        this.bankservice.getAllBankList()
            .subscribe(response => {
                this.booleans.loadBank = false;
                response.forEach(value => {
                    this.itemListBankList.push({
                        id: value.id,
                        itemName: value.name,
                        code: value.code
                    })
                });
            },
            error => {
                this.booleans.loadBank = false;
                this.notification.error('Unable to load active bank', error);
            })
    }

    getBiller(pageNo, pagesize) {
        this.booleans.loader = true;
        this.billerService.getBiller(pageNo, this.details, pagesize)
            .subscribe(response => {
                this.booleans.loader = false;
                this.list.allContent = response.content;
                this.totalElement = response.totalElements;
            },
            error => {
                this.booleans.loader = false;
                this.notification.error('Unable to load Biller', error);
            })
    }

    toggle(data) {
        this.id = data.id;
        let statusMessage = '';
        statusMessage = (data.activated) ? 'Disapprove' : 'Approve';
        Swal({
            title: '',
            html: '<b style="font-weight: bold">' + APPROVAL_MESSAGE_USER(statusMessage) + '</b>',
            type: null,
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes!'
        }).then((result) => {
            if (result.value) {
                this.billerService.toggle({ id: data.id })
                    .subscribe(response => {
                        this.list.allContent.forEach((value, i) => {
                            if (value.id === this.id) {
                                this.list.allContent[i] = value;
                            }
                        });
                        this.notification.success(CONSTANTS.TOGGLE_RESPONSE);
                    },
                    error => {
                        this.notification.error(CONSTANTS.ERROR_RESPONSE, error);
                    });
            }
        })
    }

    openModal() {
        this.modal_title = 'Add';
        this.submit_type = 'Submit';
        this.createForm.reset();
        this.booleans.showBillSetup = true;
        if (this.details['userType'] === 'BANK') {
            this.createForm.controls['bank'].patchValue([{
                'id': (this.user['userBank']['id']) ? this.user['userBank']['id'] : '',
                'itemName': (this.user['userBank']['name']) ? this.user['userBank']['name'] : '',
                'code': (this.user['userBank']['code']) ? this.user['userBank']['code'] : ''
            }]);
        }
        $('#CreateModal').modal('show');
    }

    getCsv(event?) {
        if (event === 'all') {
            this.getAllBiller(0, this.totalElement, 'csv');
        } else {
            const exportContent = this.list.allContent;
            this.exportToCsv(exportContent);
        }
    }

    private exportToCsv(exportContent) {
        const csvArray = [];
        const fields = [
            'Name',
            'RC Number',
            'Account No.',
            'Introducing Bank',
            'Date Created',
            'Status'
        ];
        exportContent.forEach((value) => {
            csvArray.push({
                'Name': value.name,
                'RC Number': value.rcNumber,
                'Account No.': value.accountNumber,
                'Introducing Bank': value.bank.name,
                'Date Created': new Date(value.createdAt).toString().split('GMT')[0],
                'Status': (value.activated) ? 'Active' : 'Inactive'
            })
        });
        JSON2CSV(fields, csvArray, 'biller');
    }

    getPdf(event?) {
        if (event === 'all') {
            this.getAllBiller(0, this.totalElement, 'pdf');
        } else {
            const exportContent = this.list.allContent;
            this.exportToPdf(exportContent);
        }
    }

    private exportToPdf(exportContent) {
        const rows = [];
        const columns = ["s/n  ", "Name", "RC Number", "Account No.", "Introducing Bank", "Role", "Status"];
        exportContent.forEach((value, i) => {
            rows.push([i + 1, value.name, value.rcNumber, value.accountNumber, value.bank.name, new Date(value.createdAt).toString().split('GMT')[0], (value.activated) ? 'Active' : 'Inactive'])
        });
        JSON2PDFTABLE(columns, rows, 'biller');
    }

    submit() {
        this.extractIds();
        if (this.errorMessage !== '') {
            this.notification.error(this.errorMessage);
            return;
        }
        this.booleans.loadsubmit = true;
        this.billerService.createbiller(this.createForm.value)
            .subscribe(response => {
                this.createFees(response.id);
                this.list.allContent.unshift(response);
                // $('#CreateModal').modal('hide');
                this.createForm.reset();
                //  this.notification.success('Setting up Fees for the biller');
                //this.notification.success(CONSTANTS.SUBMIT_RESPONSE)
            },
            error => {
                this.booleans.loadsubmit = false;
                this.notification.error(CONSTANTS.ERROR_RESPONSE, error);
            });
    }

    createEmandate() {
        this.booleans.loadsubmit = true;
        this.emandateservice.postEmandate(this.createForm.value)
            .subscribe(response => {
                this.createFormEmandate.reset();
                this.notification.success('Emandate setup was successful. Await Authorization');
            },
            error => {
                this.booleans.loadsubmit = false;
                this.notification.error(CONSTANTS.ERROR_RESPONSE, error);
            });
    }

    private extractIds() {
        this.errorMessage = '';
        if (isNaN(this.createForm.value['accountNumber'])) {
            this.errorMessage = 'Account number must be a number';
        }
        if ((this.createForm.value['bvn']) !== '' && isNaN(this.createForm.value['bvn'])) {
            this.errorMessage = 'BVN must be a number';
        }

        if (this.createForm.value['bank'] && this.createForm.value['bank'][0]) {
            this.createForm.value['bankId'] = this.createForm.value['bank'][0]['id'];
        } else {
            this.createForm.value['bankId'] = this.details['bank']['id'];
        }
        if (this.createForm.value['industry'] && this.createForm.value['industry'][0]) {
            this.createForm.value['industryId'] = this.createForm.value['industry'][0]['id'];
        }
    }

    view(data) {
        this.modal_title = 'View';
        this.list.details = data;
        this.getProductByBiller(data.id)
        this.setupFeesByBillerId(data);
        $('#ViewModal').modal('show');
    }

    getProductByBiller(id) {
        this.booleans.loadproductdetails = true;
        this.list.productList = [];
        this.billerService.getProductByBiller(id)
            .subscribe(response => {
                this.booleans.loadproductdetails = false;
                this.list.productList = response;
            },
            error => {
                this.booleans.loadproductdetails = false;
                this.notification.error('Unable to process this action', error)
            })
    }

    setupFeesByBillerId(data) {
        this.id = data.id;
        this.billerService.getFeesById(this.id)
            .subscribe(response => {
                this.list.fees = response;
            },
            error => {
                this.notification.error(CONSTANTS.ERROR_RESPONSE, error);
            })

    }


    setFormulae(data) {
        this.id = data.id;
        this.booleans.loadFee = true;
        this.billerService.shareFormularData(data);
    }

    /*setupFees(data) {
     this.id = data.id;
     this.billerService.billerId = this.id;
     this.booleans.loadsetupFees = true;
     this.billerService.shareFeeData(data);
     }*/

    edit(data) {
        this.setupFees(data);
        this.list.details = data;
        this.id = data.id;
        this.submit_type = 'Update';
        this.modal_title = 'Update';
        const formdata = function() {
            return {
                accountNumber: [data.accountNumber, Validators.compose([Validators.required, Validators.minLength(10), Validators.maxLength(10)])],
                accountName: [data.accountName, Validators.compose([Validators.required])],
                rcNumber: [data.rcNumber, Validators.compose([Validators.required])],
                domainName: [data.domainName, Validators.compose([])],
                selfBiller: [(data.billerOwner && (data.billerOwner.trim() === 'NIBSS' || data.billerOwner.trim() === 'BANK')) ? true : false, Validators.compose([])],
                bank: [[{
                    'id': data.bank.id,
                    'itemName': data.bank.name,
                    'code': data.bank.code
                }], Validators.compose([])],
                industry: [[{
                    'id': data.industry.id,
                    'itemName': data.industry.name
                }], Validators.compose([Validators.required])],
                bvn: [data.bvn, Validators.compose([Validators.required, Validators.minLength(11), Validators.maxLength(11)])],
                name: [data.name, Validators.compose([Validators.required])],
                description: [data.description, Validators.compose([Validators.required])],
                id: [data.id, Validators.compose([])],
            }
        };
        this.createForm = this.fb.group(formdata());
        $('#CreateModal').modal('show');
    }

    setupemandate(data) {
        this.billerdata = data;
        this.emandate.setupemandate(data);
        $('#Setupemandate').modal('show');
    }

    update() {
        this.extractIds();
        this.booleans.loadsubmit = true;
        this.billerService.updateBiller(this.createForm.value)
            .subscribe(response => {
                this.list.allContent.forEach((value, i) => {
                    if (value.id === this.id) {
                        this.list.allContent[i] = response.body;
                    }
                });
                $('#CreateModal').modal('hide');
                this.createForm.reset();
                this.booleans.loadsubmit = false;
                if (this.approvalStatus !== '') {
                    this.list.allContent.forEach((value, i) => {
                        if (this.id === value['id']) {
                            this.list.allContent.splice(i, 1);
                        }
                    });
                }
                this.notification.success(CONSTANTS.UPDATE_RESPONSE);
            },
            error => {
                this.booleans.loadsubmit = false;
                this.notification.error(CONSTANTS.ERROR_RESPONSE, error);
            });
    }


    getIndustry() {
        this.booleans.loadIndustry = true;
        this.industryService.getIndustryList()
            .subscribe(response => {
                this.booleans.loadIndustry = false;
                response.forEach(value => {
                    this.itemListIndustry.push({
                        id: value.id,
                        itemName: value.name
                    })
                });
            },
            error => {
                this.booleans.loadIndustry = false;
                this.notification.error('Unable to load industry', error);
            })
    }

    getAllBiller(pageNo, totalNumber, type) {
        this.booleans.getAllloader = true;
        this.billerService.getAllBiller(pageNo, totalNumber)
            .subscribe(response => {
                this.booleans.getAllloader = false;
                (type === 'csv') ? this.exportToCsv(response.content) : this.exportToPdf((response.content));
            },
            error => {
                this.booleans.getAllloader = false;
                this.notification.error('Unable to load Industry', error);
            })
    }


    search() {
    }

    getPage(event) {
        this.p = event;
        this.list.allContent = [];
        (!this.booleans.searchStatus) ? this.getBiller(this.p - 1, this.pagesize) : this.searchMethod(this.p - 1);
    }

    getSearchModal() {
        $('#searchModal').modal('show');
    }

    /*onSelect(event) {
     this.feeType = event;
     if (this.feeType === 'percentage') {
     this.createFormBearer['controls']['fixedAmount'].patchValue('');
     }
     if (this.feeType === 'fixed') {
     this.createFormBearer['controls']['percentageAmount'].patchValue('');
     }
     }*/

    searchMethod(pageNo) {
        this.booleans.searchStatus = true;
        this.booleans.loadingSearch = true;
        this.createFormSearch.value['code'] = (this.createFormSearch.value['bank'][0]) ? this.createFormSearch.value['bank'][0]['code'] : this.createFormSearch.value['bank'];
        this.billerService.search(this.createFormSearch.value, pageNo)
            .subscribe((response) => {
                this.booleans.loader = false;
                this.booleans.loadingSearch = false;
                if (response.content.length === 0) {
                    this.notification.error('No Result Found')
                } else {
                    this.list.allContent = response.content;
                    if (this.booleans.pdfsearchloader || this.booleans.csvsearchloader) {
                        this.exportAs();
                    }
                    $('#searchModal').modal('hide');
                }
                this.booleans.csvsearchloader = false;
                this.booleans.pdfsearchloader = false;

            },
            error => {
                this.booleans.loader = false;
                this.booleans.csvsearchloader = false;
                this.booleans.pdfsearchloader = false;
                this.booleans.loadingSearch = false;
                this.notification.error('Unable to Search', error);
            })
    }

    private exportAs() {
        if (this.booleans.pdfsearchloader) {
            this.getPdf()
        } else if (this.booleans.csvsearchloader) {
            this.getCsv();
        }
    }

    exportCsvSearch() {
        this.booleans.csvsearchloader = true;
        this.booleans.pdfsearchloader = false;
        this.searchMethod(0);
    }

    exportPdfSearch() {
        this.booleans.csvsearchloader = false;
        this.booleans.pdfsearchloader = true;
        this.searchMethod(0);
    }

    /*keyupAccountNumber() {
     this.isInteger = !isNaN(this.createForm.value['accountNumber']);
     if (this.createForm.value['accountNumber'].length === 10 && !isNaN(this.createForm.value['accountNumber'])) {
     this.booleans.acountloader = true;
     this.accountLookup();
     } else {
     this.list.accountResult = null;
     }
     }

     accountLookup() {
     const data = {
     accountNumber: this.createForm.value['accountNumber'],
     bankCode: ((this.createForm.value['bank'][0]) ? this.createForm.value['bank'][0]['code'] : '')
     };
     this.authservice.accountLookup(data)
     .subscribe(response => {
     this.list.accountResult = response;
     if (this.list.accountResult['accountName']) {
     this.createForm['controls']['accountName'].patchValue(this.list.accountResult['accountName']);
     }
     this.booleans.acountloader = false;
     },
     error => {
     this.booleans.acountloader = false;
     this.notification.error('Unable to process the action', error)
     })
     }*/

    /***********************************APPROVAL(MAKER AND CHECKER)*****************************************/
    public getParam(param?) {
        this.list.allContent = [];
        if (param) {
            switch (param) {
                case this.approvalName + '/pending':
                    this.statusType = 'UNAUTHORIZED';
                    this.approvalStatus = 'Pending';
                    break;
                case this.approvalName + '/approved':
                    this.statusType = 'AUTHORIZED';
                    this.approvalStatus = 'Approved';
                    break;
                case this.approvalName + '/disapproved':
                    this.statusType = 'REJECTED';
                    this.approvalStatus = 'Disapproved';
                    break;
            }
            this.router.navigate(['/' + param]);
        } else {
            const status = this.activatedRoute.snapshot.paramMap.get('status');
            switch (status) {
                case 'pending':
                    this.statusType = 'UNAUTHORIZED';
                    this.approvalStatus = 'Pending';
                    break;
                case 'approved':
                    this.statusType = 'AUTHORIZED';
                    this.approvalStatus = 'Approved';
                    break;
                case 'disapproved':
                    this.statusType = 'REJECTED';
                    this.approvalStatus = 'Disapproved';
                    break;
            }
        }
        this.getAuthorization(this.statusType, 0);
    }

    getApprovalPage(event) {
        this.p = +event;
        this.list.allContent = [];
        this.getAuthorization(this.statusType, this.p - 1);
    }


    getAuthorization(approval_type, pageNo) {
        this.booleans.loader = true;
        this.approvalservice.getAuthorization(approval_type, pageNo, this.approvalName, 'not_user')
            .subscribe(response => {
                this.booleans.loader = false;
                this.list.allContent = response.content;
                this.totalElement = response.totalElements;
            },
            error => {
                this.booleans.loader = false;
                this.notification.error('Unable to load content', error);
            })
    }

    getReason(data, authorizationStatus) {
        this.id = data.id;
        this.authorization = authorizationStatus;
        this.list.details = data;
        this.approvalType = 'disapprove';
        $('#reasonModal').modal('show');
    }

    disapprove() {
        let action = '';
        this.approvedStatus = 'reject';
        this.booleans.loadAction = true;
        switch (this.authorization) {
            case 'UNAUTHORIZED_CREATE':
                action = 'REJECT_CREATE';
                break;
            case 'UNAUTHORIZED_UPDATE':
                action = 'REJECT_UPDATE'
                break;
            case 'UNAUTHORIZED_TOGGLE':
                action = 'REJECT_TOGGLE'
                break;
        }

        this.Approve(this.id, action, this.formReason.value['reason']);
    }

    approve(id, authorizationStatus) {
        this.id = id;
        let action = '';
        this.approvedStatus = '';
        switch (authorizationStatus) {
            case 'UNAUTHORIZED_CREATE':
                action = 'APPROVE_CREATE';
                break;
            case 'UNAUTHORIZED_UPDATE':
                action = 'APPROVE_UPDATE'
                break;
            case 'UNAUTHORIZED_TOGGLE':
                action = 'APPROVE_TOGGLE'
                break;
        }
        this.approvalType = 'approve';
        this.Approve(id, action);
    }

    Approve(id, authorizationStatus, reason?) {
        this.booleans.loadAction = true;
        this.approvalservice.approve({
            id: id,
            reason: (reason && reason !== '') ? reason : ''
        }, authorizationStatus, this.approvalName, 'not_user')
            .subscribe(response => {
                this.booleans.loadAction = false;
                this.list.allContent.forEach((value, i) => {
                    if (id === value['id']) {
                        this.list.allContent.splice(i, 1);
                    }
                });
                this.formReason.reset();
                const status = (this.approvedStatus === 'reject') ? 'Disapproval' : 'Approval';
                this.notification.success(status + ' was successful');
            },
            error => {
                this.booleans.loadAction = false;
                this.notification.error(CONSTANTS.ERROR_RESPONSE, error);
            })
    }


    /*********************************Start Setup Fees******************************/
    setupFees(data) {
        this.id = data.id;
        this.billerService.billerId = this.id;
        this.booleans.loadsetupFees = true;
        this.feeData = data;
        this.id = data.id;
        this.bearerType = '';
        this.showAmount = false;
        this.list.accountResult = {};
        this.booleans.showBillSetup = true;
        this.booleans.markUpFeeSelected = false;
        this.createFormBearer.reset();
        this.billerService.getFeesById(this.id)
            .subscribe(response => {
                this.list.fees = response;
                if (this.list.fees.length !== 0) {
                    if (response.markedUp) {
                        this.booleans.markUpFeeSelected = true;
                        this.list.accountResult['accountName'] = response.accountName;
                    }
                    this.billerService.sendResponse(false);
                    if (this.list.fees['feeBearer']) {
                        this.feeSubmitType = 'Update';
                        if (response.feeBearer) {
                            this.bearerType = (response.feeBearer === 'BILLER') ? 'Biller' : '';
                            this.showAmount = (this.bearerType === 'Biller') ? true : false;
                        }
                        const dataResponse = function() {
                            return {
                                billerId: [data.id, Validators.compose([])],
                                id: [response['id'], Validators.compose([])],
                                feeBearerItem: [[{
                                    'id': response.feeBearer,
                                    'itemName': response.feeBearer
                                }], Validators.compose([])],
                                bank: [(response.bank) ? [{
                                    'id': response.bank.id,
                                    'itemName': response.bank.name,
                                    'code': response.bank.code
                                }] : '', Validators.compose([])],
                                billerDebitAccountNumber: [response.billerDebitAccountNumber, Validators.compose([Validators.minLength(10), Validators.maxLength(10)])],
                                splitType: [response.splitType, Validators.compose([])],
                                markUpFeeSelected: [response.markedUp, Validators.compose([Validators.required])],
                                markUpFee: [response.markUpFee, Validators.compose([])],
                                accountName: [response.accountName, Validators.compose([])],
                                accountNumber: [response.accountNumber, Validators.compose([])],
                                fixedAmount: [response.fixedAmount, Validators.compose([])],
                                percentageAmount: [response.percentageAmount, Validators.compose([])],
                                debitAtTransactionTime: [response.debitAtTransactionTime, Validators.compose([])],
                                billerDebitbank: ['', Validators.compose([])],
                            }
                        };
                        this.createFormBearer = this.fb.group(dataResponse());
                    }
                    console.log('form bearer :: ', this.createFormBearer.value)
                    this.modal_title = 'Setup Fees';
                    this.booleans.loadsetupFees = false;
                    $('#FeesModal').modal('show');
                }
            },
            error => {
                this.booleans.loadsetupFees = false;
                this.billerService.sendResponse(true);
                this.notification.error(CONSTANTS.ERROR_RESPONSE, error);
            })

    }

    createFees(billerId) {
        this.extractFeesIds();
        if (this.feeErrorMessage !== '') {
            this.notification.error(this.feeErrorMessage);
            return;
        }
        this.booleans.loadsubmit = true;
        this.booleans.loadsubmitFees = true;
        this.createFormBearer.value['bankCode'] = (this.createFormBearer.value['bank'] && this.createFormBearer.value['bank'][0]) ? this.createFormBearer.value['bank'][0]['code'] : '';
        this.createFormBearer.value['billerId'] = billerId;
        this.billerService.setupFees(this.createFormBearer.value)
            .subscribe(response => {
                this.notification.success(CONSTANTS.SUBMIT_RESPONSE)
                this.booleans.loadsubmit = false;
                this.createFormBearer.reset();
                this.booleans.loadsubmit = false;
                this.booleans.loadsubmitFees = false;
                $('#CreateModal').modal('hide');
                // $('#FeesModal').modal('hide');
            },
            error => {
                this.booleans.loadsubmit = false;
                this.booleans.loadsubmitFees = false;
                this.notification.error(CONSTANTS.ERROR_RESPONSE, error);
            });
    }

    showAlert() {
        Swal({
            title: 'Are you sure?',
            html: "<b>Editing this fee, will clear this biller's existing formular!</b>",
            type: null,
            showCancelButton: true,
            confirmButtonText: 'Yes, edit it!',
            cancelButtonText: 'No, keep it'
        }).then((result) => {
            if (result.value) {
                this.editFees = true;
                this.updateFees();
            } else if (result.dismiss === Swal.DismissReason.cancel) {
                Swal(
                    'Cancelled',
                    'Fee is safe :)',
                    'error'
                );
            }
        });
    }

    updateFees() {
        this.extractFeesIds();
        if (this.feeErrorMessage !== '') {
            this.notification.error(this.feeErrorMessage);
            return;
        }
        this.booleans.loadsubmitFees = true;
        this.billerService.updateSetupFees(this.createFormBearer.value)
            .subscribe(response => {
                this.notification.success('Fee was successfully updated');
                this.booleans.loadsubmitFees = false;
                this.editFees = false;
                this.createFormBearer.reset();
                $('#CreateModal').modal('hide');
            },
            error => {
                this.booleans.loadsubmitFees = false;
                this.notification.error(CONSTANTS.ERROR_RESPONSE, error);
            });
    }

    private extractFeesIds() {
        this.feeErrorMessage = '';
        if (this.createFormBearer.value['billerDebitAccountNumber'] !== ''
            && isNaN(this.createFormBearer.value['billerDebitAccountNumber'])) {
            this.feeErrorMessage = 'Account number must be a number';
        }
        this.createFormBearer.value['billerId'] = this.id;

        if (this.createFormBearer.value['splitType'] === 'Percentage' && (!this.createFormBearer.value['percentageAmount'] || this.createFormBearer.value['percentageAmount'] === '')) {
            this.feeErrorMessage = 'Percentage amount is required';
        }
        /*if (this.createFormBearer.value['splitType'] === 'Fixed' && (!this.createFormBearer.value['fixedAmount'] || this.createFormBearer.value['fixedAmount'] === '')) {
         this.feeErrorMessage = 'Fixed amount is required';
         }*/
        if (this.createFormBearer.value['feeBearerItem'] && this.createFormBearer.value['feeBearerItem'][0]) {
            this.createFormBearer.value['feeBearer'] = this.createFormBearer.value['feeBearerItem'][0]['id'];
        }
        if (this.createFormBearer.value['feeBearer'] !== 'Biller') {
            this.createFormBearer.value['debitAtTransactionTime'] = 'false';
            this.createFormBearer.value['billerDebitAccountNumber'] = '';
        } else if (this.createFormBearer.value['feeBearer'] === 'Biller') {
            if (this.createFormBearer.value['debitAtTransactionTime'] === '') {
                this.feeErrorMessage = 'Choose the debt transaction Type';
            }
            if (this.createFormBearer.value['billerDebitAccountNumber'] === '' && this.createFormBearer.value['debitAtTransactionTime'] === true) {
                this.feeErrorMessage = 'Please, Enter the debt account number';
            }
        }
    }


    onSelect(event) {
        const markupStatus = event.target.value;
        const arraybillerDebit = ['billerDebitaccountName', 'billerDebitAccountNumber', 'billerDebitbank', 'debitAtTransactionTime']
        const array = ['feeBearerItem', 'markUpFee', 'bank', 'accountNumber', 'accountName'];
        if (markupStatus === 'true') {
            this.booleans.markUpFeeSelected = true;
            array.forEach((value => {
                this.createFormBearer['controls'][value].setValidators([Validators.required]);
            }))
        }
        if (markupStatus === 'false') {
            this.booleans.markUpFeeSelected = false;
            this.list.billerDebitaccountResult = null;
            this.showAmount = false;
            this.bearerType = '';
            arraybillerDebit.forEach(value => {
                if (this.createFormBearer['controls'] && this.createFormBearer['controls'][value]) {
                    this.createFormBearer['controls'][value].clearValidators();
                    this.createFormBearer['controls'][value].setErrors(null);
                    this.createFormBearer['controls'][value].reset();
                }
            });
            array.forEach(value => {
                if (this.createFormBearer['controls'] && this.createFormBearer['controls'][value]) {
                    this.createFormBearer['controls'][value].clearValidators();
                    this.createFormBearer['controls'][value].setErrors(null);
                    this.createFormBearer['controls'][value].reset();
                }
            });
        }
    }

    onItemSelectBearer(item: any) {
        if (item['itemName'] === 'Biller') {
            this.bearerType = 'Biller'
        } else {
            this.bearerType = '';
            this.showAmount = false;
            this.createFormBearer.controls['debitAtTransactionTime'].patchValue(false);
            this.createFormBearer.controls['billerDebitAccountNumber'].patchValue('');
            this.onTransactionType();
        }
    }

    OnItemDeSelectBearer(item: any) {
    }

    onSelectAllBearer(items: any) {
    }

    onDeSelectAllBearer(items: any) {
    }

    onTransactionType() {
        const array = ['billerDebitaccountName', 'billerDebitAccountNumber', 'billerDebitbank', 'debitAtTransactionTime']
        if (this.createFormBearer.value['debitAtTransactionTime'] === true) {
            this.showAmount = true;
            this.createFormBearer['controls']['billerDebitAccountNumber'].setValidators([Validators.required])
        } else {
            this.showAmount = false;
            this.list.billerDebitaccountResult = null;
            array.forEach(value => {
                if (this.createFormBearer['controls'] && this.createFormBearer['controls'][value]) {
                    this.createFormBearer['controls'][value].clearValidators();
                    this.createFormBearer['controls'][value].setErrors(null);
                    this.createFormBearer['controls'][value].reset();
                }
            });
            this.createFormBearer.controls['debitAtTransactionTime'].patchValue(false);
        }
    }

    checkPercentageValue(event) {
        const value = +event.target.value;
        this.error['percentageError'] = '';
        if (isNaN(value)) {
            this.error['percentageError'] = 'Value must be a number';
        } else if (value > 100) {
            this.error['percentageError'] = 'Percentage value cannot be more than 100%';
        } else if (value.toString().split(".")[1] && value.toString().split(".")[1].length > 2) {
            this.error['percentageError'] = 'Decimal Places must not exceed 2';
        } else {
            this.error['percentageError'] = '';
        }
    }

    checkAmountValue(event) {
        const value = +event.target.value;
        if (isNaN(value)) {
            this.error['percentageError'] = 'Value must be a number';
        } else {
            this.error['percentageError'] = '';
        }
    }

    verifyAccountNo() {
        if (this.createFormBearer.value['billerDebitAccountNumber'] !== '' && !isNaN(this.createFormBearer.value['billerDebitAccountNumber'])) {
            this.error.accountnoerror = true;
        }
    }

    keyupAccountNumber() {
        this.isInteger = !isNaN(this.createFormBearer.value['accountNumber']);
        if (this.createFormBearer.value['accountNumber'] && this.createFormBearer.value['accountNumber'].length === 10 && !isNaN(this.createFormBearer.value['accountNumber'])) {
            this.booleans.acountloader = true;
            this.accountLookup();
        } else {
            this.list.accountResult = null;
        }
    }

    keyupAccountNumberBiller() {
        this.isInteger = !isNaN(this.createForm.value['accountNumber']);
        if (this.createForm.value['accountNumber'].length === 10 && !isNaN(this.createForm.value['accountNumber'])) {
            this.booleans.acountloaderBiller = true;
            this.accountLookupBiller();
        } else {
            this.list.accountResultBiller = null;
        }
    }

    accountLookupBiller() {
        const data = {
            accountNumber: this.createForm.value['accountNumber'],
            bankCode: ((this.createForm.value['bank'][0]) ? this.createForm.value['bank'][0]['code'] : '')
        };
        console.log('account data :: ', data);
        this.authservice.accountLookup(data)
            .subscribe(response => {
                this.list.accountResultBiller = response;
                console.log('list account loader :: ', this.list.accountResultBiller);
                this.booleans.acountloaderBiller = false;
                if (this.list.accountResultBiller && this.list.accountResultBiller['accountName']) {
                    this.createForm['controls']['accountName'].patchValue(this.list.accountResultBiller['accountName']);
                }
            },
            error => {
                this.booleans.acountloaderBiller = false;
                this.notification.error('Unable to process the action', error)
            })
    }

    accountLookup() {
        const data = {
            accountNumber: this.createFormBearer.value['accountNumber'],
            bankCode: ((this.createFormBearer.value['bank'][0]) ? this.createFormBearer.value['bank'][0]['code'] : '')
        };
        this.authservice.accountLookup(data)
            .subscribe(response => {
                this.list.accountResult = response;
                if (this.list.accountResult['accountName']) {
                    this.createFormBearer['controls']['accountName'].patchValue(this.list.accountResult['accountName']);
                }
                this.booleans.acountloader = false;
            },
            error => {
                this.booleans.acountloader = false;
                this.notification.error('Unable to process the action', error)
            })
    }

    billerDebitkeyupAccountNumber() {
        this.isInteger = !isNaN(this.createFormBearer.value['billerDebitaccountNumber']);
        if (this.createFormBearer.value['billerDebitAccountNumber'].length === 10 && !isNaN(this.createFormBearer.value['billerDebitAccountNumber'])) {
            this.booleans.billerDebitacountloader = true;
            this.billerDebitaccountLookup();
        } else {
            this.list.billerDebitaccountResult = null;
        }
    }

    billerDebitaccountLookup() {
        const data = {
            accountNumber: this.createFormBearer.value['billerDebitAccountNumber'],
            bankCode: ((this.createFormBearer.value['billerDebitbank'][0]) ? this.createFormBearer.value['billerDebitbank'][0]['code'] : '')
        };
        this.authservice.accountLookup(data)
            .subscribe(response => {
                this.list.billerDebitaccountResult = response;
                if (this.list.billerDebitaccountResult && this.list.billerDebitaccountResult['accountName']) {
                    this.createFormBearer['controls']['billerDebitaccountName'].patchValue(this.list.billerDebitaccountResult['accountName']);
                }
                this.booleans.billerDebitacountloader = false;
            },
            error => {
                this.booleans.billerDebitacountloader = false;
                this.notification.error('Unable to process the action', error)
            })
    }

    /*********************************End Setup Fees******************************/
    public setup(event) {
        this.booleans.showBillSetup = event;
    }
}
