import { Component, OnInit } from "@angular/core";
import { BillerService } from "../../../../../services/api-service/biller.service";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { NotificationService } from "../../../../../services/notification.service";
import { UserService } from "../../../../../services/user.service";
import { JSON2PDFTABLE } from "../../../../../shared/export/json2pdf-table";
import { JSON2CSV } from "../../../../../shared/export/json2csv";
import { ApprovalService } from "../../../../../services/api-service/approval.service";
import { ActivatedRoute, Router } from "@angular/router";

import Swal from "sweetalert2";
import { CONSTANTS } from "../../../../../utils/constants";
declare const $;
@Component({
    selector: 'app-setup-fees',
    templateUrl: 'setup-fees.component.html',
    styles: []
})
export class SetupFeesComponent implements OnInit {
    public moduleName = 'Biller Fees';
    public modal_title = '';
    public createFormSearch: FormGroup;
    public formReason: FormGroup;
    public id = 0;
    public details = [];
    public p = 1;
    public totalElement = '';
    public submit_type = 'Submit';
    /*******start of Approval(maker/checker) declaration*******/
    public approvalStatus = '';
    public approvalType = '';
    public statusType = '';
    public approvalName = 'fees';
    public authorization = '';
    /*******end of Approval(maker/checker) declaration*******/
    public list = {
        details: [],
        allContent: [],
        searchContent: []
    };

    private formdataSearch = function() {
        return {
            billerAccNumber: ['', Validators.compose([])],
            billerName: ['', Validators.compose([])],
            feeBearer: ['', Validators.compose([])],
            fixedAmount: ['', Validators.compose([])],
            percentageAmount: ['', Validators.compose([])],
            splitType: ['', Validators.compose([])],
        }
    };

    public booleans = {
        loader: false,
        loadFee: false,
        loadsetupFees: false,
        searchStatus: false,
        csvsearchloader: false,
        pdfsearchloader: false,
        getAllloader: false,
        loadingSearch: false,
        searchloader: false,
        loadIndustry: false,
        loadsubmit: false,
        loadProduct: false,
        initiator: false,
        authorizer: false,
        loadAction: false,
    };

    public error = {
        percentageError: '',
        accountnoerror: false,
    };

    private formdataReason = function() {
        return {
            reason: ['', Validators.compose([Validators.required])],
        }
    };
    constructor(private billerService: BillerService,
        private userservice: UserService,
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private approvalservice: ApprovalService,
        private notification: NotificationService,
        private fb: FormBuilder) {
        this.billerService.shareFeeData(null);
        this.billerService.shareFormularData(null);
        this.createFormSearch = this.fb.group(this.formdataSearch());
        this.formReason = this.fb.group(this.formdataReason());
    }

    ngOnInit() {
        this.billerService.getResponse.subscribe(response => this.booleans.loadsetupFees = response);
        this.billerService.getformularResponse.subscribe(response => this.booleans.loadFee = response);
        const status = this.activatedRoute.snapshot.paramMap.get('status');
        if (status && status !== '') {
            this.getParam();
        } else {
            this.getAllFees(0);
        }

        this.details = this.userservice.getAuthUser();
        if (this.details['roles'][0] === 'NIBSS ADMIN AUTHORIZER'
            || this.details['roles'][0] === 'BANK ADMIN AUTHORIZER'
            || this.details['roles'][0] === 'BILLER ADMIN AUTHORIZER') {
            this.booleans.authorizer = true;
        }
        if (this.details['roles'][0] === 'NIBSS ADMIN INITIATOR'
            || this.details['roles'][0] === 'BANK ADMIN INITIATOR'
            || this.details['roles'][0] === 'BILLER ADMIN INITIATOR') {
            this.booleans.initiator = true;
        }
    }

    getMainPage() {
        this.approvalStatus = '';
        this.router.navigate([this.approvalName]);
    }

    getPage(event) {
        this.p = event;
        this.list.allContent = [];
        if (!this.booleans.searchStatus) {
            this.getAllFees(this.p - 1);
        } else {
            this.searchMethod(this.p - 1)
        }
    }

    getAllFees(pageNo) {
        this.booleans.loader = true;
        this.billerService.getAllFees(pageNo)
            .subscribe(response => {
                this.booleans.loader = false;
                this.list.allContent = response.content;
                this.totalElement = response.totalElements;
            },
            error => {
                this.booleans.loader = false;
                this.notification.error('Unable to load Biller', error);
            })
    }

    view(data) {
        this.list.details = data;
        $('#viewModal').modal('show');
    }

    setupFees(data) {
        this.id = data.id;
        this.booleans.loadsetupFees = true;
        this.billerService.shareFeeData(data);
    }

    setFormulae(data) {
        console.log('data :: ', )
        this.id = data.id;
        this.booleans.loadFee = true;
        this.billerService.shareFormularData(data);
    }

    searchMethod(pageNo) {
        this.booleans.searchStatus = true;
        this.booleans.loadingSearch = true;
        this.billerService.feesSearch(this.createFormSearch.value, pageNo)
            .subscribe((response) => {
                this.booleans.loader = false;
                this.booleans.loadingSearch = false;
                if (response.content.length === 0) {
                    this.notification.error('No Result Found')
                } else {
                    this.list.allContent = response.content;
                    if (this.booleans.pdfsearchloader || this.booleans.csvsearchloader) {
                        this.exportAs();
                    }
                    $('#searchModal').modal('hide');
                }
                this.booleans.csvsearchloader = false;
                this.booleans.pdfsearchloader = false;

            },
            error => {
                this.booleans.loader = false;
                this.booleans.csvsearchloader = false;
                this.booleans.pdfsearchloader = false;
                this.booleans.loadingSearch = false;
                this.notification.error('Unable to Search', error);
            })
    }

    private exportAs() {
        if (this.booleans.pdfsearchloader) {
            this.getPdf()
        } else if (this.booleans.csvsearchloader) {
            this.getCsv();
        }
    }

    exportCsvSearch() {
        this.booleans.csvsearchloader = true;
        this.searchMethod(0);
    }

    exportPdfSearch() {
        this.booleans.pdfsearchloader = true;
        this.searchMethod(0);
    }

    getCsv(event?) {
        if (event === 'all') {
            this.retrieveAllfees(0, this.totalElement, 'csv');
        } else {
            const exportContent = this.list.allContent;
            this.exportToCsv(exportContent);
        }
    }

    private exportToCsv(exportContent) {
        const csvArray = [];
        const fields = [
            'Biller Name',
            'Account Name',
            'Account Number',
            'Fee Bearer',
            'Split Type',
            'Percentage Amount'
        ];
        exportContent.forEach((value) => {
            csvArray.push({
                'Biller Name': value.biller.name,
                'Account Name': value.biller.accountName,
                'Account Number': value.biller.accountNumber,
                'Fee Bearer': value.feeBearer,
                'Split Type': value.splitType,
                'Fixed Amount': value.fixedAmount,
                'Percentage Amount': value.percentageAmount
            })
        });
        JSON2CSV(fields, csvArray, 'bank');
    }

    getPdf(event?) {
        if (event === 'all') {
            this.retrieveAllfees(0, this.totalElement, 'pdf');
        } else {
            const exportContent = this.list.allContent;
            this.exportToPdf(exportContent);
        }
    }

    private exportToPdf(exportContent) {
        const rows = [];
        const columns = ["s/n  ", "Biller Name", "Account Name", "Account Number", "Fee Bearer", "Split Type", "Fixed Amount", "Percentage Amount"];
        exportContent.forEach((value, i) => {
            rows.push([i + 1, value.biller.name, value.biller.accountName, value.biller.accountNumber, value.feeBearer, value.splitType, value.fixedAmount, value.percentageAmount]);
        });
        JSON2PDFTABLE(columns, rows, 'bank');
    }

    retrieveAllfees(pageNo, totalNumber, type) {
        this.booleans.getAllloader = true;
        this.billerService.retrieveAllfees(pageNo, totalNumber)
            .subscribe(response => {
                this.booleans.getAllloader = false;
                (type === 'csv') ? this.exportToCsv(response.content) : this.exportToPdf((response.content));
            },
            error => {
                this.booleans.getAllloader = false;
                this.notification.error('Unable to load NIBSS Users', error);
            })
    }

    getSearchModal() {
        $('#searchModal').modal('show');
    }

    verifyAccountNo() {
        if (this.createFormSearch.value['billerDebitAccountNumber'] !== ''
            && !isNaN(this.createFormSearch.value['billerDebitAccountNumber'])) {
            this.error.accountnoerror = true;
        } else {
            this.error.accountnoerror = false;
        }
    }

    checkAmountValue(event) {
        const value = +event.target.value;
        if (isNaN(value)) {
            this.error.percentageError = 'Value must be a number';
        } else {
            this.error.percentageError = '';
        }
    }

    checkPercentageValue(event) {
        const value = +event.target.value;
        this.error.percentageError = '';
        if (isNaN(value)) {
            this.error.percentageError = 'Value must be a number';
        } else if (value > 100) {
            this.error.percentageError = 'Percentage value cannot be more than 100%';
        } else if (value.toString().split(".")[1] && value.toString().split(".")[1].length > 2) {
            this.error.percentageError = 'Decimal Places must not exceed 2';
        } else {
            this.error.percentageError = '';
        }
    }

    /***********************************APPROVAL(MAKER AND CHECKER)*****************************************/
    public getParam(param?) {
        this.list.allContent = [];
        if (param) {
            switch (param) {
                case this.approvalName + '/pending':
                    this.statusType = 'UNAUTHORIZED';
                    this.approvalStatus = 'Pending';
                    break;
                case this.approvalName + '/approved':
                    this.statusType = 'AUTHORIZED';
                    this.approvalStatus = 'Approved';
                    break;
                case this.approvalName + '/disapproved':
                    this.statusType = 'REJECTED';
                    this.approvalStatus = 'Disapproved';
                    break;
            }
            this.router.navigate(['/' + param]);
        } else {
            const status = this.activatedRoute.snapshot.paramMap.get('status');
            switch (status) {
                case 'pending':
                    this.statusType = 'UNAUTHORIZED';
                    this.approvalStatus = 'Pending';
                    break;
                case 'approved':
                    this.statusType = 'AUTHORIZED';
                    this.approvalStatus = 'Approved';
                    break;
                case 'disapproved':
                    this.statusType = 'REJECTED';
                    this.approvalStatus = 'Disapproved';
                    break;
            }
        }
        this.getAuthorization(this.statusType, 0);
    }

    getApprovalPage(event) {
        this.p = +event;
        this.list.allContent = [];
        this.getAuthorization(this.statusType, this.p - 1);
    }


    getAuthorization(approval_type, pageNo) {
        this.booleans.loader = true;
        this.approvalservice.getAuthorization(approval_type, pageNo, this.userservice.getAuthUser())
            .subscribe(response => {
                this.booleans.loader = false;
                this.list.allContent = response.content;
                this.totalElement = response.totalElements;
            },
            error => {
                this.booleans.loader = false;
                this.notification.error('Unable to load content', error);
            })
    }

    getReason(data, authorizationStatus) {
        this.id = data.id;
        this.authorization = authorizationStatus;
        this.list.details = data;
        this.approvalType = 'disapprove';
        $('#reasonModal').modal('show');
    }

    disapprove() {
        let action = '';
        this.booleans.loadAction = true;
        switch (this.authorization) {
            case 'UNAUTHORIZED_CREATE':
                action = 'REJECT_CREATE';
                break;
            case 'UNAUTHORIZED_UPDATE':
                action = 'REJECT_UPDATE'
                break;
            case 'UNAUTHORIZED_TOGGLE':
                action = 'REJECT_TOGGLE'
                break;
        }

        this.Approve(this.id, action, this.formReason.value['reason']);
    }

    approve(id, authorizationStatus) {
        this.id = id;
        let action = '';
        switch (authorizationStatus) {
            case 'UNAUTHORIZED_CREATE':
                action = 'APPROVE_CREATE';
                break;
            case 'UNAUTHORIZED_UPDATE':
                action = 'APPROVE_UPDATE'
                break;
            case 'UNAUTHORIZED_TOGGLE':
                action = 'APPROVE_TOGGLE'
                break;
        }
        this.approvalType = 'approve';
        this.Approve(id, action);
    }

    Approve(id, authorizationStatus, reason?) {
        this.booleans.loadAction = true;
        this.approvalservice.approve({
            id: id,
            reason: (reason && reason !== '') ? reason : ''
        }, authorizationStatus, this.userservice.getAuthUser())
            .subscribe(response => {
                this.booleans.loadAction = false;
                this.list.allContent.forEach((value, i) => {
                    if (id === value['id']) {
                        this.list.allContent.splice(i, 1);
                    }
                });
                this.notification.success('Approval was successful');
            },
            error => {
                this.booleans.loadAction = false;
                this.notification.error(CONSTANTS.ERROR_RESPONSE, error);
            })
    }
}