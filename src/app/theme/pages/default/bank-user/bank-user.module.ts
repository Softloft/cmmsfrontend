import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgxPaginationModule } from "ngx-pagination";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule, Routes } from "@angular/router";
import { LayoutModule } from "../../../layouts/layout.module";
import { BootstrapSwitchModule } from "angular2-bootstrap-switch";
import { DefaultComponent } from "../default.component";
import { AngularMultiSelectModule } from "angular2-multiselect-dropdown";
import { SharedModule } from "../../../../../shared/modules/shared.module";
import { NotificationModule } from "../../../../../shared/modules/notification.module";
import { PipeModule } from "../../../../../shared/modules/pipe.module";
import { BankUserComponent } from "./bank-user.component";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";

const routes: Routes = [
    {
        "path": "",
        "component": DefaultComponent,
        "children": [
            {
                "path": "",
                "component": BankUserComponent
            },
            {
                "path": ":code",
                "component": BankUserComponent
            },
            {
                "path": ":code/:status",
                "component": BankUserComponent
            }
        ]
    }
];

@NgModule({
    imports: [
        CommonModule,
        LayoutModule,
        FormsModule,
        SharedModule,
        PipeModule,
        NgbModule,
        RouterModule.forChild(routes),
        ReactiveFormsModule,
        AngularMultiSelectModule,
        NgxPaginationModule,
        BootstrapSwitchModule
    ],
    declarations: [BankUserComponent]
})
export class BankUserModule { }
