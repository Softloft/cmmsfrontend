import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormControl, FormGroup, Validators } from "@angular/forms";
import { ScriptLoaderService } from "../../../../_services/script-loader.service";
import { FormService } from "../../../../../services/api-service/formService";
import { NotificationService } from "../../../../../services/notification.service";
import { NibssService } from "../../../../../services/api-service/nibss.service";
import { Cache } from "../../../../../utils/cache";
import { APPROVAL_MESSAGE_USER, CONSTANTS } from "../../../../../utils/constants";

import { DomSanitizer } from "@angular/platform-browser";
import { JSON2PDFTABLE } from "../../../../../shared/export/json2pdf-table";
import { JSON2CSV } from "../../../../../shared/export/json2csv";
import { UserService } from "../../../../../services/user.service";
import { ApprovalService } from "../../../../../services/api-service/approval.service";
import { ActivatedRoute, Router } from "@angular/router";
import { BankService } from "../../../../../services/api-service/bank.service";
import Swal from "sweetalert2";
import {BillerService} from "../../../../../services/api-service/biller.service";

declare const $;

@Component({
    selector: 'app-bank-user',
    templateUrl: 'bank-user.component.html',
    styleUrls: ['bank-user.component.css']
})
export class BankUserComponent implements OnInit {
    public itemsPerPage = CONSTANTS.ITEMS_PER_PAGE;
    public moduleName = 'Bank Users';
    public name = '';
    public details = [];
    public bankId: any;
    private userType = '';
    public user = [];
    public totalElement = '';
    public approvedStatus = '';
    public createForm: FormGroup;
    public createFormSearch: FormGroup;
    public formReason: FormGroup;
    public id = 0;
    public code = '';
    public p = 1;
    public lga = null;
    public paramId = null;
    public role = '';
    public submit_type = 'Submit';
    public modal_title = 'Add';
    public pagesize = 10;
    public pagesizeArray = CONSTANTS.ITEMS_PER_PAGE_ARRAY;
    /*******start of Approval(maker/checker) declaration*******/
    public approvalStatus = '';
    public approvalType = '';
    public statusType = '';
    public approvalName = 'bank-user';
    public approvalName2 = 'bank';
    public authorization = '';
    /*******end of Approval(maker/checker) declaration*******/
    public list = {
        allContent: [],
        details: [],
        country: [],
        searchRoles: [],
        searchContent: [],
        itemRoleList: []
    };
    searchParams = {
        firstName: '',
        lastName: '',
        middleName: '',
        email: '',
        staffNumber: '',
        phoneNumber: '',
        city: '',
        role: '',
        state: '',
        lga: ''
    };

    public booleans = {
        loader: false,
        loaduserlist: false,
        submitEmandate: false,
        searchStatus: false,
        csvsearchloader: false,
        pdfsearchloader: false,
        getAllloader: false,
        loadingSearch: false,
        searchloader: false,
        loadCountry: false,
        loadState: false,
        loadLga: false,
        loadRole: false,
        loadBank: false,
        loadsubmit: false,
        initiator: false,
        authorizer: false,
        loadAction: false,
    };

    itemListCountry = [];
    selectedItemsCountry = [];
    settingsCountry = {};

    itemListState = [];
    selectedItemsState = [];
    settingsState = {};

    itemListLga = [];
    selectedItemsLga = [];
    settingsLga = {};

    itemListBank = [];
    selectedItemsBank = [];
    settingsBank = {};

    itemListUser = [];
    selectedItemsUser = [];
    settingsUser = {};

    public settings = {};
    public selectedItems = [];
    public itemList = [];

    private formdataSearch = function() {
        return {
            firstName: ['', Validators.compose([])],
            lastName: ['', Validators.compose([])],
            middleName: ['', Validators.compose([])],
            city: ['', Validators.compose([])],
            staffNumber: ['', Validators.compose([])],
            userAuthorisationType: ['', Validators.compose([])],
            email: ['', Validators.compose([])],
            phoneNumber: ['', Validators.compose([])],
            role: ['', Validators.compose([])],
            activated: ['', Validators.compose([])],
            createdAt: ['', Validators.compose([])],
        }
    };

    private formdata = function() {
        return {
            id: ['', Validators.compose([])],
            city: ['', Validators.compose([])],
            lga: ['', Validators.compose([])],
            country: ['', Validators.compose([])],
            state: ['', Validators.compose([])],
            streetName: ['', Validators.compose([])],
            userAuthorisationType: ['', Validators.compose([])],
            emailAddress: ['', Validators.compose([Validators.required, Validators.email])],
            phoneNumber: ['', Validators.compose([Validators.required, Validators.pattern(CONSTANTS.PHONE_REGEX)])],
            roleIds: ['', Validators.compose([])],
            userBankCode: ['', Validators.compose([])],
            userId: ['', Validators.compose([])],
            name: new FormGroup({
                firstName: new FormControl('', Validators.required),
                lastName: new FormControl('', Validators.required),
                middleName: new FormControl(''),
                title: new FormControl(''),
            }),
        }
    };

    private formdataReason = function() {
        return {
            reason: ['', Validators.compose([Validators.required])],
        }
    };

    constructor(private _script: ScriptLoaderService,
        private fb: FormBuilder,
        private domsanitizer: DomSanitizer,
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private formservice: FormService,
        private bankservice: BankService,
        private billerService: BillerService,
        private userservice: UserService,
        private approvalservice: ApprovalService,
        private nibssservice: NibssService,
        private notification: NotificationService) {
        this.createForm = this.fb.group(this.formdata());
        this.createFormSearch = this.fb.group(this.formdataSearch());
        this.formReason = this.fb.group(this.formdataReason());
    }

    ngOnInit() {
        this.user = Cache.get('userDetails');
        this.getUserParam();
        // this.getActivatedBank();
        this.getCountry();
        this.details = this.userservice.getAuthUser();
        this.role = this.userservice.getAuthUser()['roles'][0].split(' ').join('_');
        if (this.details['roles'][0] === 'NIBSS AUTHORIZER'
            || this.details['roles'][0] === 'BANK ADMIN AUTHORIZER') {
            this.booleans.authorizer = true;
        }
        if (this.details['roles'][0] === 'NIBSS INITIATOR'
            || this.details['roles'][0] === 'BANK ADMIN INITIATOR') {
            this.booleans.initiator = true;
        }
        this.settings = {
            singleSelection: true,
            text: "Select Fields",
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            searchPlaceholderText: 'Search Fields',
            enableSearchFilter: true,
            badgeShowLimit: 5,
            groupBy: "category"
        };

        this.settingsBank = {
            singleSelection: true,
            text: "Select Fields",
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            searchPlaceholderText: 'Search Fields',
            enableSearchFilter: true,
            badgeShowLimit: 5,
            groupBy: "category"
        };

        this.settingsUser = {
            singleSelection: false,
            enableSearchFilter: true,
            text: "Select User",
            badgeShowLimit: 5
        };

        this.settingsLga = {
            singleSelection: true,
            text: "Select Lga",
            enableSearchFilter: true,
            badgeShowLimit: 5
        };

        this.settingsState = {
            singleSelection: true,
            text: "Select State",
            enableSearchFilter: true,
            badgeShowLimit: 5
        };

        this.settingsCountry = {
            singleSelection: true,
            text: "Select Country",
            enableSearchFilter: true,
            badgeShowLimit: 5
        };

        this.list.allContent = [];
    }


    openModal() {
        this.modal_title = 'Add';
        this.submit_type = 'Submit';
        this.createForm.reset();
        $('#CreateModal').modal('show');
    }

    getMainPage() {
        this.approvalStatus = '';
        this.router.navigate([this.approvalName]);
    }

    onChangePageSize(event) {
        this.pagesize = event.target.value;
        this.getBankUserById(this.p - 1, this.pagesize);
    }

    getUserParam() {
        this.getRole('BANK');
        this.getSearchRole('BANK');
        if (this.activatedRoute.snapshot.paramMap.get('code')) {
            this.userType = 'admin';
            this.paramId = this.activatedRoute.snapshot.paramMap.get('code');
            this.bankId = this.activatedRoute.snapshot.paramMap.get('code');
            this.getBankById(this.bankId);
        } else if (this.user['userBank']) {
            this.userType = 'user';
            this.code = this.user['userBank']['code'];
            this.bankId = this.user['userBank']['id'];
            const status = this.activatedRoute.snapshot.paramMap.get('status');
            if (status && status !== '') {
                this.getParam();
            } else {
                this.getBankUserById(0, this.pagesize);
            }
        }

        this.createForm.patchValue({ userBankCode: this.code });
        // (this.code && this.code !== '') ? this.getBankUserById(0) : this.getBankUser(0);
    }

    getBankById(id) {
        this.booleans.loader = true;
        this.bankservice.getBankById(id)
            .subscribe(response => {
                this.code = response.code;
                this.name = response.name;
                const status = this.activatedRoute.snapshot.paramMap.get('status');
                console.log('bank user status :: ', status)
                if (status && status !== '') {
                    this.getParam();
                } else {
                    this.getBankUserById(0, this.pagesize);
                }
            },
            error => {
                this.notification.error('Unable to load Bank(s)', error);
            })
    }

    getBankUserById(pageNumber, pagesize) {
        this.booleans.loader = true;
        this.approvalStatus = '';
        this.list.allContent = [];
        this.bankservice.getUserByUsers(this.bankId, pageNumber, pagesize)
            .subscribe((response) => {
                this.booleans.loader = false;
                this.list.allContent = response.content;
                this.totalElement = (response.totalElement) ? response.totalElement : response.totalElements;
            },
            error => {
                this.booleans.loader = false;
                this.notification.error('Unable to load bank user', error);
            })
    }

    getCsv(event?) {
        if (event === 'all') {
            // (this.code && this.code !== '') ? this.getAllBankUserByCode(0, this.totalElement, 'csv') : this.getAllBankUser(0, this.totalElement, 'csv');
            this.getAllBankUser(0, this.totalElement, 'csv')
        } else {
            const exportContent = this.list.allContent;
            this.exportToCsv(exportContent);
        }
    }

    getPdf(event?) {
        if (event === 'all') {
            // (this.code && this.code !== '') ? this.getAllBankUserByCode(0, this.totalElement, 'pdf') : this.getAllBankUser(0, this.totalElement, 'pdf');
            this.getAllBankUser(0, this.totalElement, 'pdf')
        } else {
            const exportContent = this.list.allContent;
            this.exportToPdf(exportContent);
        }
    }

    private exportToCsv(exportContent) {
        const csvArray = [];
        const fields = ['First Name', 'Last Name', 'Email', 'Role', 'Created Date', 'Status'];
        exportContent.forEach((value) => {
            csvArray.push({
                'First Name': value['name'] ? value['name']['firstName'] : '',
                'Last Name': value['name'] ? value['name']['lastName'] : '',
                'Email': value.emailAddress,
                'Role': value.roles[0]['name'].split('_').join(''),
                'Created Date': new Date(value.createdAt).toString().split('GMT')[0],
                'Status': (value.activated) ? 'Active' : 'Inactive',
            })
        });
        JSON2CSV(fields, csvArray, 'nibss');
    }

    private exportToPdf(exportContent) {
        const rows = [];
        const columns = ["s/n  ", "First Name", "Last Name", "Email", "Role", "Date Created", "Status"];
        exportContent.forEach((value, i) => {
            rows.push([i + 1, value['name'] ? value['name']['firstName'] : '', value['name'] ? value['name']['lastName'] : '', value.emailAddress, value.roles[0]['name'].split('_').join(' '), new Date(value.createdAt).toString().split('GMT')[0], (value.activated) ? 'Active' : 'Inactive'])
        });
        JSON2PDFTABLE(columns, rows, this.moduleName);
    }

    toggle(data) {
        this.id = data.id;
        let statusMessage = '';
        statusMessage = (data.activated) ? 'Disapprove' : 'Approve';
        Swal({
            title: '',
            html: '<b style="font-weight: bold">' + APPROVAL_MESSAGE_USER(statusMessage) + '</b>',
            type: null,
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes!'
        }).then((result) => {
            if (result.value) {
                this.bankservice.toggleUser({ id: data.id })
                    .subscribe(response => {
                        console.log('response :: ', response);
                        this.list.allContent.forEach((value, i) => {
                            if (value.id === this.id) {
                                this.list.allContent[i] = value;
                            }
                        });
                        this.notification.success(CONSTANTS.TOGGLE_RESPONSE);
                    },
                    error => {
                        this.notification.error(CONSTANTS.ERROR_RESPONSE, error);
                    });
            }
        })
    }

    exportSearchMethod(pageNo) {
        let role = '';
        if (this.searchParams.role !== '') {
            role = this.searchParams.role.split(' ').join('_');
        }
        const params = `firstName=${this.searchParams.firstName}&lastName=${this.searchParams.lastName}&middleName=${this.searchParams.middleName}&email=${this.searchParams.email}&phoneNumber=${this.searchParams.phoneNumber}&city=${this.searchParams.city}&role=${role}&state=${this.searchParams.state}&lga=${this.searchParams.lga}`;
        this.booleans.loadingSearch = true;
        this.nibssservice.search(params, pageNo)
            .subscribe((response) => {
                this.list.allContent = response.content;
                this.totalElement = response.totalElements;
                if (this.booleans.pdfsearchloader) {
                    this.getPdf()
                } else if (this.booleans.csvsearchloader) {
                    this.getCsv();
                }
                this.booleans.csvsearchloader = false;
                this.booleans.pdfsearchloader = false;
                this.booleans.loadingSearch = false;
            },
            error => {
                this.booleans.csvsearchloader = false;
                this.booleans.pdfsearchloader = false;
                this.booleans.loadingSearch = false;
                this.notification.error('Unable to Search', error);
            })
    }

    searchMethod(pageNo) {
        this.booleans.searchStatus = true;
        this.booleans.loadingSearch = true;
        this.bankservice.search(this.createFormSearch.value, pageNo)
            .subscribe((response) => {
                this.booleans.loader = false;
                this.booleans.loadingSearch = false;
                if (response.content.length === 0) {
                    this.notification.error('No Result Found')
                } else {
                    this.list.allContent = response.content;
                    if (this.booleans.pdfsearchloader || this.booleans.csvsearchloader) {
                        this.exportAs();
                    }
                    $('#searchModal').modal('hide');
                }
                this.booleans.csvsearchloader = false;
                this.booleans.pdfsearchloader = false;
            },
            error => {
                this.booleans.loader = false;
                this.booleans.csvsearchloader = false;
                this.booleans.pdfsearchloader = false;
                this.booleans.loadingSearch = false;
                this.notification.error('Unable to Search', error);
            })
    }

    private exportAs() {
        if (this.booleans.pdfsearchloader) {
            this.getPdf()
        } else if (this.booleans.csvsearchloader) {
            this.getCsv();
        }
    }

    exportCsvSearch() {
        this.booleans.csvsearchloader = true;
        this.searchMethod(0);
    }

    exportPdfSearch() {
        this.booleans.pdfsearchloader = true;
        this.searchMethod(0);
    }

    onItemSelect(item: any) {
    }

    OnItemDeSelect(item: any) {
    }

    onSelectAll(items: any) {
    }

    onDeSelectAll(items: any) {
    }

    onItemSelectBank(item: any) {
        this.selectedItemsBank = [{ 'id': item.id, 'itemName': item.itemName }];
    }

    OnItemDeSelectBank(item: any) {
    }

    onSelectAllBank(items: any) {
    }

    onDeSelectAllBank(items: any) {
    }

    onItemSelectLga(item: any) {
        this.selectedItemsLga = [{ 'id': item.id, 'itemName': item.itemName }];
    }

    OnItemDeSelectLga(item: any) {
    }

    onSelectAllLga(items: any) {
    }

    onDeSelectAllLga(items: any) {
    }

    onItemSelectState(item: any) {
        this.itemListLga = [];
        this.getLgaByState(item.id);
    }

    getCountry() {
        this.booleans.loadCountry = true;
        this.formservice.getCountry()
            .subscribe((response) => {
                response.forEach((value) => {
                    this.itemListCountry.push({
                        "id": value.id,
                        "itemName": value.name
                    });
                });
                this.booleans.loadCountry = false;
            },
            error => {
                this.booleans.loadCountry = false;
                this.notification.error('Unable to load country', error);
            })
    }

    getStateByCountry(id) {
        this.itemListState = [];
        this.booleans.loadState = true;
        this.formservice.getStateByCountry(id)
            .subscribe((response) => {
                response.forEach((value) => {
                    this.itemListState.push({
                        "id": value.id,
                        "itemName": value.name
                    });
                });
                this.booleans.loadState = false;
            },
            (error) => {
                this.booleans.loadState = false;
                this.notification.error('Unable to load state', error);
            })
    }

    getLgaByState(id) {
        this.booleans.loadLga = true;
        this.formservice.getLgaByState(id)
            .subscribe((response) => {
                response.forEach((value) => {
                    this.itemListLga.push({
                        "id": value.id,
                        "itemName": value.name
                    });
                });
                this.booleans.loadLga = false;
            },
            (error) => {
                this.booleans.loadLga = false;
                this.notification.error('Unable to load lga', error);
            })
    }

    getRole(event) {
        this.booleans.loadRole = true;
        let userType = '';
        if (this.user['roles'][0]['name'] === 'NIBSS_ADMIN') {
            userType = 'ADMIN';
        } else {
            userType = 'USER';
        }
        this.formservice.getRoleByUser(event, userType)
            .subscribe((response) => {
                this.itemList = [];
                response.forEach(value => {
                    this.itemList.push({
                        id: value.id,
                        itemName: value.name.split('_').join(' '),
                        category: value.userType,
                        description: value.description
                    })
                })
                this.booleans.loadRole = false;
            },
            (error) => {
                this.booleans.loadRole = false;
                this.notification.error('Unable to load role', error);
            })
    }

    getSearchRole(event) {
        this.formservice.getSearchRole(event)
            .subscribe((response) => {
                this.list.searchRoles = response;
                this.booleans.loadRole = false;
            },
            (error) => {
                this.booleans.loadRole = false;
                this.notification.error('Unable to load role', error);
            })
    }

    getActivatedBank() {
        this.booleans.loadBank = true;
        this.approvalservice.getBankApproval('APPROVAL')
            .subscribe((response) => {
                console.log('approved bank :: ', response);
                this.itemListBank = [];
                response.forEach(value => {
                    this.itemListBank.push({
                        id: value.id,
                        itemName: value.name.split('_').join(' '),
                        category: value.userType
                    })
                })
                this.booleans.loadBank = false;
            },
            (error) => {
                this.booleans.loadBank = false;
                this.notification.error('Unable to load Bank', error);
            })
    }

    submit() {
        this.extractIds();
        this.booleans.loadsubmit = true;
        this.bankservice.postBankUser(this.createForm.value, this.userType)
            .subscribe(response => {
                this.list.allContent.unshift(response);
                this.booleans.loadsubmit = false;
                $('#CreateModal').modal('hide');
                this.createForm.reset();
                this.notification.success(CONSTANTS.SUBMIT_RESPONSE);
            },
            error => {
                this.booleans.loadsubmit = false;
                this.notification.error(CONSTANTS.ERROR_RESPONSE, error);
            });
    }

    view(data) {
        this.list.details = data;
        $('#ViewModal').modal('show');
    }

    edit(data) {
        this.id = data.id;
        this.createForm.reset();
        this.selectedItems = [];
        this.submit_type = 'Update';
        this.list.details = data;
        /*if (data.contactDetails && data.contactDetails.state) {
         this.itemListState.push({id: data.contactDetails.state.id, itemName: data.contactDetails.state.name})
         this.itemListCountry.push({id: data.contactDetails.country.id, itemName: data.contactDetails.country.name})
         }*/
        if (data.contactDetails && data.contactDetails.lga) {
            this.itemListLga.push({ id: data.contactDetails.lga.id, itemName: data.contactDetails.lga.name });
        }
        if (data.roles) {
            data.roles.forEach((value) => {
                this.selectedItems.push({
                    id: value.id,
                    itemName: value.name.split('_').join(' '),
                    category: value.userType
                })
            });
        }

        //        this.getLga(data.user.contactDetails.lga.state.id);
        // this.selectedItemsRole = [{'id': data.user.contactDetails.lga.state.id, 'itemName': data.user.contactDetails.lga.state.name}]
        if (data.contactDetails && data.contactDetails.lga) {
            this.lga = [{ 'id': data.contactDetails.lga.id, 'itemName': data.contactDetails.lga.name }];
        }
        const formdata = function() {
            return {

                city: [(data.contactDetails && data.contactDetails.city) ? data.contactDetails.city : '', Validators.compose([])],
                lga: [(data.contactDetails && data.contactDetails.lga) ? [{
                    'id': data.contactDetails.lga.id,
                    'itemName': data.contactDetails.lga.name
                }] : '', Validators.compose([])],
                roleId: ['', Validators.compose([])],
                // userBankCode: ['', Validators.compose([Validators.required])],


                state: [(data.contactDetails && data.contactDetails.state) ? [{
                    'id': data.contactDetails.state.id,
                    'itemName': data.contactDetails.state.name
                }] : '', Validators.compose([])],
                country: [(data.contactDetails && data.contactDetails.country) ? [{
                    'id': data.contactDetails.country.id,
                    'itemName': data.contactDetails.country.name
                }] : '', Validators.compose([])],
                roleIds: [[{
                    'id': data.roles[0]['id'],
                    'itemName': data.roles[0]['name']
                }], Validators.compose([Validators.required])],
                emailAddress: [data.emailAddress, Validators.compose([Validators.required, Validators.email])],
                userAuthorisationType: [(data.makerCheckerType) ? data.makerCheckerType : '', Validators.compose([])],
                streetName: [(data.contactDetails && data.contactDetails.streetName) ? data.contactDetails.streetName : '', Validators.compose([])],
                userId: [data.id, Validators.compose([])],
                id: [data.id, Validators.compose([])],
                name: new FormGroup({
                    firstName: new FormControl((data.name && data.name.firstName) ? data.name.firstName : '', Validators.required),
                    lastName: new FormControl((data.name && data.name.lastName) ? data.name.lastName : '', Validators.required),
                    middleName: new FormControl(((data.name && data.name.middleName) ? data.name.middleName : '')),
                }),
                phoneNumber: [data.phoneNumber, Validators.compose([Validators.required, Validators.pattern(CONSTANTS.PHONE_REGEX)])],
            }
        };
        this.createForm = this.fb.group(formdata());
        $('#CreateModal').modal('show');
    }

    update() {
        this.extractIds();
        this.booleans.loadsubmit = true;
        this.bankservice.updateBankUser(this.createForm.value, this.userType)
            .subscribe(response => {
                this.list.allContent.forEach((value, i) => {
                    if (value.id === this.id) {
                        this.list.allContent[i] = response;
                    }
                });
                $('#CreateModal').modal('hide');
                this.createForm.reset();
                this.booleans.loadsubmit = false;
                if (this.approvalStatus !== '') {
                    this.list.allContent.forEach((value, i) => {
                        if (this.id === value['id']) {
                            this.list.allContent.splice(i, 1);
                        }
                    });
                }
                this.notification.success(CONSTANTS.UPDATE_RESPONSE);
            },
            error => {
                this.booleans.loadsubmit = false;
                this.notification.error(CONSTANTS.ERROR_RESPONSE, error);
            });
    }

    private extractIds() {
        const role = [];
        this.createForm.value['bankId'] = this.bankId;
        if (this.code && this.code !== '') {
            this.createForm.value['userBankCode'] = this.code;
        }
        this.createForm.value['countryId'] = (this.createForm.value['country'] && this.createForm.value['country'][0] ? this.createForm.value['country'][0]['id'] : '');
        this.createForm.value['stateId'] = (this.createForm.value['state'] && this.createForm.value['state'][0] ? this.createForm.value['state'][0]['id'] : '');
        this.createForm.value['lgaId'] = (this.createForm.value['lga'] && this.createForm.value['lga'][0] ? this.createForm.value['lga'][0]['id'] : '');
        this.createForm.value['roleIds'].forEach((value) => {
            role.push(value.id);
        });
        this.createForm.value['roleId'] = role[0];
    }

    getBankUser(pageNo, pagesize) {
        this.booleans.searchStatus = false;
        this.booleans.loader = true;
        console.log('id :: ', this.bankId)
        this.bankservice.getBankUser(pageNo, pagesize, this.bankId)
            .subscribe(response => {
                this.booleans.loader = false;
                this.list.allContent = response.content;
                this.totalElement = response.totalElements;
            },
            error => {
                this.booleans.loader = false;
                this.notification.error('Unable to load Users', error);
            })
    }

    getAllBankUserByCode(pageNo, totalNumber, type) {
        this.booleans.getAllloader = true;
        this.bankservice.getAllBankUserByCode(this.code, pageNo, totalNumber)
            .subscribe(response => {
                this.booleans.getAllloader = false;
                (type === 'csv') ? this.exportToCsv(response.content) : this.exportToPdf((response.content));
            },
            error => {
                this.booleans.getAllloader = false;
                this.notification.error('Unable to load Bank Users', error);
            })
    }

    getAllBankUser(pageNo, totalNumber, type) {
        this.booleans.getAllloader = true;
        this.bankservice.getUserByUsers(this.bankId, pageNo, totalNumber)
            .subscribe(response => {
                this.booleans.getAllloader = false;
                (type === 'csv') ? this.exportToCsv(response.content) : this.exportToPdf((response.content));
            },
            error => {
                this.booleans.getAllloader = false;
                this.notification.error('Unable to load Bank Users', error);
            })
    }

    /*search() {
        this.booleans.searchloader = true;
        this.nibssservice.getNibss('search')
            .subscribe(response => {
                    this.booleans.searchloader = false;
                    this.list.searchContent = response.content;
                },
                error => {
                    this.booleans.searchloader = false;
                    this.notification.error('Unable to search Bank Users', error);
                })
    }*/

    getPage(event) {
        this.p = event;
        this.list.allContent = [];
        if (!this.booleans.searchStatus) {
            (this.code !== '') ? this.getBankUser(this.p - 1, this.pagesize) : this.getBankUserById(0, this.pagesize);
        } else {
            this.searchMethod(this.p - 1)
        }
    }

    getSearchModal() {
        for (let params in this.searchParams) {
            this.searchParams[params] = '';
        }
        $('#searchModal').modal('show');
    }

    OnItemDeSelectState(item: any) {
        this.itemListLga = [];
    }

    onSelectAllState(items: any) {
    }

    onDeSelectAllState(items: any) {
    }

    onItemSelectCountry(item: any) {
        this.getStateByCountry(item.id);
    }

    OnItemDeSelectCountry(item: any) {
        this.itemListState = [];
        this.itemListLga = [];
    }

    onSelectAllCountry(items: any) {
    }

    onDeSelectAllCountry(items: any) {
    }

    /***********************************APPROVAL(MAKER AND CHECKER)*****************************************/
    public getParam(param?) {
        console.log('param :: ', param)
        this.list.allContent = [];
        if (param) {
            switch (param) {
                case this.approvalName + '/' + this.bankId + '/pending':
                    this.statusType = 'UNAUTHORIZED';
                    this.approvalStatus = 'Pending';
                    break;
                case this.approvalName + '/' + this.bankId + '/approved':
                    this.statusType = 'AUTHORIZED';
                    this.approvalStatus = 'Approved';
                    break;
                case this.approvalName + '/' + this.bankId + '/disapproved':
                    this.statusType = 'REJECTED';
                    this.approvalStatus = 'Disapproved';
                    break;
            }
            this.router.navigate(['/' + param]);
        } else {
            const status = this.activatedRoute.snapshot.paramMap.get('status');
            switch (status) {
                case 'pending':
                    this.statusType = 'UNAUTHORIZED';
                    this.approvalStatus = 'Pending';
                    break;
                case 'approved':
                    this.statusType = 'AUTHORIZED';
                    this.approvalStatus = 'Approved';
                    break;
                case 'disapproved':
                    this.statusType = 'REJECTED';
                    this.approvalStatus = 'Disapproved';
                    break;
            }
        }
        this.getAuthorization(this.statusType, 0);
    }

    getApprovalPage(event) {
        this.p = +event;
        this.list.allContent = [];
        this.getAuthorization(this.statusType, this.p - 1);
    }


    getAuthorization(approval_type, pageNo) {
        this.booleans.loader = true;
        this.approvalservice.getAuthorization(approval_type, pageNo, this.approvalName2, this.bankId)
            .subscribe(response => {
                this.booleans.loader = false;
                this.list.allContent = response.content;
                this.totalElement = response.totalElements;
            },
            error => {
                this.booleans.loader = false;
                this.notification.error('Unable to load content', error);
            })
    }

    getReason(data, authorizationStatus) {
        this.id = data.id;
        this.authorization = authorizationStatus;
        this.list.details = data;
        this.approvalType = 'disapprove';
        $('#reasonModal').modal('show');
    }

    disapprove() {
        let action = '';
        this.approvedStatus = 'reject';
        this.booleans.loadAction = true;
        switch (this.authorization) {
            case 'UNAUTHORIZED_CREATE':
                action = 'REJECT_CREATE';
                break;
            case 'UNAUTHORIZED_UPDATE':
                action = 'REJECT_UPDATE'
                break;
            case 'UNAUTHORIZED_TOGGLE':
                action = 'REJECT_TOGGLE'
                break;
        }

        this.Approve(this.id, action, this.formReason.value['reason']);
    }

    approve(id, authorizationStatus) {
        this.id = id;
        this.approvedStatus = '';
        let action = '';
        switch (authorizationStatus) {
            case 'UNAUTHORIZED_CREATE':
                action = 'APPROVE_CREATE';
                break;
            case 'UNAUTHORIZED_UPDATE':
                action = 'APPROVE_UPDATE'
                break;
            case 'UNAUTHORIZED_TOGGLE':
                action = 'APPROVE_TOGGLE'
                break;
        }
        this.approvalType = 'approve';
        this.Approve(this.id, action);
    }

    Approve(id, authorizationStatus, reason?) {
        this.booleans.loadAction = true;
        this.approvalservice.approve({
            id: id,
            reason: (reason && reason !== '') ? reason : ''
        }, authorizationStatus, this.approvalName2)
            .subscribe(response => {
                this.booleans.loadAction = false;
                this.list.allContent.forEach((value, i) => {
                    if (id === value['id']) {
                        this.list.allContent.splice(i, 1);
                    }
                });
                this.formReason.reset();
                const status = (this.approvedStatus === 'reject') ? 'Disapproval' : 'Approval';
                this.notification.success(status + ' was successful');
            },
            error => {
                this.booleans.loadAction = false;
                this.notification.error(CONSTANTS.ERROR_RESPONSE, error);
            })
    }

    /************************************** Emandate *******************************/
    onOpenEmandateonfig() {
        this.getUserList();
        $('#emandateForward').modal('show')
    }

    getUserList() {
        this.booleans.loaduserlist = true;
        this.bankservice.getUserList(this.bankId)
            .subscribe((response) => {
                    response.forEach((value) => {
                        this.itemListUser.push({
                            "id": value.emailAddress,
                            "itemName": value.name.firstName+ ' '+ value.name.lastName
                        });
                    });
                    this.booleans.loaduserlist = false;
                },
                (error) => {
                    this.booleans.loaduserlist = false;
                    this.notification.error('Unable to load user(s)', error);
                })
    }

    forwardEmandate() {
        const emails = [];
        this.selectedItemsUser.forEach((value => {
            emails.push(value.id)
        }));
        this.booleans.submitEmandate = true;
        this.billerService.forwardEmandate({emails: emails, entityTypeEmandate: 'BANK', objectId: this.bankId})
            .subscribe((response) => {
                    this.selectedItemsUser = [];
                    this.booleans.submitEmandate = false;
                    this.notification.success('Emandate details successfully sent')
                },
                (error) => {
                    this.booleans.submitEmandate = false;
                    this.notification.error('Unable to process this action', error);
                })
    }
}