import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgxPaginationModule } from "ngx-pagination";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule, Routes } from "@angular/router";
import { LayoutModule } from "../../../layouts/layout.module";
import { BootstrapSwitchModule } from "angular2-bootstrap-switch";
import { DefaultComponent } from "../default.component";
import { AngularMultiSelectModule } from "angular2-multiselect-dropdown";
import { SharedModule } from "../../../../../shared/modules/shared.module";
import { PipeModule } from "../../../../../shared/modules/pipe.module";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { SettingsComponent } from "./settings.component";
import { ApprovalComponent } from "../../../../../shared/components/setup-emandate/approvals/approval.component";

const routes: Routes = [
    {
        "path": "",
        "component": DefaultComponent,
        "children": [
            {
                "path": ":entityType/:status",
                "component": ApprovalComponent
            }
        ]
    }
];

@NgModule({
    imports: [
        CommonModule,
        LayoutModule,
        FormsModule,
        SharedModule,
        PipeModule,
        NgbModule,
        RouterModule.forChild(routes),
        ReactiveFormsModule,
        AngularMultiSelectModule,
        NgxPaginationModule,
        BootstrapSwitchModule
    ],
    declarations: [SettingsComponent]
})
export class SettingsModule {
}
