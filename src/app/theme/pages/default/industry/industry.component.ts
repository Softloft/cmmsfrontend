import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ScriptLoaderService } from "../../../../_services/script-loader.service";
import { NotificationService } from "../../../../../services/notification.service";
import { IndustryService } from "../../../../../services/api-service/industry.service";
import { JSON2CSV } from "../../../../../shared/export/json2csv";
import { JSON2PDFTABLE } from "../../../../../shared/export/json2pdf-table";
import { UserService } from "../../../../../services/user.service";
import { ApprovalService } from "../../../../../services/api-service/approval.service";
import { ActivatedRoute, Router } from "@angular/router";

import Swal from "sweetalert2";
import { APPROVAL_MESSAGE_USER, CONSTANTS } from "../../../../../utils/constants";
declare const $;
@Component({
    selector: 'app-industry',
    templateUrl: 'industry.component.html',
    styles: []
})
export class IndustryComponent implements OnInit {
    public moduleName = 'Category';

    public approvedStatus = '';
    public createForm: FormGroup;
    public createFormSearch: FormGroup;
    public formReason: FormGroup;
    public p = 1;
    public lgaId = null;
    public submit_type = 'Submit';
    public modal_title = 'Add';
    public user = [];
    public details = [];
    public totalElement = '';
    public id = 0;
    public pagesize = 10;
    public pagesizeArray = CONSTANTS.ITEMS_PER_PAGE_ARRAY;
    /*******start of Approval(maker/checker) declaration*******/
    public approvalStatus = '';
    public approvalType = '';
    public statusType = '';
    public approvalName = 'industry';
    public authorization = '';
    /*******end of Approval(maker/checker) declaration*******/

    public list = {
        allContent: [],
        details: [],
        country: [],
        searchContent: []
    };

    public booleans = {
        loader: false,
        searchStatus: false,
        csvsearchloader: false,
        pdfsearchloader: false,
        getAllloader: false,
        loadingSearch: false,
        searchloader: false,
        loadRole: false,
        loadsubmit: false,
        initiator: false,
        authorizer: false,
        loadAction: false,
    };

    private formsearchdata = function() {
        return {
            name: ['', Validators.compose([])],
            description: ['', Validators.compose([])]
        }
    };

    private formdata = function() {
        return {
            name: ['', Validators.compose([Validators.required])],
            description: ['', Validators.compose([Validators.required])]
        }
    };

    private formdataReason = function() {
        return {
            reason: ['', Validators.compose([Validators.required])],
        }
    };

    constructor(private _script: ScriptLoaderService,
        private fb: FormBuilder,
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private approvalservice: ApprovalService,
        private industryService: IndustryService,
        private userservice: UserService,
        private notification: NotificationService) {
        this.createForm = this.fb.group(this.formdata());
        this.createFormSearch = this.fb.group(this.formsearchdata());
        this.formReason = this.fb.group(this.formdataReason());
    }

    ngOnInit() {
        // this.user['userType'] === 'NIBSS'
        const status = this.activatedRoute.snapshot.paramMap.get('status');
        if (status && status !== '') {
            this.getParam();
        } else {
            this.getIndustry(0, this.pagesize);
        }
        this.user = this.userservice.getAuthUser();
        this.list.allContent = [];
        this.details = this.userservice.getAuthUser();
        if (this.details['roles'][0] === 'NIBSS AUTHORIZER') {
            this.booleans.authorizer = true;
        }
        if (this.details['roles'][0] === 'NIBSS INITIATOR') {
            this.booleans.initiator = true;
        }
    }


    openModal() {
        this.modal_title = 'Add';
        this.submit_type = 'Submit';
        $('#CreateModal').modal('show');
    }

    getMainPage() {
        this.approvalStatus = '';
        this.getIndustry(0, this.pagesize)
        // this.router.navigate([this.approvalName]);
    }

    onChangePageSize(event) {
        this.pagesize = event.target.value;
        this.getIndustry(this.p - 1, this.pagesize);
    }

    getCsv(event?) {
        if (event === 'all') {
            this.getAllIndustry(0, this.totalElement, 'csv');
        } else {
            const exportContent = this.list.allContent;
            this.exportToCsv(exportContent);
        }
    }

    private exportToCsv(exportContent) {
        const csvArray = [];
        const fields = [
            'Name',
            'Description',
            'Date Created'
        ];
        exportContent.forEach((value) => {
            csvArray.push({
                'Name': value.name,
                'Description': value.description,
                'Date Created': new Date(value.createdAt).toString().split('GMT')[0]
            })
        });
        JSON2CSV(fields, csvArray, 'industry');
    }

    getPdf(event?) {
        if (event === 'all') {
            this.getAllIndustry(0, this.totalElement, 'pdf');
        } else {
            const exportContent = this.list.allContent;
            this.exportToPdf(exportContent);
        }
    }

    private exportToPdf(exportContent) {
        const rows = [];
        const columns = ["s/n  ", "Name", "Description", "Date Created"];
        exportContent.forEach((value, i) => {
            rows.push([i + 1, value.name, value.description, new Date(value.createdAt).toString().split('GMT')[0]])
        });
        JSON2PDFTABLE(columns, rows, 'industry');
    }

    toggle(data) {
        this.id = data.id;
        let statusMessage = '';
        statusMessage = (data.activated) ? 'Disapprove' : 'Approve';
        Swal({
            title: '',
            html: '<b style="font-weight: bold">' + APPROVAL_MESSAGE_USER(statusMessage) + '</b>',
            type: null,
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes!'
        }).then((result) => {
            if (result.value) {
                this.industryService.toggle({ id: data.id })
                    .subscribe(response => {
                        console.log('response :: ', response);
                        this.list.allContent.forEach((value, i) => {
                            if (value.id === this.id) {
                                this.list.allContent[i] = value;
                            }
                        });
                        this.notification.success(CONSTANTS.TOGGLE_RESPONSE);
                    },
                    error => {
                        this.notification.error(CONSTANTS.ERROR_RESPONSE, error);
                    });
            }
        })
    }

    exportCsvSearch() {
        this.booleans.csvsearchloader = true;
        this.searchMethod(this.totalElement);
    }

    exportPdfSearch() {
        this.booleans.pdfsearchloader = true;
        this.searchMethod(this.totalElement);
    }


    submit() {
        this.booleans.loadsubmit = true;
        this.industryService.postIndustry(this.createForm.value)
            .subscribe(response => {
                this.list.allContent.unshift(response);
                this.booleans.loadsubmit = false;
                $('#CreateModal').modal('hide');
                this.createForm.reset();
                this.notification.success('Creation was successful');
            },
            error => {
                this.booleans.loadsubmit = false;
                this.notification.error(CONSTANTS.ERROR_RESPONSE, error);
            });
    }

    view(data) {
        this.modal_title = 'View';
        this.list.details = data;
        $('#ViewModal').modal('show');
    }

    edit(data) {
        this.id = data.id;
        this.submit_type = 'Update';
        this.modal_title = 'Update';
        const formdata = function() {
            return {
                name: [data.name, Validators.compose([Validators.required])],
                description: [data.description, Validators.compose([Validators.required])],
                id: [data.id, Validators.compose([])],
            }
        };
        this.createForm = this.fb.group(formdata());
        $('#CreateModal').modal('show');
    }

    update() {
        this.booleans.loadsubmit = true;
        this.industryService.updateIndustry(this.createForm.value)
            .subscribe(response => {
                this.list.allContent.forEach((value, i) => {
                    if (value.id === this.id) {
                        this.list.allContent[i] = response;
                    }
                });
                $('#CreateModal').modal('hide');
                this.createForm.reset();
                this.booleans.loadsubmit = false;
                if (this.approvalStatus !== '') {
                    this.list.allContent.forEach((value, i) => {
                        if (this.id === value['id']) {
                            this.list.allContent.splice(i, 1);
                        }
                    });
                }
                this.notification.success(CONSTANTS.UPDATE_RESPONSE);
            },
            error => {
                this.booleans.loadsubmit = false;
                this.notification.error(CONSTANTS.ERROR_RESPONSE, error);
            });
    }

    searchMethod(pageNo) {
        this.booleans.searchStatus = true;
        this.booleans.loadingSearch = true;
        this.industryService.search(this.createFormSearch.value, pageNo)
            .subscribe((response) => {
                    this.booleans.loader = false;
                    this.booleans.loadingSearch = false;
                    if (response.content.length === 0) {
                        this.notification.error('No Result Found')
                    } else {
                        this.list.allContent = response.content;
                      }
                },
                error => {
                    this.booleans.loader = false;
                    this.booleans.loadingSearch = false;
                    this.notification.error('Unable to Search', error);
                })
    }

    getIndustry(pageNo, pagesize) {
        this.booleans.searchStatus = false;
        this.booleans.loader = true;
        this.industryService.getIndustry(pageNo, pagesize)
            .subscribe(response => {
                this.booleans.loader = false;
                this.list.allContent = response.content;
                this.totalElement = response.totalElements;
            },
            error => {
                this.booleans.loader = false;
                this.notification.error('Unable to load industry', error);
            })
    }

    getAllIndustry(pageNo, totalNumber, type) {
        this.booleans.getAllloader = true;
        this.industryService.getAllIndustry(pageNo, totalNumber)
            .subscribe(response => {
                this.booleans.getAllloader = false;
                (type === 'csv') ? this.exportToCsv(response.content) : this.exportToPdf((response.content));
            },
            error => {
                this.booleans.getAllloader = false;
                this.notification.error('Unable to load Industry', error);
            })
    }

    search() {
    }

    getPage(event) {
        this.p = event;
        this.list.allContent = [];
        (!this.booleans.searchStatus) ? this.getIndustry(this.p - 1, this.pagesize) : this.searchMethod(this.p - 1);
    }

    getSearchModal() {
        this.createFormSearch.reset();
        $('#searchModal').modal('show');

    }

    /***********************************APPROVAL(MAKER AND CHECKER)*****************************************/
    public getParam(param?) {
        this.list.allContent = [];
        if (param) {
            switch (param) {
                case this.approvalName + '/pending':
                    this.statusType = 'UNAUTHORIZED';
                    this.approvalStatus = 'Pending';
                    break;
                case this.approvalName + '/approved':
                    this.statusType = 'AUTHORIZED';
                    this.approvalStatus = 'Approved';
                    break;
                case this.approvalName + '/disapproved':
                    this.statusType = 'REJECTED';
                    this.approvalStatus = 'Disapproved';
                    break;
            }
            this.router.navigate(['/' + param]);
        } else {
            const status = this.activatedRoute.snapshot.paramMap.get('status');
            switch (status) {
                case 'pending':
                    this.statusType = 'UNAUTHORIZED';
                    this.approvalStatus = 'Pending';
                    break;
                case 'approved':
                    this.statusType = 'AUTHORIZED';
                    this.approvalStatus = 'Approved';
                    break;
                case 'disapproved':
                    this.statusType = 'REJECTED';
                    this.approvalStatus = 'Disapproved';
                    break;
            }
        }
        this.getAuthorization(this.statusType, 0);
    }

    getApprovalPage(event) {
        this.p = +event;
        this.list.allContent = [];
        this.getAuthorization(this.statusType, this.p - 1);
    }


    getAuthorization(approval_type, pageNo) {
        this.booleans.loader = true;
        this.approvalservice.getAuthorization(approval_type, pageNo, this.approvalName)
            .subscribe(response => {
                this.booleans.loader = false;
                this.list.allContent = response.content;
                this.totalElement = response.totalElements;
            },
            error => {
                this.booleans.loader = false;
                this.notification.error('Unable to load content', error);
            })
    }

    getReason(data, authorizationStatus) {
        this.id = data.id;
        this.authorization = authorizationStatus;
        this.list.details = data;
        this.approvalType = 'disapprove';
        $('#reasonModal').modal('show');
    }

    disapprove() {
        let action = '';
        this.approvedStatus = 'reject';
        this.booleans.loadAction = true;
        switch (this.authorization) {
            case 'UNAUTHORIZED_CREATE':
                action = 'REJECT_CREATE';
                break;
            case 'UNAUTHORIZED_UPDATE':
                action = 'REJECT_UPDATE'
                break;
            case 'UNAUTHORIZED_TOGGLE':
                action = 'REJECT_TOGGLE'
                break;
        }

        this.Approve(this.id, action, this.formReason.value['reason']);
    }

    approve(id, authorizationStatus) {
        this.id = id;
        let action = '';
        this.approvedStatus = '';
        switch (authorizationStatus) {
            case 'UNAUTHORIZED_CREATE':
                action = 'APPROVE_CREATE';
                break;
            case 'UNAUTHORIZED_UPDATE':
                action = 'APPROVE_UPDATE'
                break;
            case 'UNAUTHORIZED_TOGGLE':
                action = 'APPROVE_TOGGLE'
                break;
        }
        this.approvalType = 'approve';
        this.Approve(id, action);
    }

    Approve(id, authorizationStatus, reason?) {
        this.booleans.loadAction = true;
        this.approvalservice.approve({
            id: id,
            reason: (reason && reason !== '') ? reason : ''
        }, authorizationStatus, this.approvalName)
            .subscribe(response => {
                this.booleans.loadAction = false;
                this.list.allContent.forEach((value, i) => {
                    if (id === value['id']) {
                        this.list.allContent.splice(i, 1);
                    }
                });
                this.formReason.reset();
                const status = (this.approvedStatus === 'reject') ? 'Disapproval' : 'Approval';
                this.notification.success(status + ' was successful');
            },
            error => {
                this.booleans.loadAction = false;
                this.notification.error(CONSTANTS.ERROR_RESPONSE, error);
            })
    }
}