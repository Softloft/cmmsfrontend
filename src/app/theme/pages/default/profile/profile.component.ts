import {Component, OnInit, ViewEncapsulation} from "@angular/core";
import {Cache} from "../../../../../utils/cache";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {NotificationService} from "../../../../../services/notification.service";
import {AuthService} from "../../../../../services/api-service/authService";
import {UserService} from "../../../../../services/user.service";
import {Router} from "@angular/router";
import {EmandateService} from "../../../../../services/api-service/emandate.service";


@Component({
    selector: "app-profile",
    templateUrl: "./profile.component.html",
    encapsulation: ViewEncapsulation.None,
})
export class ProfileComponent implements OnInit {
    public form: FormGroup;
    public formRecover: FormGroup;
    public formEmandatePassword: FormGroup;
    public loader = false;
    public newEmandatePassword = false;
    public showResetEmandate = false;
    public user = [];
    public confirmPassword = '';

    public formdata = () => {
        return {
            password: ['', [Validators.compose([Validators.required])]],
            newPassword: ['', [Validators.compose([Validators.required])]],
        }
    };

    public formemandatedata = () => {
        return {
            password: ['', [Validators.compose([Validators.required])]],
        }
    };

    public recoverForm = () => {
        return {
            recoveryCode: ['', [Validators.compose([Validators.required])]],
        }
    };

    constructor(private fb: FormBuilder,
                private notification: NotificationService,
                private router: Router,
                private emandateservice: EmandateService,
                private userservice: UserService,
                private authservice: AuthService) {
        this.form = this.fb.group(this.formdata());
        this.formRecover = this.fb.group(this.recoverForm());
        this.formEmandatePassword = this.fb.group(this.formemandatedata());
    }

    ngOnInit() {
        if (this.emandateservice.resetEmandate) {
            this.showResetEmandate = this.emandateservice.resetEmandate;
        }
        this.user = Cache.get('userDetails');
        console.log('password user :: ', this.user['change_password']);
    }

    getConfirmPassword(event) {
        this.confirmPassword = event.target.value;
    }

    save() {
        if (this.form.value['newPassword'] !== this.confirmPassword) {
            this.notification.error('New Password and Confirm Password are not the same');
            return;
        }
        this.loader = true;
        this.form['value']['emailAddress'] = '';
        this.form['value']['token'] = '';
        this.authservice.changePassword(this.form.value)
            .subscribe(response => {
                    this.loader = false;
                    this.form.reset();
                    this.notification.success('Change of password was successful');
                    setTimeout(() => {
                        if (this.user['change_password']) {
                            this.userservice.logout();
                            this.router.navigate(['/login']);
                        }
                    }, 2000);
                }, (error) => {
                    this.loader = false;
                    let errorMessage = 'Unable to perfotm this action, please try again';
                    if (error['error'] && error['error']['responseMessage']) {
                        errorMessage = error['error']['responseMessage'];
                    }
                    this.notification.error(errorMessage, error);
                }
            )

    }

    postRecoverCode() {
        this.loader = true;
        this.emandateservice.recoverCode(this.formRecover.value)
            .subscribe(response => {
                    this.loader = false;
                    this.formRecover.reset();
                    console.log('response :: ', response)
                    if (response['responseCode'] === '00') {
                        this.newEmandatePassword = true
                    }
                    this.notification.success('Recovery Code was successfully Authenticated');
                }, (error) => {
                    this.loader = false;
                    this.notification.error('Unable to perfotm this action, please try again', error);
                }
            )

    }

    postEmandatePassword() {
        this.loader = true;
        this.emandateservice.postEmandatePassword(this.formEmandatePassword.value)
            .subscribe(response => {
                    this.loader = false;
                    this.formEmandatePassword.reset();
                    this.newEmandatePassword = false;
                    this.router.navigate(['/'])
                    console.log('response :: ', response)
                    this.notification.success('Change of E-mandate password was successful');
                }, (error) => {
                    this.loader = false;
                    this.notification.error('Unable to perfotm this action, please try again', error);
                }
            )

    }
}