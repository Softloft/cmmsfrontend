import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ThemeComponent } from './theme/theme.component';
import { LayoutModule } from './theme/layouts/layout.module';
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ThemeRoutingModule } from "./theme/theme-routing.module";
import { AuthModule } from "./auth/auth.module";
import { BootstrapSwitchModule } from "angular2-bootstrap-switch";
import { HttpClientModule } from "@angular/common/http";
import { NotificationModule } from "../shared/modules/notification.module";
import { SharedModule } from "../shared/modules/shared.module";
import { SERVICES } from "../shared/modules/services";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";

@NgModule({
    declarations: [
        ThemeComponent,
        AppComponent
    ],
    imports: [
        LayoutModule,
        BrowserModule,
        BrowserAnimationsModule,
        AppRoutingModule,
        ThemeRoutingModule,
        AuthModule,
        HttpClientModule,
        NotificationModule,
        SharedModule,
        NgbModule.forRoot(),
        BootstrapSwitchModule.forRoot(),
    ],
    providers: SERVICES,
    bootstrap: [AppComponent]
})
export class AppModule {
}