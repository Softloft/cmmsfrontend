import {
    Component,
    ComponentFactoryResolver,
    OnInit,
    ViewChild,
    ViewContainerRef,
    ViewEncapsulation
} from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { ScriptLoaderService } from "../_services/script-loader.service";
import { AuthenticationService } from "./_services/authentication.service";
import { AlertService } from "./_services/alert.service";
import { AlertComponent } from "./_directives/alert.component";
import { LoginCustom } from "./_helpers/login-custom";
import { Helpers } from "../helpers";
import { AuthService } from "../../services/api-service/authService";
import { Cache } from "../../utils/cache";
import { UserService } from "../../services/user.service";
import { NotificationService } from "../../services/notification.service";

@Component({
    selector: '.m-grid.m-grid--hor.m-grid--root.m-page',
    templateUrl: './templates/login-1.component.html',
    styleUrls: ['./templates/login-1.component.css'],
    encapsulation: ViewEncapsulation.None,
})

export class AuthComponent implements OnInit {
    model: any = {};
    loading = false;
    loadingToken = false;
    loadingReset = false;
    returnUrl: string;
    showTokenForm = false;
    userId = '';
    requestId = '';
    sessionKey = '';
    softToken = '';

    @ViewChild('alertReset',
        { read: ViewContainerRef }) alertReset: ViewContainerRef;
    @ViewChild('alertSignin',
        { read: ViewContainerRef }) alertSignin: ViewContainerRef;
    @ViewChild('alertEmail',
        { read: ViewContainerRef }) alertEmail: ViewContainerRef;
    @ViewChild('alertSignup',
        { read: ViewContainerRef }) alertSignup: ViewContainerRef;
    @ViewChild('alertForgotPass',
        { read: ViewContainerRef }) alertForgotPass: ViewContainerRef;

    constructor(private _router: Router,
        private authservice: AuthService,
        private userservice: UserService,
        private _script: ScriptLoaderService,
        private _userService: UserService,
        private _route: ActivatedRoute,
        private _authService: AuthenticationService,
        private _alertService: AlertService,
        private notification: NotificationService,
        private cfr: ComponentFactoryResolver) {
    }

    ngOnInit() {
        this.model.remember = true;
        // get return url from route parameters or default to '/'
        this.returnUrl = this._route.snapshot.queryParams['returnUrl'] || '/';
        this._router.navigate([this.returnUrl]);

        this._script.loadScripts('body', [
            'assets/vendors/base/vendors.bundle.js',
            'assets/demo/demo7/base/scripts.bundle.js'], true).then(() => {
                Helpers.setLoading(false);
                LoginCustom.init();
            });

        window.setInterval(() => {
            if (!navigator.onLine) {
                this.notification.error('Network disconnected, please connect back');
            }
        }, 5000);
    }

    /*signin() {
        this.loading = true;
        this.showTokenForm = false;
        this.authservice.getAuth({ emailAddress: this.model.email, password: this.model.password }).subscribe(
            (data) => {
                this.loading = false;
                this.sessionKey = data['sessionKey'];
                this.userId = data['userId'];
                this.requestId = data['requestId'];
                this.showTokenForm = true;
                setTimeout(() => {
                    this.showAlert('alertEmail');
                    this._alertService.success('Check your email to access your token');

                    let login = $('#m_login');
                    login.removeClass('m-login--signin');
                    login.removeClass('m-login--signup');

                    login.addClass('m-login--forget-password');
                    (<any>login.find('.m-login__token')).animateClass(
                        'flipInX animated');
                }, 200)
            },
            error => {
                this.loading = false;
                let err = 'Unable to process request due to network'
                this.showAlert('alertSignin');
                if (error && error['errors']) {
                    err = error['errors'][0];
                }else if(error && error['error'] && error['error']['errors'] && error['error']['errors'][0]){
                    err = error['error']['errors'][0];
                    console.log('error in login :: ', error['error']['errors'][0]);
                }
                // this._alertService.error((err === 'Invalid credential') ? 'Incorrect Email or Password' : err);
                this._alertService.error(err);
            });
    }*/

    signin() {
        this.loading = true;
        this.showTokenForm = false;
        // this.displayTokenForm();
        this.authservice.getAuth({ emailAddress: this.model.email, password: this.model.password }).subscribe(
            (data) => {
                this.loading = false;
                Cache.set('token', data['token']);
                Cache.set('userDetails', data['userDetail']);
                this.userservice.setAuthUser(data['token']);
                this._router.navigate([this.returnUrl]);
            },
            error => {
                this.loading = false;
                let err = 'Unable to process request due to network'
                this.showAlert('alertSignin');
                console.log('error :: ', error);
                if (error && error['errors']) {
                    err = error['errors'][0];
                } else if (error && error['error'] && error['error']['errors'] && error['error']['errors'][0]) {
                    err = error['error']['errors'][0];
                    console.log('error in login :: ', error['error']['errors'][0]);
                }
                // this._alertService.error((err === 'Invalid credential') ? 'Incorrect Email or Password' : err);
                this._alertService.error(err);
            });
    }


    recoverPassword() {
        this.loadingReset = true;
        this.showTokenForm = false;
        // this.displayTokenForm();
        const userLink = window.location;
        this.authservice.recoverPassword({
            email: this.model.reset_email,
            path: userLink.origin + '/password-change?token='
        }).subscribe(
            (data) => {
                this.loadingReset = false;
                this.showAlert('alertReset');
                this._alertService.success(data.responseMessage);
            },
            error => {
                let err = 'Unable to process request due to network';
                this.loadingReset = false;
                this.showAlert('alertReset');
                console.log('error :: ', error)
                if (error && error['error'] && error['error']['responseMessage']) {
                    err = error['error']['responseMessage'];
                }
                this._alertService.error(err);
                this.loading = false;
            });
    }

    /*signup() {
     this.loading = true;
     this._userService.create(this.model).subscribe(
     data => {
     this.showAlert('alertSignin');
     this._alertService.success(
     'Thank you. To complete your registration please check your email.',
     true);
     this.loading = false;
     LoginCustom.displaySignInForm();
     this.model = {};
     },
     error => {
     this.showAlert('alertSignup');
     this._alertService.error(error);
     this.loading = false;
     });
     }

     forgotPass() {
     this.loading = true;
     this._userService.forgotPassword(this.model.email).subscribe(
     data => {
     this.showAlert('alertSignin');
     this._alertService.success(
     'Cool! Password recovery instruction has been sent to your email.',
     true);
     this.loading = false;
     LoginCustom.displaySignInForm();
     this.model = {};
     },
     error => {
     this.showAlert('alertForgotPass');
     this._alertService.error(error);
     this.loading = false;
     });
     }*/

    showAlert(target) {
        this[target].clear();
        let factory = this.cfr.resolveComponentFactory(AlertComponent);
        let ref = this[target].createComponent(factory);
        ref.changeDetectorRef.detectChanges();
    }

    displayTokenForm() {
        this.showTokenForm = true;
        this.loadingToken = true;
        console.log('payload token :: ', {
            softToken: this.model.softToken,
            sessionKey: this.sessionKey,
            userId: this.userId,
            requestId: this.requestId
        });
        this.authservice.getAuthToken({
            softToken: this.model.softToken,
            sessionKey: this.sessionKey,
            userId: this.userId,
            requestId: this.requestId
        }).subscribe(
            (data) => {
                this.loadingToken = false;
                Cache.set('token', data['token']);
                Cache.set('userDetails', data['userDetail']);
                this.userservice.setAuthUser(data['token']);
                this._router.navigate([this.returnUrl]);
            },
            error => {
                this.loadingToken = false;
                console.log('email token error');
                let err = 'Unable to process request due to network'
                this.showAlert('alertEmail');
                if (error && error['errors']) {
                    err = error['errors'][0];
                } else if (error && error['error'] && error['error']['errors'] && error['error']['errors'][0]) {
                    err = error['error']['errors'][0];
                    console.log('error in login :: ', error['error']['errors'][0]);
                }
                // this._alertService.error((err === 'Invalid credential') ? 'Incorrect Email or Password' : err);
                this._alertService.error(err);
            });

    }

    removeTokenForm() {
        LoginCustom.displaySignInForm();
        this.showTokenForm = false;
        /* $('#m_login_forget_password_cancel').click((e) => {
         //  e.preventDefault();
         LoginCustom.displaySignInForm();
         this.showTokenForm = false;
         });*/


        /*    this.showTokenForm = false;
         let login = $('#m_login');
         login.removeClass('m-login--forget-password');
         login.removeClass('m-login--signin');

         login.addClass('m-login--signup');
         (<any>login.find('.m-login__signup')).animateClass('flipInX animated');*/
    }

    viewPassword() {
        const x = document.getElementById("myInput");
        x['type'] = "text";
    }

    passwordMode() {
        const x = document.getElementById("myInput");
        x['type'] = "password";
    }
}