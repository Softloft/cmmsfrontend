import { Component, ComponentFactoryResolver, OnInit, ViewChild, ViewContainerRef } from "@angular/core";
import { UserService } from "../_services/user.service";
import { ScriptLoaderService } from "../../_services/script-loader.service";
import { AuthService } from "../../../services/api-service/authService";
import { ActivatedRoute, Router } from "@angular/router";
import { AuthenticationService } from "../_services/authentication.service";
import { AlertService } from "../_services/alert.service";
import { LoginCustom } from "../_helpers/login-custom";
import { Helpers } from "../../helpers";
import { AlertComponent } from "../_directives/alert.component";
import { NotificationService } from "../../../services/notification.service";

@Component({
    selector: 'app-change-password',
    templateUrl: 'change-password.component.html',
    styles: []
})
export class ChangePasswordComponent implements OnInit {

    model: any = {};
    loading = false;
    loadingReset = false;
    returnUrl: string;
    showTokenForm = false;

    @ViewChild('alertPasswordChange',
        { read: ViewContainerRef })
    alertPasswordChange: ViewContainerRef;

    constructor(private _router: Router,
        private authservice: AuthService,
        private userservice: UserService,
        private _script: ScriptLoaderService,
        private _userService: UserService,
        private _route: ActivatedRoute,
        private notification: NotificationService,
        private _authService: AuthenticationService,
        private _alertService: AlertService,
        private cfr: ComponentFactoryResolver) {
    }

    ngOnInit() {
        this.model.remember = true;


        this._script.loadScripts('body', [
            'assets/vendors/base/vendors.bundle.js',
            'assets/demo/demo7/base/scripts.bundle.js'], true).then(() => {
                Helpers.setLoading(false);
                LoginCustom.init();
            });
    }

    passwordChange() {
        // this.displayTokenForm();
        this.showAlert('alertPasswordChange');
        if (this.model.newPassword !== this.model.confirmPassword) {
            this._alertService.error('New Password and Confirm Password must be the same');
            return;
        }
        if (this.model.newPassword === '') {
            this._alertService.error('New Password Field is empty');
            return;
        }
        this.loading = true;
        const recoveryCode = this._route.snapshot.queryParamMap.get('token');
        this.authservice.resetPassword({ newPassword: this.model.newPassword, recoveryCode: recoveryCode }).subscribe(
            (data) => {
                this.loading = false;
                this.model.newPassword = '';
                this.model.confirmPassword = '';
                this._alertService.success('Password changed successfully. You can login with the new password');
                //this._router.navigate(['/login']);
            },
            error => {
                this.loading = false;
                // this.notification.error('Unable to process request due to network', error);
                let err = 'Unable to process request due to network'
                if (error['error']['responseMessage']) {
                    err = error['error']['responseMessage'];
                }
                this._alertService.error(err);
            });
    }

    showAlert(target) {
        this[target].clear();
        let factory = this.cfr.resolveComponentFactory(AlertComponent);
        let ref = this[target].createComponent(factory);
        ref.changeDetectorRef.detectChanges();
    }

}