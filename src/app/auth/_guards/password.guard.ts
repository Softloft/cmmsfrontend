import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { UserService } from "../../../services/user.service";
import { Cache } from "../../../utils/cache";

@Injectable()
export class PasswordGuard implements CanActivate {
    constructor(private _router: Router, private userService: UserService) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | boolean {
        const user = Cache.get('userDetails');
        console.log('user details :: ', user['change_password']);
        if (user['change_password']) {
            this._router.navigate(['profile']);
        }
        return true;
    }
}