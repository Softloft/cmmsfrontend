import { Injectable } from "@angular/core";
import { environment } from "../../environments/environment";
import { CONSTANTS } from "../../utils/constants";
import { ApiHandlerService } from "../api-handler.service";
import { BehaviorSubject } from "rxjs/BehaviorSubject";

@Injectable()
export class BillerService {
    public billerId = 0;
    private biller = environment.API_URL.biller;
    private emandate = environment.API_URL.cmms_emandate;
    private paginate = CONSTANTS.ITEMS_PER_PAGE;
    private formularData = new BehaviorSubject('default message');
    getFormularData = this.formularData.asObservable();

    private feeData = new BehaviorSubject('default message');
    getFeeData = this.feeData.asObservable();

    private responseData = new BehaviorSubject(false);
    getResponse = this.responseData.asObservable();

    private responseformularData = new BehaviorSubject(false);
    getformularResponse = this.responseformularData.asObservable();

    constructor(private apiservice: ApiHandlerService) {
    }

    shareFeeData(data) {
        this.feeData.next(data);
    }

    shareFormularData(data) {
        this.formularData.next(data);
    }

    sendResponse(data) {
        this.responseData.next(data);
    }

    sendFormularResponse(data) {
        this.responseformularData.next(data);
    }

    getBiller(pageNo, user, pagesize) {
        return this.apiservice.get(`${this.biller}/biller/onboarded?pageNumber=${pageNo}&pageSize=${pagesize}`);
        /*if (user['userType'] === 'NIBSS') {
            return this.apiservice.get(`${this.biller}/biller?pageNumber=${pageNo}&pageSize=${pagesize}`);
        } else if (user['userType'] === 'BANK') {
            // return this.apiservice.get(`${this.biller}/biller?pageNumber=${pageNo}&pageSize=${pagesize}`);
            return this.apiservice.get(`${this.biller}/biller/onboarded?pageNumber=${pageNo}&pageSize=${pagesize}`);
        }*/
    }

    postBiller(data) {
        return this.apiservice.post(`${this.biller}/api/billers`, data);
    }

    updateBiller(data) {
        return this.apiservice.put(`${this.biller}/biller`, data);
    }

    toggle(data) {
        return this.apiservice.put(`${this.biller}/biller/toggle`, data);
    }

    toggleUser(data) {
        return this.apiservice.put(`${this.biller}/user/biller/toggle`, data);
    }

    getAllBillers(pageNo: any, totalNumber: any) {
        return this.apiservice.get(`${this.biller}/api/billers?pageNumber=${pageNo}&pageSize=${totalNumber}`);
    }

    getBillerUserById(id, pageNo: any, pagesize) {
        return this.apiservice.get(`${this.biller}/user/biller?biller=${id}&pageNumber=${pageNo}&pageSize=${pagesize}`);
    }

    updateBillerUser(data) {
        return this.apiservice.put(`${this.biller}/user/biller`, data);
    }

    getAllBillersUser(id, pageNo: any, totalNumber: any) {
        return this.apiservice.get(`${this.biller}/user/biller?biller=${id}&pageNumber=${pageNo}&pageSize=${totalNumber}`);
    }

    getAllBillersList() {
        return this.apiservice.get(`${this.biller}/biller/activated?activeStatus=true`);
    }

    createbiller(value: any) {
        return this.apiservice.post(`${this.biller}/biller`, value);
    }

    getProduct(id) {
        return this.apiservice.get(`${this.biller}/product/get/${id}`);
    }

    getBillerById(id) {
        return this.apiservice.get(`${this.biller}/biller/${id}`);
    }


    createBillerUser(value: any, userType: string) {
        return this.apiservice.post(`${this.biller}/user/biller`, value);
    }

    createProduct(data) {
        return this.apiservice.post(`${this.biller}/product`, data);
    }

    getAllProduct(billerId, pageNo: any, pagesize) {
        return this.apiservice.get(`${this.biller}/product?billerId=${billerId}&pageNumber=${pageNo}&pageSize=${pagesize}`);
    }

    toggleProduct(data) {
        return this.apiservice.put(`${this.biller}/product/toggle`, data);
    }

    updateProduct(data: any) {
        return this.apiservice.put(`${this.biller}/product`, data);
    }

    setupFees(data) {
        return this.apiservice.post(`${this.biller}/fees`, data);
    }

    updateSetupFees(data) {
        return this.apiservice.put(`${this.biller}/fees`, data);
    }

    getFeesById(id) {
        return this.apiservice.get(`${this.biller}/fees/${id}`);
    }

    getAllFees(pageNo: any) {
        return this.apiservice.get(`${this.biller}/biller/list/fees?pageNumber=${pageNo}&pageSize=${this.paginate}`);
    }

    createBeneficiary(data) {
        return this.apiservice.post(`${this.biller}/biller/add/beneficiary`, data);
    }

    getAllBeneficiary(pageNo: any) {
        return this.apiservice.get(`${this.biller}/biller/beneficiary/all?pageNumber=${pageNo}&pageSize=${this.paginate}`);
    }

    toggleBeneficiary(data) {
        return this.apiservice.put(`${this.biller}/biller/beneficiary/toggle`, data);
    }

    getBeneficiaryList() {
        return this.apiservice.get(`${this.biller}/biller/beneficiary`);
    }

    updateBeneficiary(data) {
        return this.apiservice.put(`${this.biller}/biller/beneficiary`, data);
    }

    createFormula(data: any) {
        return this.apiservice.post(`${this.biller}/biller/sharingformular`, data);
    }

    updateFormular(data) {
        return this.apiservice.put(`${this.biller}/biller/sharingformular`, data);
    }

    getFormularById(id: number) {
        return this.apiservice.get(`${this.biller}/biller/sharingformular/${id}`);
    }

    retrieveAllBeneficiary(pageNo: any, totalNumber: any) {
        return this.apiservice.post(`${this.biller}/biller/beneficiary/all?pageNumber=${pageNo}&pageSize=${totalNumber}`);
    }

    beneficiarySearch(data, pageNo) {
        return this.apiservice.post(`${this.biller}/search/beneficiary?pageNumber=${pageNo}&pageSize=${this.paginate}`, data);
    }

    feesSearch(data, pageNo: any) {
        return this.apiservice.post(`${this.biller}/search/fees?pageNumber=${pageNo}&pageSize=${this.paginate}`, data);
    }

    retrieveAllfees(pageNo: any, totalNumber: any) {
        return this.apiservice.post(`${this.biller}/biller/list/fees?pageNumber=${pageNo}&pageSize=${totalNumber}`);
    }


    search(data: any, pageNo: any) {
        return this.apiservice.post(`${this.biller}/biller/search?pageNumber=${pageNo}&pageSize=${this.paginate}`, data);
    }

    searchUser(data: any, pageNo: any) {
        return this.apiservice.post(`${this.biller}/user/biller/search?pageNumber=${pageNo}&pageSize=${this.paginate}`, data);
    }

    getFees(billerId: number, pageNo) {
        return this.apiservice.get(`${this.biller}/fees/${billerId}?pageNumber=${pageNo}&pageSize=${this.paginate}`);
    }

    getProductByBiller(id: any) {
        return this.apiservice.get(`${this.biller}/product/get/${id}`);
    }

    getAllBiller(pageNo: any, totalNumber: any) {
        return this.apiservice.get(`${this.biller}/biller/onboarded?pageNumber=${pageNo}&pageSize=${totalNumber}`);
        // return this.apiservice.get(`${this.biller}/biller?pageNumber=${pageNo}&pageSize=${totalNumber}`);
    }

    productSearch(data: any, pageNo: any) {
        return this.apiservice.post(`${this.biller}/product/search?pageNumber=${pageNo}&pageSize=${this.paginate}`, data);
    }

    getUserList(billerId) {
        return this.apiservice.get(`${this.biller}/user/biller/list?billerId=${billerId}&status=true`);
    }

    forwardEmandate(data) {
        return this.apiservice.post(`${this.emandate}/emandate-config/sendDetails`, data);
    }
}