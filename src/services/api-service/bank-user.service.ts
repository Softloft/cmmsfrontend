import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { ApiHandlerService } from '../api-handler.service';
import { CONSTANTS } from "../../utils/constants";

@Injectable()
export class BankUserService {
    private bank = environment.API_URL.bank;
    private paginate = CONSTANTS.ITEMS_PER_PAGE;

    constructor(private apiservice: ApiHandlerService) {
    }

    getBank(pageNo) {
        return this.apiservice.get(`${this.bank}/user/bank?pageNumber=${pageNo}&pageSize=${this.paginate}`);
    }

    postBank(data) {
        return this.apiservice.post(`${this.bank}/user/bank`, data);
    }

    updateBank(data) {
        return this.apiservice.put(`${this.bank}/user/bank`, data);
    }

    toggle(data) {
        return this.apiservice.put(`${this.bank}/user/bank/toggle`, data);
    }

    search(params, pageNo) {
        return this.apiservice.get(`${this.bank}/user/bank/searchNIBBSAttributes?${params}pageNumber=${pageNo}&pageSize=${this.paginate}`);
    }

    getBankbyId(id) {
        return this.apiservice.get(`${this.bank}/user/bank/${id}`);
    }

    getAllBank(pageNo: any, totalNumber: any) {
        return this.apiservice.get(`${this.bank}/user/bank?pageNumber=${pageNo}&pageSize=${totalNumber}`);
    }
}
