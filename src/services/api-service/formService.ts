import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { ApiHandlerService } from '../api-handler.service';

@Injectable()
export class FormService {
    private auth = environment.API_URL.auth;
    private role = environment.API_URL.role;
    private state = environment.API_URL.state;
    private country = environment.API_URL.country;

    constructor(private apiservice: ApiHandlerService) {
    }

    getCountry() {
        return this.apiservice.get(`${this.auth}/${this.country}`);
    }

    getCountryById(id) {
        return this.apiservice.get(`${this.auth}/${this.country}/${id}`);
    }

    getStateById(id) {
        return this.apiservice.get(`${this.auth}/${this.state}/${id}`);
    }

    getStateByCountry(id) {
        return this.apiservice.get(`${this.auth}/${this.country}/${id}/states`);
    }

    getLgaByState(id) {
        return this.apiservice.get(`${this.auth}/${this.state}/${id}/lga`);
    }


    getRole(event) {
        return this.apiservice.get(`${this.auth}/role/activated/${event}`);
    }

    getSearchRole(event) {
        return this.apiservice.get(`${this.auth}/role/${event}`);
    }

    getRoleByUser(event, userType?) {
        return this.apiservice.get(`${this.auth}/role/activated/roleType/${event}`);
    }
}
