import { Injectable } from "@angular/core";
import { environment } from "../../environments/environment";
import { CONSTANTS } from "../../utils/constants";
import { ApiHandlerService } from "../api-handler.service";
import { BehaviorSubject } from "rxjs/BehaviorSubject";

@Injectable()
export class PsspService {
    private pssp = environment.API_URL.pssp;
    private paginate = CONSTANTS.ITEMS_PER_PAGE;
    private formularData = new BehaviorSubject('default message');
    getFormularData = this.formularData.asObservable();

    private feeData = new BehaviorSubject('default message');
    getFeeData = this.feeData.asObservable();

    private responseData = new BehaviorSubject(false);
    getResponse = this.responseData.asObservable();

    private responseformularData = new BehaviorSubject(false);
    getformularResponse = this.responseformularData.asObservable();

    constructor(private apiservice: ApiHandlerService) {
    }

    shareFeeData(data) {
        this.feeData.next(data);
    }

    shareFormularData(data) {
        this.formularData.next(data);
    }

    sendResponse(data) {
        this.responseData.next(data);
    }

    sendFormularResponse(data) {
        this.responseformularData.next(data);
    }

    getPSSP(pageNo, user, pagesize) {
        if (user['userType'] === 'NIBSS') {
            return this.apiservice.get(`${this.pssp}/pssp?pageNumber=${pageNo}&pageSize=${pagesize}`);
        } else if (user['userType'] === 'BANK') {
            return this.apiservice.get(`${this.pssp}/pssp?pageNumber=${pageNo}&pageSize=${pagesize}`);
        }
    }

    getAllPSSP(pageNo, pagesize) {
        return this.apiservice.get(`${this.pssp}/pssp?pageNumber=${pageNo}&pageSize=${pagesize}`);
    }

    postPSSP(data) {
        return this.apiservice.post(`${this.pssp}/api/pssps`, data);
    }

    updatePSSP(data) {
        return this.apiservice.put(`${this.pssp}/pssp`, data);
    }

    toggle(data) {
        return this.apiservice.put(`${this.pssp}/pssp/toggle`, data);
    }

    toggleUser(data) {
        return this.apiservice.put(`${this.pssp}/user/pssp/toggle`, data);
    }

    getAllPSSPs(pageNo: any, totalNumber: any) {
        return this.apiservice.get(`${this.pssp}/api/pssps?pageNumber=${pageNo}&pageSize=${totalNumber}`);
    }

    getPSSPUserById(id, pageNo: any, pagesize) {
        return this.apiservice.get(`${this.pssp}/user/pssp?pssp=${id}&pageNumber=${pageNo}&pageSize=${pagesize}`);
    }

    updatePSSPUser(data) {
        return this.apiservice.put(`${this.pssp}/user/pssp`, data);
    }

    getAllPSSPsUser(id, pageNo: any, totalNumber: any) {
        return this.apiservice.get(`${this.pssp}/user/pssp?pssp=${id}&pageNumber=${pageNo}&pageSize=${totalNumber}`);
    }

    getAllPSSPsList() {
        return this.apiservice.get(`${this.pssp}/pssp/activated?activeStatus=true`);
    }

    createpssp(value: any) {
        return this.apiservice.post(`${this.pssp}/pssp`, value);
    }

    getProduct(id) {
        return this.apiservice.get(`${this.pssp}/product/get/${id}`);
    }

    getPSSPById(id) {
        return this.apiservice.get(`${this.pssp}/pssp/${id}`);
    }


    createPSSPUser(value: any, userType: string) {
        return this.apiservice.post(`${this.pssp}/user/pssp`, value);
    }

    createProduct(data) {
        return this.apiservice.post(`${this.pssp}/product`, data);
    }

    getAllProduct(psspId, pageNo: any) {
        return this.apiservice.get(`${this.pssp}/product?psspId=${psspId}&pageNumber=${pageNo}&pageSize=${this.paginate}`);
    }

    toggleProduct(data) {
        return this.apiservice.put(`${this.pssp}/product/toggle`, data);
    }

    updateProduct(data: any) {
        return this.apiservice.put(`${this.pssp}/product`, data);
    }

    setupFees(data) {
        return this.apiservice.post(`${this.pssp}/pssp/fees/setup`, data);
    }

    updateSetupFees(data) {
        return this.apiservice.put(`${this.pssp}/pssp/fees/edit`, data);
    }

    getFeesById(id) {
        return this.apiservice.get(`${this.pssp}/pssp/fees/${id}`);
    }

    getAllFees(pageNo: any) {
        return this.apiservice.get(`${this.pssp}/pssp/list/fees?pageNumber=${pageNo}&pageSize=${this.paginate}`);
    }

    createBeneficiary(data) {
        return this.apiservice.post(`${this.pssp}/pssp/add/beneficiary`, data);
    }

    getAllBeneficiary(pageNo: any) {
        return this.apiservice.get(`${this.pssp}/pssp/beneficiary/all?pageNumber=${pageNo}&pageSize=${this.paginate}`);
    }

    toggleBeneficiary(data) {
        return this.apiservice.put(`${this.pssp}/pssp/beneficiary/toggle`, data);
    }

    getBeneficiaryList() {
        return this.apiservice.get(`${this.pssp}/pssp/beneficiary`);
    }

    updateBeneficiary(data) {
        return this.apiservice.put(`${this.pssp}/pssp/beneficiary`, data);
    }

    createFormula(data: any) {
        return this.apiservice.post(`${this.pssp}/pssp/sharingformular`, data);
    }

    updateFormular(data) {
        return this.apiservice.put(`${this.pssp}/pssp/sharingformular`, data);
    }

    getFormularById(id: number) {
        return this.apiservice.get(`${this.pssp}/pssp/sharingformular/${id}`);
    }

    retrieveAllBeneficiary(pageNo: any, totalNumber: any) {
        return this.apiservice.post(`${this.pssp}/pssp/beneficiary/all?pageNumber=${pageNo}&pageSize=${totalNumber}`);
    }

    beneficiarySearch(data, pageNo) {
        return this.apiservice.post(`${this.pssp}/search/beneficiary?pageNumber=${pageNo}&pageSize=${this.paginate}`, data);
    }

    feesSearch(data, pageNo: any) {
        return this.apiservice.post(`${this.pssp}/search/fees?pageNumber=${pageNo}&pageSize=${this.paginate}`, data);
    }

    retrieveAllfees(pageNo: any, totalNumber: any) {
        return this.apiservice.post(`${this.pssp}/pssp/list/fees?pageNumber=${pageNo}&pageSize=${totalNumber}`);
    }


    search(data: any, pageNo: any) {
        return this.apiservice.post(`${this.pssp}/pssp/search?pageNumber=${pageNo}&pageSize=${this.paginate}`, data);
    }

    searchUser(data: any, pageNo: any) {
        return this.apiservice.post(`${this.pssp}/user/pssp/search?pageNumber=${pageNo}&pageSize=${this.paginate}`, data);
    }
}