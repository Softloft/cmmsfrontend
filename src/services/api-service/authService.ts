import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { ApiHandlerService } from '../api-handler.service';

@Injectable()
export class AuthService {
    private loginUrl = environment.API_URL.auth;

    constructor(private apiservice: ApiHandlerService) { }

    getAuth(data) {
         return this.apiservice.post(`${this.loginUrl}/login`, data);
         // return this.apiservice.post(`${this.loginUrl}/authenticate`, data);
    }

    getAuthToken(data: { softToken: (any | string); sessionKey: string; userId: string; requestId: string }) {
        return this.apiservice.post(`${this.loginUrl}/emailToken`, data);
    }
    resetPassword(data) {
        return this.apiservice.put(`${this.loginUrl}/password/update`, data);
    }

    changePassword(data) {
        return this.apiservice.put(`${this.loginUrl}/password/update-password`, data);
    }

    recoverPassword(data) {
        return this.apiservice.post(`${this.loginUrl}/password/reset`, data);
    }

    accountLookup(data) {
        return this.apiservice.post(`${this.loginUrl}/account/verify`, data);
    }

    getUserDashboard() {
        return this.apiservice.get(`${this.loginUrl}/dashboard/users`);
    }

    getBillerDashboard() {
        return this.apiservice.get(`${this.loginUrl}/dashboard/biller`);
    }

    getMandateDashboard(type) {
        return this.apiservice.get(`${this.loginUrl}/dashboard/mandates/${type}`);
    }

    getActiveStatusUserDashboard() {
        return this.apiservice.get(`${this.loginUrl}/dashboard/users/activeStatus`);
    }

    getMandateReport(pageNo: any) {
        return this.apiservice.get(`${this.loginUrl}/dashboard/mandates/nibss/banks`);
    }

    getBillerMandateReport(pageNo: any) {
        return this.apiservice.get(`${this.loginUrl}/dashboard/mandates/biller/subscribers`);
    }

    getBankMandateReport(pageNo: any) {
        return this.apiservice.get(`${this.loginUrl}/dashboard/mandates/bank/billers`);
    }

    getBillerActiveDashboard(type) {
        return this.apiservice.get(`${this.loginUrl}/dashboard/${type}/billers`);
    }

}
