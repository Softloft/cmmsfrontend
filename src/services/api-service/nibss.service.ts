import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { ApiHandlerService } from '../api-handler.service';
import { CONSTANTS } from "../../utils/constants";

@Injectable()
export class NibssService {
    private nibss = environment.API_URL.nibss;
    private paginate = CONSTANTS.ITEMS_PER_PAGE;

    constructor(private apiservice: ApiHandlerService) {
    }

    getNibss(pageNo, pagesize) {
        return this.apiservice.get(`${this.nibss}/user/nibss?pageNumber=${pageNo}&pageSize=${pagesize}`);
    }

    postNibss(data) {
        return this.apiservice.post(`${this.nibss}/user/nibss`, data);
    }

    updateNibss(data) {
        return this.apiservice.put(`${this.nibss}/user/nibss`, data);
    }

    toggle(data) {
        return this.apiservice.put(`${this.nibss}/user/nibss/toggle`, data);
    }

    search(data, pageNo) {
        return this.apiservice.post(`${this.nibss}/user/nibss/search?pageNumber=${pageNo}&pageSize=${this.paginate}`, data);
    }

    getNibssbyId(id) {
        return this.apiservice.get(`${this.nibss}/user/nibss/${id}`);
    }

    getAllNibss(pageNo: any, totalNumber: any) {
        return this.apiservice.get(`${this.nibss}/user/nibss?pageNumber=${pageNo}&pageSize=${totalNumber}`);
    }

    /*getMandate(pageNo: any) {
        return this.apiservice.get(`${this.nibss}/mandate/list?pageNumber=${pageNo}&pageSize=${this.paginate}`);
    }*/

    getAllMandate(pageNo: any, totalNumber: any) {
        return this.apiservice.get(`${this.nibss}/mandate/list?pageNumber=${pageNo}&pageSize=${totalNumber}`);
    }

    getMandateType() {
        return this.apiservice.get(`${this.nibss}/mandateTypes`);
    }

    getMandateChannel() {
        return this.apiservice.get(`${this.nibss}/mandateChannels`);
    }

    getMandateCategory() {
        return this.apiservice.get(`${this.nibss}/mandateCategories`);
    }

    getDashboard(type) {
        return this.apiservice.get(`${this.nibss}/transactions/summary/${type}`);
    }

    getTransaction(data, pageNo?, download?) {
        return this.extractDateProcessWeb(data, pageNo, `${this.nibss}/transactions/billing/list`, download);
        // return this.apiservice.get(`${this.nibss}/transactions/billing/list`);
    }

    public getSearchLogWeb(data, pageNo) {
        return this.extractDateProcessWeb(data, pageNo, `${this.nibss}/webaudit`);
    }

    public extractDateProcessWeb(data, pageNo, url, download?) {
        let fromDate = '';
        let toDate = '';
        let param5 = '';
        let param4 = '';
        let param3 = '';
        let param2 = '';
        let param1 = '';
        let param0 = '';
        const params = data['params'];
        const dataSize = data['dataSize'];
        const className = data['className'];
        if (data['from']['year']) {
            fromDate = `${data['from']['year']}-${data['from']['month']}-${data['from']['day']}`;
        }
        if (data['to']['year']) {
            toDate = `${data['to']['year']}-${data['to']['month']}-${data['to']['day']}`;
        }
        param2 = `?startDate=${fromDate}&endDate=${toDate}`;
        param1 = `?startDate=${fromDate}`;
        console.log('download value :: ', download)
        if (download && download === 'download') {
            if (toDate !== '' && fromDate !== '') {
                return this.apiservice.getFile(`${this.nibss}/transactions/billing${param2}`);
            } else if (fromDate !== '' && toDate === '') {
                return this.apiservice.getFile(`${this.nibss}/transactions/billing${param1}`);
            }
        } else if (toDate !== '' && fromDate !== '') {
            return this.apiservice.get(`${url}${param2}`);
        } else if (fromDate !== '' && toDate === '') {
            return this.apiservice.get(`${url}${param1}`);
        }
    }

    getTransactionReport(pageNo: any) {
        return this.apiservice.get(`${this.nibss}/transactions/details?status=PAYMENT_SUCCESSFUL&pageNumber=${pageNo}&pageSize=${this.paginate}`);
    }

    getUserDashboard() {
        return this.apiservice.get(`${this.nibss}/user/nibss/all`);
    }

    getTransactionBillings() {
        return this.apiservice.get(`${this.nibss}/transactions/billing/list`);
    }
}
