import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { ApiHandlerService } from '../api-handler.service';
import { CONSTANTS } from "../../utils/constants";
import {Subject} from "rxjs/Subject";
import {from} from "rxjs/observable/from";
import * as Rx from "rxjs/Rx";

@Injectable()
export class EmandateService {
    private emandate = environment.API_URL.cmms_emandate;
    private nibss = environment.API_URL.nibss;
    private paginate = CONSTANTS.ITEMS_PER_PAGE;
    public emandateId = 0;
    public status = new Rx.Subject();
    public completedStatus = Rx.Observable.from(status);
    resetEmandate: boolean;
    constructor(private apiservice: ApiHandlerService) {
        this.completedStatus.subscribe(response => {
            console.log('response api :: ', response)
        })
    }

    getEmandate() {
        return this.apiservice.get(`${this.emandate}/emandate-config/previewUpdate`);
    }

    postEmandate(data) {
        return this.apiservice.post(`${this.emandate}/emandate-config`, data);
    }

    putEmandate(data) {
        return this.apiservice.put(`${this.emandate}/emandate-config`, data);
    }

    toggle(entityType, data) {
        return this.apiservice.put(`${this.emandate}/emandate-config/toggle?entityType=${entityType}`, data);
    }

    getAuthorization(entityType: any, statusType: any, pageNo: any) {
        return this.apiservice.get(`${this.emandate}/emandate-config/viewAction?entityType=${entityType}&viewAction=${statusType}&pageNumber=${pageNo}&pageSize=10`);
    }
    approve(data, authorization, entityType) {
        return this.apiservice.put(`${this.emandate}/emandate-config/authorization?action=${authorization}&userType=${entityType}`, data);
    }

    generateCode() {
        return this.apiservice.get(`${this.emandate}/emandate-password/generate-code`);
    }

    recoverCode(data) {
        return this.apiservice.post(`${this.emandate}/emandate-password/confirm-code`, data);
    }

    postEmandatePassword(data) {
        return this.apiservice.post(`${this.emandate}/emandate-password/update`, data);
    }
}
