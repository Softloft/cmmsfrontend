import { Injectable } from '@angular/core';
import { CONSTANTS } from "../../utils/constants";
import { environment } from "../../environments/environment";
import { ApiHandlerService } from "../api-handler.service";

@Injectable()
export class IndustryService {
    private biller = environment.API_URL.biller;
    private paginate = CONSTANTS.ITEMS_PER_PAGE;
    constructor(private apiservice: ApiHandlerService) { }

    getIndustry(pageNo, pagesize) {
        return this.apiservice.get(`${this.biller}/industry?pageNumber=${pageNo}&pageSize=${pagesize}`);
    }

    postIndustry(data) {
        return this.apiservice.post(`${this.biller}/industry`, data);
    }

    updateIndustry(data) {
        return this.apiservice.put(`${this.biller}/industry`, data);
    }

    toggle(data) {
        return this.apiservice.put(`${this.biller}/industry`, data);
    }

    getAllIndustry(pageNo: any, totalNumber: any) {
        return this.apiservice.get(`${this.biller}/api/industries?pageNumber=${pageNo}&pageSize=${totalNumber}`);
    }

    getIndustryList() {
        return this.apiservice.get(`${this.biller}/industry`);
    }

    search(data, pageNo: any) {
        return this.apiservice.post(`${this.biller}/industry/search`, data);
    }
}

