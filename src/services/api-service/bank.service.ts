import { Injectable } from '@angular/core';
import { CONSTANTS } from "../../utils/constants";
import { environment } from "../../environments/environment";
import { ApiHandlerService } from "../api-handler.service";

@Injectable()
export class BankService {
    private bank = environment.API_URL.bank;
    private paginate = CONSTANTS.ITEMS_PER_PAGE;
    constructor(private apiservice: ApiHandlerService) { }

    getBank(pageNo, pagesize) {
        return this.apiservice.get(`${this.bank}/bank?pageNumber=${pageNo}&pageSize=${pagesize}`);
    }

    postBank(data) {
        return this.apiservice.post(`${this.bank}/bank`, data);
    }

    updateBank(data) {
        return this.apiservice.put(`${this.bank}/bank`, data);
    }

    toggle(data) {
        return this.apiservice.put(`${this.bank}/bank/toggle`, data);
    }

    toggleUser(data) {
        return this.apiservice.put(`${this.bank}/user/bank/toggle`, data);
    }

    search(data, pageNo) {
        return this.apiservice.post(`${this.bank}/user/bank/search?pageNumber=${pageNo}&pageSize=${this.paginate}`, data);
    }

    banksearch(data, pageNo) {
        return this.apiservice.post(`${this.bank}/bank/search?pageNumber=${pageNo}&pageSize=${this.paginate}`, data);
    }

    getBankbyId(id) {
        return this.apiservice.get(`${this.bank}/user/bank/${id}`);
    }

    updateBankUser(data, userType) {
        //return this.apiservice.put(`${this.bank}/user/bank/${userType}`, data);
        return this.apiservice.put(`${this.bank}/user/bank`, data);
    }

    getBankUser(pageNo: any, pagesize, bankId?) {
        return this.apiservice.get(`${this.bank}/user/bank?bankId=${bankId}&pageNumber=${pageNo}&pageSize=${pagesize}`);
    }

    getAllBank(pageNo: any, totalNumber: any) {
        return this.apiservice.get(`${this.bank}/bank?pageNumber=${pageNo}&pageSize=${totalNumber}`);
    }

    getBankByCode(code, pageNo) {
        return this.apiservice.get(`${this.bank}/user/bank/all?bankCode=${code}&pageNumber=${pageNo}&pageSize=${this.paginate}`);
    }

    getUserByUsers(id, pageNo, pagesize) {
        return this.apiservice.get(`${this.bank}/user/bank?bankId=${id}&pageNumber=${pageNo}&pageSize=${pagesize}`);
    }

    getAllBankUserByCode(code, pageNo: any, totalNumber: any) {
        return this.apiservice.get(`${this.bank}/user/bank?bankCode=${code}&pageNumber=${pageNo}&pageSize=${totalNumber}`);
    }

    getAllBankUser(pageNo: any, pageSize) {
        return this.apiservice.get(`${this.bank}/user/bank?pageNumber=${pageNo}&pageSize=${pageSize}`);
    }

    postBankUser(data, usertype) {
        return this.apiservice.post(`${this.bank}/user/bank`, data);
    }

    getActiveBank() {
        return this.apiservice.get(`${this.bank}/bank/activated/pageNumber/pageSize?activeStatus=true`);
    }

    getAllBankList() {
        return this.apiservice.get(`${this.bank}/bank/activated/pageNumber/pageSize?activeStatus=true`);
    }

    getBankById(id: any) {
        return this.apiservice.get(`${this.bank}/bank/${id}`)
    }

    getUserList(bankId) {
        return this.apiservice.get(`${this.bank}/user/bank/list?bankId=${bankId}&status=true`);
    }
}

