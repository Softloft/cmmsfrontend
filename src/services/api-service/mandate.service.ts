import { Injectable } from "@angular/core";
import { ApiHandlerService } from "../api-handler.service";
import { environment } from "../../environments/environment";
import { CONSTANTS } from "../../utils/constants";
import { UserService } from "../user.service";

@Injectable()
export class MandateService {
    private nibss = environment.API_URL.nibss;
    private mandate = environment.API_URL.mandate;
    private paginate = CONSTANTS.ITEMS_PER_PAGE;
    private user = [];

    constructor(private apiservice: ApiHandlerService,
        private userService: UserService) {
        /*console.log('user service :: ', this.userService.getAuthUser()['userType']);
         this.user = this.userService.getAuthUser();*/
    }

    getMandate(pageNo, user) {
        if (user['userType'] === 'BILLER') {
            return this.apiservice.get(`${this.nibss}/biller/mandate/listAll?pageNumber=${pageNo}&pageSize=${this.paginate}`);
        } else if (user['userType'] === 'BANK') {
            return this.apiservice.get(`${this.nibss}/bank/mandate/listAll?pageNumber=${pageNo}&pageSize=${this.paginate}`);
        } else if (user['userType'] === 'NIBSS') {
            return this.apiservice.get(`${this.nibss}/nibss/mandate?pageNumber=${pageNo}&pageSize=${this.paginate}`);
        }
    }

    updatemandate(data) {
        if (this.user['userType'] === 'BILLER') {
            return this.apiservice.put(`${this.nibss}/biller/mandate/edit`, data);
        } else if (this.user['userType'] === 'BANK') {
            return this.apiservice.put(`${this.nibss}/bank/mandate/edit`, data);
        } else if (this.user['userType'] === 'NIBSS') {
            return this.apiservice.put(`${this.nibss}/nibss/mandate`, data);
        }
    }

    toggle(data) {
        return this.apiservice.put(`${this.mandate}/mandate/toggle`, data);
    }


    getFrequency() {
        return this.apiservice.get(`${this.nibss}/biller/mandate/frequencies`)
    }

    getMandateAction(data, user) {
        if (user['userType'] === 'BILLER') {
            return this.apiservice.put(`${this.nibss}/biller/mandate/action`, data)
        } else if (user['userType'] === 'BANK') {
            return this.apiservice.put(`${this.nibss}/bank/mandate/action`, data)
        } else if (user['userType'] === 'NIBSS') {
            return this.apiservice.put(`${this.nibss}/nibss/mandate/action`, data)
        }
    }

    create(data, user) {
        if (user['userType'] === 'BILLER') {
            return this.apiservice.post(`${this.nibss}/biller/mandate/add`, data);
        } else if (user['userType'] === 'BANK') {
            return this.apiservice.post(`${this.nibss}/bank/mandate/add`, data);
        } else if (user['userType'] === 'NIBSS') {
            return this.apiservice.post(`${this.nibss}/nibss/mandate`, data);
        }
    }

    getAllMandate(pageNo: any, totalNumber: any, user) {
        if (user['userType'] === 'BILLER') {
            return this.apiservice.get(`${this.nibss}/biller/mandate/listAll?pageNumber=${pageNo}&pageSize=${totalNumber}`);
        } else if (user['userType'] === 'BANK') {
            return this.apiservice.get(`${this.nibss}/bank/mandate/listAll?pageNumber=${pageNo}&pageSize=${totalNumber}`);
        } else if (user['userType'] === 'NIBSS') {
            return this.apiservice.get(`${this.nibss}/nibss/mandate/listAll?pageNumber=${pageNo}&pageSize=${totalNumber}`);
        }
    }

    getMandateByStatus(statusType, pageNo: any, user) {
        if (user['userType'] === 'BILLER') {
            return this.apiservice.get(`${this.nibss}/biller/mandate/show?operation=${statusType}&pageNumber=${pageNo}&pageSize=${this.paginate}`);
        } else if (user['userType'] === 'BANK') {
            return this.apiservice.get(`${this.nibss}/bank/mandate/show?operation=${statusType}&pageNumber=${pageNo}&pageSize=${this.paginate}`);
        } else if (user['userType'] === 'NIBSS') {
            return this.apiservice.get(`${this.nibss}/nibss/mandate/show?operation=${statusType}&pageNumber=${pageNo}&pageSize=${this.paginate}`);
        }
    }

    getBankMandateByStatus(statusType: any, pageNo: any) {
        return this.apiservice.get(`${this.nibss}/bank/mandate/show?operation=${statusType}&pageNumber=${pageNo}&pageSize=${this.paginate}`);
    }

    approvedBankMandate(data) {
        return this.apiservice.put(`${this.nibss}/bank/mandate/approve`, data);
    }

    approvedMandate(data, userType) {
        if (userType['userType'] === 'BILLER') {
            return this.apiservice.put(`${this.nibss}/biller/mandate/approve`, data);
        } else if (userType['userType'] === 'BANK') {
            return this.apiservice.put(`${this.nibss}/bank/mandate/approve`, data);
        } else if (userType['userType'] === 'NIBSS') {
            return this.apiservice.put(`${this.nibss}/nibss/mandate/approve`, data);
        }
    }

    getRejectionReason() {
        return this.apiservice.get(`${this.nibss}/biller/mandate/rejectionReasons`)
    }

    rejectMandate(id: any, data: any, userType) {
        if (userType['userType'] === 'BILLER') {
            return this.apiservice.put(`${this.nibss}/biller/mandate/reject/${id}`, data);
        } else if (userType['userType'] === 'BANK') {
            return this.apiservice.put(`${this.nibss}/bank/mandate/reject/${id}`, data);
        } else if (userType['userType'] === 'NIBSS') {
            return this.apiservice.put(`${this.nibss}/nibss/mandate/reject/${id}`, data);
        }
    }

    getMandateById(id: any) {
        return this.apiservice.get(`${this.nibss}/biller/mandate/view/${id}`);
    }

    mandatesearch(data: any, pageNo: any) {
        return this.apiservice.post(`${this.nibss}/search/mandate?pageNumber=${pageNo}&pageSize=${this.paginate}`, data);
    }

    getTemplate() {
        return this.apiservice.getFile(`${this.nibss}/biller/mandate/template`);
    }

    postTemplate(file) {
        return this.apiservice.postBulkFile(file, `${this.nibss}/biller/mandate/bulk`);
    }
}