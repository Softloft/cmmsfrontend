import { Injectable } from "@angular/core";
import { environment } from "../../environments/environment";
import { ApiHandlerService } from "../api-handler.service";
import { CONSTANTS } from "../../utils/constants";

@Injectable()
export class ApprovalService {
    private nibss = environment.API_URL.nibss;
    private bank = environment.API_URL.bank;
    private pssp = environment.API_URL.pssp;
    private biller = environment.API_URL.biller;
    private paginate = CONSTANTS.ITEMS_PER_PAGE;

    constructor(private apiservice: ApiHandlerService) {
    }

    getBankApproval(page: any, status?) {
        return this.apiservice.get(`${this.bank}/bank/activated/{pageNumber}/{pageSize}?activeStatus=true`);
    }

    approve(data, action, approvalName, userType?) {
        console.log('biller :: ', approvalName)
        if (userType && userType === 'not_user') {
            if (approvalName === 'biller') {
                return this.apiservice.put(`${this.biller}/biller/authorization?action=${action}`, data);
            } else if (approvalName === 'bank') {
                return this.apiservice.put(`${this.bank}/bank/authorization?action=${action}`, data);
            } else if (approvalName === 'pssp') {
                return this.apiservice.put(`${this.pssp}/pssp/authorization?action=${action}`, data);
            }
        } else if (approvalName === 'product') {
            return this.apiservice.put(`${this.biller}/product/authorization?action=${action}`, data);
        }else if (approvalName === 'industry') {
            return this.apiservice.put(`${this.biller}/industry/authorization?action=${action}`, data);
        } else {
            if (approvalName === 'biller') {
                return this.apiservice.put(`${this.biller}/user/biller/authorization?action=${action}`, data);
            } else if (approvalName === 'bank') {
                return this.apiservice.put(`${this.bank}/user/bank/authorization?action=${action}`, data);
            } else if (approvalName === 'nibss') {
                return this.apiservice.put(`${this.nibss}/user/nibss/authorization?action=${action}`, data);
            } else if (approvalName === 'pssp') {
                return this.apiservice.put(`${this.pssp}/user/pssp/authorization?action=${action}`, data);
            }
        }
    }

    getAuthorization(approval_type, pageNo, approvalName, userType?) {
        if (userType && userType === 'not_user') {
            if (approvalName === 'biller' || approvalName === 'product') {
                return this.apiservice.get(`${this.biller}/${approvalName}/viewAction?viewAction=${approval_type}&pageNumber=${pageNo}&pageSize=${this.paginate}`);
            } else if (approvalName === 'bank') {
                return this.apiservice.get(`${this.bank}/bank/viewAction?viewAction=${approval_type}&pageNumber=${pageNo}&pageSize=${this.paginate}`);
            } else if (approvalName === 'pssp') {
                return this.apiservice.get(`${this.pssp}/pssp/viewAction?viewAction=${approval_type}&pageNumber=${pageNo}&pageSize=${this.paginate}`);
            }
        } else if (approvalName === 'industry') {
            return this.apiservice.get(`${this.biller}/industry/viewAction?viewAction=${approval_type}&pageNumber=${pageNo}&pageSize=${this.paginate}`);
        } else {
            console.log('approval type :: ', approval_type);
            if (approvalName === 'biller') {
                return this.apiservice.get(`${this.biller}/user/biller/viewAction?billerId=${userType}&viewAction=${approval_type}&pageNumber=${pageNo}&pageSize=${this.paginate}`);
            } else if (approvalName === 'bank') {
                return this.apiservice.get(`${this.bank}/user/bank/viewAction?bankId=${userType}&viewAction=${approval_type}&pageNumber=${pageNo}&pageSize=${this.paginate}`);
            } else if (approvalName === 'nibss') {
                return this.apiservice.get(`${this.nibss}/user/nibss/viewAction?viewAction=${approval_type}&pageNumber=${pageNo}&pageSize=${this.paginate}`);
            } else if (approvalName === 'pssp') {
                return this.apiservice.get(`${this.pssp}/user/pssp/viewAction?psspId=${userType}&viewAction=${approval_type}&pageNumber=${pageNo}&pageSize=${this.paginate}`);
            }
        }
    }

    getMandateAction(data, user) {
        if (user['userType'] === 'BILLER') {
            return this.apiservice.put(`${this.nibss}/biller/mandate/action`, data)
        } else if (user['userType'] === 'BANK') {
            return this.apiservice.put(`${this.nibss}/bank/mandate/action`, data)
        } else if (user['userType'] === 'NIBSS') {
            return this.apiservice.put(`${this.nibss}/nibss/mandate/action`, data)
        }
    }

    getMandateByStatus(statusType, pageNo: any, user) {
        if (user['userType'] === 'BILLER') {
            return this.apiservice.get(`${this.nibss}/biller/mandate/show?operation=${statusType}&pageNumber=${pageNo}&pageSize=${this.paginate}`);
        } else if (user['userType'] === 'BANK') {
            return this.apiservice.get(`${this.nibss}/bank/mandate/show?operation=${statusType}&pageNumber=${pageNo}&pageSize=${this.paginate}`);
        } else if (user['userType'] === 'NIBSS') {
            return this.apiservice.get(`${this.nibss}/nibss/mandate/show?operation=${statusType}&pageNumber=${pageNo}&pageSize=${this.paginate}`);
        }
    }


}

