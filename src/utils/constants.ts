export const CONSTANTS = {
    DEFUALT_ERROR_MESSAGE: 'Error was encountered while processing this request. please try again',
    TIMEOUT_5: 5000,
    TIMEOUT_9: 9000,
    TIMEOUT_MIL_2: 200,
    TIMEOUT_MIL_5: 500,
    TIMEOUT_15: 15000,
    TIMEOUT_20: 20000,
    PAYMENT_CONFIRMATION_COMPLETED: 'payment-confirmation-completed',
    PAYMENT_CONFIRMED: 'payment-confirmed',
    SHOW_CONFIRM: 'show-confirm',
    SHOW_RECEIPT: 'show-receipt',
    SHOW_TABS: 'show-tabs',
    SWITCH_DISPLAY_MODE: 'switch-display-mode',
    SEARCH_COMPLETED: 'search-completed',
    FILL_GLOBAL_FORM_FIELD: 'fill-global-form-field',
    ACCEPT_LOGIN: 'accept-login',
    ITEMS_PER_PAGE: 10,
    ITEMS_PER_PAGE_ARRAY: [10, 25, 50, 100],
    SUBMIT_RESPONSE: 'Submission was successful. Awaiting Authorization',
    UPDATE_RESPONSE: 'Update was successful. Awaiting Authorization',
    TOGGLE_RESPONSE: 'Status was successfully changed. Awaiting Authorization',
    ERROR_RESPONSE: 'Unable to perform this action',
    PHONE_REGEX: /^(?:(?:\(?(?:00|\+)([0-4]\d\d|[1-9]\d?)\)?)?[\-\.\ \\\/]?)?((?:\(?\d{1,}\)?[\-\.\ \\\/]?){0,})(?:[\-\.\ \\\/]?(?:#|ext\.?|extension|x)[\-\.\ \\\/]?(\d+))?$/i
};

export const APPROVAL_MESSAGE_USER = (message) => {
    return 'Are Sure you want to ' + message + ' this user?';
};
export const APPROVAL_MESSAGE_EMANDATE = (message) => {
    return 'Are Sure you want to ' + message + ' this emandate?';
};