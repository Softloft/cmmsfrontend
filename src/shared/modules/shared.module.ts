import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
// import {NgbModule} from "@ng-bootstrap/ng-bootstrap";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { ValidateHashModule } from "./validate-hash.module";
import { NotificationModule } from "./notification.module";
import { SwitchComponent } from "../components/switch/switch.component";
import { PasswordService } from "../../services/password.service";
import { FeeConfigurationComponent } from "../components/fee-configuration/fee-configuration.component";
import { BootstrapSwitchModule } from "angular2-bootstrap-switch";
import { NgxPaginationModule } from "ngx-pagination";
import { AngularMultiSelectModule } from "angular2-multiselect-dropdown";
import { RouterModule, Routes } from "@angular/router";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { LayoutModule } from "../../app/theme/layouts/layout.module";
import { SharingFormularComponent } from "../components/sharing-formular/sharing-formular.component";
import { SetupEmandateComponent } from "../components/setup-emandate/setup-emandate.component";
import { ApprovalComponent } from "../components/setup-emandate/approvals/approval.component";
import { DefaultComponent } from "../../app/theme/pages/default/default.component";
import { PipeModule } from "./pipe.module";
// import {CartSlotsComponent} from "../components/cart-slots/cart-slots.component";
declare const CartSlotsComponent;

@NgModule({
    imports: [
        CommonModule,
        CommonModule,
        LayoutModule,
        NgbModule,
        PipeModule,
        ReactiveFormsModule,
        RouterModule,
        FormsModule,
        AngularMultiSelectModule,
        NgxPaginationModule,
        BootstrapSwitchModule,
        // NgbModule,
        FormsModule,
        AngularMultiSelectModule,
        ReactiveFormsModule,
        ValidateHashModule,
        NotificationModule,
    ],
    providers: [],
    declarations: [
        SwitchComponent,
        ApprovalComponent,
        SetupEmandateComponent,
        FeeConfigurationComponent,
        SharingFormularComponent
    ],
    exports: [
        ApprovalComponent,
        SetupEmandateComponent,
        FeeConfigurationComponent,
        SharingFormularComponent
    ]
})
export class SharedModule { }
