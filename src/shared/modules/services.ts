import { BankService } from "../../services/api-service/bank.service";
import { ApprovalService } from "../../services/api-service/approval.service";
import { NibssService } from "../../services/api-service/nibss.service";
import { EventsService } from "../../services/event.service";
import { ValidationErrorService } from "../../services/validation-error.service";
import { NotificationService } from "../../services/notification.service";
import { FormService } from "../../services/api-service/formService";
import { UserService } from "../../services/user.service";
import { ApiHandlerService } from "../../services/api-handler.service";
import { AuthService } from "../../services/api-service/authService";
import { ScriptLoaderService } from "../../app/_services/script-loader.service";
import { BankUserService } from "../../services/api-service/bank-user.service";
import { IndustryService } from "../../services/api-service/industry.service";
import { BillerService } from "../../services/api-service/biller.service";
import { MandateService } from "../../services/api-service/mandate.service";
import { PsspService } from "../../services/api-service/pssp.service";
import { AuditService } from "../../services/api-service/audit.service";
import { EmandateService } from "../../services/api-service/emandate.service";
export const SERVICES = [
    ScriptLoaderService,
    AuthService,
    ApiHandlerService,
    UserService,
    FormService,
    NotificationService,
    ValidationErrorService,
    EventsService,
    NibssService,
    BillerService,
    IndustryService,
    ApprovalService,
    BankService,
    MandateService,
    BankUserService,
    PsspService,
    AuditService,
    EmandateService]