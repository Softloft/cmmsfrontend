import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'transformtext'
})
export class TransformText implements PipeTransform {
    transform(data: any, args?: any): any {
        if (!data) {
            return data;
        }
        if (data === 'NIBSS_INITIATOR') {
            data = 'NIBSS_ADMIN_INITIATOR';
        } else if (data === 'NIBSS_AUTHORIZER') {
            data = 'NIBSS_ADMIN_AUTHORIZER';
        } else if (data === 'NIBSS INITIATOR') {
            data = 'NIBSS ADMIN INITIATOR';
        } else if (data === 'NIBSS AUTHORIZER') {
            data = 'NIBSS ADMIN AUTHORIZER';
        }
        return data;
    }
}
