import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'removeSpecialword'
})
export class RemoveSpecialwordPipe implements PipeTransform {

    transform(value: any, args?: any): any {
        return value.split(".xml").join(" ");
    }

}
