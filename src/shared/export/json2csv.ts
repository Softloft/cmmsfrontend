import * as json2csv from 'json2csv';
import { saveAs } from 'file-saver';

export const JSON2CSV = (fields, csvArray, fileName) => {
    const opts = { fields };
    const parser = new json2csv.Parser(opts);
    const csv = parser.parse(csvArray);
    const blob = new Blob([csv], { type: 'type/csv' });
    saveAs(blob, fileName + '.csv');
};