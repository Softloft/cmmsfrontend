declare const jsPDF: any;


const pdfSetting = {
    // Styling
    theme: 'striped', // 'striped', 'grid' or 'plain'
    styles: {
        cellPadding: 5, // a number, array or object (see margin below)
        fontSize: 10,
        font: "helvetica", // helvetica, times, courier
        lineColor: 200,
        lineWidth: 0,
        fontStyle: 'normal', // normal, bold, italic, bolditalic
        overflow: 'ellipsize', // visible, hidden, ellipsize or linebreak
        fillColor: false, // false for transparent or a color as described below
        textColor: 20,
        halign: 'left', // left, center, right
        valign: 'middle', // top, middle, bottom
        columnWidth: 'auto' // 'auto', 'wrap' or a number
    },
    headerStyles: {
        cellPadding: 5, // a number, array or object (see margin below)
        fontSize: 10,
        font: "helvetica", // helvetica, times, courier
        lineColor: 200,
        lineWidth: 0,
        fontStyle: 'normal', // normal, bold, italic, bolditalic
        overflow: 'ellipsize', // visible, hidden, ellipsize or linebreak
        fillColor: false, // false for transparent or a color as described below
        textColor: 20,
        halign: 'left', // left, center, right
        valign: 'middle', // top, middle, bottom
        columnWidth: 'auto' // 'auto', 'wrap' or a number
    },
    bodyStyles: {},
    alternateRowStyles: {},
    columnStyles: {},

    // Properties
    startY: false, // false (indicates margin top value) or a number
    margin: 40, // a number, array or object
    pageBreak: 'auto', // 'auto', 'avoid' or 'always'
    tableWidth: 'auto', // 'auto', 'wrap' or a number,
    showHeader: 'everyPage', // 'everyPage', 'firstPage', 'never',
    tableLineColor: 200, // number, array (see color section below)
    tableLineWidth: 0,

    // Hooks
    createdHeaderCell: function(cell, data) {
    },
    createdCell: function(cell, data) {
    },
    drawHeaderRow: function(row, data) {
    },
    drawRow: function(row, data) {
    },
    drawHeaderCell: function(cell, data) {
    },
    drawCell: function(cell, data) {
    },
    addPageContent: function(data) {
    }
};

export const JSON2PDFTABLE = (HEADER, BODY, FILENAME) => {
    const columns = HEADER;
    const rows = BODY;

    // Only pt supported (not mm or in)
    const doc = new jsPDF('l', 'pt');
    doc.autoTable(columns, rows, this.pdfSetting);
    doc.save(FILENAME + '.pdf');
}
