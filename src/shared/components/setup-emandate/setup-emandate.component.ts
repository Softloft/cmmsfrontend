import {Component, Input, OnInit} from "@angular/core";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ActivatedRoute, Router} from "@angular/router";
import {ScriptLoaderService} from "../../../app/_services/script-loader.service";
import {ApprovalService} from "../../../services/api-service/approval.service";
import {BillerService} from "../../../services/api-service/biller.service";
import {UserService} from "../../../services/user.service";
import {EmandateService} from "../../../services/api-service/emandate.service";
import {NotificationService} from "../../../services/notification.service";
import {CONSTANTS} from "../../../utils/constants";
declare const $;

@Component({
    selector: 'app-setupEmandate',
    templateUrl: 'setup-emandate.component.html',
    styles: []
})
export class SetupEmandateComponent implements OnInit {
    @Input() billerData: any

    public moduleName = 'Biller';
    public createFormEmandate: FormGroup;
    public formReason: FormGroup;

    public id = 0;
    public p = 0;
    public pagesize = 0;
    public details = [];
    public totalElement = 0;
    public submit_type = '';

    public list = {
        allContent: [],
        productList: [],
        details: [],
        fees: [],
        searchContent: [],
        accountResult: {},
        billerDebitaccountResult: '',
        accountResultBiller: '',
        country: [],
        beneficiaryList: [],
        beneficiaryArray: [],
    };

    /*******start of Approval(maker/checker) declaration*******/
    public approvalStatus = '';
    public approvalType = '';
    public statusType = '';
    public approvalName = 'biller';
    public authorization = '';
    public approvedStatus = '';
    /*******end of Approval(maker/checker) declaration*******/

    public booleans = {
        loader: false,
        loadFee: false,
        loadsubmitFees: false,
        showBillSetup: true,
        acountloaderBiller: false,
        billerDebitacountloader: false,
        loadproductdetails: false,
        acountloader: false,
        loadsetupFees: false,
        searchStatus: false,
        formulaError: false,
        loadBeneficiary: false,
        csvsearchloader: false,
        pdfsearchloader: false,
        getAllloader: false,
        loadingSearch: false,
        searchloader: false,
        loadIndustry: false,
        loadsubmit: false,
        loadProduct: false,
        initiator: false,
        authorizer: false,
        loadAction: false,
        loadBank: false,
        markUpFeeSelected: false,
    };

    private formdataEmandate = function () {
        return {
            id: ['', Validators.compose([])],
            domainName: ['', Validators.compose([Validators.required])],
            entityType: ['', Validators.compose([])],
            notificationUlr: ['', Validators.compose([])],
            ownerId: ['', Validators.compose([])],
            username: ['', Validators.compose([Validators.required])]
        }
    };

    constructor(private _script: ScriptLoaderService,
                private fb: FormBuilder,
                private activatedRoute: ActivatedRoute,
                private router: Router,
                private approvalservice: ApprovalService,
                private billerService: BillerService,
                private userservice: UserService,
                private emandateservice: EmandateService,
                private notification: NotificationService) {
        this.createFormEmandate = this.fb.group(this.formdataEmandate());
    }

    ngOnInit() {
        console.log('biller data :: ', this.billerData);
        this.billerService.getResponse.subscribe(response => this.booleans.loadsetupFees = response);
        this.billerService.getformularResponse.subscribe(response => this.booleans.loadFee = response);
        this.details = this.userservice.getAuthUser();
        if (this.details['roles'][0] === 'NIBSS AUTHORIZER'
            || this.details['roles'][0] === 'BANK ADMIN AUTHORIZER'
            || this.details['roles'][0] === 'PSSP ADMIN AUTHORIZER') {
            this.booleans.authorizer = true;
        }
        if (this.details['roles'][0] === 'NIBSS INITIATOR'
            || this.details['roles'][0] === 'BANK ADMIN INITIATOR'
            || this.details['roles'][0] === 'PSSP ADMIN INITIATOR') {
            this.booleans.initiator = true;
        }
    }

    getMainPage() {
        this.approvalStatus = '';
        this.router.navigate([this.approvalName]);
    }

    onChangePageSize(event) {
        // this.getBiller(this.p - 1, this.pagesize);
    }

    putEmandate() {
        this.booleans.loadsubmit = true;
        this.emandateservice.putEmandate(this.createFormEmandate.value)
            .subscribe(response => {
                    this.createFormEmandate.reset();
                    $('#Setupemandate').modal('show');
                    this.booleans.loadsubmit = false;
                    this.emandateservice.emandateId = response['id'];
                    this.emandateservice.status.next({status: true})
                    // this.emandateservice.status.next({status: true})
                    this.notification.success('Emandate update was successful. Await Authorization');
                },
                error => {
                    this.booleans.loadsubmit = false;
                    this.emandateservice.status.next({status: false})
                    this.notification.error(CONSTANTS.ERROR_RESPONSE, error);
                });
    }

    createEmandate() {
        this.booleans.loadsubmit = true;
        this.createFormEmandate['controls']['entityType'].patchValue(this.billerData['entityType']);
        this.createFormEmandate['controls']['ownerId'].patchValue(this.billerData['id']);
        this.emandateservice.postEmandate(this.createFormEmandate.value)
            .subscribe(response => {
                    this.createFormEmandate.reset();
                    $('#Setupemandate').modal('hide');
                    this.booleans.loadsubmit = false;
                    this.notification.success('Emandate setup was successful. Await Authorization');
                },
                error => {
                    this.booleans.loadsubmit = false;
                    this.notification.error(CONSTANTS.ERROR_RESPONSE, error);
                });
    }

    view(data) {
        this.list.details = data;
        $('#ViewModal').modal('show');
    }

    setupemandate(data) {
        this.submit_type = 'Submit';
        this.createFormEmandate.reset();
        $('#Setupemandate').modal('show');
    }

    editemandate(data) {
        this.list.details = data;
        this.id = data.id;
        this.submit_type = 'Update';
        const formdata = function () {
            return {
                id: [data.emandateConfig['id'], Validators.compose([Validators.required])],
                domainName: [data.emandateConfig['domainName'], Validators.compose([Validators.required])],
                entityType: [data.emandateConfig['entityType'], Validators.compose([Validators.required])],
                notificationUlr: [data.emandateConfig['notificationUlr'], Validators.compose([])],
                username: [data.emandateConfig['username'], Validators.compose([Validators.required])],
                ownerId: [data['id'], Validators.compose([])],
            }
        };
        this.createFormEmandate = this.fb.group(formdata());
        $('#Setupemandate').modal('show');
    }
}
