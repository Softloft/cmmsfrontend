import {Component, Input, OnInit, ViewChild} from "@angular/core";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ActivatedRoute, Router} from "@angular/router";
import {BillerService} from "../../../../services/api-service/biller.service";
import {ApprovalService} from "../../../../services/api-service/approval.service";
import {ScriptLoaderService} from "../../../../app/_services/script-loader.service";
import {UserService} from "../../../../services/user.service";
import {EmandateService} from "../../../../services/api-service/emandate.service";
import {NotificationService} from "../../../../services/notification.service";
import {APPROVAL_MESSAGE_EMANDATE, CONSTANTS} from "../../../../utils/constants";
import Swal from "sweetalert2";
import {SetupEmandateComponent} from "../setup-emandate.component";

declare const $;

@Component({
    selector: 'app-emandate=approval',
    templateUrl: 'approval.component.html',
    styles: []
})
export class ApprovalComponent implements OnInit {
    @Input() billerData: any;
    @ViewChild(SetupEmandateComponent) emandate: SetupEmandateComponent;

    public moduleName = 'Biller';
    public formReason: FormGroup;

    public id = 0;
    public p = 1;
    public pagesize = 10;
    public details = [];
    public totalElement = '';
    public entityType = '';
    public status = '';
    public billerdata = [];

    public list = {
        allContent: [],
        productList: [],
        details: [],
        fees: [],
        searchContent: [],
        accountResult: {},
        billerDebitaccountResult: '',
        accountResultBiller: '',
        country: [],
        beneficiaryList: [],
        beneficiaryArray: [],
    };

    /*******start of Approval(maker/checker) declaration*******/
    public approvalStatus = '';
    public approvalType = '';
    public statusType = '';
    public approvalName = 'biller';
    public authorization = '';
    public approvedStatus = '';
    /*******end of Approval(maker/checker) declaration*******/

    public booleans = {
        loader: false,
        loadFee: false,
        loadsubmitFees: false,
        showBillSetup: true,
        acountloaderBiller: false,
        billerDebitacountloader: false,
        loadproductdetails: false,
        acountloader: false,
        loadsetupFees: false,
        searchStatus: false,
        formulaError: false,
        loadBeneficiary: false,
        csvsearchloader: false,
        pdfsearchloader: false,
        getAllloader: false,
        loadingSearch: false,
        searchloader: false,
        loadIndustry: false,
        loadsubmit: false,
        loadProduct: false,
        initiator: false,
        authorizer: false,
        loadAction: false,
        loadBank: false,
        markUpFeeSelected: false,
    };

    private formdataReason = function () {
        return {
            reason: ['', Validators.compose([Validators.required])],
        }
    };

    constructor(private _script: ScriptLoaderService,
                private fb: FormBuilder,
                private activatedRoute: ActivatedRoute,
                private router: Router,
                private approvalservice: ApprovalService,
                private billerService: BillerService,
                private userservice: UserService,
                private emandateservice: EmandateService,
                private notification: NotificationService) {
        this.formReason = this.fb.group(this.formdataReason());
    }

    ngOnInit() {
        this.status = this.activatedRoute.snapshot.paramMap.get('status');
        this.entityType = this.activatedRoute.snapshot.paramMap.get('entityType');
        this.getParam();
        // this.getAuthorization(this.entityType, this.status, 0);
        this.details = this.userservice.getAuthUser();
        if (this.details['roles'][0] === 'NIBSS AUTHORIZER'
            || this.details['roles'][0] === 'BANK ADMIN AUTHORIZER'
            || this.details['roles'][0] === 'PSSP ADMIN AUTHORIZER') {
            this.booleans.authorizer = true;
        }
        if (this.details['roles'][0] === 'NIBSS INITIATOR'
            || this.details['roles'][0] === 'BANK ADMIN INITIATOR'
            || this.details['roles'][0] === 'PSSP ADMIN INITIATOR') {
            this.booleans.initiator = true;
        }
        this.emandateservice.status.subscribe(response => {
            console.log('completed status :: ', response);
            if (response['status'] || response['status'] === 'true') {
                console.log('emandate id :: ', this.emandateservice.emandateId);
                this.list.allContent.forEach((value, i) => {
                    if (this.emandateservice.emandateId === value['emandateConfig']['id']) {
                        this.list.allContent.splice(i, 1);
                    }
                });
            }
        })
    }

    getMainPage() {
        this.getAuthorization(this.entityType, this.status, 0);
    }

    onChangePageSize(event) {
        // this.getBiller(this.p - 1, this.pagesize);
    }

    view(data) {
        this.list.details = data;
        $('#ViewModal').modal('show');
    }


    /***********************************APPROVAL(MAKER AND CHECKER)*****************************************/
    public getParam(param?) {
        this.list.allContent = [];
        if (param) {
            switch (param) {
                case 'pending':
                    this.statusType = 'UNAUTHORIZED';
                    this.approvalStatus = 'Pending';
                    break;
                case 'approved':
                    this.statusType = 'AUTHORIZED';
                    this.approvalStatus = 'Approved';
                    break;
                case 'disapproved':
                    this.statusType = 'REJECTED';
                    this.approvalStatus = 'Disapproved';
                    break;
            }
            this.router.navigate(['/settings/' + this.entityType + '/' + param]);
        } else {
            const status = this.activatedRoute.snapshot.paramMap.get('status');
            switch (status) {
                case 'pending':
                    this.statusType = 'UNAUTHORIZED';
                    this.approvalStatus = 'Pending';
                    break;
                case 'approved':
                    this.statusType = 'AUTHORIZED';
                    this.approvalStatus = 'Approved';
                    break;
                case 'disapproved':
                    this.statusType = 'REJECTED';
                    this.approvalStatus = 'Disapproved';
                    break;
            }
        }
        this.getAuthorization(this.entityType, this.statusType, 0);
    }

    getApprovalPage(event) {
        this.p = +event;
        this.list.allContent = [];
        this.getAuthorization(this.entityType, this.statusType, this.p - 1);
    }

    getPage(event) {
        this.p = +event;
        this.list.allContent = [];
        this.getAuthorization(this.statusType, this.statusType, this.p - 1);
    }

    getAuthorization(entityType, statusType, pageNo) {
        this.booleans.loader = true;
        this.emandateservice.getAuthorization(entityType, statusType, pageNo)
            .subscribe(response => {
                    this.booleans.loader = false;
                    this.list.allContent = response.content;
                    this.totalElement = response.numberOfElements;
                    console.log('total element :: ', this.totalElement);
                },
                error => {
                    this.booleans.loader = false;
                    this.notification.error('Unable to load content', error);
                })
    }

    toggle(data) {
        this.id = data.id;
        let statusMessage = '';
        statusMessage = (data.activated) ? 'Disapprove' : 'Approve';
        Swal({
            title: '',
            html: '<b style="font-weight: bold">' + APPROVAL_MESSAGE_EMANDATE(statusMessage) + '</b>',
            type: null,
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes!'
        }).then((result) => {
            if (result.value) {
                this.emandateservice.toggle(this.entityType, {id: data.emandateConfig['id']})
                    .subscribe(response => {
                            this.list.allContent.forEach((value, i) => {
                                if (value.id === this.id) {
                                    this.list.allContent[i] = value;
                                }
                            });
                            this.notification.success(CONSTANTS.TOGGLE_RESPONSE);
                        },
                        error => {
                            this.notification.error(CONSTANTS.ERROR_RESPONSE, error);
                        });
            }
        })
    }

    edit(data) {
        this.billerdata = data;
        this.emandate.editemandate(data);
    }

    disapprove() {
        let action = '';
        this.approvedStatus = 'reject';
        this.booleans.loadAction = true;
        switch (this.authorization) {
            case 'UNAUTHORIZED_CREATE':
                action = 'REJECT_CREATE';
                break;
            case 'UNAUTHORIZED_UPDATE':
                action = 'REJECT_UPDATE'
                break;
            case 'UNAUTHORIZED_TOGGLE':
                action = 'REJECT_TOGGLE'
                break;
        }

        this.Approve(this.id, action, this.formReason.value['reason']);
    }

    approve(id, authorizationStatus) {
        this.id = id;
        let action = '';
        this.approvedStatus = '';
        switch (authorizationStatus) {
            case 'UNAUTHORIZED_CREATE':
                action = 'APPROVE_CREATE';
                break;
            case 'UNAUTHORIZED_UPDATE':
                action = 'APPROVE_UPDATE'
                break;
            case 'UNAUTHORIZED_TOGGLE':
                action = 'APPROVE_TOGGLE'
                break;
        }
        this.approvalType = 'approve';
        this.Approve(id, action);
    }

    getReason(data, authorizationStatus) {
        this.id = data.emandateConfig['id'];
        this.authorization = authorizationStatus;
        this.list.details = data;
        this.approvalType = 'disapprove';
        $('#reasonModal').modal('show');
    }

    Approve(id, authorizationStatus, reason?) {
        this.booleans.loadAction = true;
        this.emandateservice.approve({
            id: id,
            reason: (reason && reason !== '') ? reason : ''
        }, authorizationStatus, this.entityType)
            .subscribe(response => {
                    this.booleans.loadAction = false;
                    this.list.allContent.forEach((value, i) => {
                        if (id === value['emandateConfig']['id']) {
                            this.list.allContent.splice(i, 1);
                        }
                    });
                    this.formReason.reset();
                    const status = (this.approvedStatus === 'reject') ? 'Disapproval' : 'Approval';
                    this.notification.success(status + ' was successful');
                },
                error => {
                    this.booleans.loadAction = false;
                    this.notification.error(CONSTANTS.ERROR_RESPONSE, error);
                })
    }

    public setup(event) {
        this.booleans.showBillSetup = event;
    }
}
