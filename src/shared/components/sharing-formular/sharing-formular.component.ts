import { Component, OnInit } from "@angular/core";
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from "@angular/forms";
import { ScriptLoaderService } from "../../../app/_services/script-loader.service";
import { BillerService } from "../../../services/api-service/biller.service";
import { NotificationService } from "../../../services/notification.service";
import Swal from "sweetalert2";
import { CONSTANTS } from "../../../utils/constants";
declare const $;

@Component({
    selector: 'app-sharing-formular',
    templateUrl: 'sharing-formular.component.html',
    styles: []
})
export class SharingFormularComponent implements OnInit {
    public moduleName = 'Biller';
    public totalFeeSubmitted = 0;
    public totalFeeCalculated = 0;
    public totalFee = 0;
    public feeType = '';

    public formulaErrorMessage = '';

    public createFormFormula: FormGroup;
    public id = 0;
    public details = [];
    public p = 1;
    public user = [];
    public submit_type = 'Submit';
    public modal_title = 'Add';
    public list = {
        fees: [],
        details: [],
        beneficiaryList: [],
        beneficiaryArray: [],
        searchContent: []
    };

    public booleans = {
        loadFee: false,
        loader: false,
        searchStatus: false,
        formulaError: false,
        loadBeneficiary: false,
        loadsubmit: false,
        loadProduct: false,
    };


    public settingsBeneficiary = {};
    public selectedBeneficiary = [];
    public itemListBeneficiary = [];

    private formularData = function() {
        return {
            billerId: ['', Validators.compose([])],
            formular: new FormArray([]),
        }
    };

    constructor(private _script: ScriptLoaderService,
        private fb: FormBuilder,
        private billerService: BillerService,
        private notification: NotificationService) {
        this.billerService.shareFeeData(null);
        this.billerService.shareFormularData(null);
        this.createFormFormula = this.fb.group(this.formularData());
    }

    ngOnInit() {
        this.billerService.getFormularData.subscribe(response => {
            if (response && response['accountName'] && response !== '') {
                this.getFormularById(response);
            }
        });
        this.getBeneficiaryList();
        this.settingsBeneficiary = {
            singleSelection: true,
            text: "Select Beneficiary",
            enableSearchFilter: true,
            badgeShowLimit: 5
        };
    }


    submitFormula() {
        this.extractIdFormula();
        if (this.formulaErrorMessage !== '') {
            this.notification.error(this.formulaErrorMessage);
            return;
        }
        this.booleans.loadsubmit = true;
        this.billerService.createFormula(this.createFormFormula.value)
            .subscribe(response => {
                this.booleans.loadsubmit = false;
                this.createFormFormula.reset();
                this.notification.success('New Formular was successfully created');
                $('#FormulaModal').modal('hide');
            },
            error => {
                this.booleans.loadsubmit = false;
                this.notification.error(CONSTANTS.ERROR_RESPONSE, error);
            });
    }

    updateFormula() {
        this.extractIdFormula();
        if (this.formulaErrorMessage !== '') {
            this.notification.error(this.formulaErrorMessage);
            return;
        }
        this.booleans.loadsubmit = true;
        this.billerService.updateFormular(this.createFormFormula.value)
            .subscribe(response => {
                this.booleans.loadsubmit = false;
                this.notification.success('Formular was successfully updated');
                this.createFormFormula.reset();
                $('#FormulaModal').modal('hide');
            },
            error => {
                this.booleans.loadsubmit = false;
                this.notification.error(CONSTANTS.ERROR_RESPONSE, error);
            });
    }

    private extractIdFormula() {
        this.totalFee = 0;
        this.totalFeeSubmitted = 0;
        this.formulaErrorMessage = '';
        this.booleans.formulaError = false;
        if (this.list.fees['splitType'].toLowerCase() === 'percentage') {
            this.totalFee = this.list.fees['percentageAmount'];
        } else {
            this.totalFee = this.list.fees['fixedAmount'];
        }
        this.createFormFormula.value['billerId'] = this.id;
        this.createFormFormula.value['formular'].forEach((value) => {
            if (+value['fee'] <= 0) {
                this.formulaErrorMessage = 'Beneficiary value cannot be 0 or less';
            };
            this.totalFeeSubmitted += +value['fee'];
            if (value['beneficiaryId'] && value['beneficiaryId'][0]) {
                value['beneficiaryId'] = value['beneficiaryId'][0]['id'];
            };
        });
        if (this.totalFee !== this.totalFeeSubmitted) {
            this.formulaErrorMessage = 'Total Beneficiary value must be equal to ' + this.totalFee;
        }
    }


    getFormularById(data) {
        this.id = data.id;
        this.createFormFormula = this.fb.group(this.formularData());
        this.billerService.getFormularById(this.id)
            .subscribe(response => {
                if (response['formularResponse'] && response['formularResponse'].length && response['formularResponse'].length > 0) {
                    response['formularResponse'].forEach(data => {
                        const control = new FormGroup({
                            beneficiaryId: new FormControl([{
                                'id': data['beneficiary']['id'],
                                'itemName': data['beneficiary']['beneficiaryName']
                            }], Validators.required),
                            fee: new FormControl(data['fee'], Validators.required),
                        });
                        (<FormArray>this.createFormFormula.get('formular')).push(control);
                    });
                    this.submit_type = 'Update';
                    this.list.fees = response['totalFee'];
                } else {
                    this.list.fees = response['totalFee'] ? response['totalFee'] : [];
                    if (this.list.fees.length === 0) {
                        this.billerService.sendFormularResponse(false);
                        return this.notification.error('No Fee found for this biller. Set the fee to proceed')
                    }
                    this.submit_type = 'Submit';
                }
                this.billerService.sendFormularResponse(false);
                this.modal_title = 'Setup Formular';
                this.booleans.loadFee = false;
                this.onItemSelectBeneficiary();
                this.calculateTotalValue();
                $('#FormulaModal').modal('show');
            },
            error => {
                this.booleans.loadFee = false;
                this.billerService.sendFormularResponse(false);
                this.notification.error(CONSTANTS.ERROR_RESPONSE, error);
            })

    }

    onAddBeneficiary() {
        const control = new FormGroup({
            beneficiaryId: new FormControl('', Validators.required),
            fee: new FormControl('', Validators.required),
        });
        (<FormArray>this.createFormFormula.get('formular')).push(control);
    }

    onRemoveBeneficiary(i) {
        if (this.createFormFormula.value['formular']
            && this.createFormFormula.value['formular'][i]
            && this.createFormFormula.value['formular'][i]['beneficiary']
            && this.createFormFormula.value['formular'][i]['beneficiary'][0]
            && this.createFormFormula.value['formular'][i]['beneficiary'][0]['id']) {
            this.OnItemDeSelectBeneficiary(this.createFormFormula.value['formular'][i]['beneficiary'][0]);
        }
        (<FormArray>this.createFormFormula.get('formular')).removeAt(i);
    }

    getBeneficiaryList() {
        this.booleans.loadBeneficiary = true;
        this.billerService.getBeneficiaryList()
            .subscribe(response => {
                this.booleans.loadBeneficiary = false;
                response.forEach(value => {
                    this.itemListBeneficiary.push({
                        id: value.id,
                        itemName: value.beneficiaryName
                    })
                });
                this.list.beneficiaryList = this.itemListBeneficiary;
            },
            error => {
                this.booleans.loadBeneficiary = false;
                this.notification.error('Unable to load Beneficiary', error);
            })
    }

    /**
     * this method is use to ensure that a selected beneficiary is removed
     * from the dropdown list to avoid select it again
     * @param item
     */
    onItemSelectBeneficiary(item?: any) {
        this.itemListBeneficiary = [];
        this.list.beneficiaryList.forEach(value => {
            let i = 0;
            this.createFormFormula.value['formular'].forEach((formularvalue) => {
                if (formularvalue['beneficiaryId'] && formularvalue['beneficiaryId'][0] && formularvalue['beneficiaryId'][0]['id'] === value.id) {
                    i = 1;
                } else if (formularvalue['beneficiaryId'] === value.id) {
                    i = 1
                }
                ;
            });
            if (i === 0) {
                this.itemListBeneficiary.push(value)
            }
        })
    }

    OnItemDeSelectBeneficiary(item: any) {
        this.itemListBeneficiary.push(item);
    }

    onSelectAllBeneficiary(items: any) {
    }

    onDeSelectAllBeneficiary(items: any) {
    }

    calculateTotalValue() {
        this.totalFeeCalculated = 0;
        this.createFormFormula.value['formular'].forEach((value) => {
            this.totalFeeCalculated += +value['fee'];
        });
    }
}
