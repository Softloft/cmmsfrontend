import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ScriptLoaderService } from "../../../app/_services/script-loader.service";
import { BillerService } from "../../../services/api-service/biller.service";
import { UserService } from "../../../services/user.service";
import { NotificationService } from "../../../services/notification.service";
import { ActivatedRoute } from "@angular/router";
import { BankService } from "../../../services/api-service/bank.service";
import { AuthService } from "../../../services/api-service/authService";

import Swal from "sweetalert2";
import { CONSTANTS } from "../../../utils/constants";
declare const $;
@Component({
    selector: 'app-fee-configuration',
    templateUrl: 'fee-configuration.component.html',
    styles: []
})
export class FeeConfigurationComponent implements OnInit {
    public feeType = '';
    public editFees = false;
    public bearerType = '';
    public showAmount = false;
    public feeData = [];

    public feeSubmitType = '';
    public feeErrorMessage = '';

    public settingsBank = {};
    public selectedBank = [];
    public itemListBank = [];

    public createFormBearer: FormGroup;
    public id = 0;
    public details = [];
    public p = 1;
    public user = [];
    public submit_type = 'Submit';
    public modal_title = 'Add';
    public list = {
        details: [],
        fees: [],
        searchContent: [],
        accountResult: []
    };

    public booleans = {
        loader: false,
        loadFee: false,
        loadsetupFees: false,
        searchStatus: false,
        formulaError: false,
        loadsubmit: false,
        markUpFeeSelected: false,
        acountloader: false,
    };

    public error = {
        percentageError: '',
        accountnoerror: false
    };
    public settingsBearer = {};
    public selectedBearer = [];
    public itemListBearer = [{ id: 'Bank', itemName: 'Bank' }, { id: 'Subscriber', itemName: 'Subscriber' }, {
        id: 'Biller', itemName: 'Biller'
    }];

    private formdataBearer = function() {
        return {
            id: ['', Validators.compose([])],
            bankCode: ['', Validators.compose([])],
            feeBearer: ['', Validators.compose([])],
            feeBearerItem: ['', Validators.compose([])],
            billerDebitAccountNumber: ['', Validators.compose([Validators.minLength(10), Validators.maxLength(10)])],
            splitType: ['Fixed', Validators.compose([])],
            fixedAmount: ['', Validators.compose([])],
            percentageAmount: ['', Validators.compose([])],
            debitAtTransactionTime: [false, Validators.compose([])],
            accountName: ['', Validators.compose([])],
            accountNumber: ['', Validators.compose([])],
            bank: ['', Validators.compose([])],
            billerId: ['', Validators.compose([])],
            markUpFee: ['', Validators.compose([])],
            markUpFeeSelected: ['', Validators.compose([Validators.required])],
        }
    };

    constructor(private _script: ScriptLoaderService,
        private fb: FormBuilder,
        private activatedRoute: ActivatedRoute,
        private authservice: AuthService,
        private bankservice: BankService,
        private billerService: BillerService,
        private userservice: UserService,
        private notification: NotificationService) {
        this.createFormBearer = this.fb.group(this.formdataBearer());
    }

    ngOnInit() {
        this.billerService.getFeeData.subscribe(response => {
            if (response && response['accountName'] && response !== '') {
                this.setupFees(response);
                this.getActiveBank()
            }
        });
        this.settingsBearer = {
            singleSelection: true,
            text: "Select Bearer",
            enableSearchFilter: true,
            badgeShowLimit: 5
        };

        this.settingsBank = {
            singleSelection: true,
            text: "Select Bank",
            enableSearchFilter: true,
            badgeShowLimit: 5
        };
    }


    setupFees(data) {
        this.feeData = data;
        this.id = data.id;
        this.createFormBearer.reset();
        this.billerService.getFeesById(this.id)
            .subscribe(response => {
                this.list.fees = response;
                if (response.markedUp) {
                    this.booleans.markUpFeeSelected = true;
                    this.list.accountResult['accountName'] = response.accountName;
                }
                this.billerService.sendResponse(false);
                if (this.list.fees['feeBearer']) {
                    this.feeSubmitType = 'Update';
                    const data = function() {
                        return {
                            id: [response.biller['id'], Validators.compose([])],
                            feeBearerItem: [[{
                                'id': response.feeBearer,
                                'itemName': response.feeBearer
                            }], Validators.compose([])],
                            bank: [[{
                                'id': response.bank.id,
                                'itemName': response.bank.name,
                                'code': response.bank.code
                            }], Validators.compose([])],
                            billerDebitAccountNumber: [response.billerDebitAccountNumber, Validators.compose([Validators.minLength(10), Validators.maxLength(10)])],
                            splitType: [response.splitType, Validators.compose([Validators.required])],
                            markUpFeeSelected: [response.markedUp, Validators.compose([])],
                            markUpFee: [response.markUpFee, Validators.compose([])],
                            accountName: [response.accountName, Validators.compose([])],
                            accountNumber: [response.accountNumber, Validators.compose([])],
                            fixedAmount: [response.fixedAmount, Validators.compose([])],
                            percentageAmount: [response.percentageAmount, Validators.compose([])],
                            debitAtTransactionTime: [response.debitAtTransactionTime, Validators.compose([])],
                        }
                    };
                    this.createFormBearer = this.fb.group(data());
                } else {
                    this.feeSubmitType = 'Submit';
                }
                this.modal_title = 'Setup Fees';
                this.booleans.loadsetupFees = false;
                $('#FeesModal').modal('show');
            },
            error => {
                this.booleans.loadsetupFees = false;
                this.billerService.sendResponse(true);
                this.notification.error(CONSTANTS.ERROR_RESPONSE, error);
            })

    }

    getActiveBank() {
        this.bankservice.getActiveBank()
            .subscribe(response => {
                response.forEach(value => {
                    this.itemListBank.push({
                        id: value.id,
                        itemName: value.name,
                        code: value.code
                    })
                });
            },
            error => {
                this.notification.error('Unable to load active bank', error);
            })
    }

    createFees() {
        this.extractFeesIds();
        if (this.feeErrorMessage !== '') {
            this.notification.error(this.feeErrorMessage);
            return;
        }
        this.booleans.loadsubmit = true;
        this.createFormBearer.value['bankCode'] = this.createFormBearer.value['bank'][0]['code'];
        this.createFormBearer.value['billerId'] = this.billerService.billerId;
        this.billerService.setupFees(this.createFormBearer.value)
            .subscribe(response => {
                this.notification.success('New Fees successfully created');
                this.booleans.loadsubmit = false;
                this.createFormBearer.reset();
                $('#FeesModal').modal('hide');
            },
            error => {
                this.booleans.loadsubmit = false;
                this.notification.error(CONSTANTS.ERROR_RESPONSE, error);
            });
    }

    showAlert() {
        Swal({
            title: 'Are you sure?',
            html:
            "<b>Editing this fee, will clear this biller's existing formular!</b>",
            type: null,
            showCancelButton: true,
            confirmButtonText: 'Yes, edit it!',
            cancelButtonText: 'No, keep it'
        }).then((result) => {
            if (result.value) {
                this.editFees = true;
                this.updateFees();
            } else if (result.dismiss === Swal.DismissReason.cancel) {
                Swal(
                    'Cancelled',
                    'Fee is safe :)',
                    'error'
                );
            }
        });
    }

    updateFees() {
        this.extractFeesIds();
        if (this.feeErrorMessage !== '') {
            this.notification.error(this.feeErrorMessage);
            return;
        }
        this.booleans.loadsubmit = true;
        this.billerService.updateSetupFees(this.createFormBearer.value)
            .subscribe(response => {
                this.notification.success('Fee was successfully updated');
                this.booleans.loadsubmit = false;
                this.editFees = false;
                this.createFormBearer.reset();
                $('#FeesModal').modal('hide');
            },
            error => {
                this.booleans.loadsubmit = false;
                this.notification.error(CONSTANTS.ERROR_RESPONSE, error);
            });
    }

    private extractFeesIds() {
        this.feeErrorMessage = '';
        if (this.createFormBearer.value['billerDebitAccountNumber'] !== ''
            && isNaN(this.createFormBearer.value['billerDebitAccountNumber'])) {
            this.feeErrorMessage = 'Account number must be a number';
        }
        this.createFormBearer.value['billerId'] = this.id;

        if (this.createFormBearer.value['splitType'] === 'Percentage' && (!this.createFormBearer.value['percentageAmount'] || this.createFormBearer.value['percentageAmount'] === '')) {
            this.feeErrorMessage = 'Percentage amount is required';
        }
        /*if (this.createFormBearer.value['splitType'] === 'Fixed' && (!this.createFormBearer.value['fixedAmount'] || this.createFormBearer.value['fixedAmount'] === '')) {
            this.feeErrorMessage = 'Fixed amount is required';
        }*/
        if (this.createFormBearer.value['feeBearerItem'] && this.createFormBearer.value['feeBearerItem'][0]) {
            this.createFormBearer.value['feeBearer'] = this.createFormBearer.value['feeBearerItem'][0]['id'];
        }
        if (this.createFormBearer.value['feeBearer'] !== 'Biller') {
            this.createFormBearer.value['debitAtTransactionTime'] = 'false';
            this.createFormBearer.value['billerDebitAccountNumber'] = '';
        } else if (this.createFormBearer.value['feeBearer'] === 'Biller') {
            if (this.createFormBearer.value['debitAtTransactionTime'] === '') {
                this.feeErrorMessage = 'Choose the debt transaction Type';
            }
            if (this.createFormBearer.value['billerDebitAccountNumber'] === '' && this.createFormBearer.value['debitAtTransactionTime'] === true) {
                this.feeErrorMessage = 'Please, Enter the debt account number';
            }
        }
    }


    onSelect(event) {
        const markupStatus = event.target.value;
        if (markupStatus === 'true') {
            this.booleans.markUpFeeSelected = true;
            // this.createFormBearer['controls']['fixedAmount'].patchValue('');
        }
        if (markupStatus === 'false') {
            this.booleans.markUpFeeSelected = false;
            // this.createFormBearer['controls']['percentageAmount'].patchValue('');
        }
    }

    onItemSelectBearer(item: any) {
        if (item['itemName'] === 'Biller') {
            this.bearerType = 'Biller'
        } else {
            this.bearerType = '';
            this.showAmount = false;
            this.createFormBearer.controls['debitAtTransactionTime'].patchValue(false);
            this.createFormBearer.controls['billerDebitAccountNumber'].patchValue('');
            this.onTransactionType();
        }
    }

    OnItemDeSelectBearer(item: any) {
    }

    onSelectAllBearer(items: any) {
    }

    onDeSelectAllBearer(items: any) {
    }

    onTransactionType() {
        if (this.createFormBearer.value['debitAtTransactionTime'] === true) {
            this.showAmount = true;
        } else {
            this.showAmount = false;
            this.createFormBearer.controls['debitAtTransactionTime'].patchValue(false);
            this.createFormBearer.controls['billerDebitAccountNumber'].patchValue('');
        }
    }

    checkPercentageValue(event) {
        const value = +event.target.value;
        this.error['percentageError'] = '';
        if (isNaN(value)) {
            this.error['percentageError'] = 'Value must be a number';
        } else if (value > 100) {
            this.error['percentageError'] = 'Percentage value cannot be more than 100%';
        } else if (value.toString().split(".")[1] && value.toString().split(".")[1].length > 2) {
            this.error['percentageError'] = 'Decimal Places must not exceed 2';
        } else {
            this.error['percentageError'] = '';
        }
    }

    checkAmountValue(event) {
        const value = +event.target.value;
        if (isNaN(value)) {
            this.error['percentageError'] = 'Value must be a number';
        } else {
            this.error['percentageError'] = '';
        }
    }

    verifyAccountNo() {
        if (this.createFormBearer.value['billerDebitAccountNumber'] !== '' && !isNaN(this.createFormBearer.value['billerDebitAccountNumber'])) {
            this.error.accountnoerror = true;
        }
    }

    keyupAccountNumber() {
        if (this.createFormBearer.value['accountNumber'].length === 10) {
            this.booleans.acountloader = true;
            this.accountLookup();
        } else {
            this.list.accountResult = null;
        }
    }

    accountLookup() {
        const data = {
            accountNumber: this.createFormBearer.value['accountNumber'],
            bankCode: ((this.createFormBearer.value['bank'][0]) ? this.createFormBearer.value['bank'][0]['code'] : '')
        };
        this.authservice.accountLookup(data)
            .subscribe(response => {
                this.list.accountResult = response;
                if (this.list.accountResult['accountName']) {
                    this.createFormBearer['controls']['accountName'].patchValue(this.list.accountResult['accountName']);
                }
                this.booleans.acountloader = false;
            },
            error => {
                this.booleans.acountloader = false;
                this.notification.error('Unable to process the action', error)
            })
    }
}

